-- premake5.lua
require "export-compile-commands"

workspace "BloopCAD"
architecture "x64"
configurations { "Debug", "Release", "Dist" }
startproject "App"

-- Workspace-wide build options for MSVC
filter "system:windows"
buildoptions { "/EHsc", "/Zc:preprocessor", "/Zc:__cplusplus" }

OutputDir = "%{cfg.system}-%{cfg.architecture}/%{cfg.buildcfg}"

group "CoreGroup"
include "Core/Build-Core.lua"
group "CoreTests"
include "Tests/Build-Tests.lua"
group ""
include "App/Build-App.lua"
