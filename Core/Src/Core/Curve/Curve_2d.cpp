
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "Curve_2d.hpp"

#include <Core/Utils/Visitor.hpp>

namespace Core {
namespace Crv {
namespace _2d {

Nurbs to_nurbs(Curve curve_)
{
	return std::visit(Visitor{
		[&](Line const& line) -> Nurbs {
			return Crv::_2d::Nurbs::from_basecontrols(1, {0, 0, 1, 1}, { line.pt1, line.pt2 });
		},
		[&](Circle const& circle) -> Nurbs {

	    return Nurbs( 2, 
                  { 0, 0, 0, 0.25, 0.25, 0.5, 0.5, 0.75, 0.75, 1, 1, 1 },
                  { 
                    glm::dvec3(glm::dvec2(1, 0)  * circle.radius+circle.center, 1),
                    glm::dvec3(glm::dvec2(1, 1)  * circle.radius+circle.center, std::sqrt(2.0)/2.0),
                    glm::dvec3(glm::dvec2(0, 1)  * circle.radius+circle.center, 1),
                    glm::dvec3(glm::dvec2(-1, 1) * circle.radius+circle.center, std::sqrt(2.0)/2.0),
                    glm::dvec3(glm::dvec2(-1, 0) * circle.radius+circle.center, 1),
                    glm::dvec3(glm::dvec2(-1, -1)* circle.radius+circle.center, std::sqrt(2.0)/2.0),
                    glm::dvec3(glm::dvec2(0, -1) * circle.radius+circle.center, 1),
                    glm::dvec3(glm::dvec2(1, -1) * circle.radius+circle.center, std::sqrt(2.0)/2.0),
                    glm::dvec3(glm::dvec2(1, 0)  * circle.radius+circle.center, 1)
                  });
		}
	}, curve_);
}

std::ostream& operator<<(std::ostream& os, Curve curve_)
{
	std::visit(Visitor{
		[&](Line const& line) {
			os<<"Line ("<<line.pt1.x<<", "<<line.pt1.y<<") => ("<<line.pt2.x<<", "<<line.pt2.y<<")";
		},
		[&](Circle const& circle) {
			os<<"Circle ("<<circle.center.x<<", "<<circle.center.y<<") -> "<<circle.radius;
		}
	}, curve_);
	return os;
}

Loop_t lines_loop(std::vector<glm::dvec2> const& points)
{
	if(points.size() < 3) 
		return Loop_t();

	Loop_t loop(points.size());
	for(int i = 0; i < points.size()-1; ++i) {
		loop[i] = Line(points[i], points[i+1]);
	}
	loop.back() = Line(points.back(), points[0]);
	return loop;
}

} // ! _2d
} // !!Crv
} // !Core
