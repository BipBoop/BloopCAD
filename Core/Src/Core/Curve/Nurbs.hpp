
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef BLOOP_CORE_NURBSCURVE_HPP_
#define BLOOP_CORE_NURBSCURVE_HPP_

#include <Core/Maths/NurbsUtils.hpp>
#include <Core/Precision.hpp>
#include <Core/Maths/Utils.hpp>
#include "Core/Curve/Line.hpp"
#include "Core/Maths/BoundingBox.hpp"
#include "CurvePoint.hpp"
#include "glm/fwd.hpp"
#include "glm/gtx/norm.hpp"
#include <Core/Utils/StlShenanigans.hpp>

#include <algorithm>
#include <glm/gtx/string_cast.hpp>
#include <glm/geometric.hpp>

#include <optional>
#include <iostream>
#include <algorithm>

namespace Core {
namespace Crv {

template<int dim>
class NurbsCurve {
public:
  using vec_t = glm::vec<dim, double>;
  using vecw_t = glm::vec<dim+1, double>;
  using point_t = Point<dim>;

// private:
  int degree;
  std::vector<double> knots;
  std::vector<vecw_t> controls;
  BoundingBox<dim> bbox;
  double pseudoCurv { 0.0 };
  bool is_pt { true };

private:
  NurbsCurve(int degree_, int n_knots_, int n_controls_)
    : degree(degree_)
    , knots(n_knots_)
    , controls(n_controls_)
  {

  }

public:
  NurbsCurve() = default;
  NurbsCurve(int degree_, NurbsUtils::KnotVec knots_, std::vector<vecw_t> controls_, bool normalize_ = true)
    : degree(degree_)
    , controls(controls_)
  {
    if(normalize_) {
      knots = NurbsUtils::normalize(std::move(knots_));
    } else {
      knots = std::move(knots_);
    }
    comp_metadata();
  }
  static NurbsCurve from_controlsW(int degree_, NurbsUtils::KnotVec knots_, std::vector<vecw_t> controls_, bool normalize_)
  {
    NurbsCurve<dim> out;
    out.degree = degree_;
    out.controls = controls_;
    
    if(normalize_) {
      out.knots = NurbsUtils::normalize(knots_);
    } else {
      out.knots = knots_;
    }
    out.comp_metadata();
    return out;
  }
  static NurbsCurve from_basecontrols(int degree, NurbsUtils::KnotVec knots, std::vector<vec_t> base_controls)
  {
    std::vector<vecw_t> controls(base_controls.size());
    for(int i = 0; i < controls.size(); ++i) {
      controls[i] = vecw_t(base_controls[i], 1.0);
    }
    return NurbsCurve(degree, knots, controls);
  }
  BoundingBox<dim> boundingBox() const { return bbox; }

  LineAbst<dim> as_line() const
  {
    return LineAbst<dim>(control(0), control(n_controls()-1));
  }
  bool is_point() const { return is_pt; }
  double pseudoCurvature() const { return pseudoCurv; }

  int deg() const { return degree; }
  // NurbsUtils::KnotVec 
  double start_knot() const
  {
    return knots[0];
  }
  double end_knot() const
  {
    return knots.back();
  }
  // remaps a parameter space value between two 
  // parameter space values accounting for closedness
  // of the curve
  // this assumes t is between 0.0 and 1.0
  double remap(double t, double start, double end) const
  {
    if(start < end || !closed()) {
      return map(t, 0.0, 1.0, start, end);
    }

    double diff = 1.0 - start + end;
    return std::fmod(start + diff * t, 1.0);
  }

  int n_controls() const { return controls.size(); }
  vec_t control(int ind) const { return controls.at(ind); }
  double weight(int ind) const { return NurbsUtils::weight(controls.at(ind)); }
  bool closed() const { return epsilon_equal(control(0), control(n_controls()-1), Prc::kNurbs); }
  
  vec_t at(double t) const
  {
    using namespace NurbsUtils;
    int span = knot_span(degree, knots, t);
    std::vector<double> basis = basis_fun(span, degree, t, knots);
    vecw_t atw(0.0);

    for(int i = 0; i <= degree; ++i) {
      atw += basis.at(i) * NurbsUtils::multiply_by_weight(controls.at(span-degree+i));
    }
    return divide_by_weight(atw);
  }
  vec_t start_pt() const
  {
    return at(start_knot());
  }
  vec_t end_pt() const
  {
    return at(end_knot());
  }
  // Returns pair of closest point and distance2
  std::pair<vec_t, double> closestPoint(vec_t pt) const
  {
    std::function<double(double)> adjust_fun;
    double start = start_knot();
    double end = end_knot();
    
    if(closed()) {
      adjust_fun = [start, end](double t) {
        while(t < start) {
          t += (end-start);
        }
        while(t > end) {
          t -= (end-start);
        }
        return t;
      };
    } else {
      adjust_fun = [start, end](double t) { return clamp(t, start, end); };
    }    
    
    int n_samples = n_controls() + 1;
    double t = 0.0;
    double min_dist2 = glm::distance2(at(0.0), pt);
    for(size_t i = 1; i < n_samples; ++i) {
      double tmp = static_cast<double>(i) / static_cast<double>(n_samples-1);
      double dist2 = glm::distance2(at(tmp), pt);
      if(dist2 < min_dist2) {
        min_dist2 = dist2;
        t = tmp;
      }
    }
    
    constexpr int max_it = 50;
    for(size_t i = 0; i < max_it; ++i) {
      double old_t = t;
      
      auto derivs = derivatives(t, 2);
      double fun = glm::dot(derivs[1], derivs[0]-pt);
      double deriv = glm::dot(derivs[2], (derivs[0]-pt)) + glm::length2(derivs[1]);
      
      t = adjust_fun(t - fun / deriv);
      
      if(std::abs((t-old_t) * deriv) < Prc::kNurbs) {
        break;
      }
    }
    vec_t pos = at(t);
    return { pos, glm::distance2(pos, pt) };
  }

  // Discretize by taking n evenly spaced samples in the parameter space
  std::vector<vec_t> discretize_n(size_t n) const
  {
    std::vector<vec_t> out(n);

    for(size_t i = 0; i < n; ++i) {
      double t = static_cast<double>(i) / static_cast<double>(n-1);
      out[i] = at(t);
    }
    return out;
  }
  std::vector<vec_t> discretize_curvature(double target_curvature) const
  {
    return discretize(target_curvature, [](vec_t a, vec_t elbow, vec_t b) -> double { return curvature_3pts(a, elbow, b); });
  }

  std::vector<vec_t> discretize(double end_condition, std::function<double(vec_t, vec_t, vec_t)> evaluator) const
  {
    if(is_point()) {
      return { at(0.0) };
    }
    std::vector<vec_t> out;
    for(size_t i = 0; i < controls.size(); ++i) {
      double t1 = static_cast<double>(i) / static_cast<double>(controls.size());
      double t2 = static_cast<double>(i+1) / static_cast<double>(controls.size());

      std::vector<vec_t> disc_seg = discretize(t1, t2, end_condition, evaluator);
      
      // avoid duplicates by only adding the first point
      // of the first segment
      if(i == 0) {
        out.insert(out.end(), disc_seg.begin(), disc_seg.end());
      } else {
        out.insert(out.end(), disc_seg.begin()+1, disc_seg.end());
      }
    }
    return out;
  }
  std::vector<vec_t> discretize(double from, double to, double end_condition, std::function<double(vec_t, vec_t, vec_t)> evaluator) const
  {
    double mid = (from+to) / 2.0;
    vec_t pt_from = at(from);
    vec_t pt_to = at(to);
    vec_t pt_mid = at(mid);
    double pt_eval = evaluator(pt_from, pt_mid, pt_to);

    if(pt_eval < end_condition) {
      std::vector<vec_t> out = discretize(from, mid, end_condition, evaluator);
      std::vector<vec_t> right = discretize(mid, to, end_condition, evaluator);
      out.insert(out.end(), right.begin()+1, right.end());
      return out;
    } else {
      return { pt_from, pt_to };
    }
  }

  // A4.2
  std::vector<vecw_t> bspline_derivatives(double t, int up_to) const
  {
    int du = std::min(degree, up_to);
    std::vector<vecw_t> out(du + 1, vecw_t(0.0));
    int span = NurbsUtils::knot_span(degree, knots, t);
    Eigen::MatrixXd derivs = NurbsUtils::basis_fun_deriv(span, degree, t, knots, up_to);

    for(int k = 0; k <= du; ++k) {
      for(int j = 0; j <= degree; ++j) {
        out.at(k) += derivs(k, j)*NurbsUtils::multiply_by_weight(controls.at(span-degree+j));
      }
    }
    return out;
  }
  std::vector<vec_t> derivatives(double u, int up_to) const
  {
    std::vector<vec_t> out(up_to+1);
    auto ders = bspline_derivatives(u, up_to);
    int du = std::min(degree, up_to);

    for(int k = 0; k <= du; ++k) {
      vec_t v = ders.at(k);
      for(int i = 1; i <= k; ++i) {
        v -= NurbsUtils::binomial(k, i) * NurbsUtils::weight(ders.at(i)) * out.at(k-i);
      }
      out.at(k) = v / NurbsUtils::weight(ders.at(0));
    }

    return out;
  }
  std::pair<std::optional<NurbsCurve<dim>>, std::optional<NurbsCurve<dim>>>
    split(double t, bool normalize_) const
  {
    // Check validity and extreme cases
    if(t < start_knot() || t > end_knot()) {
      return { std::nullopt, std::nullopt };
    }
    if(sepsilon_equal(t, start_knot(), Prc::kNurbs)) {
      return { std::nullopt, *this };
    }
    if(sepsilon_equal(t, end_knot(), Prc::kNurbs)) {
      return { *this, std::nullopt };
    }
    
    // Add knots such that the number of knots at the
    // split point is degree+1
    // https://pages.mtu.edu/~shene/COURSES/cs3621/NOTES/spline/NURBS-knot-insert.html
    int multiplicity = NurbsUtils::knot_multiplicity(degree, knots, t);
    int n_insertion = degree - multiplicity + 1;
    
    // Insert the knots on the future left portion
    NurbsCurve<dim> left = insert_knot(t, n_insertion, false);
    
    int span = NurbsUtils::knot_span(left.degree, left.knots, t);
    int right_knot_start = span-degree;
    int n_knots_right = left.knots.size() - right_knot_start;
    int n_controls_right = NurbsUtils::n_controls_for_knots(degree, n_knots_right);

    NurbsCurve<dim> right(degree, n_knots_right, n_controls_right);

    // Copy knots
    for(int i = right_knot_start; i < left.knots.size(); ++i) {
      right.knots.at(i-right_knot_start) = left.knots.at(i);
    }
    // Only keep left knots
    left.knots.resize(span+1);
    
    // Normalize if requested
    if(normalize_) {
      right.knots = NurbsUtils::normalize(right.knots);
      left.knots = NurbsUtils::normalize(left.knots);      
    }
    
    // Copy controls to right side
    int right_control_start = left.n_controls() - n_controls_right;
    for(int i = 0; i < n_controls_right; ++i) {
      right.controls.at(i) = left.controls.at(i+right_control_start);
    }
    
    // Truncate controls on left side
    left.controls.resize(NurbsUtils::n_controls_for_knots(degree, left.knots.size()));

    left.comp_metadata();
    right.comp_metadata();
    return { left, right };
  }
  std::pair<std::optional<NurbsCurve<dim>>, std::optional<NurbsCurve<dim>>>
    split_half(bool normalize_) const
  {
    return split((start_knot() + end_knot()) / 2.0, normalize_);
  }
  // Creates a nurbs that is a segment from a lerp value to another
  std::optional<NurbsCurve<dim>> split(double from, double to, bool normalize_) const
  {
    // Ensure parameters validity
    if(from > to) {
      if(closed()) {
        auto split_a = split(from, end_knot(), false);
        auto split_b = split(start_knot(), to, false);
        
        if(!split_a) {
          if(normalize_) {
            split_b->knots = NurbsUtils::normalize(split_b->knots);
          }
          return split_b;
        }
        if(!split_b) {
          if(normalize_) {
            split_a->knots = NurbsUtils::normalize(split_a->knots);
          }
          return split_a;
        }
        // both splits are valid, we need to stich them somehow
        // even if user asked not to normalize, we'll have to
        // because we cannot have a hole in the parameter space
        return stich(*split_a, *split_b);
      } else {
        return std::nullopt;
      }
    }
    if(sepsilon_equal(from, to, Prc::kNurbs)) {
      return std::nullopt;
    }
    // Clamp parameters
    if(from > end_knot()) {
      from = end_knot();
    }
    if(to < start_knot()) {
      to = start_knot();
    }
    
    // If a split is requested at an extreme position, it should be dealthwith
    bool from_is_start = sepsilon_equal(from, start_knot(), Prc::kNurbs);
    bool to_is_end = sepsilon_equal(to, end_knot(), Prc::kNurbs);
    if(from_is_start && to_is_end) {
      return *this;
    }
    if(from_is_start) {
      return split(to, normalize_).first;
    }
    if(to_is_end) {
      return split(from, normalize_).second;
    }
  
    // Split twice, normalize only once (if requested)
    // to ensure that both from and to are interpreted in
    // the right parameter space
    return split(from, false).second->split(to, normalize_).first;
  }
  NurbsCurve<dim> invert() const
  {
    double start = start_knot();
    double end = end_knot();
    
    NurbsUtils::KnotVec new_knots(knots.size());
    for(size_t i = 0; i < new_knots.size(); ++i) {
      new_knots[i] = map(knots[knots.size()-i-1], start, end, end, start);
    }
    
    std::vector<vecw_t> new_controls = controls;
    std::reverse(new_controls.begin(), new_controls.end());
    return NurbsCurve<dim>(degree, new_knots, new_controls, false);
  }

  NurbsCurve<dim> reverse_controls()
  {
    auto controls_copy = controls;
    std::reverse(controls_copy.begin(), controls_copy.end());
    return NurbsCurve<dim>(degree, knots, controls_copy);
  }

  // Finds all intersections between two curves within a kNurbs tolerance
  // returns intersections sorted by parametric space value
  // sort_self => sort the intersections according to this curve's parametric values
  // not sort_self => sort the intersections according to the other curve's parametric values
  std::vector<std::pair<point_t, point_t>> intersections(NurbsCurve<dim> const& other, bool sort_self = true) const
  {
    using inter_t = std::pair<point_t, point_t>;
    std::vector<inter_t> all_inters = intersections_impl(other);

    // Avoid problems where two intersections are the same but have different
    // parametric values (e.g. at the seem point of a circle)
    if(closed()) {
      for(size_t i = 0; i < all_inters.size(); ++i) {
        if(sepsilon_equal(all_inters[i].first.t, end_knot(), Prc::kNurbs)) {
          all_inters[i].first.t = start_knot();
        }
      }
    }
    if(other.closed()) {
      for(size_t i = 0; i < all_inters.size(); ++i) {
        if(sepsilon_equal(all_inters[i].second.t, other.end_knot(), Prc::kNurbs)) {
          all_inters[i].second.t = other.start_knot();
        }
      }
    }

    // Sort the vector
    if(sort_self) {
      std::sort(all_inters.begin(), all_inters.end(), [](inter_t const& a, inter_t const& b) { 
        return a.first.t < b.first.t; 
      });
    } else {
      std::sort(all_inters.begin(), all_inters.end(), [](inter_t const& a, inter_t const& b) { 
        return a.first.t < b.first.t; 
      });
    }

    // Remove duplicates
    // std::unique(a1,a1+n,[](double x, double y){return std::abs(x-y) < tolerance;});
    all_inters.erase(unique(all_inters.begin(), all_inters.end(), [](inter_t const& a, inter_t const& b) { 
        return sepsilon_equal(a.first.t, b.first.t, Prc::kNurbs); 
      }), all_inters.end());
    
    return all_inters;
  }
  // Same as previous but with line, assumes the line parametric space goes from 0.0 to 1.0
  std::vector<std::pair<point_t, point_t>> intersections(LineAbst<dim> const& other, bool sort_self = true) const
  {
    using inter_t = std::pair<point_t, point_t>;
    std::vector<inter_t> all_inters = intersections_impl(other, 0.0, 1.0);

    if(closed()) {
      for(size_t i = 0; i < all_inters.size(); ++i) {
        if(sepsilon_equal(all_inters[i].first.t, end_knot(), Prc::kNurbs)) {
          all_inters[i].first.t = start_knot();
        }
      }
    }

    // Sort the vector
    if(sort_self) {
      std::sort(all_inters.begin(), all_inters.end(), [](inter_t const& a, inter_t const& b) { 
        return a.first.t < b.first.t; 
      });
    } else {
      std::sort(all_inters.begin(), all_inters.end(), [](inter_t const& a, inter_t const& b) { 
        return a.first.t < b.first.t; 
      });
    }

    // Remove duplicates
    // std::unique(a1,a1+n,[](double x, double y){return std::abs(x-y) < tolerance;});
    all_inters.erase(unique(all_inters.begin(), all_inters.end(), [](inter_t const& a, inter_t const& b) { 
        return sepsilon_equal(a.first.t, b.first.t, Prc::kNurbs); 
      }), all_inters.end());
    
    return all_inters;
  }

  void comp_metadata()
  {
    bbox = BoundingBox<dim>();
    for(size_t i = 0; i < n_controls(); ++i) {
      bbox = bbox.extend(control(i));
    }

    is_pt = bbox.is_point(Prc::kIntersection);

    using namespace NurbsUtils;
    // Pseudo curvature as defined in
    // On Intersections of B-Spline Curves
    // Ying-Ying Yu, Xin Li and Ye Ji
    double running_sum = 0.0;
    for(size_t i = 0; i < n_controls()-1; ++i) {
      running_sum += glm::distance(control(i), control(i+1));
    }
    double denom;
    
    // Divisions by 0 are not so cool amongst the youth
    if(closed()) {
      denom = Prc::kNurbs;
    } else {
      denom = glm::distance(control(0), control(n_controls()-1));
    }
    pseudoCurv = running_sum / denom;

    for(size_t i = 1; i < degree+1; ++i) {
      if(!sepsilon_equal(knots[i], knots[0], Prc::kNurbs) || !sepsilon_equal(knots[knots.size()-1-i], knots.back(), Prc::kNurbs)) {
        std::cerr<<"Invalid curve!!"<<std::endl;
      }
    }
  }
  
  NurbsCurve<dim> transform(std::function<vec_t(vec_t)> trans_fun) const
  {
    std::vector<vecw_t> new_controls(controls.size());
    for(size_t i = 0; i < controls.size(); ++i) {
      new_controls[i] = vecw_t(trans_fun(vec_t(controls[i])), NurbsUtils::weight(controls[i]));
    }
    return NurbsCurve<dim>(degree, knots, new_controls);
  }

private:
  std::vector<std::pair<point_t, point_t>> intersections_impl(NurbsCurve<dim> const& other) const
  {
    if(is_point() || other.is_point()) {
      return {};
    }
    // If bounding box don't intersect, there can be no intersection because of 
    // the convex hull property of NURBS
    if(!boundingBox().overlaps(other.boundingBox())) {
      return {};
    }
    
    // Check if curves can be approximated reasonably well with a line
    bool self_is_line = sepsilon_equal(pseudoCurvature(), 1.0, Prc::kStraightEnough);
    bool other_is_line = sepsilon_equal(other.pseudoCurvature(), 1.0, Prc::kStraightEnough);
    
    if(self_is_line && other_is_line) {
      // linelineintersect
      auto self_line = as_line();
      auto other_line = other.as_line();
      auto inter = self_line.intersection(other_line);
      
      if(inter) {
        auto inter_pos = self_line.at(inter->first);
        return { {  point_t { .t=map(inter->first, 0.0, 1.0, start_knot(), end_knot()), .pos=inter_pos},
                    point_t { .t=map(inter->second, 0.0, 1.0, other.start_knot(), other.end_knot()), .pos=inter_pos}} };
      }
      return {};      
    } else if(self_is_line && !other_is_line) {
      return swap_twinvec(other.intersections_impl(as_line(), start_knot(), end_knot()));
    } else if(!self_is_line && other_is_line) {
      return intersections_impl(other.as_line(), other.start_knot(), other.end_knot());  
    } else {
      // Both curves are sill curvy curves, split them up
      // in the hope they will become straighter
      // (the limit of any curve spliting is a line)
      auto self_split = split_half(false);
      auto other_split = other.split_half(false);
      
      // Add any intersections found by recursive calls 
      // to the out vector
      // Both splits of the first curve (self) and both splits of the other curve are paired
      // to find intersections
      std::vector<std::pair<point_t, point_t>> out;
      if(self_split.first && other_split.first) {
        insert_back(out, self_split.first->intersections_impl(*other_split.first));
      }
      if(self_split.first && other_split.second) {
        insert_back(out, self_split.first->intersections_impl(*other_split.second));
      }
      if(self_split.second && other_split.first) {
        insert_back(out, self_split.second->intersections_impl(*other_split.first));
      }
      if(self_split.second && other_split.second) {
        insert_back(out, self_split.second->intersections_impl(*other_split.second));
      }
      return out;
    }
  }
  std::vector<std::pair<point_t, point_t>> 
    intersections_impl(LineAbst<dim> const& other, double lineStart_t, double lineEnd_t) const
  {
    if(is_point() || sepsilon_equal(other.length2(), 0.0, Prc::kNurbs*Prc::kNurbs)) {
      return {};
    }
    // std::cout<<"ln_"<<start_knot()<<" => "<<end_knot()<<"\n";
    if(!boundingBox().overlaps(BoundingBox<dim>(other.pt1, other.pt2))) {
      // std::cout<<"\tno overlap\n";
      return {};
    }
    // Intersections of a line and the curve is like nurbs-nurbs intersection
    // except only the nurbs is splitted
    bool self_is_line = sepsilon_equal(pseudoCurvature(), 1.0, Prc::kStraightEnough);
    if(self_is_line) {
      auto self_line = as_line();
      auto inter = self_line.intersection(other);
      // std::cout<<"\tbothlines!\n";
      if(inter) {
        // std::cout<<"\t\tintersection!\n";
        auto inter_pos = self_line.at(inter->first);
        return { {  point_t { .t=map(inter->first, 0.0, 1.0, start_knot(), end_knot()), .pos=inter_pos},
                    point_t { .t=map(inter->second, 0.0, 1.0, lineStart_t, lineEnd_t), .pos=inter_pos}} };
      }
      // std::cout<<"\t\tnointersection\n";
      return {};
    }
    // std::cout<<"\tsplit"<<"\n";
    auto self_split = split_half(false);
    
    std::vector<std::pair<point_t, point_t>> out;
    if(self_split.first) {
      insert_back(out, self_split.first->intersections_impl(other, lineStart_t, lineEnd_t));
    }
    if(self_split.second) {
      insert_back(out, self_split.second->intersections_impl(other, lineStart_t, lineEnd_t));
    }
    return out;
  }
  NurbsCurve<dim> insert_knot(double knot, int n_times, bool normalize_) const
  {
    using namespace NurbsUtils;
    std::vector<double> new_knots(knots.size()+n_times);
    std::vector<vecw_t> new_controls(controls.size() + n_times);
    std::vector<vecw_t> temp(degree+1);

    int span = knot_span(degree, knots, knot);
    int multiplicity = knot_multiplicity(degree, knots, knot);

    for(int i = 0; i <= span; ++i) {
      new_knots.at(i) = knots.at(i);
    }
    for(int i = 1; i <= n_times; ++i) {
      new_knots.at(span+i) = knot;
    }
    for(int i = span+1; i < knots.size(); ++i) {
      new_knots.at(i+n_times) = knots.at(i);
    }

    for(int i = 0; i <= span-degree; ++i) {
      new_controls.at(i) = NurbsUtils::multiply_by_weight(controls.at(i));
    }
    // Maybe a typo in the NURBS book, it would go i <= controls.size()
    // Which would make an invalid access
    for(int i = span-multiplicity; i < controls.size(); ++i) {
      new_controls.at(i+n_times) = NurbsUtils::multiply_by_weight(controls.at(i));
    }
    for(int i = 0; i <= degree-multiplicity; ++i) {
      temp.at(i) = NurbsUtils::multiply_by_weight(controls.at(span-degree+i));
    }
    int L = 0;
    for(int j = 1; j <= n_times; ++j) {
      L = span - degree + j;

      for(int i = 0; i <= degree-j-multiplicity; ++i) {
        double alpha = (knot-knots.at(L+i)) / (knots.at(i+span+1)-knots.at(L+i));
        temp.at(i) = alpha * temp.at(i+1) + (1.0-alpha) * temp.at(i);
      }
      new_controls.at(L) = temp.at(0);

      // Maybe a mistake in the NURBS book, this check ensures
      // that there is no illegal access
      if(degree-j-multiplicity > 0) {
        new_controls.at(span+n_times-j-multiplicity) = temp.at(degree-j-multiplicity);
      }
    }
    for(int i = L+1; i < span - multiplicity; ++i) {
      new_controls.at(i) = temp.at(i-L);
    }
    for(size_t i = 0; i < new_controls.size(); ++i) {
      new_controls.at(i) = NurbsUtils::divide_by_weight(new_controls.at(i));
    }
    return NurbsCurve<dim>(degree, new_knots, new_controls, normalize_);
  }
  static NurbsCurve<dim> stich(NurbsCurve<dim> lhs, NurbsCurve<dim> rhs)
  {
    if(lhs.degree != rhs.degree) {
      std::cerr<<"Cannot stich NURBS of different degree "<<lhs.degree<<" vs "<<rhs.degree<<std::endl;
      return lhs;
    }
    if(!epsilon_equal(lhs.end_pt(), rhs.start_pt(), Prc::kNurbs)) {
      std::cerr<<"Cannot stich NURBSs, end of left is not start of right"<<std::endl;
      return lhs;
    }
    
    int degree = lhs.degree;
    std::vector<vecw_t> controls(lhs.controls.size() + rhs.controls.size() - 1);
    NurbsUtils::KnotVec knots(NurbsUtils::n_knots_for_controls(degree, controls.size()));

    for(size_t i = 0; i < lhs.controls.size(); ++i) {
      controls[i] = lhs.controls[i];
    }
    for(size_t i = 1; i < rhs.controls.size(); ++i) {
      controls[i+lhs.controls.size()-1] = rhs.controls[i];
    }

    // lhs.knots = NurbsUtils::normalize(lhs.knots);
    // rhs.knots = NurbsUtils::normalize(rhs.knots);

    size_t i = 0;
    for(i = 0; i < lhs.knots.size()-1; ++i) {
      knots[i] = lhs.knots[i] - lhs.knots[0];
    }
    for(; i < knots.size(); ++i) {
      knots[i] = rhs.knots[i-lhs.knots.size() + degree + 2] + lhs.knots.back();
    }
    return NurbsCurve<dim>(degree, knots, controls);
  }
};

namespace _2d {

using Nurbs = NurbsCurve<2>;

}
namespace _3d {

using NurbsC = NurbsCurve<3>;

}

} // !Crv
} // !Core

#endif
