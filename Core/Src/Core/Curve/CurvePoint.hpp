
#ifndef BLOOP_CORE_CURVEPOINT_HPP_
#define BLOOP_CORE_CURVEPOINT_HPP_

#include <glm/glm.hpp>

namespace Core {
namespace Crv {
  
template<int dim>
struct Point {
  using vec_t = glm::vec<dim, double>;
  double t;
  vec_t pos;
}; 
  
} // !Crv
} // !Core

#endif
