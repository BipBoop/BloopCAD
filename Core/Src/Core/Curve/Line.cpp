
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "Line.hpp"

namespace Core {
namespace Crv {

template<>
std::optional<std::pair<double, double>> LineAbst<2>::intersection(LineAbst<2> const& other) const
{
  if(epsilon_equal(direction_norm(), other.direction_norm(), Prc::kIntersection)) // These are parallel lines
    return std::nullopt;

  glm::dvec2 root_diff = pt1 - other.pt1;
  double dir_cross = cross(direction(), other.direction());

  double t = cross_2d(other.direction(), root_diff) / dir_cross;
  if(t < -Prc::kIntersection || t > (1.0+Prc::kIntersection))
    return std::nullopt;
  
  // Second line must be checked entirely even if it is in halfInfiniteLine
  double u = cross_2d(direction(), root_diff) / dir_cross;
  if(u < -Prc::kIntersection || u > (1.0+Prc::kIntersection))
    return std::nullopt;

  return std::make_pair(t, u);
}

} // !Crv
} // !Core