
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "MinShapeBasis.hpp"
#include "Core/Curve/ClosedShape/Shape.hpp"
#include "Core/Curve/Curve_2d.hpp"
#include "Core/Curve/Nurbs.hpp"

#include <Core/Surface//Surface.hpp>
#include <Core/Utils/Range.hpp>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/graph_utility.hpp>

#include <set>

namespace Core {
namespace Crv {
namespace _2d {

// Half of an intersection, to be used in a data structure
struct HalfIntersection {
	size_t id { 0 };
	double at { 0.0 }; // where is the intersection for the other curve
	int curve_id { -1 }; // Curve that is interesected at a point or -1 if no curve intersected

	std::partial_ordering operator<=>(HalfIntersection const& other) const 
	{
		return at <=> other.at;
	};
};
struct Segment_id {
	size_t inter_id_1, inter_id_2;
	bool is_swapped { false };

	Segment_id(size_t inter_id_1_, size_t inter_id_2_)
		: inter_id_1(inter_id_1_)
		, inter_id_2(inter_id_2_)
	{
		if(inter_id_1 > inter_id_2) {
			std::swap(inter_id_1, inter_id_2);
			is_swapped = true;
		}
	}

	std::strong_ordering operator<=>(Segment_id const& other) const
	{
		auto out_1 = inter_id_1 <=> other.inter_id_1;
		
		if(out_1 == std::strong_ordering::equal)
			return inter_id_2 <=> other.inter_id_2;
		
		return out_1;
	}
};
struct Segment {
	Nurbs curve;
	int support_curve; // Parent curve index received from constructor
};

// For each curve, there is a set of ordered half intersections
using IntersectionMap = std::vector<std::set<HalfIntersection>>;

// Maps Segment_id to curve created from said
// Segment_id (e.g segment id of 2 intersections on a circle is mapped to the
// coresponding arc)
using SegmentMap = std::map<Segment_id, Segment>;

std::pair<IntersectionMap, size_t> gen_intersectionMap(std::vector<Nurbs> const& curves_);
std::pair<IntersectionMap, size_t> add_voidIntersections(std::vector<Nurbs> const& curves_, IntersectionMap interMap, size_t n_intersections);
std::pair<SegmentMap, Pmcb::Graph_t> 
	gen_graph(IntersectionMap const& interMap, std::vector<Nurbs> const& curves_, size_t n_intersections);
std::pair<std::vector<Shape>, std::vector<std::vector<int>>>
	gen_shapes(std::vector<Crv::_2d::Pmcb::Cycle> const& cycles, SegmentMap const& segMap);
std::map<int, std::vector<int>> order_shapes(std::vector<Shape> const& shapes, std::vector<IndRange> clusters);

MinShapeBasis::MinShapeBasis(std::vector<Curve> curves_)
{
	std::vector<Crv::_2d::Nurbs> curves_nurbs(curves_.size());
	for(size_t i = 0; i < curves_.size(); ++i) {
		curves_nurbs[i] = to_nurbs(curves_[i]);
	}

	// std::cout<<"Compute min shape basis with "<<curves_.size()<<" curves!\n";

	// std::cout<<"-- Gen intersection map\n";
	auto [interMap, n_intersections] = gen_intersectionMap(curves_nurbs);

	std::tie(interMap, n_intersections) = add_voidIntersections(curves_nurbs, interMap, n_intersections);

	for(int i = 0; i < interMap.size(); ++i) {
		// std::cout<<"Intersections on "<<i<<":\t";
		for(auto inter : interMap[i]) {
			// std::cout<<"with "<<inter.curve_id<<" at "<<inter.at<<" ["<<inter.id<<"] :: ";
		}
		// std::cout<<"\n";
	}

	// Add void intersections

	// std::cout<<"-- Gen graph\n";

	auto [segMap, graph] = gen_graph(interMap, curves_nurbs, n_intersections);

	// std::cout<<"-- Find mcbs\n";

	auto [cycles, shape_clusters] = _2d::Pmcb::planarMincCycleBasis(graph);
	
	std::cout<<"Found "<<cycles.size()<<" cycles\n";

	// std::cout<<"-- Gen shapes\n";
	std::tie(shapes, shapes_desc) = gen_shapes(cycles, segMap);

	// std::cout<<"-- Order shapes\n";
	children = order_shapes(shapes, shape_clusters);	
	for(auto children_of_shape : children) {
		// std::cout<<children_of_shape.first <<" => ";
		for(auto child : children_of_shape.second) {
			// std::cout<<child<<" :: ";
		}
		// std::cout<<"\n";
	}
	// std::cout<<"done min shape basis\n";
}

// Returns the index of the shape at a position
// or -1 if no shape is at the position;
int MinShapeBasis::at(glm::dvec2 const& pos) const
{
	for(int i = 0; i < shapes.size(); ++i) {
		if(shapes[i].point_inside(pos))
			return at_recursive(i, pos);
	}
	return -1;
}
int MinShapeBasis::at_recursive(int parent, glm::dvec2 const& pos) const
{
	auto children_of_shape = children.find(parent);
	if(children_of_shape == children.end())
		return parent;
	for(int child : children_of_shape->second) {
		if(shapes[child].point_inside(pos))
			return at_recursive(child, pos);
	}
	return parent;
}

// _3d::ShapeWithHoles::Loop_t MinShapeBasis::loop(int ind, Srf::Surface const& surf) const
// {
// 	ClosedShape shape_2d = shapes[ind];
// 	_3d::ShapeWithHoles::Loop_t out(shape_2d.edges.size());

// 	for(int i = 0; i < shape_2d.edges.size(); ++i) {
// 		out[i] = _3d::from_2d(shape_2d.edges[i], surf);		
// 	}
// 	return out;
// }
// _3d::ShapeWithHoles MinShapeBasis::shapeWithHoles_3d(int ind, Srf::Surface const& surf) const
// {
// 	_3d::ShapeWithHoles out;
// 	out.normal = _3d::normal_at(surf, shapes[ind].anchor);
// 	out.outer = loop(ind, surf);

// 	auto children_of_shape = children.find(ind);
// 	if(children_of_shape == children.end())
// 		return out;


// 	for(auto child : children_of_shape->second) {
// 		out.holes.push_back(loop(child, surf));
// 	}
// 	return out;
// }

_2d::Shape MinShapeBasis::shape(int ind) const
{
	return shapes[ind];
}
_2d::ShapeWithHoles MinShapeBasis::shapeWithHoles(int ind) const
{
	
	auto children_of_shape = children.find(ind);
	if(children_of_shape == children.end())
		return _2d::ShapeWithHoles(shape(ind), {});
		
	std::vector<Shape> holes;
		
	for(auto child : children_of_shape->second) {
		// out.holes.push_back(invert(loop(child)));
		holes.push_back(shape(child)); // Should invert???
	}
	return _2d::ShapeWithHoles(shape(ind), holes);
}

std::pair<IntersectionMap, size_t> gen_intersectionMap(std::vector<Nurbs> const& curves_)
{
	IntersectionMap out(curves_.size());
	size_t n_inters = 0;

	for(int i = 0; i < curves_.size(); ++i) {
		for(int j = i+1; j < curves_.size(); ++j) {
			auto inters = curves_[i].intersections(curves_[j]);
			// std::cout<<"N inters: "<<inters.size()<<"\n";

			for(auto inter : inters) {
				HalfIntersection halfInter_i { .id=n_inters, .at=inter.first.t, .curve_id=j };
				HalfIntersection halfInter_j { .id=n_inters, .at=inter.second.t, .curve_id=i };
				out[i].insert(halfInter_i);
				out[j].insert(halfInter_j);
				n_inters++;
			}
		}
	}
	return { out, n_inters };
}
std::pair<IntersectionMap, size_t> add_voidIntersections(std::vector<Nurbs> const& curves_, IntersectionMap interMap, size_t n_intersections)
{
	IntersectionMap newMap = interMap;
	for(int i = 0; i < curves_.size(); ++i) {
		int deg = curves_[i].deg();
		int additional = deg-1;
		if(additional == 0)
			continue;

		double lerpStep = 1.0/static_cast<double>(additional+1);

		if(interMap[i].size() < 2) { // If a circle is crossed in one point, we still want to inflate it
			if(curves_[i].closed()) {
				lerpStep = 1.0 / static_cast<double>(deg+1);
				double l1 = 0.0;
				for(int j = 0; j < deg+1; ++j, l1+=lerpStep) {
					HalfIntersection hinter{ .id=n_intersections++, .at=l1 };
					newMap[i].insert(hinter);
				}
			}
		} else {
			auto it_1 = interMap[i].begin();
			auto it_2 = interMap[i].begin();
			it_2++;

			for(; it_2 != interMap[i].end(); ++it_2) {
				newMap[i].insert(HalfIntersection{	.id=n_intersections++, 
														.at=curves_[i].remap(lerpStep, it_1->at, it_2->at)});
				it_1 = it_2;
			}

			if(curves_[i].closed()) {
				newMap[i].insert(HalfIntersection{	.id=n_intersections++, 
														.at=curves_[i].remap(lerpStep, it_1->at, interMap[i].begin()->at)});
			}
		}
	}

	// std::cout<<"Intersections";
	// for(int i = 0; i < curves_.size(); ++i) {
		// std::cout<<"\n curve "<<i<<" => ";
		// for(auto inter : newMap[i]) {
			// std::cout<<"["<<inter.id<<"] "<<inter.at<<" :: ";
		// }
	// }
	// std::cout<<"\n";

	return { newMap, n_intersections };
}

std::pair<SegmentMap, Pmcb::Graph_t> 
	gen_graph(IntersectionMap const& interMap, std::vector<Nurbs> const& curves_, size_t n_intersections)
{
	SegmentMap out_edges;
	Pmcb::Graph_t out_graph(n_intersections);

	for(int i = 0; i < curves_.size(); ++i) {
		if(interMap[i].size() < 2)
			continue;
		auto it_a = interMap[i].begin();
		auto it_b = it_a;
		it_b++;

		// it_a and it_b represent 2 intersections (b after a along the support curve)
		// together they represent a segment of the support curve
		for(; it_b != interMap[i].end(); it_a++, it_b++) {
			// std::cout<<"Add segment to graph ["<<it_a->id<<",  "<<it_b->id<<"]\n";
			auto seg = curves_[i].split(it_a->at, it_b->at, true);
			if(!seg) {
				std::cerr<<"could not split curve "<<i<<" ["<<it_a->at<<",  "<<it_b->at<<"]"<<std::endl;
			}
			out_edges[Segment_id(it_a->id, it_b->id)] = Segment{.curve=*seg, .support_curve=i};
			boost::add_edge(it_a->id, it_b->id, out_graph);

			// Graph position will be set twice, not sure how to set them only once
			out_graph[it_a->id] = curves_[i].at(it_a->at);
			out_graph[it_b->id] = curves_[i].at(it_b->at);
		}

		if(curves_[i].closed()) {
			auto seg = curves_[i].split(it_a->at, interMap[i].begin()->at, true);
			if(!seg) {
				std::cerr<<"could not split closed curve curve "<<i<<" ["<<it_a->at<<",  "<<interMap[i].begin()->at<<"]"<<std::endl;
			}
			out_edges[Segment_id(it_a->id, interMap[i].begin()->id)] = Segment{.curve=*seg, .support_curve=i};
			boost::add_edge(it_a->id, interMap[i].begin()->id, out_graph);
		}
	}
	// std::cout<<"Graph\n";
	boost::print_graph(out_graph);
	return { out_edges, out_graph };
}

std::pair<std::vector<Shape>, std::vector<std::vector<int>>>
	gen_shapes(std::vector<Crv::_2d::Pmcb::Cycle> const& cycles, SegmentMap const& segMap)
{
	std::vector<Shape> out;
	std::vector<std::vector<int>> out_desc;

	out.reserve(cycles.size());

	for(auto cycle : cycles) {
		std::vector<Nurbs> edges;
		out_desc.push_back({});
		for(int i = 0; i < cycle.size()-1; ++i) {
			Segment_id seg_id(cycle[i], cycle[i+1]);
			auto found_seg = segMap.find(seg_id);

			if(found_seg == segMap.end()) {
				// std::cout<<"Could not find segment "<<seg_id.inter_id_1<<",  "<<seg_id.inter_id_2<<",  "<<seg_id.is_swapped<<"\n";
				return {};
			}
			// For convenience seg_id which contain 2 ids have their id1 <= id2, so
			// the curve segment that is indexed has to be swapped if the requested 
			// segment is asked in the other order than is stored
			if(seg_id.is_swapped != found_seg->first.is_swapped) {
				edges.push_back(found_seg->second.curve.invert());
				out_desc.back().push_back(found_seg->second.support_curve);
			} else {
				edges.push_back(found_seg->second.curve);
				out_desc.back().push_back(found_seg->second.support_curve);
			}
		}
		// std::cout<<"Create shape\n";
		out.push_back(Shape(edges));
		// std::cout<<"Done create shape\n";
	}
	return { out, out_desc };
}
// bool minShapeInside(ClosedShape const& inner, ClosedShape const& outer)
// {
// 	return outer.contains(inner.border_point());
// }
// void update_shape_parent(std::vector<ClosedShape> const& shapes, std::vector<int>& parents, int inner, int outer)
// {
// 	if(parents[inner] == -1) {
// 		parents[inner] = outer;
// 		return;
// 	}

// 	// - If the parent of the inner shape is inside the outer shape,
// 	// the parent of the inner shape becomes child of the outer
// 	// - If the parent of the inner shape has the outer shape within it
// 	// the inner shape gets the outer shape as parent and the outer shape
// 	// gets the previous parent of the inner shape as parent
// 	if(minShapeInside(shapes[parents[inner]], shapes[outer])) {
// 		update_shape_parent(shapes, parents, parents[inner], outer);
// 	} else if(minShapeInside(shapes[outer], shapes[parents[inner]])) {
// 		parents[inner] = outer;
// 		update_shape_parent(shapes, parents, outer, parents[inner]);
// 	}
// }
// std::vector<int> order_shapes(std::vector<ClosedShape> const& shapes)
// {
// 	std::vector<int> parents(shapes.size(), -1);

// 	for(int i = 0; i < shapes.size(); ++i) {
// 		for(int j = i+1; j < shapes.size(); ++j) {
// 			if(minShapeInside(shapes[j], shapes[i])) {
// 				update_shape_parent(shapes, parents, j, i);
// 			} else if(minShapeInside(shapes[i], shapes[j])) {
// 				update_shape_parent(shapes, parents, i, j);
// 			}
// 		}
// 	}
// 	return parents;
// }

void add_relation(std::vector<Shape> const& shapes, int inner, int outer, 
					std::map<int, std::vector<int>>& ordered, std::vector<int>& has_parent)
{
	std::map<int, std::vector<int>>::iterator outer_children = ordered.find(outer);

	if(outer_children != ordered.end()) {
		std::vector<int> erase_vec;
		for(int i = 0; i < outer_children->second.size(); ++i) {
			int child_shape = outer_children->second[i];
			if(shapes[child_shape].point_inside(shapes[inner].borderPoint())) {
				add_relation(shapes, inner, child_shape, ordered, has_parent);
				if(!erase_vec.empty())
					std::cerr<<"WOOOT? erase_vec not empty "<<erase_vec.size()<<"\n";
				return;
			} else if(shapes[inner].point_inside(shapes[child_shape].borderPoint())) {
				erase_vec.push_back(child_shape);
				add_relation(shapes, child_shape, inner, ordered, has_parent);
			}
		}
		for(auto erase : erase_vec) {
			outer_children->second[erase] = outer_children->second.back();
			outer_children->second.pop_back();
		}
	}
	has_parent[inner] = 1;
	ordered[outer].push_back(inner);
}
void order_shapes(std::vector<Shape> const& shapes, IndRange clusterA, IndRange clusterB, 
					std::map<int, std::vector<int>>& ordered, std::vector<int>& has_parent)
{
	for(int i = clusterA.first; i < clusterA.last; ++i) {
		Shape const& shape_a = shapes[i];
		for(int j = clusterB.first; j < clusterB.last; ++j) {
			Shape const& shape_b = shapes[j];

			if(!has_parent[j] && shape_a.point_inside(shape_b.borderPoint())) {
				add_relation(shapes, j, i, ordered, has_parent);
			} else if(!has_parent[i] && shape_b.point_inside(shape_a.borderPoint())) {
				add_relation(shapes, i, j, ordered, has_parent);
			}
		}
	}
}
std::map<int, std::vector<int>> order_shapes(std::vector<Shape> const& shapes, std::vector<IndRange> clusters)
{
	std::map<int, std::vector<int>> ordered_children;
	std::vector<int> has_parent(shapes.size(), 0);
	for(int i = 0; i < clusters.size(); ++i) {
		for(int j = i+1; j < clusters.size(); ++j) {
			order_shapes(shapes, clusters[i], clusters[j], ordered_children, has_parent);			
		}
	}
	return ordered_children;
}

} // !_2d
} // !Crv
} // !Core


// ClosedShapeFactory::IntersectionMap ClosedShapeFactory::add_voidIntersections(IntersectionMap map, GeomContent const& geomContext, size_t& inter_count)
// {
// 	IntersectionMap out_map = map;
// 	for(int i = 0; i < map.size(); ++i) {
// 		if(ClosedShape::has_curvature(curveMiror[i]) && map[i].size() > 1) {
// 			auto it_a = map[i].begin();
// 			auto it_b = it_a;
// 			it_b++;
// 			for(it_a; it_a != map[i].end(); it_a++, it_b++) {
// 				HalfIntersection hi { 	inter_count++, 
// 										Crv::_2d::get_middle_point(mathGeom(geomContext.get(curveMiror[i])), it_a->at, it_b->at), 
// 										std::nullopt };
// 				out_map[i].insert(hi);
// 			}
// 		}
// 	}
// 	return out_map;
// }
