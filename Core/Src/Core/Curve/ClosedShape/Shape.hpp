
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef BLOOP_CORE_2DSHAPE_HPP_
#define BLOOP_CORE_2DSHAPE_HPP_

#include <Core/Curve/Nurbs.hpp>
#include <Core/Maths/BoundingBox.hpp>
#include <Core/Discrete/Triangulation/DelaunayTrig_2d.hpp>

#include <glm/glm.hpp>

namespace Core {
namespace Crv {
namespace _2d {

// A Shape is an array of nurbs curve that ends where it starts
// It contains a discretized version of itself to perform most
// tasks
class Shape {
public:
  enum class Direction { X_dir, Y_dir, Both };
public:
  std::vector<Nurbs> edges_;
  BoundingBox<2> bbox_;

public:
  Shape() = default;
  Shape(std::vector<Nurbs> edgs_);
  
  size_t n_edges() const { return edges_.size(); }

  // Flip the shapes within it's own bounding box
  Shape flip(Direction dir) const;
  // Flip the shape within another box
  Shape flip_within(Direction dir, dBox flipbox) const;

  Shape transform(std::function<glm::dvec2(glm::dvec2)> trans_fun) const;

  // Returns true if a point is inside the shape
  bool point_inside(glm::dvec2 pt) const;
  // Returns true if the provided box crosses
  // the shape's boundary'
  bool crosses(BoundingBox<2> abox) const;
  
  // Returns the shape's bounding box'
  BoundingBox<2> boundingBox() const { return bbox_; }
  
  // Returns a point on the border (usefull for shape basis
  // hierarchies)
  glm::dvec2 borderPoint() const { return edges_[0].at(0.0); }
    
  // Returns the closest point to another point
  std::pair<glm::dvec2, double> closestPoint(glm::dvec2 pt) const;

  // Used for tests and visualization
  std::vector<glm::dvec2> rough_discretization(int n) const;
  std::vector<glm::dvec2> discretization(double target_curvature,                         
                                          std::function<double(glm::dvec2, glm::dvec2, glm::dvec2)> curvature_evaluator = 
                                          [](glm::dvec2 a, glm::dvec2 elbow, glm::dvec2 b) { return curvature_3pts(a, elbow, b); }) const;

};

// template<typename Shape>
struct ShapeWithHoles {
public:
  std::vector<Shape> shapes_;

public:
  ShapeWithHoles() = default;
  ShapeWithHoles(Shape outer_, std::vector<Shape> holes_);
  ShapeWithHoles(std::vector<Shape> outer_then_holes);
	static ShapeWithHoles NDC_square();

  size_t n_shapes() const { return shapes_.size(); }
  size_t n_holes() const { return n_shapes() - 1; }

	dBox boundingBox() const { return shapes_[0].boundingBox(); }

  // Flips the coordinates of every control point
  // and ensures that the clockwise-ness of the 
  // shape stays the same
  ShapeWithHoles flip(Shape::Direction dir) const;

  // Normalize the coordinates of every control points
  // so that the bounding box of the shape is [0, 1]
  // for every coordinate
  ShapeWithHoles normalize() const;

	bool point_inside(glm::dvec2 const& point) const;
	bool crosses(BoundingBox<2> abox) const;
	
	std::pair<glm::dvec2, double> closestPoint(glm::dvec2 pt) const;

  // Returns a delaunay triangulation input
  // with border loop and hole loops
  // if normalize is true, each vertez will be spat out
  // as a normalized coordinate inside the bounding box of the
  // border shape
  DelaunayTrig::Input delaunayInput(double target_curvature, bool do_normalize, 
                        std::function<double(glm::dvec2, glm::dvec2, glm::dvec2)> curvature_evaluator = 
                        [](glm::dvec2 a, glm::dvec2 elbow, glm::dvec2 b) { return curvature_3pts(a, elbow, b); }) const;
};


} // !_2d
} // !Crv
} // !Core

#endif
