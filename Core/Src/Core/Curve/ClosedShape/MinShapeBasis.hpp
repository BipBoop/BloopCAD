
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef BLOOP_CORE_MINSHAPEBASIS_HPP_
#define BLOOP_CORE_MINSHAPEBASIS_HPP_

#include "Shape.hpp"
#include <Core/Curve/Curve_2d.hpp>
#include <Core/Surface/Surface.hpp>
#include <Core/Discrete/Triangulation/Triangulation.hpp>
#include <Core/Discrete/Triangulation/DelaunayTrig_2d.hpp>
#include "PlanarMinCycleBasis.hpp"

#include <optional>
#include <map>

namespace Core {
namespace Crv {
namespace _2d {

// Group of closed shapes
// with a hierarchy
struct MinShapeBasis {
	std::vector<Shape> shapes;
	std::vector<std::vector<int>> shapes_desc;

	// Each shape has an associated entry
	// in this vector shapes[ind] ==> parents[ind]
	// if the value is -1, the shape is first level
	// other wise it is within the shape pointed by
	// the parent index
	// std::vector<int> parents;
	std::map<int, std::vector<int>> children;

	MinShapeBasis() = default;
	MinShapeBasis(std::vector<Curve> curves_);

	// Returns the index of the shape at a position
	// or -1 if no shape is at the position;
	int at(glm::dvec2 const& pos) const;
	int at_recursive(int parent, glm::dvec2 const& pos) const;

	_2d::Shape shape(int ind) const;
	_2d::ShapeWithHoles shapeWithHoles(int ind) const;
};

} // !_2d
} // !Crv
} // !Core


#endif
