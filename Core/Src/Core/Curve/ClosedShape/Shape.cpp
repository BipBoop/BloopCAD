
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "Shape.hpp"
#include "Core/Curve/Curve_2d.hpp"
#include "Core/Curve/Line.hpp"

namespace Core {
namespace Crv {
namespace _2d {


Shape::Shape(std::vector<Nurbs> edgs_)
  : edges_(edgs_)
{
  for(size_t i = 0; i < edgs_.size(); ++i) {
    auto comb = edges_[i].boundingBox();
    bbox_ = bbox_.combine(comb);
  }
  if(edgs_.empty()) {
    std::cerr<<"Creating empty shape >:("<<std::endl;
  } else if(!epsilon_equal(edges_[0].at(0.0), edges_.back().at(1.0), Prc::kIntersection)) {
    std::cerr<<"Creating open shape :(("<<std::endl;
  }
}
Shape Shape::flip(Direction dir) const
{
  return flip_within(dir, boundingBox());
}
Shape Shape::flip_within(Direction dir, dBox flipbox) const
{
  std::vector<Nurbs> new_edges(edges_.size());

  // When flipping only in one dimension, the edges_ order
  // must be reversed to keep the same clockwise-ness
  if(dir == Direction::X_dir) {
    for(size_t i = 0; i < edges_.size(); ++i) {
      new_edges[edges_.size()-i-1] = edges_[i].transform([&](glm::dvec2 ctrl) {
        return glm::dvec2(map(ctrl.x, flipbox.smoll.x, flipbox.big.x, flipbox.big.x, flipbox.smoll.x), ctrl.y);
      }).reverse_controls();
    }
  } else if(dir == Direction::Y_dir) {
    for(size_t i = 0; i < edges_.size(); ++i) {
      new_edges[edges_.size()-i-1] = edges_[i].transform([&](glm::dvec2 ctrl) {
        return glm::dvec2(ctrl.x, map(ctrl.y, flipbox.smoll.y, flipbox.big.y, flipbox.big.y, flipbox.smoll.y));
      }).reverse_controls();
    }
  } else {
    return transform([&](glm::dvec2 ctrl) {
      return glm::dvec2(map(ctrl.x, flipbox.smoll.x, flipbox.big.x, flipbox.big.x, flipbox.smoll.x), 
                        map(ctrl.y, flipbox.smoll.y, flipbox.big.y, flipbox.big.y, flipbox.smoll.y));});
  }

  return Shape(new_edges);
}
Shape Shape::transform(std::function<glm::dvec2(glm::dvec2)> trans_fun) const
{
  std::vector<Nurbs> new_edges(edges_.size());
  for(size_t i = 0; i < edges_.size(); ++i) {
    new_edges[i] = edges_[i].transform(trans_fun);
  }
  return Shape(new_edges);
}
// Returns true if a point is inside the shape
bool Shape::point_inside(glm::dvec2 pt) const
{
  // Make a ray as long as bounding box which should be enough
  auto ray = Line::from_posDir(pt, glm::dvec2(std::abs(pt.x-bbox_.smoll.x) + 3.0*bbox_.width(), 0.0));

  int n_intersections = 0;
  glm::dvec2 last_inter(std::numeric_limits<double>::max());
  for(size_t i = 0; i < edges_.size(); ++i) {
    auto intersections = edges_[i].intersections(ray);

    if(!intersections.empty()) {
      // Try to avoid duplicates
      if(epsilon_equal(intersections[0].first.pos, last_inter, Prc::kNurbs)) {
        n_intersections += intersections.size() - 1;
      } else {
        n_intersections += intersections.size();
      }
      last_inter = intersections.back().first.pos;
    }
  }
  return n_intersections % 2;
}
// Returns true if the provided box crosses
// the shape's boundary'
bool Shape::crosses(BoundingBox<2> abox) const
{
  // Try to intersect all box lines with all 
  // edges until there is one intersection
  std::array<Line, 4> boxlines = {
    Line(abox.bottomLeft(), abox.bottomRight()),
    Line(abox.bottomRight(), abox.topRight()),
    Line(abox.topRight(), abox.topLeft()),
    Line(abox.topLeft(), abox.bottomLeft())
  };
  
  for(size_t i = 0; i < boxlines.size(); ++i) {
    for(size_t j = 0; j < edges_.size(); ++j) {
      if(!edges_[j].intersections(boxlines[i]).empty()) {
        return true;
      }
    }
  }
  return false;
}
std::pair<glm::dvec2, double> Shape::closestPoint(glm::dvec2 pt) const
{
  std::pair<glm::dvec2, double> closest = edges_[0].closestPoint(pt);
  
  for(size_t i = 1; i < edges_.size(); ++i) {
    std::pair<glm::dvec2, double> candidate = edges_[i].closestPoint(pt);
    
    if(candidate.second < closest.second) {
      closest = candidate;
    }
  }
  return closest;
}
std::vector<glm::dvec2> Shape::rough_discretization(int n) const
{
  std::vector<glm::dvec2> points;
  for(size_t i = 0; i < edges_.size(); ++i) {
    auto tmp = edges_[i].discretize_n(n);

    if(tmp.empty())
      continue;
    points.insert(points.end(), tmp.begin()+1, tmp.end());
  }
  return points;
}
std::vector<glm::dvec2> Shape::discretization(double target_curvature, 
                          std::function<double(glm::dvec2, glm::dvec2, glm::dvec2)> curvature_evaluator) const
{
  std::vector<glm::dvec2> points;
  for(size_t i = 0; i < edges_.size(); ++i) {
    auto tmp = edges_[i].discretize(target_curvature, curvature_evaluator);

    if(tmp.empty())
      continue;
    points.insert(points.end(), tmp.begin()+1, tmp.end());  
  }
  return points;
}

ShapeWithHoles::ShapeWithHoles(Shape outer_, std::vector<Shape> holes_)
  : shapes_(holes_.size() + 1)
{
  shapes_[0] = outer_;
  for(size_t i = 0; i < holes_.size(); ++i) {
    shapes_[i+1] = holes_[i];
  }
}
ShapeWithHoles::ShapeWithHoles(std::vector<Shape> outer_then_holes)
  : shapes_(outer_then_holes)
{

}
ShapeWithHoles ShapeWithHoles::NDC_square()
{
  return ShapeWithHoles(Shape({ to_nurbs(Line(glm::dvec2(0.0, 0.0), glm::dvec2(1.0, 0.0))),
                                to_nurbs(Line(glm::dvec2(1.0, 0.0), glm::dvec2(1.0, 1.0))),
                                to_nurbs(Line(glm::dvec2(1.0, 1.0), glm::dvec2(0.0, 1.0))),
                                to_nurbs(Line(glm::dvec2(0.0, 1.0), glm::dvec2(0.0, 0.0)))}), {});
}
ShapeWithHoles ShapeWithHoles::flip(Shape::Direction dir) const
{
  dBox flip_box = boundingBox();
  std::vector<Shape> new_shapes(shapes_.size());
  for(size_t i = 0; i < shapes_.size(); ++i) {
    new_shapes[i] = shapes_[i].flip_within(dir, flip_box);
  }
  return ShapeWithHoles(new_shapes);
}
ShapeWithHoles ShapeWithHoles::normalize() const
{
  auto bbox = boundingBox();
  auto trans_fun = [bbox](glm::dvec2 ctrl) {
    return glm::dvec2(map(ctrl.x, bbox.smoll.x, bbox.big.x, 0.0, 1.0),
                      map(ctrl.y, bbox.smoll.y, bbox.big.y, 0.0, 1.0));
  };
  std::vector<Shape> new_shapes(shapes_.size());
  for(size_t i = 0; i < shapes_.size(); ++i) {
    new_shapes[i] = shapes_[i].transform(trans_fun);
  }
  return ShapeWithHoles(new_shapes);
}
bool ShapeWithHoles::point_inside(glm::dvec2 const& point) const
{
  if(!shapes_[0].point_inside(point)) {
		return false;
	}
	
	// If a hole ocontains the point, 
	// it is not contained in the overall shape!
	for(size_t i = 1; i < shapes_.size(); ++i) {
	  if(shapes_[i].point_inside(point)) {
			return false;
		}
	}
	return true;
}

bool ShapeWithHoles::crosses(BoundingBox<2> abox) const
{
  for(size_t i = 0; i < shapes_.size(); ++i) {
    if(shapes_[i].crosses(abox))
      return true;
  }
  return false;
}

std::pair<glm::dvec2, double> ShapeWithHoles::closestPoint(glm::dvec2 pt) const
{
  std::pair<glm::dvec2, double> closest = shapes_[0].closestPoint(pt);
  
  for(size_t i = 1; i < shapes_.size(); ++i) {
    std::pair<glm::dvec2, double> candidate = shapes_[i].closestPoint(pt);
    
    if(candidate.second < closest.second) {
      closest = candidate;
    }
  }
  return closest;
}
DelaunayTrig::Input ShapeWithHoles::delaunayInput(double target_curvature, bool do_normalize, 
                                      std::function<double(glm::dvec2, glm::dvec2, glm::dvec2)> curvature_evaluator) const
{
  DelaunayTrig::Input out;

  out.boundaryLoop = shapes_[0].discretization(target_curvature, curvature_evaluator);
  if(do_normalize) {
    out.boundaryLoop = shapes_[0].boundingBox().normalize(out.boundaryLoop);
  }
  for(size_t i = 1; i < shapes_.size(); ++i) {
    // normalize still with the border box 
    auto tmp = shapes_[i].discretization(target_curvature, curvature_evaluator);
    std::reverse(tmp.begin(), tmp.end());
    out.holeLoops.push_back(tmp);

    if(do_normalize) {
      out.holeLoops.back() = shapes_[0].boundingBox().normalize(out.holeLoops.back());
    }
  }
  return out;
}

}  // !_2d
}  // !Crv
}  // !Core
