
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "PlanarMinCycleBasis.hpp"

#include <Core/Maths/Utils.hpp>

#include <glm/gtx/string_cast.hpp>
#include <boost/graph/graph_utility.hpp>
#include <iostream>
#include <vector>

namespace Core {
namespace Crv {
namespace _2d {

namespace Pmcb {

struct Edge {
	int from, to;

	Edge() = default;
	Edge(int from_, int to_)
		: from(from_)
		, to(to_)
	{
		if(from > to)
			std::swap(from, to);
	}
	bool operator==(Edge const& other) const
	{
		return from == other.from && to == other.to;
	}
	bool in_vec(std::vector<Edge> const& edges) const
	{
		for(int i = 0; i < edges.size(); ++i) {
			if(edges[i] == *this) {
				return true;
			}
		}
		return false;
	}
};

struct Vertex {
	glm::dvec2 pos; // position in 2d
	std::set<int> adj {};
};
struct CustomGraph {
	std::map<int, Vertex> vertices;
	int n_edges;

	void add_vertex(int id, Vertex vert);
	void add_edge(Edge edg);
	void remove_edge(Edge edg);

	// Repeatedly removes lonely and deadend vertices
	// until the graph is clean
	void remove_lonelyAndDeadends();

	std::optional<Vertex> at(int id) const
	{
		auto find = vertices.find(id);
		if(find == vertices.end()) {
			return std::nullopt;
		}
		return find->second;
	}
	// Vertex& at(int id)
	// {
	// 	try {
	// 		return vertices.at(id);
	// 	} catch(std::exception& e) {
	// 		std::cerr<<"Could not reach vertex id "<<id<<" "<<__FILE__<<",  "<<__FUNCTION__<<",  "<<__LINE__<<"\n";
	// 	}
	// }

	Vertex operator[](int id) const { return *at(id); }
	// Vertex& operator[](int id) { return at(id); }

	std::pair<std::set<int>::iterator, std::set<int>::iterator> adjacency(int vert) { return { vertices[vert].adj.begin(), vertices[vert].adj.end()}; }
	std::pair<std::set<int>::const_iterator, std::set<int>::const_iterator> adjacency(int vert) const { return { vertices.at(vert).adj.begin(), vertices.at(vert).adj.end()}; }

private:
	int remove_lonelyAndDeadends_impl();
};

void CustomGraph::add_vertex(int id, Vertex vert)
{
	vertices[id] = vert;
}
void CustomGraph::add_edge(Edge edg)
{
	if(vertices.find(edg.from) == vertices.end()) {
		std::cerr<<"Cannot add edge, vertex "<<edg.from<<" does not exist"<<std::endl;
		return;
	}
	if(vertices.find(edg.to) == vertices.end()) {
		std::cerr<<"Cannot add edge, vertex "<<edg.to<<" does not exist"<<std::endl;
		return;
	}
	
	if(vertices[edg.from].adj.insert(edg.to).second && vertices[edg.to].adj.insert(edg.from).second) {
		n_edges++;
	}
}
void CustomGraph::remove_edge(Edge edg)
{
	if(vertices.find(edg.from) == vertices.end()) {
		std::cerr<<"Cannot remove edge, vertex "<<edg.from<<" does not exist"<<std::endl;
		return;
	}
	if(vertices.find(edg.to) == vertices.end()) {
		std::cerr<<"Cannot remove edge, vertex "<<edg.to<<" does not exist"<<std::endl;
		return;
	}
	// std::cout<<"Remove edge "<<edg.from<<" <--> "<<edg.to<<"\n";
	if(vertices[edg.from].adj.erase(edg.to) && vertices[edg.to].adj.erase(edg.from)) {
		n_edges--;
	}
}
void CustomGraph::remove_lonelyAndDeadends()
{
	while(remove_lonelyAndDeadends_impl() > 0) {

	}
}
int CustomGraph::remove_lonelyAndDeadends_impl()
{
	std::vector<int> lonely;
	std::vector<int> deadends;

	for(auto vert : vertices) {
		if(vert.second.adj.size() == 0) {
			lonely.push_back(vert.first);
		}
		if(vert.second.adj.size() == 1) {
			deadends.push_back(vert.first);
		}
	}
	for(auto it : lonely) {
		vertices.erase(it);		
	}
	for(auto it : deadends) {
		vertices.erase(it);
		for(auto& vert : vertices) {
			vert.second.adj.erase(it);
		}
	}
	return lonely.size() + deadends.size();
}
std::ostream& operator<<(std::ostream& os, CustomGraph const& graph)
{
	for(auto vert : graph.vertices) {
		os<<vert.first<<" <--> ";
		for(auto adj : vert.second.adj) {
			os<<adj<<" ";
		}
		os<<"\n";
	}
	return os;
}

int ccw_vertex(int prev, int curr, CustomGraph const& graph)
{
	glm::dvec2 curr_pos = graph[curr].pos;
	glm::dvec2 ref_vec;

	if(prev == -1) {
		ref_vec = glm::dvec2(0.0, -1.0);
 	} else {
		ref_vec = curr_pos - graph[prev].pos;
	}

	glm::dvec2 perpRef = perpendicular_cw(ref_vec);

	int next = -1;
	auto [ai, adj_end] = graph.adjacency(curr);
	for (; ai != adj_end; ++ai) {
		int adj = *ai;
		
		if(adj == prev)
			continue;
		
		next = adj;
		break;
	}
	if(next == -1)
		return -1;
	glm::dvec2 dNext = graph[next].pos - curr_pos;
	bool currConvex = glm::dot(dNext, perpRef) <= 0.0;
	for (; ai != adj_end; ++ai) {
		int adj = *ai;

		if(adj == prev)
			continue;
		
		glm::dvec2 dAdg = graph[adj].pos - curr_pos;
		glm::dvec2 perpDAdj = perpendicular_cw(dAdg);
		

		if(currConvex) {
			if(glm::dot(ref_vec, perpDAdj) > 0.0 && glm::dot(dNext, perpDAdj) > 0.0) {
				next = adj;
				dNext = dAdg;
				currConvex = glm::dot(dNext, perpRef) <= 0.0;
			}			
		} else {
			if(glm::dot(ref_vec, perpDAdj) > 0.0 || glm::dot(dNext, perpDAdj) > 0.0) {
				next = adj;
				dNext = dAdg;
				currConvex = glm::dot(dNext, perpRef) <= 0.0;
			}
		}
	}
	return next;
}
int cw_vertex(int prev, int curr, CustomGraph const& graph)
{
	glm::dvec2 curr_pos = graph[curr].pos;
	glm::dvec2 ref_vec;

	if(prev == -1) {
		ref_vec = glm::dvec2(0.0, -1.0);
 	} else {
		ref_vec = curr_pos - graph[prev].pos;
	}

	glm::dvec2 perpRef = perpendicular_cw(ref_vec);

	int next = -1;
	auto [ai, adj_end] = graph.adjacency(curr);
	for (; ai != adj_end; ++ai) {
		int adj = *ai;

		if(adj == prev)
			continue;
		
		next = adj;
		break;
	}
	if(next == -1)
		return -1;

	glm::dvec2 dNext = graph[next].pos - curr_pos;
	bool currConvex = glm::dot(dNext, perpRef) <= 0.0;
	for (; ai != adj_end; ++ai) {
		int adj = *ai;

		if(adj == prev)
			continue;
		
		glm::dvec2 dAdg = graph[adj].pos - curr_pos;
		glm::dvec2 perpDAdj = perpendicular_cw(dAdg);

		if(currConvex) {
			if(glm::dot(ref_vec, perpDAdj) < 0.0 || glm::dot(dNext, perpDAdj) < 0.0) {
				next = adj;
				dNext = dAdg;
				currConvex = glm::dot(dNext, perpRef) <= 0.0;
			}			
		} else {
			if(glm::dot(ref_vec, perpDAdj) < 0.0 && glm::dot(dNext, perpDAdj) < 0.0) {
				next = adj;
				dNext = dAdg;
				currConvex = glm::dot(dNext, perpRef) <= 0.0;
			}
		}
	}
	return next;
}

// returns a cycle and the number of removed edges
Cycle cycleFromVertex(int vertex, CustomGraph& graph)
{
	graph.remove_lonelyAndDeadends();
	if(!graph.at(vertex)) {
		return {};
	}
	int prev = vertex;
	int curr = cw_vertex(-1, vertex, graph);
	if(curr == -1) {
		std::cerr<<"No clockwise edge"<<std::endl;
	}
	// std::cout<<"first vert "<<vertex<<"\n";
	// std::cout<<"First: "<<curr<<"\n";
	Cycle out;
	out.push_back(vertex);
	while(curr != vertex) {
	// for(int i = 0; i < 20; ++i) {
		// if(curr == vertex)
			// break;
		// std::cout<<curr<<"\n";
		out.push_back(curr);
		int next = ccw_vertex(prev, curr, graph);

		if(next == -1) {
			std::cerr<<"Failed to find cycle\n";
			return Cycle{};
		}

		prev = curr;
		curr = next;
	}
	out.push_back(vertex);
	
	// Remove as many edges as possible
	graph.remove_edge(Edge(out[0], out[1]));
	int delReach;
	for(delReach = 1; delReach < out.size()-1; ++delReach) {
		if(graph[out[delReach]].adj.size() > 1)
			break;
		graph.remove_edge(Edge(out[delReach], out[delReach+1]));
	}
	for(int i = out.size()-1; i > delReach; --i) {
		if(graph[out[i]].adj.size() > 1)
			break;
		graph.remove_edge(Edge(out[i], out[i-1]));
	}
	graph.remove_lonelyAndDeadends();

	// std::cout<<"Found cycle: \n";
	for(int i = 0; i < out.size(); ++i) {
		// std::cout<<out[i]<<" :: ";
	}
	// std::cout<<"\n";
	return out;
}

int select_vertex(CustomGraph const& graph)
{
	int seedVertex = -1;
	glm::dvec2 leftMost_pos = glm::dvec2(std::numeric_limits<double>::max());
	
	for(auto vert : graph.vertices) {
		glm::dvec2 vPos = vert.second.pos;

		 // Fall back on y if two vertices have the same x values
		if((vPos.x < leftMost_pos.x) || (vPos.x == leftMost_pos.x && vPos.y < leftMost_pos.y)) {
			leftMost_pos = vPos;
			seedVertex = vert.first;
		}
	}
	return seedVertex;
}
// Find the cycles in a single connected component
std::vector<Cycle> cyclesFromComponent(CustomGraph graph)
{
	int num_cycles = graph.n_edges - graph.vertices.size() + 1;
	std::vector<Cycle> out;

	for(int i = 0; i < num_cycles; ++i) {
		int seed = select_vertex(graph);
		Cycle cycle = cycleFromVertex(seed, graph);

		if(cycle.empty())
			break;
		out.push_back(cycle);
	}
	return out;
}

void add_vert_to_graph_rec(CustomGraph& dstGraph, int vertex, int comp_ind, std::vector<int>& components, int from, Graph_t const& srcGraph)
{
	using namespace boost;

	components[vertex] = comp_ind;

	dstGraph.add_vertex(vertex, Vertex{.pos=srcGraph[vertex]});

	for (auto [ei, edge_end] = out_edges(vertex, srcGraph); ei != edge_end; ++ei) {
		int adj_vert_id = target(*ei, srcGraph);
		if(adj_vert_id == from)
			continue;

		if(components[adj_vert_id] == -1) {
			add_vert_to_graph_rec(dstGraph, adj_vert_id, comp_ind, components, vertex, srcGraph);
		}
		Edge new_edge(vertex, adj_vert_id);
		dstGraph.add_edge(new_edge);
	}
}
std::vector<CustomGraph> create_intGraphs(Graph_t const& graph)
{
	std::vector<CustomGraph> out;

	std::vector<int> components(boost::num_vertices(graph), -1);
	int comp_ind = 0;

	for (auto [i, end] = vertices(graph); i != end; ++i) {
		if(components[*i] != -1)
			continue;
		

		out.push_back(CustomGraph());
		add_vert_to_graph_rec(out.back(), *i, comp_ind, components, -1, graph);
		comp_ind++;
	}
	return out;
}
std::pair<std::vector<Cycle>, std::vector<IndRange>>
	 planarMincCycleBasis(Graph_t graph)
{
	std::vector<CustomGraph> intGraphs = create_intGraphs(graph);
	std::vector<Cycle> retCycles;
	std::vector<IndRange> retClusters;

	// std::cout<<"N graphs "<<intGraphs.size()<<"\n";

	// Find cycles in each searchGraph, potential for parallelism,
	// but would have to check if there is often more than one searchGraph in practice
	for(int i = 0; i < intGraphs.size(); ++i) {
		// std::cout<<"Graph "<<i<<"\n";
		// std::cout<<intGraphs[i];

		std::vector<Cycle> tmp = cyclesFromComponent(intGraphs[i]);
		retClusters.push_back(IndRange(retCycles.size(), retCycles.size()+tmp.size()));
		retCycles.insert(retCycles.end(), tmp.begin(), tmp.end());
	}

	return { retCycles, retClusters };
}


bool cycle_equal_impl(Cycle c1, Cycle c2)
{
	int start_ind = -1;
	for(int i = 0; i < c2.size(); ++i) {
		if(c1[0] == c2[i]) {
			start_ind = i;
		}
	}
	if(start_ind == -1)
		return false;

	for(int i = 0; i < c1.size(); ++i) {
		if(c1[i] != c2[(i+start_ind) % c2.size()])
			return false;
	}
	return true;
}
CycleEq cycle_equal(Cycle c1, Cycle c2)
{
	if(c1.size() != c2.size())
		return CycleEq::NotEqal;

	// Remove end duplicate
	c1.resize(c1.size()-1);
	c2.resize(c2.size()-1);

	if(c1.empty())
		return CycleEq::NotEqal;

	if(cycle_equal_impl(c1, c2))
		return CycleEq::Equal;

	std::reverse(c2.begin(), c2.end());
	if(cycle_equal_impl(c1, c2))
		return CycleEq::EqualInvert;

	return CycleEq::NotEqal;
}

} // !Pmcb
} // !_2d
} // !Crv
} // !Core