
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
// sddssd

#ifndef BLOOP_CORE_PLANARMINCYCLEBASIS_HPP_
#define BLOOP_CORE_PLANARMINCYCLEBASIS_HPP_

#include <Core/Utils/Range.hpp>

#include <glm/glm.hpp>

#include <boost/graph/adjacency_list.hpp>

#include <vector>

namespace Core {
namespace Crv {
namespace _2d {
namespace Pmcb {

using Cycle = std::vector<int>;
enum class CycleEq { Equal, NotEqal, EqualInvert };

using Graph_t = boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS, glm::dvec2>;

// Return all cycles and a map to remember which cycles are in which component of the graph
std::pair<std::vector<Cycle>, std::vector<IndRange>> planarMincCycleBasis(Graph_t graph);

CycleEq cycle_equal(Cycle c1, Cycle c2);

} // !Pmcb
} // !_2d
} // !Crv
} // !Core

#endif
