
#ifndef BLOOP_CORE_CURVE_DISCRETECURVE_HPP_
#define BLOOP_CORE_CURVE_DISCRETECURVE_HPP_

#include <Core/Maths/BoundingBox.hpp>
#include "Core/Curve/Line.hpp"
#include "Nurbs.hpp"
#include <Core/Id.hpp>
#include "CurvePoint.hpp"

#include "glm/geometric.hpp"

#include <glm/glm.hpp>

#include <vector>
#include <stack>

namespace Core {
namespace Crv {

struct Node {
  size_t pt1_i, pt2_i; // Points 1 has the smallest index
  BoundingBox<2> bbox;
  size_t left_child { invalid_index() }, right_child { invalid_index() };
  
  bool is_leaf() const { return left_child == invalid_index(); } // Cant have only one child..
};

template<int dim>
class DiscreteCurve {
private:
  std::vector<Point<dim>> points;
  std::vector<Node> nodes;
  
public:
  DiscreteCurve(NurbsCurve<dim> const& crv)
  {
    size_t n_controls = crv.n_controls();
    
    for(size_t i = 0; i < n_controls; ++i) {
      double t = static_cast<double>(i) / static_cast<double>(n_controls - 1);
      points.push_back(Point<dim>{ .t=t, .pos=crv.at(t) });
    }
    
    nodes.push_back(Node{ .pt1_i=0, .pt2_i=points.size() - 1 });
    recursive_split(0);
    make_bboxes();
  }
  
  void recursive_split(int from)
  {
    std::stack<int> to_split;
    to_split.push(from);
    while(!to_split.empty()) {
      auto spl = split(to_split.top());
      nodes[to_split.top()].left_child = spl.first;
      nodes[to_split.top()].right_child = spl.second;
      to_split.pop();
      if(spl.first != invalid_index()) {
        to_split.push(spl.first);
      }
      if(spl.second != invalid_index()) {
        to_split.push(spl.second);
      }
    }
  }
  void make_bboxes()
  {
    for(size_t i = 0; i < nodes.size(); ++i) {
      nodes.at(i).bbox = make_bbox(nodes.at(i).pt1_i, nodes.at(i).pt2_i);
    }
  }
  BoundingBox<dim> make_bbox(int ptfirst, int ptlast)
  {
    BoundingBox<dim> box;
    for(size_t i = ptfirst; i < ptlast+1; ++i) {
      box = box.extend(points.at(i).pos);
    }
    return box;
  }
  std::pair<size_t, size_t> split(int parent_ind)
  {
    Node parent = nodes[parent_ind];
    if(parent.pt1_i != parent.pt2_i - 1) {
      Node left { .pt1_i=parent.pt1_i, .pt2_i=parent.pt1_i+(parent.pt2_i-parent.pt1_i)/2 };
      Node right { .pt1_i=left.pt2_i, .pt2_i=parent.pt2_i };
      
      nodes.push_back(left);
      nodes.push_back(right);

      return { nodes.size() - 2, nodes.size() - 1 };
    }
    // Split the curve geometrically and
    // make each existing node make sense
    // (if a point is added in it's range, the range must be extended)
    
    return { invalid_index(), invalid_index() };
  }
    
  LineAbst<dim> segment(int node_ind) const
  {
    Node nd = nodes.at(node_ind);
    return LineAbst<dim>(points.at(nd.pt1_i).pos, points.at(nd.pt2_i).pos);
  }
  // Returns the parametric range of a node
  std::pair<double, double> range(int node_ind) const
  {
    return { points.at(nodes.at(node_ind).pt1_i).t, points.at(nodes.at(node_ind).pt2_i).t };
  }
  // Returns indices in both curves of nodes that intersect
  std::vector<std::pair<int, int>> intersections(DiscreteCurve<dim> const& other)
  {
    std::vector<std::pair<int, int>> out;
    std::stack<std::pair<int, int>> curve_sections_pairs; // index of a node in self, index of node in other
    curve_sections_pairs.push({ 0, 0 });
    
    while(!curve_sections_pairs.empty()) {
      std::pair<int, int> curr_pair = curve_sections_pairs.top();
      curve_sections_pairs.pop();

      Node nodeA = nodes.at(curr_pair.first);
      Node nodeB = other.nodes.at(curr_pair.second);
      
      if(nodeA.bbox.overlaps(nodeB.bbox)) {
        if(nodeA.is_leaf() && nodeB.is_leaf()) {
          if(segment(curr_pair.first).intersection(other.segment(curr_pair.second)))
            out.push_back(curr_pair);
        } else if(nodeA.is_leaf()) {
          curve_sections_pairs.push( { curr_pair.first, nodeB.left_child });
          curve_sections_pairs.push( { curr_pair.first, nodeB.right_child });
        } else if(nodeB.is_leaf()) {
          curve_sections_pairs.push( { nodeA.left_child, curr_pair.second });
          curve_sections_pairs.push( { nodeA.right_child, curr_pair.second });
        } else {
          curve_sections_pairs.push({ nodeA.left_child, nodeB.left_child });
          curve_sections_pairs.push({ nodeA.right_child, nodeB.right_child });
          curve_sections_pairs.push({ nodeA.left_child, nodeB.right_child });
          curve_sections_pairs.push({ nodeA.right_child, nodeB.left_child });
        }
      }
    }
    return out;
  }
};

} // !Crv
} // !Core

#endif
