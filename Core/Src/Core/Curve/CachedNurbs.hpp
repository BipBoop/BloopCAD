
#ifndef BLOOP_CORE_CURVE_CACHED_NURBS_HPP_
#define BLOOP_CORE_CURVE_CACHED_NURBS_HPP_

#include "Core/Curve/CurvePoint.hpp"
#include "Nurbs.hpp"
#include "DiscreteCurve.hpp"

namespace Core {
namespace Crv {

template<int dim>
class CachedNurbs {
public:
  using vec_t = glm::vec<dim, double>;
  using point_t = CurvePoint<dim>;
private:
  DiscreteCurve<dim> lowRes;
  NurbsCurve<dim> parametric;
  
public:
  CachedNurbs(NurbsCurve<dim> param_)
    : parametric(param_)
    , lowRes(param_)
  {
    
  }
  vec_t at(double t) const  { return parametric.at(t); }
  
  
  std::vector<std::pair<point_t, point_t>> intersections(CachedNurbs<dim> const& other)
  {
    std::vector<std::pair<int, int>> hints = lowRes.intersections(other.lowRes);
    std::vector<std::pair<point_t, point_t>> out;
    for(size_t i = 0; i < hints.size(); ++i) {
      
      // Find intersections with range hints
      std::vector<std::pair<point_t, point_t>> tmp = 
        parametric.intersections(other.parametric, lowRes.range(hints[i].first), other.lowRes.range(hints[i].second));
      
    }
  }
};
  
}   
} // !Core

#endif
