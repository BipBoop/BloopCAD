
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef BLOOP_CORE_LINE_2D_HPP_
#define BLOOP_CORE_LINE_2D_HPP_


#include <glm/glm.hpp>
#include <glm/gtx/string_cast.hpp>

#include <Core/Maths/Utils.hpp>
#include <Core/Precision.hpp>

#include <iostream>
#include <optional>

namespace Core {
namespace Crv {

template<int dim>
struct LineAbst {
  using vec_t = glm::vec<dim, double>;
	vec_t pt1, pt2;

	LineAbst() = default;
	LineAbst(vec_t pt1_, vec_t pt2_)
		: pt1(pt1_)
		, pt2(pt2_)
	{

	}
	static LineAbst<dim> from_posDir(vec_t const& pos, vec_t dir)
	{
		return LineAbst<dim>(pos, pos+dir);
	}

	double length() const
	{
		return glm::distance(pt1, pt2);
	}
	double length2() const
	{
		return glm::distance2(pt1, pt2);
	}
	vec_t direction_norm() const
	{
		return glm::normalize(direction());
	}
	vec_t direction() const
	{
		return pt2 - pt1;
	}
	vec_t normal() const
	{
		return perpendicular_ccw(direction_norm());
	}
	vec_t at(double lerp) const
	{
		return pt1 + direction() * lerp;
	}
	std::pair<LineAbst<dim>, LineAbst<dim>> split(double lerp) const 
	{
		vec_t mid = at(lerp);
		return { LineAbst<dim>(pt1, mid), LineAbst<dim>(mid, pt2)};
	}
	double dist(vec_t const& pos) const
	{
		// https://www.omnicalculator.com/math/triangle-height
		double a, b, c; // the side lengths of the triangle;
		a = glm::distance(pos, pt1);
		b = length();
		c = glm::distance(pos, pt2);

		if(b == 0) {
			return a;
		}
		double h = 0.5 * std::sqrt((a + b + c) * (-a + b + c) * (a - b + c) * (a + b - c)) / b;
		
		return h;
	}
	double dist_signed(vec_t const& pos) const
	{
		return ccw(pt1, pos, pt2) ? dist(pos) : -dist(pos);
	}

	double closest_lerp(vec_t const& pos) const
	{
		double len2 = length2();
		if(len2 == 0) {
			return 0.0;
		}
		return glm::dot(direction(), pos-pt1) / len2;
	}
	std::optional<std::pair<double, double>> intersection(LineAbst<dim> const& other) const;
};

template<> std::optional<std::pair<double, double>> LineAbst<2>::intersection(LineAbst<2> const& other) const;

namespace _2d {

using Line = LineAbst<2>;
  
} // !_2d

namespace _3d {

using Line = LineAbst<3>;
  
} // !_3d

} // !Crv
} // !Core

#endif
