
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef BLOOP_CORE_SOLID_HPP_
#define BLOOP_CORE_SOLID_HPP_

#include <Core/Surface/Surface.hpp>

#include <unordered_set>

namespace Core {

struct HalfEdge {
	int prev { -1 }, next { -1 };
	int self_id { -1 }; // Index in the half edges vector
	int twin { -1 };

	int curve { -1 }; // Non unique id, it refers to the loop it is part of
	int face { -1 };
};

struct Face {
  Srf::Surface surface; // Parametric trimmed surface
  
	std::vector<int> halfEdges;

	// Gives the specified curve projected on the 
	// face's surface
	// the hole ind specifies on which hole the curve_ind acts
	// default is -1, means the outer perimeter
	Crv::NurbsCurve<2> curve_2d(int curve_ind, int hole_ind = -1) const;
	Crv::NurbsCurve<3> curve_3d(int curve_ind, int hole_ind = -1) const;
	
	size_t loop_size(int hole_ind = -1) const;
	int he(int hole_ind = -1) const;
  int border_he() { return he(-1); }
};

struct Solid {
	std::vector<HalfEdge> halfEdges;
	std::vector<Face> faces;

	static Solid make_extrusion(Srf::Surface surface, glm::dvec3 direction);

	int add_face(Srf::Surface surface);
	HalfEdge& create_loop(int n, int face);
	HalfEdge& n_forward(HalfEdge he, int n);
	HalfEdge& n_forward(int he_id, int n);

	// Sow two faces along their respective edges
	// edges are relative to their border_he 
	// e.g. if it's 3, it's edge border_he->next->next->next
	void sow_faces(HalfEdge& loopHE1, int edge1, HalfEdge& loopHE2, int edge2);

	// loop_ind is -1 for outer loop and > 0 for hole loops
	void extrusion_sideFaceHelper(int botFace_ind, int topFace_ind, int loop_ind, Crv::NurbsCurve<3> sweepCurve);
};

} // !Core

#endif