
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "Solid.hpp"

#include <Core/Curve/Line.hpp>

#include <unordered_set>

namespace Core {

using namespace Crv;
using namespace Srf;

NurbsCurve<2> Face::curve_2d(int curve_ind, int hole_ind) const
{
  // hole_ind = -1 fo border edge
  return surface.trimming.shapes_[hole_ind+1].edges_.at(curve_ind);
}
NurbsCurve<3> Face::curve_3d(int curve_ind, int hole_ind) const
{
	return surface->embed_curve(curve_2d(curve_ind, hole_ind));
}
size_t Face::loop_size(int hole_ind) const
{
  return surface.trimming.shapes_[hole_ind+1].n_edges();
}
int Face::he(int hole_ind) const
{
  return halfEdges[hole_ind+1];
}
Solid Solid::make_extrusion(Surface surface, glm::dvec3 direction)
{
	std::cout<<"make extrusion ["<<glm::to_string(direction)<<"]\n";
	Solid out;
	auto sweepCurve = _3d::to_nurbs(_3d::Line(glm::dvec3(0.0), direction));

	// the bottom face should point away from the direction
	// Assume it's ok and deal with it later
	auto botFace_ind = out.add_face(surface.invert());
	auto top_surf = surface;
  top_surf.support = top_surf.support.offset_by(sweepCurve);
	auto topFace_ind = out.add_face(top_surf);

	if(glm::dot(surface.support.normal(0.0, 0.0), direction) < 0.0) { // Bottom face is not well oriented
		std::cout<<"bot-top-faces swap!\n";
		std::swap(botFace_ind, topFace_ind); // Ok because the topface surface has not been offseted yet
	}
	
	for(int i = 0; i < static_cast<int>(surface.trimming.n_shapes()); ++i) {
		out.extrusion_sideFaceHelper(botFace_ind, topFace_ind, i-1, sweepCurve);
	}

	return out;
}
void Solid::extrusion_sideFaceHelper(int botFace_ind, int topFace_ind, int loop_ind, NurbsCurve<3> sweepCurve)
{
	HalfEdge bot_he_it = halfEdges.at(faces.at(botFace_ind).he(loop_ind));
	HalfEdge top_he_it = halfEdges.at(faces.at(topFace_ind).he(loop_ind));

	int firstAddedFace_ind = faces.size();
	int loopsize = faces.at(botFace_ind).loop_size(loop_ind);
	bool invert_profile = loop_ind == -1; // Loop is border!

	for(int i = 0; i < loopsize; ++i) {
		// Got to invert the curve because twin half edges go
		// in opposite directions
		auto profile = invert_profile ? faces[botFace_ind].curve_3d(i, loop_ind).invert() : faces[botFace_ind].curve_3d(i, loop_ind);
		auto sweep = invert_profile ? sweepCurve : sweepCurve;
		int sideFace_ind = add_face(
								Srf::Surface(Srf::NurbsSurface::make_sweep(profile, sweep)));
		
		if(i > 0) {
			sow_faces(halfEdges.at(faces.at(sideFace_ind).border_he()), 1, 
								halfEdges.at(faces.at(sideFace_ind-1).border_he()), 3);
		}

		sow_faces(bot_he_it, 0, halfEdges.at(faces.at(sideFace_ind).border_he()), 0);
		sow_faces(top_he_it, 0, halfEdges.at(faces.at(sideFace_ind).border_he()), 2);

		bot_he_it = halfEdges.at(bot_he_it.next);
		top_he_it = halfEdges.at(top_he_it.prev);
	}
	sow_faces(halfEdges.at(faces.back().border_he()), 3, halfEdges.at(faces[firstAddedFace_ind].border_he()), 1);
}

int Solid::add_face(Surface surface)
{
  size_t newFace_ind = faces.size();
	Face newFace;
	newFace.surface = surface;

  for(size_t i = 0; i < surface.trimming.n_shapes(); ++i) {
    newFace.halfEdges.push_back(create_loop(surface.trimming.shapes_[i].n_edges(), newFace_ind).self_id);
  }

	faces.push_back(newFace);
	return newFace_ind;
}

HalfEdge& Solid::create_loop(int n, int face)
{
	if(n <= 0) {
		std::cerr<<"Invalid n in create he loop: "<<n<<std::endl;
	}
	int first_he = halfEdges.size();
	for(int i = 0; i < n; ++i) {
		halfEdges.push_back(HalfEdge{.prev=(i==0 ? n-1 : i-1) + first_he, 
										.next=(i+1)%n + first_he,
										.self_id=static_cast<int>(halfEdges.size()),
										.twin=-1,
										.curve=i,
										.face=face});
	}
	std::cout<<"Create loop from "<<first_he<<" to "<<first_he+n-1<<"\n";
	return halfEdges.at(first_he);
}
HalfEdge& Solid::n_forward(HalfEdge he, int n)
{
	for(int i = 0; i < n; ++i) {
		he = halfEdges.at(he.next);
	}
	return halfEdges.at(he.self_id);
}
HalfEdge& Solid::n_forward(int he_id, int n)
{
	return n_forward(halfEdges.at(he_id), n);
}

void Solid::sow_faces(HalfEdge& loopHE1, int edge1, HalfEdge& loopHE2, int edge2)
{
	HalfEdge& edge1_sow = n_forward(loopHE1, edge1);
	HalfEdge& edge2_sow = n_forward(loopHE2, edge2);
	edge1_sow.twin = edge2_sow.self_id;
	edge2_sow.twin = edge1_sow.self_id;

	std::cout<<"sow "<<edge1_sow.self_id<<",  "<<edge2_sow.self_id<<"\n";
}

} // !Core