
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef BLOOP_CORE_SOLVER_SUBSTITUTION_HPP_
#define BLOOP_CORE_SOLVER_SUBSTITUTION_HPP_

#include <Core/Id.hpp>

#include <map>
#include <vector>
#include <unordered_set>
#include <optional>

namespace Core {
namespace Slv {
namespace Subst { 

struct Relation {
  // first and second are indices in a vector of 
  // parameters (numbers)
	size_t first { invalid_index() }, second { invalid_index() };

  // Offset between the two parameters 
  // [if the relation is not an assignement]
  // params[first] + offset = params[second]
  // [if the relation is an assignement]
  // params[first] = offset
	double offset { 0.0 };
	bool is_assignement { false };
};

struct Blob {
	// Param are in the global space
  // root + offset = param
	std::map<size_t, double> param_offsetToRoot;

  // Root is the parameter around which all offsets are based
	size_t root { 0 };

  // Lead is the parameter that leads a specific solve
  // if the blob is not const, the value of the root
  // parameter is computed from the value of the lead 
  // parameter and the rest is a normal solve
	size_t lead { 0 };

  // If the blob is const, const_val is
  // the value of the root parameter
  std::optional<double> const_val { std::nullopt };

  bool is_const() const { return const_val.has_value(); }

  void choose_lead(std::unordered_set<size_t> const& dragged);
  void update(std::vector<double>& flow);
};
struct Map {
  // Map between the param index in the param flow (numbers)
  // and the index of their blob in the blobs vector 
  // usefull to build and merge blobs and queery 
  // the blob that contains a specific parameter
  // a parameter cannot be in two distinct blobs
  // otherwise they would merge
	std::map<size_t, size_t> param_to_blob;
	std::vector<Blob> blobs;

  // Adds a relation and returns if it worked
	bool add_relation(Relation rel);
	bool add_relation_assignement(Relation rel);

	// this function assumes the first element of rel is in the blob
  // 
	bool add_relation_oneBlob(Relation rel, size_t blob);
	bool merge_blobs(Relation rel, size_t blob_a, size_t blob_b);
	bool create_blob(Relation rel);
	bool create_constBlob(Relation rel);
  // Makes a blob const by adding an assignement relation
  // returns if it worked
  bool constify_blob(Relation rel, size_t blob_ind);

	// void assign_constness(std::vector<size_t> const_params);
	bool in_same_blob(size_t a, size_t b) const;

  // Returns the distance between two parameters if they
  // are in the same blob
  // the returned value if a+offset = b
  std::optional<double> offset(size_t a, size_t b) const;

  void prepare_solve(std::unordered_set<size_t> const& dragged);
  void solve(std::vector<double>& params);
};


} // !Subst
} // !Slv
} // !Core

#endif