
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "Substitution.hpp"

#include <iostream>

namespace Core {
namespace Slv {
namespace Subst {

void Blob::choose_lead(std::unordered_set<size_t> const& dragged)
{
	lead = root;

	if(!is_const()) {
		// TODO is it better to iterate dragged and search param_offsetToRoot or the opposite?
		for(auto one_dragged : dragged) {
			if(param_offsetToRoot.find(one_dragged) != param_offsetToRoot.end()) {
				lead = one_dragged;
			}
		}
	}
}
void Blob::update(std::vector<double>& flow)
{
  if(is_const()) {
    flow[root] = *const_val;
  } else {
    flow[root] = flow[lead] - param_offsetToRoot[lead];
  }
  for(auto p_o : param_offsetToRoot) {
    flow[p_o.first] = flow[root] + p_o.second;
  }
}

bool Map::add_relation(Relation rel)
{
	if(rel.is_assignement) {
		return add_relation_assignement(rel);
	} 
  // Cannot have offset relation 
  // between a parameter and itself
  if(rel.first == rel.second) {
    return false;
  }

	auto blob_a = param_to_blob.find(rel.first);
	auto blob_b = param_to_blob.find(rel.second);

	bool blob_a_in = blob_a != param_to_blob.end();
	bool blob_b_in = blob_b != param_to_blob.end();

	if(blob_a_in && blob_b_in) { // Both have blobs, merge
		return merge_blobs(rel, blob_a->second, blob_b->second);
	} else if(!blob_a_in && blob_b_in) {
		std::swap(rel.first, rel.second);
		rel.offset = -rel.offset;
		return add_relation_oneBlob(rel, blob_b->second);
	} else if(blob_a_in && !blob_b_in) {
		return add_relation_oneBlob(rel, blob_a->second);
	} else {
		return create_blob(rel);
	}
}
bool Map::add_relation_assignement(Relation rel)
{
	auto blob = param_to_blob.find(rel.first);
	bool blob_in = blob != param_to_blob.end();

	if(!blob_in) {
		return create_constBlob(rel);
	} else {
    return constify_blob(rel, blob->second);
	}
}
bool Map::add_relation_oneBlob(Relation rel, size_t blob_ind)
{
	Blob& blob = blobs[blob_ind];
	blob.param_offsetToRoot[rel.second] = blob.param_offsetToRoot[rel.first] + rel.offset;
	param_to_blob[rel.second] = blob_ind;
  return true;
}
bool Map::merge_blobs(Relation rel, size_t blob_a_ind, size_t blob_b_ind)
{
	if(blob_a_ind == blob_b_ind) {
		std::cerr<<"Cannot merge same blob "<<blob_a_ind<<"\n";
    return false;
	}

	Blob& blob_a = blobs[blob_a_ind];
	Blob& blob_b = blobs[blob_b_ind];

	double offset_a_to_b = rel.offset-blob_a.param_offsetToRoot[rel.first];

	for(auto param_offset : blob_b.param_offsetToRoot) {
		blob_a.param_offsetToRoot[param_offset.first] = param_offset.second + offset_a_to_b;
		param_to_blob[param_offset.first] = blob_a_ind;
	}

	// Offset by one the blob pointer of parameters whose blob index > blob_b_ind
	// because blob_b is removed
	for(auto it = param_to_blob.begin(); it != param_to_blob.end(); ++it) {
		if(it->second > blob_b_ind)
			it->second--;
	}

	blobs.erase(blobs.begin()+blob_b_ind);

  return true;
}
bool Map::create_blob(Relation rel)
{
	Blob new_blob;
	new_blob.root = rel.first;
	new_blob.param_offsetToRoot[rel.first] = 0.0;
	new_blob.param_offsetToRoot[rel.second] = rel.offset;
	param_to_blob[rel.first] = blobs.size();
	param_to_blob[rel.second] = blobs.size();
	blobs.push_back(new_blob);
  return true;
}
bool Map::create_constBlob(Relation rel)
{
	Blob new_blob;
	new_blob.root = rel.first;
	new_blob.param_offsetToRoot[rel.first] = 0.0;
  new_blob.const_val = rel.offset;
	param_to_blob[rel.first] = blobs.size();
	blobs.push_back(new_blob);
  return true;
}
bool Map::constify_blob(Relation rel, size_t blob_ind)
{
  Blob& blob = blobs[blob_ind];
  if(blob.is_const()) {
    return false;
  }
  // The blob is already centered around this parameter!
  if(blob.root == rel.first) {
    blob.const_val = rel.offset;
    return true;
  }
  // the parameter pointed by the assignment rel
  // will become the new root
  // find old_root + offset = new_root
  // to update the blob's map
  auto find = blob.param_offsetToRoot.find(rel.first);
  if(find == blob.param_offsetToRoot.end()) {
    std::cerr<<"Unexpected: param is not in blob in function "<<__FUNCTION__<<std::endl;
    return false;
  }
  double oldRoot_to_newConst = find->second;

  for(auto& p_o : blob.param_offsetToRoot) {
    p_o.second += oldRoot_to_newConst;
  }
  blob.const_val = rel.offset;
  blob.root = rel.first;

  return true;
}
bool Map::in_same_blob(size_t a, size_t b) const
{
	auto blob_a = param_to_blob.find(a);
	auto blob_b = param_to_blob.find(b);
	
	return	blob_a != param_to_blob.end() && 
			blob_b != param_to_blob.end() && 
			blob_a->second == blob_b->second;
}
std::optional<double> Map::offset(size_t a, size_t b) const
{
	auto blob_a = param_to_blob.find(a);
	auto blob_b = param_to_blob.find(b);
	
	if(	blob_a == param_to_blob.end() || 
			blob_b == param_to_blob.end() || 
			blob_a->second != blob_b->second) {
		return std::nullopt;
	}

	// Both params are in the same blob!
	// Now we need to find their offset
	double offset_a = blobs[blob_a->second].param_offsetToRoot.at(a);
	double offset_b = blobs[blob_a->second].param_offsetToRoot.at(b);

	return offset_b - offset_a;
}
void Map::prepare_solve(std::unordered_set<size_t> const& dragged)
{
  for(size_t i = 0; i < blobs.size(); ++i) {
    blobs[i].choose_lead(dragged);
  }
}
void Map::solve(std::vector<double>& params)
{
  for(size_t i = 0; i < blobs.size(); ++i) {
    blobs[i].update(params);
  }
}



} // !
} // !Slv
} // !Core