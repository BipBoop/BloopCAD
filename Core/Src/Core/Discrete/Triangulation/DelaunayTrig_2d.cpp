
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "DelaunayTrig_2d.hpp"

#include <Core/Maths/Utils.hpp>
#include <Core/Curve/Curve_decl.hpp>

#include <glm/gtx/string_cast.hpp>

namespace Core {

std::vector<glm::dvec2> DelaunayTrig::Input::unwrap() const
{
	std::vector<glm::dvec2> out = boundaryLoop;
	for(int i = 0; i < holeLoops.size(); ++i) {
		out.insert(out.end(), holeLoops[i].rbegin(), holeLoops[i].rend());
	}
	out.insert(out.end(), pointCloud.begin(), pointCloud.end());

	return out;
}


void add_triangle_to_indices(DelaunayTrig::Triangle const& src, std::vector<unsigned int>& dst)
{
	dst.insert(dst.end(), src.vertices.begin(), src.vertices.end());
}

DelaunayTrig::Triangle::Triangle(std::array<int, 3> const& vertices_) : vertices(vertices_) {}
DelaunayTrig::Triangle::Triangle(Triangle const& other)
{
	for(int i = 0; i < 3; ++i) {
		vertices[i] = other.vertices[i];
		adjacency[i] = other.adjacency[i];
	}
}
DelaunayTrig::Triangle::Triangle(Triangle&& other)
{
	for(int i = 0; i < 3; ++i) {
		vertices[i] = other.vertices[i];
		adjacency[i] = other.adjacency[i];
	}
}
DelaunayTrig::Triangle& DelaunayTrig::Triangle::operator=(Triangle const& other)
{
	for(int i = 0; i < 3; ++i) {
		vertices[i] = other.vertices[i];
		adjacency[i] = other.adjacency[i];
	}
	return *this;
}
DelaunayTrig::Triangle& DelaunayTrig::Triangle::operator=(Triangle&& other)
{
	for(int i = 0; i < 3; ++i) {
		vertices[i] = other.vertices[i];
		adjacency[i] = other.adjacency[i];
	}
	return *this;
}

DelaunayTrig::ConcreteTriangle::ConcreteTriangle(std::array<glm::dvec2, 3> vertices_) 
	: vertices(vertices_)
{

}
DelaunayTrig::ConcreteTriangle::ConcreteTriangle(
						glm::dvec2 const& vertA, glm::dvec2 const& vertB, glm::dvec2 const& vertC) 
	: vertices({ vertA, vertB, vertC }) 
{

}

DelaunayTrig::TriangleEdge::TriangleEdge(int vertA_, int vertB_)
	: vertA(vertA_)
	, vertB(vertB_)
{

}
bool DelaunayTrig::TriangleEdge::operator==(TriangleEdge const& other) const
{
	return (vertA == other.vertA && vertB == other.vertB) || (vertA == other.vertB && vertB == other.vertA);
}

DelaunayTrig::TriangleVert_proxy::TriangleVert_proxy(int triangle_, int vert_inner_ind_)
	: triangle(triangle_)
	, vert_inner_ind(vert_inner_ind_)
{

}


// std::vector<DelaunayTrig::ConcreteTriangle> DelaunayTrig::triangulate(Input const& inputVertices)
// {
// 	mVertices.resize(vertices.size() + 3);
	
// 	make_superTriangle();
// 	preprocess_vertices(vertices);

// 	triangulate_impl();

// 	std::vector<ConcreteTriangle> out;
// 	out.reserve(mTriangles.size());
// 	for(int i = 0; i < mTriangles.size(); ++i) {
// 		if(!triangle_has_superTriangleVert(i))
// 			out.push_back(to_concreteTriangle(i, false));
// 	}
// 	return out;
// }
TrigData2d DelaunayTrig::triangulate_to_data(Input const& inputVertices)
{
	// std::cout<<"Triangulate boundary: \n";
	// for(int i = 0; i < inputVertices.boundaryLoop.size(); ++i) {
	// 	std::cout<<"("<<inputVertices.boundaryLoop[i].x<<", "<<inputVertices.boundaryLoop[i].y<<")\n";
	// }
	// std::cout<<"\n";

	std::vector<glm::dvec2> initial_vertices = inputVertices.unwrap();

	mVertices.resize(initial_vertices.size() + 3);
	
	make_superTriangle();
	preprocess_vertices(initial_vertices);

	triangulate_impl();

	std::vector<unsigned int> ind_triangles;
	ind_triangles.reserve(mTriangles.size()*3); // Will almost always be a bit too big, is it ok?

	if(!inputVertices.boundaryLoop.empty() || !inputVertices.holeLoops.empty()) {
		constrain_triangulation(inputVertices);

		for(int i = 0; i < mTriangles.size(); ++i) {
			if(triangle_has_superTriangleVert(i))
				continue;
			if(triangleLoop_adjacency(i, 0, inputVertices.boundaryLoop.size()) 
									== TriangleLoop_adjacency::Outside) // Triangle is outside the boundaries
				continue;
			
			bool in_innerBoundary = false;
			int innerBoundary_indOffset = inputVertices.boundaryLoop.size();
			for(int j = 0; j < inputVertices.holeLoops.size(); ++j) {
				if(triangleLoop_adjacency(i, innerBoundary_indOffset, 
											innerBoundary_indOffset+inputVertices.holeLoops[j].size()) 
											== TriangleLoop_adjacency::Inside) {
					in_innerBoundary = true;
					break;
				}
				innerBoundary_indOffset += inputVertices.holeLoops[j].size();
			}
			if(in_innerBoundary)
				continue;

			add_triangle_to_indices(mTriangles[i], ind_triangles);
		}
	} else {
		for(int i = 0; i < mTriangles.size(); ++i) {
			if(!triangle_has_superTriangleVert(i))
				add_triangle_to_indices(mTriangles[i], ind_triangles);
		}
	}

	return TrigData2d { initial_vertices, ind_triangles };
}
std::vector<unsigned int> DelaunayTrig::triangulate_to_indices(Input const& inputVertices)
{
	return triangulate_to_data(inputVertices).triangles;
}

bool DelaunayTrig::point_in_triangle_impl(glm::dvec2 point, ConcreteTriangle triangle)
{
	for(int i = 0; i < 3; ++i) {
		if(cross_2d(triangle.vertices[(i+1)%3] - triangle.vertices[i], point - triangle.vertices[i]) < 0.0)
			return false;
	}
	return true;
}
bool DelaunayTrig::point_in_circumcircle_impl(glm::dvec2 point, ConcreteTriangle triangle)
{
	// https://stackoverflow.com/questions/39984709/how-can-i-check-wether-a-point-is-inside-the-circumcircle-of-3-points	
	glm::dvec2 PA = triangle.vertices[0] - point;
	glm::dvec2 PB = triangle.vertices[1] - point;
	glm::dvec2 PC = triangle.vertices[2] - point;

	return 	(	glm::length2(PA) * cross_2d(PB, PC) -
				glm::length2(PB) * cross_2d(PA, PC) +
				glm::length2(PC) * cross_2d(PA, PB)) > 0;
}

void DelaunayTrig::make_superTriangle()
{
	mSuperTriangle[0] = mVertices.size() - 3;
	mSuperTriangle[1] = mVertices.size() - 2;
	mSuperTriangle[2] = mVertices.size() - 1;

	// Huge triangle (considering all coordinates are normalized between [0, 1])
	mVertices[mSuperTriangle[0]] = glm::dvec2(-100, -100);
	mVertices[mSuperTriangle[1]] = glm::dvec2(100, -100);
	mVertices[mSuperTriangle[2]] = glm::dvec2(0, 100);

	mTriangles.push_back(Triangle(mSuperTriangle));
}
void DelaunayTrig::preprocess_vertices(std::vector<glm::dvec2> const& vertices)
{
	// Find the dimensions of the bounding window

	glm::dvec2 min_coord(std::numeric_limits<double>::max());
	glm::dvec2 max_coord(std::numeric_limits<double>::min());
	for(int i = 0; i < vertices.size(); ++i) {
		if(vertices[i].x < min_coord.x) {
			min_coord.x = vertices[i].x;
		} else if(vertices[i].x > max_coord.x) {
			max_coord.x = vertices[i].x;
		}

		if(vertices[i].y < min_coord.y) {
			min_coord.y = vertices[i].y;
		} else if(vertices[i].y > max_coord.y) {
			max_coord.y = vertices[i].y;
		}
	}
	glm::dvec2 dims = max_coord - min_coord;

	// Normalize and store every coordinate in range [0, 1]

	for(int i = 0; i < vertices.size(); ++i) {
		mVertices[i] = (vertices[i] - min_coord) / dims;
	}

	mWindowDimensions = dims;
	mMinCoord = min_coord;

	// Some kind of sort??
}
void DelaunayTrig::triangulate_impl()
{
	for(int i = 0; i < mVertices.size() - 3; ++i) {
		for(int j = 0; j < mTriangles.size(); ++j) {
			Triangle triangle = mTriangles[j];
			if(!point_in_triangle(i, j))
				continue;
			// Indices containing triangle which becomes a sub triangles
			// and of the other two (not yet created) sub triangles
			int t_0 = j;
			int t_1 = mTriangles.size()-1+1;
			int t_2 = mTriangles.size()-1+2;

			// Delete current triangle and add three new triangles and update adjacency
			mTriangles[t_0].vertices = { i, triangle.vertices[0], triangle.vertices[1] };
			mTriangles[t_0].adjacency[0] = triangle.adjacency[2]; // Outside othe containing triangle
			mTriangles[t_0].adjacency[1] = t_1;
			mTriangles[t_0].adjacency[2] = t_2;

			Triangle triangle_t1;
			triangle_t1.vertices = { i, triangle.vertices[1], triangle.vertices[2] };
			triangle_t1.adjacency[0] = triangle.adjacency[0];
			triangle_t1.adjacency[1] = t_2;
			triangle_t1.adjacency[2] = t_0;
			mTriangles.push_back(triangle_t1);
			adjust_adjacency(triangle.adjacency[0], t_0, t_1);
			
			Triangle triangle_t2;
			triangle_t2.vertices = { i, triangle.vertices[2], triangle.vertices[0] };
			triangle_t2.adjacency[0] = triangle.adjacency[1];
			triangle_t2.adjacency[1] = t_0;
			triangle_t2.adjacency[2] = t_1;
			mTriangles.push_back(triangle_t2);
			adjust_adjacency(triangle.adjacency[1], t_0, t_2);

			std::stack<int> new_triangles;
			new_triangles.push(t_0);
			new_triangles.push(t_1);
			new_triangles.push(t_2);
			restore_delaunayTriangulation(i, new_triangles);

			break;
		}
	}
}
void DelaunayTrig::build_vertToTriangleMap(std::vector<std::vector<TriangleVert_proxy>>& vert_to_triangle)
{
	vert_to_triangle.clear();
	vert_to_triangle.resize(mVertices.size());

	for(int j = 0; j < mTriangles.size(); ++j) {
		for(int k = 0; k < 3; ++k) {
			vert_to_triangle[mTriangles[j].vertices[k]].push_back(TriangleVert_proxy(j, k));
		}
	}
}
void DelaunayTrig::constrain_triangulation(Input const& inputVertices)
{
	int ind_offset = 0; 
	constrain_loop(ind_offset, ind_offset + inputVertices.boundaryLoop.size());
	ind_offset += inputVertices.boundaryLoop.size();

	for(int i = 0; i < inputVertices.holeLoops.size(); ++i) {
		constrain_loop(ind_offset, ind_offset + inputVertices.holeLoops[i].size());
		ind_offset += inputVertices.holeLoops[i].size();
	}
}
void DelaunayTrig::constrain_loop(int min_vert, int max_vert)
{
	std::vector<std::vector<TriangleVert_proxy>> vert_to_triangle; 	// The index in the outer vector acts as a key to access 
																	// the triangles adjacents to the vertex

	for(int i =  min_vert; i < max_vert; ++i) {
		build_vertToTriangleMap(vert_to_triangle);
		TriangleEdge constraining_edge(i, i+1);
		if(constraining_edge.vertB == max_vert)
			constraining_edge.vertB = min_vert;
		constrain_edge(constraining_edge, vert_to_triangle);
	}
}
void DelaunayTrig::constrain_edge(	TriangleEdge const& constraining_edge, 
											std::vector<std::vector<TriangleVert_proxy>> const& vert_to_triangle)
{
	std::list<TriangleEdge> intersecting_edges;
	std::vector<TriangleEdge> new_edges;
	find_all_intersecting(constraining_edge, vert_to_triangle, intersecting_edges);

	if(intersecting_edges.empty()) // Edge already in triangulation
		return;

	remove_intersectingEdges(intersecting_edges, new_edges, constraining_edge);

	restore_constrainedDelaunayTrig(new_edges, constraining_edge);
}
void DelaunayTrig::find_all_intersecting(	TriangleEdge const& edge, 
													std::vector<std::vector<TriangleVert_proxy>> const& vertToTriangle, 
													std::list<TriangleEdge>& intersecting_edges)
{
	int currentTriangle = -1;
	int currentIntersection = -1; // index (0, 1, 2) of the firstTriangle's vertex opposed to the edge crossed by the constraint-edge

	for(TriangleVert_proxy triangle_vert : vertToTriangle[edge.vertA]) {
		TriangleEdge currEdg = edge_from_triangleVert(triangle_vert.triangle, triangle_vert.vert_inner_ind);

		// The edge that is being constrained is already present
		if(	mTriangles[triangle_vert.triangle].vertices[(triangle_vert.vert_inner_ind + 1) % 3] == edge.vertB 
			|| mTriangles[triangle_vert.triangle].vertices[(triangle_vert.vert_inner_ind + 2) % 3] == edge.vertB) 
			return;

		if(edges_share_vertex(currEdg, edge))
			continue;

		if(is_edge_crossed_on_exit(triangle_vert.triangle, triangle_vert.vert_inner_ind, edge.vertB)) {
			currentTriangle = triangle_vert.triangle;
			currentIntersection = triangle_vert.vert_inner_ind;
			break;
		}
	}

	// March toward general line direction
	while(currentTriangle != -1 && currentIntersection != -1) {

		intersecting_edges.push_back(edge_from_triangleVert(currentTriangle, currentIntersection));
		currentTriangle = mTriangles[currentTriangle].adjacency[currentIntersection];

		// Find the intersecting edge
		currentIntersection = -1;
		for(int i = 0; i < 3; ++i) {
			if(mTriangles[currentTriangle].vertices[i] == edge.vertB) {
				currentIntersection = -1;
				break;
			}

			if(is_edge_crossed_on_exit(currentTriangle, i, edge.vertB)) {
				currentIntersection = i;
				break;
			}
		}
	}
}
void DelaunayTrig::remove_intersectingEdges(	std::list<TriangleEdge>& intersecting_edges, 
														std::vector<TriangleEdge>& newEdges, 
														TriangleEdge const& constraining_edge)
{
	using Crv::_2d::Line;

	while(!intersecting_edges.empty()) {
		TriangleEdge edg = intersecting_edges.front();
		intersecting_edges.pop_front();

		int triangle_1_ind;
		int triangle_2_ind;
		int vert_not_edge_1 = -1; // Vertex not part of the edge of the triangle 1

		std::pair<int, int> triangle_and_sharedEdge = triangle_with_edge(edg);
		triangle_1_ind = triangle_and_sharedEdge.first;
		vert_not_edge_1 = triangle_and_sharedEdge.second;
			
		triangle_2_ind = mTriangles[triangle_1_ind].adjacency[vert_not_edge_1];
		
		DiagonalSwap_out swap_out = swap_diagonal(triangle_1_ind, triangle_2_ind, vert_not_edge_1, true);
		if(!swap_out.success) {
			intersecting_edges.push_back(edg);
			continue;
		}

		if(edges_share_vertex(swap_out.new_edge, constraining_edge)) {
			newEdges.push_back(swap_out.new_edge);
			continue;
		}
		
		// Add the new edge to the list of intersecting edges if it does intersect
		if(Line(mVertices[swap_out.new_edge.vertA], mVertices[swap_out.new_edge.vertB]).intersection(
													Line(mVertices[constraining_edge.vertA], mVertices[constraining_edge.vertB]))) {
			intersecting_edges.push_back(swap_out.new_edge);

		} else {
			newEdges.push_back(swap_out.new_edge);
		}
	}
}
void DelaunayTrig::restore_constrainedDelaunayTrig(std::vector<TriangleEdge> const& newEdges, TriangleEdge const& constraining_edge)
{
	for(int i = 0; i < newEdges.size(); ++i) {
		if(newEdges[i] == constraining_edge)
			continue;

		std::pair<int, int> triangle_and_edge = triangle_with_edge(newEdges[i]);
		int adj_triangle_ind = mTriangles[triangle_and_edge.first].adjacency[triangle_and_edge.second];
		if(point_in_circumcircle(mTriangles[triangle_and_edge.first].vertices[triangle_and_edge.second], adj_triangle_ind))
			swap_diagonal(triangle_and_edge.first, adj_triangle_ind, triangle_and_edge.second);
	}
}
DelaunayTrig::TriangleEdge DelaunayTrig::edge_from_triangleVert(int triangle, int opposedVert)
{
	return TriangleEdge(mTriangles[triangle].vertices[(opposedVert+1)%3], mTriangles[triangle].vertices[(opposedVert+2)%3]);
}
bool DelaunayTrig::is_edge_crossed_on_exit(int triangle, int opposed_vert_edge, int edge_endPoint)
{
	if(	ccw(mVertices[edge_endPoint], 
					mVertices[mTriangles[triangle].vertices[opposed_vert_edge]],
					mVertices[mTriangles[triangle].vertices[(opposed_vert_edge + 1) % 3]]) &&
		ccw(mVertices[edge_endPoint], 
					mVertices[mTriangles[triangle].vertices[(opposed_vert_edge + 2) % 3]],
					mVertices[mTriangles[triangle].vertices[(opposed_vert_edge) % 3]])) { 
			// The edge being constrained intersects the examined triangle at it's edge opposing 
			// the vertex shared with the first vertex of the constrained edge
			return true;
		}
	return false;
}
std::pair<int, int> DelaunayTrig::triangle_with_edge(TriangleEdge const& edg)
{
	for(int i = 0; i < mTriangles.size(); ++i) {
		Triangle triangle = mTriangles[i];
		for(int j = 0; j < 3; ++j) {
			if(triangle.vertices[j] == edg.vertA && triangle.vertices[(j+1)%3] == edg.vertB)
				return { i, (j+2)%3 };
		}
	}
	return { -1, -1 };
}

void DelaunayTrig::adjust_adjacency(int triangle_to_adjust, int change, int to)
{
	if(triangle_to_adjust == -1)
		return;
	for(int k = 0; k < 3; ++k) {
		if(mTriangles[triangle_to_adjust].adjacency[k] == change) {
			mTriangles[triangle_to_adjust].adjacency[k] = to;
			break;
		}
	}

}
void DelaunayTrig::restore_delaunayTriangulation(int point, std::stack<int> adj_stack)
{
	while(!adj_stack.empty()) {
		int triangle = adj_stack.top();
		int adj = mTriangles[triangle].adjacency[0];
		adj_stack.pop();

		if(adj == -1 || !point_in_circumcircle(point, adj))
			continue;
		
		DiagonalSwap_out swapOut = swap_diagonal(triangle, adj, 0);
		
		// Update stack
		if(swapOut.new_adj[0] != -1)
			adj_stack.push(triangle);
		if(swapOut.new_adj[1] != -1)
			adj_stack.push(adj);
	}
}
DelaunayTrig::DiagonalSwap_out DelaunayTrig::swap_diagonal(	int triangle1_ind, int triangle2_ind, 
																				int new_origin_vertex_localInd, bool check_convex)
{
	DiagonalSwap_out out;

	Triangle triangle1 = mTriangles[triangle1_ind];
	Triangle triangle2 = mTriangles[triangle2_ind];

	int new_origin_vertex = triangle1.vertices[new_origin_vertex_localInd];
	int sharedEdge_vertA = triangle1.vertices[(new_origin_vertex_localInd+1)%3]; // Since origin vertex is at index 0, the shared edge is between vertices 1 and 2

	int opposite_to_origin_vert = -1;
	out.new_adj = { -1, -1 }; // New triangles that will be adjacent-oposite to new_origin_vertex
	for(int i = 0; i < 3; ++i) {
		if(triangle2.vertices[i] == sharedEdge_vertA) {
			opposite_to_origin_vert = triangle2.vertices[(i+1)%3];

			// The shared edge is in reversed order from one triangle to the other
			// E.g. A-B is B-A in the other triangle
			out.new_adj[0] = triangle2.adjacency[i];
			out.new_adj[1] = triangle2.adjacency[(i+2)%3];
			break;
		}
	}

	if(opposite_to_origin_vert == -1) {
		out.success = false;
		return out;
	}
		

	if(check_convex) {
		int sharedEdge_vertB = triangle1.vertices[(new_origin_vertex_localInd+2)%3];
		bool is_convex = 	ccw(	mVertices[sharedEdge_vertA], 
										mVertices[new_origin_vertex], 
										mVertices[opposite_to_origin_vert]) !=
							ccw(	mVertices[sharedEdge_vertB], 
										mVertices[new_origin_vertex], 
										mVertices[opposite_to_origin_vert]);

		if(!is_convex) {
			out.success = false;
			return out;
		}
	}

	out.new_edge = TriangleEdge(new_origin_vertex, opposite_to_origin_vert);

	// Swap diagonal
	mTriangles[triangle1_ind].vertices = { new_origin_vertex, opposite_to_origin_vert, triangle1.vertices[(new_origin_vertex_localInd+2)%3] };
	mTriangles[triangle2_ind].vertices = { new_origin_vertex, triangle1.vertices[(new_origin_vertex_localInd+1)%3], opposite_to_origin_vert };

	// Update adjacency
	mTriangles[triangle1_ind].adjacency = { out.new_adj[0], triangle1.adjacency[(new_origin_vertex_localInd+1)%3], triangle2_ind };
	mTriangles[triangle2_ind].adjacency = { out.new_adj[1], triangle1_ind, triangle1.adjacency[(new_origin_vertex_localInd+2)%3] };
	
	// Notify neighbours
	adjust_adjacency(triangle1.adjacency[(new_origin_vertex_localInd+2)%3], triangle1_ind, triangle2_ind);
	adjust_adjacency(out.new_adj[0], triangle2_ind, triangle1_ind);

	out.success = true;
	return out;
}
	
bool DelaunayTrig::point_in_triangle(int point_ind, int triangle_ind)
{
	return point_in_triangle_impl(mVertices[point_ind], to_concreteTriangle(triangle_ind));
}

// returns true if it is ON the circumcircle as well
bool DelaunayTrig::point_in_circumcircle(int point_ind, int triangle_ind)
{
	return point_in_circumcircle_impl(mVertices[point_ind], to_concreteTriangle(triangle_ind));
}
DelaunayTrig::ConcreteTriangle DelaunayTrig::to_concreteTriangle(int triangle_ind, bool normalized)
{
	ConcreteTriangle triangle;
	Triangle symbTriangle = mTriangles[triangle_ind];
	
	for(int i = 0; i < 3; ++i) {
		triangle.vertices[i] = mVertices[symbTriangle.vertices[i]];

		if(!normalized) {
			triangle.vertices[i] = triangle.vertices[i] * mWindowDimensions + mMinCoord;
		}
	}
	return triangle;
}
bool DelaunayTrig::triangle_has_superTriangleVert(int triangle_ind)
{
	Triangle triangle = mTriangles[triangle_ind];
	for(int i = 0; i < 3; ++i) {
		if(triangle.vertices[i] >= mVertices.size()-3) // The super triangle vertices are srotred at the end of the vertices vector
			return true;
	}
	return false;
}
DelaunayTrig::TriangleLoop_adjacency DelaunayTrig::triangleLoop_adjacency(int triangle_ind, int minLoop, int maxLoop) 
{
	for(int i = 0; i < 3; ++i) {
		int vert = mTriangles[triangle_ind].vertices[i];
		if(vert < minLoop || vert >= maxLoop)
			return TriangleLoop_adjacency::NotConnected;
	}
	int n_vertIndIncrease = 0;
	for(int i = 0; i < 3; ++i) {
		int vert = mTriangles[triangle_ind].vertices[i];
		int nextVert = mTriangles[triangle_ind].vertices[(i+1)%3];
		
		if(nextVert > vert)
			n_vertIndIncrease++;
	}
	return n_vertIndIncrease == 1 ? TriangleLoop_adjacency::Outside : TriangleLoop_adjacency::Inside; // If outside of the loop it would be 2
}
bool DelaunayTrig::edges_share_vertex(TriangleEdge const& edg1, TriangleEdge const& edg2)
{
	if(edg1.vertA == edg2.vertA || edg1.vertA == edg2.vertB ||
			edg1.vertB == edg2.vertA || edg1.vertB == edg2.vertB)
		return true;
	return false;
}

} // !Core
