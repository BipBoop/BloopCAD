
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef BLOOP_CORE_TRIANGULATION_HPP_
#define BLOOP_CORE_TRIANGULATION_HPP_

#include <Core/Surface/Nurbs.hpp>

#include <glm/glm.hpp>

#include <vector>

namespace Core {


template<typename Vert_t>
struct TrigData {
	std::vector<Vert_t> verts;
	// Groups of three indices over vert vector are triangles
	std::vector<unsigned int> triangles;
};

struct VertNorm {
	glm::vec3 vert;
	glm::vec3 norm;
};

using TrigData2d = TrigData<glm::dvec2>;
using TrigDataRaw = TrigData<glm::vec3>;
using TrigDataNormal = TrigData<VertNorm>;

TrigDataRaw to_3d(TrigData2d const& data, Srf::NurbsSurface const& surf);
TrigDataNormal to_3dNormal(TrigData2d const& data, Srf::NurbsSurface const& surf);

} // !Core

#endif