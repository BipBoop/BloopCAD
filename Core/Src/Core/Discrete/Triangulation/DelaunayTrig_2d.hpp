
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef BLOOP_CORE_DELAUNAYTRIG_2D_HPP_
#define BLOOP_CORE_DELAUNAYTRIG_2D_HPP_

#include "Triangulation.hpp"

#include <glm/glm.hpp>

#include <vector>
#include <array>
#include <stack>
#include <list>

namespace Core {

class DelaunayTrig {
public:
	// Input of the triangulation algorithms,
	// loops are expected to be in ccw fashion
	struct Input {
		std::vector<glm::dvec2> pointCloud; // Points to include in the triangulation

		// If not empty (and more than 3 entries), constrained triangulation is used to limit the triangulation to fit within this boundary
		std::vector<glm::dvec2> boundaryLoop; 
		// If not empty, each hole loop is used to constrain the triangulation
		std::vector<std::vector<glm::dvec2>> holeLoops;

		std::vector<glm::dvec2> unwrap() const;
	};

	struct Triangle {
		std::array<int, 3> vertices { -1, -1, -1 };	// Indices of the vertices of the triangle
													// They are ordered in a counter clockwise fashion


		std::array<int, 3> adjacency { -1, -1, -1}; 	// Indices of the triangle of the adjacent triangles
														// They are ordered as the adjacent of the side oposite to 
														// the vertex in vertices at the same internal index
														// -1 means it has no adjacency, on the boundary

		Triangle() = default;
		Triangle(std::array<int, 3> const& vertices_);
		Triangle(Triangle const& other);
		Triangle(Triangle&& other);
		Triangle& operator=(Triangle const& other);
		Triangle& operator=(Triangle&& other);
		
	};
	struct ConcreteTriangle {
		std::array<glm::dvec2, 3> vertices;

		ConcreteTriangle() = default;
		ConcreteTriangle(std::array<glm::dvec2, 3> vertices_);
		ConcreteTriangle(glm::dvec2 const& vertA, glm::dvec2 const& vertB, glm::dvec2 const& vertC);
	};
	struct TriangleEdge {
		int vertA { -1 }, vertB { -1 };

		TriangleEdge(int vertA_, int vertB_);
		TriangleEdge() = default;
		bool operator==(TriangleEdge const& other) const;
	};
	struct TriangleVert_proxy {
		int triangle;
		int vert_inner_ind;

		TriangleVert_proxy(int triangle_, int vert_inner_ind_);
	};
	struct DiagonalSwap_out {
		bool success { false };
		std::array<int, 2> new_adj { -1, -1 };
		TriangleEdge new_edge;
	};
	struct Loop {
		std::vector<glm::dvec2> const& vertices;
	};

	enum class TriangleLoop_adjacency { Outside, Inside, NotConnected };

private:
	std::vector<glm::dvec2> mVertices; // Vertices to triangulate + 3 vertices of the super triangle

	std::vector<Triangle> mTriangles; 	// Each triangle has 3 indices representing vertices in mVertices
										// of an adjacent triangle in mTriangles or -1 if the edge is on a boundary

	std::array<int, 3> mSuperTriangle; // Last 3 vertices of mVertices which define a very big triangle

	glm::dvec2 mWindowDimensions, mMinCoord;

public:
	// std::vector<ConcreteTriangle> triangulate(Input const& inputVertices);
	std::vector<unsigned int> triangulate_to_indices(Input const& inputVertices);
	TrigData2d triangulate_to_data(Input const& inputVertices);

	static bool point_in_triangle_impl(glm::dvec2 point, ConcreteTriangle triangle);
	static bool point_in_circumcircle_impl(glm::dvec2 point, ConcreteTriangle triangle);

private:


	void make_superTriangle();
	void preprocess_vertices(std::vector<glm::dvec2> const& vertices);
	void triangulate_impl();
	void build_vertToTriangleMap(std::vector<std::vector<TriangleVert_proxy>>& vert_to_triangle);
	void constrain_triangulation(Input const& inputVertices);
	void constrain_loop(int min_vert, int max_vert);
	void constrain_edge(TriangleEdge const& constraining_edge, std::vector<std::vector<TriangleVert_proxy>> const& vert_to_triangle);
	void find_all_intersecting(	TriangleEdge const& edge, std::vector<std::vector<TriangleVert_proxy>> const& vertToTriangle, 
								std::list<TriangleEdge>& intersecting_edges);
	void remove_intersectingEdges(	std::list<TriangleEdge>& intersecting_edges, std::vector<TriangleEdge>& newEdges, 
									TriangleEdge const& constraining_edge);
	void restore_constrainedDelaunayTrig(	std::vector<TriangleEdge> const& newEdges, 
													TriangleEdge const& constraining_edge);
	TriangleEdge edge_from_triangleVert(int triangle, int opposedVert);
	bool is_edge_crossed_on_exit(int triangle, int opposed_vert_edge, int edge_endPoint);
	std::pair<int, int> triangle_with_edge(TriangleEdge const& edg);

	void adjust_adjacency(int triangle_to_adjust, int change, int to);
	void restore_delaunayTriangulation(int point, std::stack<int> adj_stack);
	DiagonalSwap_out swap_diagonal(	int triangle1_ind, int triangle2_ind, 
									int new_origin_vertex_localInd, bool check_convex = false);
	
	bool point_in_triangle(int point_ind, int triangle_ind);

	// returns true if it is ON the circumcircle as well
	bool point_in_circumcircle(int point_ind, int triangle_ind);
	ConcreteTriangle to_concreteTriangle(int triangle_ind, bool normalized = true);
	bool triangle_has_superTriangleVert(int triangle_ind);
	TriangleLoop_adjacency triangleLoop_adjacency(int triangle_ind, int minLoop, int maxLoop);
	bool edges_share_vertex(TriangleEdge const& edg1, TriangleEdge const& edg2);
};

} // !Core

#endif