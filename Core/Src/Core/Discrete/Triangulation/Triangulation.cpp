
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "Triangulation.hpp"

namespace Core {

TrigDataRaw to_3d(TrigData2d const& data, Srf::NurbsSurface const& surf)
{
	TrigDataRaw out;
	out.triangles = data.triangles;
	out.verts.reserve(data.verts.size());
	for(int i = 0; i < data.verts.size(); ++i) {
		out.verts.push_back(surf.at(data.verts[i]));
	}
	return out;    
}
TrigDataNormal to_3dNormal(TrigData2d const& data, Srf::NurbsSurface const& surf)
{
	TrigDataNormal out;
	out.triangles = data.triangles;
	out.verts.reserve(data.verts.size());
	for(int i = 0; i < data.verts.size(); ++i) {
		out.verts.push_back({ .vert=surf.at(data.verts[i]), .norm=surf.normal(data.verts[i]) });
	}
	return out;  
}

} // !Core