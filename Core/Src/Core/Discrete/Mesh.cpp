
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "Mesh.hpp"

namespace Core {

Mesh::Mesh(Kind kind_, std::vector<float> data_, std::vector<unsigned int> indices_)
  : kind(kind_)
  , data(data_)
  , indices(indices_)
{

}
Mesh Mesh::from_trigdata(TrigDataNormal trigdata)
{
  Mesh out(Kind::TrianglesNorm, {}, trigdata.triangles);
  out.data.reserve(trigdata.verts.size()*6);
  for(auto vert : trigdata.verts) {
    out.add_pointNorm(vert.vert, vert.norm);
  }
  return out;
}
unsigned int Mesh::add_pointNorm(glm::dvec3 point, glm::dvec3 norm)
{
  int start_ind = data.size()/6;
  data.push_back(point.x);
  data.push_back(point.y);
  data.push_back(point.z);

  data.push_back(norm.x);
  data.push_back(norm.y);
  data.push_back(norm.z);

  return start_ind;
}
void Mesh::add_triangle(unsigned int a, unsigned int b, unsigned int c)
{
  indices.push_back(a);
  indices.push_back(b);
  indices.push_back(c);
}
bool Mesh::empty() const
{
  return data.empty();
}
std::vector<Mesh::PointNorm_t> Mesh::get_pointNorm() const
{
  int stride_ = 6;

  std::vector<PointNorm_t> out;
  out.reserve(data.size() / stride_);
  for(int i = 0; i < data.size(); i += stride_) {
    out.push_back(PointNorm_t{.point=glm::vec3(data[i], data[i+1], data[i+2]), 
                              .norm=glm::vec3(data[i+3], data[i+4], data[i+5])});
  }
  return out;
}
std::vector<Mesh::Point_t> Mesh::get_points() const
{
  int stride_ = 3;

  std::vector<Point_t> out;
  out.reserve(data.size() / stride_);
  for(int i = 0; i < data.size(); i += stride_) {
    out.push_back(glm::vec3(data[i], data[i+1], data[i+2]));
  }
  return out;
}
std::vector<std::array<Mesh::Point_t, 3>> Mesh::get_triangles() const
{
  if(indices.size() % 3 != 0) {
    return {};
  }
  std::vector<std::array<Point_t, 3>> out;
  size_t stride = 6;

  for(size_t i = 0; i < indices.size(); i+=3) {
    Point_t pt1, pt2, pt3;
    pt1.x = data.at(indices[i]*stride+0);
    pt1.y = data.at(indices[i]*stride+1);
    pt1.z = data.at(indices[i]*stride+2);

    pt2.x = data.at(indices[i+1]*stride+0);
    pt2.y = data.at(indices[i+1]*stride+1);
    pt2.z = data.at(indices[i+1]*stride+2);

    pt3.x = data.at(indices[i+2]*stride+0);
    pt3.y = data.at(indices[i+2]*stride+1);
    pt3.z = data.at(indices[i+2]*stride+2);

    out.push_back({pt1, pt2, pt3});
  }
  return out;
}

} // !Core  