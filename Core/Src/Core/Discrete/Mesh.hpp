
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef BLOOP_CORE_MESH_HPP_
#define BLOOP_CORE_MESH_HPP_

#include <Core/Curve/Line.hpp>
#include <Core/Curve/Circle.hpp>
#include "Triangulation/Triangulation.hpp"

#include <Eigen/Core>

#include <vector>


namespace Core {

using LOD = double;

struct Mesh {
  using Point_t = glm::vec3;
  using Point2d_t = glm::vec2;
  struct PointNorm_t {
    Point_t point;
    Point_t norm;
  };
  struct PointTexture_t {
    Point_t point;
    Point2d_t textDim;
  };
  struct OffsetPointTexture_t {
    PointTexture_t pointTexture;
    Point2d_t offset;
  };
  struct AnnotLine_t {
    Point_t pt0;
    float offset;
  };
  
  
  enum class Kind { Point,
                    PointTexture,
                    Curve,
                    TrianglesNorm,
                    OffsetPointTexture,
                    AnnotLine,
                    None
                  };
  enum class TextureKind { None, Text, Texture };
  
  Kind kind;
  TextureKind textureKind;
  std::string texture { "" };
  size_t tag { 0 };

  // Eigen::MatrixXf data;
  std::vector<float> data;
  std::vector<unsigned int> indices;

  Mesh() = default;
  Mesh(Kind kind_, std::vector<float> data_, std::vector<unsigned int> indices_);

  static Mesh from_trigdata(TrigDataNormal trigdata);

  unsigned int add_pointNorm(glm::dvec3 point, glm::dvec3 norm);

  void add_triangle(unsigned int a, unsigned int b, unsigned int c);

  bool empty() const;
  std::vector<PointNorm_t> get_pointNorm() const;
  std::vector<Point_t> get_points() const;
  std::vector<std::array<Point_t, 3>> get_triangles() const;
};


} // !Core

#endif