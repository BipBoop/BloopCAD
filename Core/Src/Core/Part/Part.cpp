
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "Part.hpp"

#include <Core/Curve/Curve_2d.hpp>

#include <Core/Print/IDOstream.hpp>

#include <glm/gtx/string_cast.hpp>

namespace Core {
namespace Prt {

Part::Part(WorkpieceId self_id_)
  : origin(glm::dvec3(0.0), glm::dvec3(1.0, 0.0, 0.0), glm::dvec3(0.0, 1.0, 0.0), glm::dvec3(0.0, 0.0, 1.0))
  , self_id(self_id_)
{
  // test_surf = Srf::Surface(Srf::NurbsSurface::make_sweep(
  //                           origin.planes[0].support.embed_curve(*Crv::_2d::to_nurbs(Crv::_2d::Circle{.center={0.5, 0.5}, .radius=0.4}).split(0.5, true).first),
  //                           Crv::_3d::to_nurbs(Crv::_3d::Line(glm::dvec3(0.0), glm::dvec3(0.0, 0.0, 1.0)))));
}
Part::Part()
  : Part(WorkpieceId{})
{

}
// Part Part::build(immer::vector<BuildAction> buildActions_, WorkpieceId id)
// {
//   Part prt(id);
//   for(size_t i = 0; i < buildActions_.size(); ++i) {
//     prt = std::move(update(prt, buildActions_[i]));
//   }
//   return prt;
// }

IdVec Part::children() const
{
  // std::cout<<"Part ask for children\n";
  IdVec out;
  
  out.push_back({ .wpid=self_id, .entId=SibId{.kind=Kind::Origin_point, .index=0} });
  out.push_back({ .wpid=self_id, .entId=SibId{.kind=Kind::Origin_plane, .index=0} });
  out.push_back({ .wpid=self_id, .entId=SibId{.kind=Kind::Origin_plane, .index=1} });
  out.push_back({ .wpid=self_id, .entId=SibId{.kind=Kind::Origin_plane, .index=2} });

  for(size_t i = 0; i < surfaces.size(); ++i) {
    out.push_back({ .wpid=self_id, .entId=SibId{.kind=Kind::Surface, .index=surfaces[i].first.index} });
  }
  
  return out;
}
std::vector<Mesh> Part::meshes(SibId childId) const
{
  switch(childId.kind) {
  case Kind::Origin_point:
    return { origin.point_mesh() };
  case Kind::Origin_plane:
    return { origin.plane_mesh(childId.index) };
  case Kind::Surface: {
    auto surf = *condensed_get(childId.index, surfaces);
    return { surf.to_mesh() };
  }
  }

  return {};
}

std::string Part::name_of(SibId id) const
{
  switch(id.kind) {
  case Kind::Origin_point:
    return "Origin";
  case Kind::Origin_plane: {
    if(id.index == 0) {
      return "Plane_xy";
    } else if(id.index == 1) {
      return "Plane_yz";
    } else if(id.index == 2) {
      return "Plane_zx";
    }
  }
  }
  return "No matched entity";
}
std::optional<Id> Part::find(std::string name, Kind::inner hint) const
{
  std::optional<SibId> maybe_sib = find_sib(name, hint);

  if(maybe_sib) {
    return Id{.wpid=self_id, .entId=*maybe_sib};
  }
  return std::nullopt;
}
std::optional<Id> Part::find(std::string name) const
{
  std::optional<SibId> maybe_sib = find_sib(name);

  if(maybe_sib) {
    return Id{.wpid=self_id, .entId=*maybe_sib};
  }
  return std::nullopt;
}
std::optional<SibId> Part::find_sib(std::string name, Kind::inner hint) const
{
  switch(hint) {
  case Kind::Origin_point:
    if(name == "Origin") {
      return SibId{.kind=Kind::Origin_point, .index=0};
    }
    break;
  case Kind::Origin_plane:
    if(name == "Plane_xy") {
      return SibId{.kind=Kind::Origin_plane, .index=0};
    } else if(name == "Plane_yz") {
      return SibId{.kind=Kind::Origin_plane, .index=1};
    } else if(name == "Plane_zx") {
      return SibId{.kind=Kind::Origin_plane, .index=2};
    }
  default:
    break;
  }
  return std::nullopt;
}
std::optional<SibId> Part::find_sib(std::string name) const
{
  std::optional<SibId> out;

  out = find_sib(name, Kind::Origin_point);
  if(out) { return out; }

  out = find_sib(name, Kind::Origin_plane);
  if(out) { return out; }

  std::cerr<<"Could not resolve name "<<name<<std::endl;
  return std::nullopt;
}


Wp::VecsSnapshot Part::idsSnapshot() const
{
  using namespace Wp;
  VecsSnapshot snapshot;
  snapshot.push_back(Kind_indexVec{.kind=Kind::Origin_point,   .indexVec={VersionedIndex{.index=0}}});
  snapshot.push_back(Kind_indexVec{.kind=Kind::Origin_plane,   .indexVec={VersionedIndex{.index=0},
                                                                          VersionedIndex{.index=1},
                                                                          VersionedIndex{.index=2}}});

  snapshot.push_back(Kind_indexVec{.kind=Kind::Surface, .indexVec=strip_inner(surfaces)});

  return snapshot;
}

template<>
std::optional<Srf::ParamFlat> Part::get(SibId sid) const
{
  if(sid.kind == Kind::Origin_plane) {
    return origin.planes[sid.index].support.as_flat();
  } else if(sid.kind == Kind::Surface) {
    auto surf = *condensed_get(sid.index, surfaces);

    if(!surf->is_flat(Srf::NurbsSurface::Direction::Both)) {
      return std::nullopt;
    }
    return surf->as_flat();
  }
  std::cerr<<"Id "<<sid<<" invalid for cast into plane"<<std::endl;
  return std::nullopt;
}

} // !Prt
} // !Core