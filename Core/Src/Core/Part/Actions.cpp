
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "Actions.hpp"

#include <Core/Utils/Visitor.hpp>

namespace Core {
namespace Prt {

Action set_attach(WorkpieceId wpid, Action act)
{
  if(std::holds_alternative<BuildAction>(act)) {
    BuildAction baction = std::get<BuildAction>(act);
    if(std::holds_alternative<AttachSketch>(baction)) {
      AttachSketch attAct = std::get<AttachSketch>(baction);
      attAct.sk_id = wpid;
      return attAct;
    }
  }

  return act;
}
BuildAction name_action(BuildAction bact, Part const& currentpart, std::function<std::optional<std::pair<Sk::Sketch, glm::dmat4>>(WorkpieceId)> sketch_access)
{
  bact = std::visit(Visitor{
    [&](AttachSketch act) -> BuildAction {
      act.plane_id.name = currentpart.name_of(*act.plane_id);
      return act;
    },
    [&](Extrude ext) -> BuildAction {
      auto sk = sketch_access(ext.sk_id);
      if(!sk) {
        return ext;
      }
      for(size_t i = 0; i < ext.loopdiloops.size(); ++i) {
        ext.loopdiloops[i].name = sk->first.name_of(ext.loopdiloops[i]);
      }
      return ext;
    },
    [&](auto& act) -> BuildAction { return act; }
  }, bact);

  return bact;
}
std::pair<BuildAction, bool> id_action(BuildAction act, Part const& currentpart, std::function<std::optional<std::pair<Sk::Sketch, glm::dmat4>>(WorkpieceId)> sketch_access)
{
  bool success = true;
  act = std::visit(Visitor {
    [&](AttachSketch act) -> BuildAction {
      auto maybe_id = currentpart.find_sib(act.plane_id.name);
      if(!maybe_id) {
        success = false;
        return act;
      }
      *act.plane_id = *maybe_id;
      return act;
    },
    [&](Extrude ext) -> BuildAction {
      auto sk = sketch_access(ext.sk_id);
      if(!sk) {
        success = false;
        return ext;
      }
      for(size_t i = 0; i < ext.loopdiloops.size(); ++i) {
        auto maybe_id = sk->first.find_sib(ext.loopdiloops[i].name);
        if(maybe_id) {
          *ext.loopdiloops[i] = *maybe_id;
        }
      }
      return ext;
    },
    [&](auto& act) -> BuildAction { return act; }
  }, act);
  return { act, success };
}
} // !Prt
} // !Core