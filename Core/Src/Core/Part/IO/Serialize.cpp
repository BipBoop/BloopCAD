
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "Serialize.hpp"

#include <Core/Serialize/Serialize_glm.hpp>
#include <Core/Utils/Paths.hpp>

namespace Core {

using namespace Prt;

YAML::Node encode(BuildAction const& buildAction, WorkpieceId prt_id, 
                  std::map<WorkpieceId, std::string> const& wpids_to_locanames)
{
  std::string prt_locaname = wpids_to_locanames.at(prt_id);
  return std::visit(Visitor{
    [&](AttachSketch act) {
      YAML::Node attach_node;
      attach_node["kind"] = "attach_sketch";
      auto found_locaname = wpids_to_locanames.find(act.sk_id);
      if(found_locaname == wpids_to_locanames.end()) {
        std::cerr<<"Could not find sketch #"<<act.sk_id.index<<std::endl;
        return YAML::Node();
      }
      attach_node["sk"] = relative_locaname(prt_locaname, found_locaname->second);
      attach_node["to"] = act.plane_id.name;
      attach_node["mode"] = static_cast<int>(act.mode);

      return attach_node;
    },
    [&](Extrude ext) {
      YAML::Node ext_node;
      ext_node["kind"] = "extrude";
      for(size_t i = 0; i < ext.loopdiloops.size(); ++i) {
        ext_node["loops"].push_back(ext.loopdiloops[i].name);
      }
      ext_node["amount"] = ext.amount;
      ext_node["flip"] = ext.flipped;
      ext_node["expr"] = ext.amount_expr;

      auto found_locaname = wpids_to_locanames.find(ext.sk_id);
      if(found_locaname == wpids_to_locanames.end()) {
        std::cerr<<"Could not find sketch #"<<ext.sk_id.index<<std::endl;
        return YAML::Node();
      }
      ext_node["sk"] = relative_locaname(prt_locaname, found_locaname->second);

      return ext_node;
    },
    [&](auto not_handled) {
      std::cerr<<"Encode build action index="<<buildAction.index()<<" not handled"<<std::endl;
      return YAML::Node();
    }
  }, buildAction);
}
YAML::Node encode(Factory const& prt_factory, std::map<WorkpieceId, std::string> const& wpids_to_locanames)
{
  // std::cout<<"Encode prt factory with "<<prt_factory.buildActions.size()<<" steps\n";
  YAML::Node node;

  node["kind"] = "part";
  for(size_t i = 0; i < prt_factory.buildActions.size(); ++i) {
    node["steps"].push_back(encode(prt_factory.buildActions[i], prt_factory.wpid, wpids_to_locanames));
  }

  return node;
}

std::optional<BuildAction> decode(YAML::Node const& node, std::string filename,
                                  std::unordered_map<std::string, WorkpieceId> const& locaname_to_wpids)
{
  auto kind = node["kind"];
  if(!kind) {
    std::cerr<<"Could not decode buildstep: no kind"<<std::endl;
    return std::nullopt;
  }
  if(kind.as<std::string>() == "attach_sketch") {
    std::string locaname = absolute_locaname(filename, node["sk"].as<std::string>());
    auto found_sk = locaname_to_wpids.find(locaname);
    if(found_sk == locaname_to_wpids.end()) {
      std::cerr<<"Could not find sketch \""<<locaname<<"\""<<std::endl;
      return std::nullopt;
    }
    Named<SibId> planeId(node["to"].as<std::string>());

    return AttachSketch{  .sk_id=found_sk->second, 
                          .plane_id=planeId, 
                          .mode=static_cast<AttachSketch::Mode>(node["mode"].as<int>())};
  } else if(kind.as<std::string>() == "extrude") {
    // Find the base sketch
    std::string locaname = absolute_locaname(filename, node["sk"].as<std::string>());
    auto found_sk = locaname_to_wpids.find(locaname);
    if(found_sk == locaname_to_wpids.end()) {
      std::cerr<<"Could not find sketch \""<<locaname<<"\""<<std::endl;
      return std::nullopt;
    }
    
    // Find the extruded loops
    auto str_loops = node["loops"].as<std::vector<std::string>>();
    std::vector<Named<SibId>> named_loops {};
    for(size_t i = 0; i < str_loops.size(); ++i) {
      named_loops.push_back(str_loops[i]);
    }

    return Extrude {
              .sk_id=found_sk->second,
              .loopdiloops=named_loops,
              .amount=node["amount"].as<double>(),
              .flipped=node["flip"].as<bool>(),
              .amount_expr=node["expr"].as<std::string>(),
      };
  }

  return std::nullopt;
}
bool decode(YAML::Node const& node, Factory& outfact, WorkpieceId prt_id, std::string filename, 
                              std::unordered_map<std::string, WorkpieceId> const& locanames_to_wpids)
{
  // std::cout<<"Part decode node:\n"<<node<<"\n";
  auto steps = node["steps"];

  std::vector<BuildAction> buildActions;
  for(auto step : steps) {
    std::optional<BuildAction> act = decode(step, filename, locanames_to_wpids);
    if(act) {
      buildActions.push_back(*act);
    }
  }

  outfact = Factory(buildActions, prt_id, 0);
  return true;
}

}  // !Core
