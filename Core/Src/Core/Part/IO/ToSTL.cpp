
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "ToSTL.hpp"

#include <sstream>

namespace Core {
namespace Prt {

std::string to_stl(Part const& part)
{
  std::stringstream stream;
  stream<<"solid bloopsolid\n";

  for(auto surface : part.surfaces) {
    auto mesh = surface.second->to_mesh();

    auto triangles = mesh.get_triangles();
    for(auto triangle : triangles) {
      glm::dvec3 normal = normal_ccw(triangle);
      stream<<"facet normal "<<normal.x<<" "<<normal.y<<" "<<normal.z<<"\n";
      stream<<"\touter loop\n";
      stream<<"\t\tvertex "<<triangle[0].x<<" "<<triangle[0].y<<" "<<triangle[0].z<<"\n";
      stream<<"\t\tvertex "<<triangle[1].x<<" "<<triangle[1].y<<" "<<triangle[1].z<<"\n";
      stream<<"\t\tvertex "<<triangle[2].x<<" "<<triangle[2].y<<" "<<triangle[2].z<<"\n";
      stream<<"\tendloop\n";
      stream<<"endfacet\n";
    }
  }

  stream<<"endsolid\n";
  return stream.str();
}

} // !Prt
} // !Core