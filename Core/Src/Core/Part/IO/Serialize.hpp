
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef BLOOP_CORE_PRT_IO_SERIALIZE_HPP_
#define BLOOP_CORE_PRT_IO_SERIALIZE_HPP_

#include <Core/Part/Factory.hpp>

#include <yaml-cpp/yaml.h>

#include <string>
#include <optional>

namespace Core {

YAML::Node encode(Prt::Factory const& prt_factory, std::map<WorkpieceId, std::string> const& wpids_to_locanames);

bool decode(YAML::Node const& node, Prt::Factory& outfact, WorkpieceId prt_id, std::string filename, 
            std::unordered_map<std::string, WorkpieceId> const& locanames_to_wpids);

}  // !Core

#endif