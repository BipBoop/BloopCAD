
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef BLOOP_CORE_PRTACTION_HPP_
#define BLOOP_CORE_PRTACTION_HPP_

#include "Part.hpp"
#include <Core/Sketch/Sketch.hpp>
#include <Core/Id.hpp>

#include <variant>

namespace Core {

namespace Prt {
// struct AddOriginGeometry {
//   Named<SibId> which;
// };
struct AttachSketch {
  WorkpieceId sk_id;

  // Named Ids can have only and Id, only a name or both
  // and the factory will attempt to resolve the entity
  Named<SibId> plane_id;

  // Axis can be straight or inverted relative to the plane's axis
  enum Mode { xStr_yStr, xStr_yInv, xInv_yStr, xInv_yInv };
  Mode mode { xStr_yStr };
};
struct Extrude {
  WorkpieceId sk_id; 

  // This assumes that all loopdiloops originate from the same sketch
  std::vector<Named<SibId>> loopdiloops; // Which loops will make the profile to extrude
  double amount; // How much in base units to extrude
  bool flipped; // not flipped would be normal to the plane of the loops

  std::string amount_expr; // Mathematical expression describing the amount, not used in core, but must be saved in file..
  // something about additive-substractive-intersection
};
struct Revolve {

};

using BuildAction = std::variant<AttachSketch, Extrude, Revolve>;

// Sets a build action already added 
struct SetBuildAction {
  size_t action_index;
  BuildAction new_buildAction;
};

using Action = std::variant<BuildAction, SetBuildAction>;

Action set_attach(WorkpieceId wpid, Action act);
BuildAction name_action(BuildAction act, Part const& currentpart, std::function<std::optional<std::pair<Sk::Sketch, glm::dmat4>>(WorkpieceId)> sketch_access);
std::pair<BuildAction, bool> id_action(BuildAction act, Part const& currentpart, std::function<std::optional<std::pair<Sk::Sketch, glm::dmat4>>(WorkpieceId)> sketch_access);

} // !Prt
} // !Core

#endif