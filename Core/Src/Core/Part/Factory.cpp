
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "Factory.hpp"

#include <Core/Solid/Solid.hpp>
#include <Core/Print/IDOstream.hpp>

namespace Core {
namespace Prt {

Factory::Factory(WorkpieceId wpid_)
  : wpid(wpid_)
{

}
Factory::Factory(std::vector<BuildAction> buildActions_, WorkpieceId wpid_, size_t version_)
  : wpid(wpid_)
  , version(version_)
{
  for(auto baction : buildActions_) {
    buildActions = std::move(buildActions.push_back(baction));
  }
}
Factory::Factory(immer::vector<BuildAction> buildActions_, WorkpieceId wpid_, size_t version_)
  : wpid(wpid_)
  , version(version_)
  , buildActions(std::move(buildActions_))
{

}

std::pair<Part, Factory> Factory::build(Part current, size_t step, 
    std::function<std::optional<std::pair<Sk::Sketch, glm::dmat4>>(WorkpieceId)> sketch_access, bool& success) const
{
  // Actualy build a part T_T
  std::cout<<"Build part "<<wpid<<" ["<<step<<"]\n";
  
  auto [act, idsuccess] = id_action(buildActions[step], current, sketch_access);

  if(!idsuccess) {
    std::cerr<<"Could not id action #"<<step<<std::endl;
    success = false;
    return { current, *this };
  }
  current.version = version+1;
  std::visit(Visitor {
    [&](Extrude ext) {
      if(ext.loopdiloops.empty())
        return;
      auto sketch = sketch_access(ext.sk_id);
      if(!sketch) {
        std::cerr<<"Could not find sketch "<<ext.sk_id<<" to build extrusion"<<std::endl;
      }
      glm::dvec3 offset(sketch->second * glm::dvec4(0.0, 0.0, ext.amount * (ext.flipped ? -1.0 : 1.0), 1.0));
      Solid solid = Solid::make_extrusion(*sketch->first.get<Srf::Surface>(ext.loopdiloops[0]), offset);
      for(size_t i = 0; i < solid.faces.size(); ++i) {
        current.surfaces.push_back(std::make_pair(VersionedIndex{.index=i, .version=current.version}, 
          solid.faces[i].surface));
      }
    },
    [&](auto act) {

    }
  }, act);

  return { current, Factory(buildActions.set(step, act), wpid, current.version) };
}
glm::dmat4 Factory::transform_for_step(size_t step, Prt::Part const& currpart) const
{
  // std::cout<<"N buildactions: "<<buildActions.size()<<",  want to access "<<step<<"\n";
  BuildAction act = buildActions[step];

  if(std::holds_alternative<AttachSketch>(act)) {
    return attachSketch_transform(std::get<AttachSketch>(act), currpart);
  }
  std::cerr<<"No transform for prt action at "<<step<<std::endl;
  return glm::dmat4(1.0);
}

glm::dmat4 Factory::attachSketch_transform(AttachSketch act, Prt::Part const& currpart) const
{
  auto surf = currpart.get<Srf::ParamFlat>(act.plane_id);
  if(!surf) {
    std::cerr<<"Cannot attach sketch to part, surface not found"<<std::endl;
    return glm::dmat4(1.0);
  }

  glm::dmat3 rot(0);
  switch(act.mode) {
  case AttachSketch::Mode::xStr_yStr: {
    glm::dvec3 z = glm::cross(surf->x_, surf->y_);

    // Access with operators [][] is column major..
    rot[0][0] = surf->x_.x;
    rot[0][1] = surf->x_.y;
    rot[0][2] = surf->x_.z;

    rot[1][0] = surf->y_.x;
    rot[1][1] = surf->y_.y;
    rot[1][2] = surf->y_.z;

    rot[2][0] = z.x;
    rot[2][1] = z.y;
    rot[2][2] = z.z;
  }
  default:
    break;
  }

  return glm::translate(glm::dmat4(rot), surf->origin);
}

Factory next(Factory current, Action action, Part const& currpart, std::function<std::optional<std::pair<Sk::Sketch, glm::dmat4>>(WorkpieceId)> sketch_access)
{
  // For now the heavy lifting is done when we try to build the
  // part and the next realy only pushes an action on the stack
  // for the build function to resolve

  // Eventualy a mecanism to reduce the lost work on build 
  // (keep track of current state&modification to not rebuild
  // everything everytime)
  current.mod_struct = -1;
  if(std::holds_alternative<SetBuildAction>(action)) {
    SetBuildAction set = std::get<SetBuildAction>(action);

    if(set.new_buildAction.index() != current.buildActions[set.action_index].index()) {
      std::cerr<<"Build action set mismatch"<<std::endl;
      return current;
    }
    if(std::holds_alternative<Extrude>(set.new_buildAction)) {
      Extrude old_extrude = std::get<Extrude>(current.buildActions[set.action_index]);
      Extrude new_extrude = std::get<Extrude>(set.new_buildAction);
      if(old_extrude.sk_id != new_extrude.sk_id) {
        current.mod_struct = set.action_index; // Extrusion structuraly modifies the part
      }
    }
    current.buildActions = current.buildActions.set(set.action_index, name_action(set.new_buildAction, currpart, sketch_access));
  } else {
    current.buildActions = current.buildActions.push_back(name_action(std::get<BuildAction>(action), currpart, sketch_access));
    current.mod_struct = current.buildActions.size()-1; // Adding an action structuraly modifies the part
  }
  current.version = current.version+1;
  return current;
}

} // !Prt
} // !Core