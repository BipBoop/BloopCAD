
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef BLOOP_CORE_PART_HPP_
#define BLOOP_CORE_PART_HPP_ 

#include "Kinds.hpp"

#include <Core/Surface/Surface.hpp>
#include <Core/Discrete/Mesh.hpp>
#include <Core/Origin/Origin_3d.hpp>
#include <Core/UpdReport.hpp>
#include <Core/Id.hpp>
#include <Core/Maths/Ray_3d.hpp>

#include <variant>

namespace Core {
namespace Prt {

struct Part {
  Origin_3d origin;
  WorkpieceId self_id;
  
  CondensedList<Named<Srf::Surface>> surfaces;

  size_t version { 0 };

  Part(WorkpieceId self_id_);
  Part();
  
  IdVec children() const;
  std::vector<Mesh> meshes(SibId childId) const;

  template<typename T>
  std::optional<T> get(SibId sid) const
  { 
    std::cerr<<"Type not handled in Part::get()"<<std::endl;
    return std::nullopt;
  }
  template<typename T>
  std::optional<T> get(Named<SibId> sid) const
  {
    std::cout<<"SEARCHING "<<sid.name<<"\n";
    if(*sid == SibId{}) { // No id
      std::optional<SibId> found = find_sib(sid.name);
      if(!found) {
        std::cerr<<"Could not resolve named index \""<<sid.name<<"\""<<std::endl;
        return std::nullopt;
      }
      return get<T>(*found);
    } else if(sid.name == "") { // No name 
      return get<T>(*sid);
    } else if(*sid != SibId{} && sid.name != "") { // Has both a name and an id
      std::optional<T> try_out = get<T>(*sid); // Try id first because it's simpler
      if(try_out)
        return try_out;
      
      return get<T>(Named<SibId>(SibId{}, sid.name)); // Recursive call as to not duplicate logic
    }

    std::cerr<<"Could not resolve invalid named index"<<std::endl;
    return std::nullopt;
  }
  

  std::string name_of(SibId id) const;

  // Find an id by name, the name does not always need to perfectly match
  // it will try to use a toponaming algorithm (in conjunction with the naming system)
  // to find a valid entity. If it fails, it will return nullopt
  std::optional<Id> find(std::string name, Kind::inner hint) const;
  std::optional<Id> find(std::string name) const;
  std::optional<SibId> find_sib(std::string name, Kind::inner hint) const;
  std::optional<SibId> find_sib(std::string name) const;

  Wp::VecsSnapshot idsSnapshot() const;
  Wp::Diff account_relationships(Wp::Diff diff) const { return std::move(diff); }
};

// Part update(Part part, Action action);
// Part update(Part part, BuildAction buildAction);


} // !Prt

} // !Core

#endif