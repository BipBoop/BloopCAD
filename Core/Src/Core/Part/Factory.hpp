
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef BLOOP_CORE_PRT_FACTORY_HPP_
#define BLOOP_CORE_PRT_FACTORY_HPP_

#include "Actions.hpp"
#include "Part.hpp"
#include <Core/Sketch/Sketch.hpp>

#include <immer/vector.hpp>

#include <functional>

namespace Core {
namespace Prt {

struct Factory {
  using action_t = Action;

  immer::vector<BuildAction> buildActions {
    /*{ 
      AddOriginGeometry {.which=Named<SibId>(SibId{.kind=Kind::Origin_point, .index=0}, "Origin")},
      AddOriginGeometry {.which=Named<SibId>(SibId{.kind=Kind::Origin_plane, .index=0}, "XY plane")},
      AddOriginGeometry {.which=Named<SibId>(SibId{.kind=Kind::Origin_plane, .index=1}, "YZ plane")},
      AddOriginGeometry {.which=Named<SibId>(SibId{.kind=Kind::Origin_plane, .index=2}, "ZX plane")}}*/};
  WorkpieceId wpid;
  int mod_struct { -1 };
  size_t version { 0 };

  Factory() = default;
  Factory(WorkpieceId wpid_);
  Factory(std::vector<BuildAction> buildActions_, WorkpieceId wpid_, size_t version_);
  Factory(immer::vector<BuildAction> buildActions_, WorkpieceId wpid_, size_t version_);

  std::pair<Part, Factory> build(Part current, size_t step, std::function<std::optional<std::pair<Sk::Sketch, glm::dmat4>>(WorkpieceId)> sketch_access, bool& success) const;

  size_t n_steps() const { return buildActions.size(); }

  glm::dmat4 transform_for_step(size_t step, Prt::Part const& currpart) const;

  glm::dmat4 attachSketch_transform(AttachSketch act, Prt::Part const& currpart) const;
};

// Generates the next part factory from the previous
// part factory and an action to apply
Factory next(Factory current, Action action, Part const& currpart, std::function<std::optional<std::pair<Sk::Sketch, glm::dmat4>>(WorkpieceId)> sketch_access);

} // !Prt
} // !Core

#endif