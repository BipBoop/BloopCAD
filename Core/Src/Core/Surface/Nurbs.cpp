
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "Nurbs.hpp"

#include <Core/Print/NurbsOstream.hpp>

namespace Core {
namespace Srf {

using namespace NurbsUtils;

NurbsSurface::NurbsSurface( size_t degree_u_, size_t degree_v_, NurbsUtils::KnotVec knots_u_, 
                            NurbsUtils::KnotVec knots_v_, std::vector<std::vector<vecw_t>> controls_)
  : degree_u(degree_u_)
  , degree_v(degree_v_)
  , knots_u(knots_u_)
  , knots_v(knots_v_)
  , controls(controls_)
{

}
NurbsSurface NurbsSurface::make_flat_rectangle(std::array<glm::dvec3, 4> corners)
{
  std::vector<std::vector<vecw_t>> controls(2, std::vector<vecw_t>(2));
  controls[0][0] = glm::dvec4(corners[0], 1.0);
  controls[1][0] = glm::dvec4(corners[1], 1.0);
  controls[1][1] = glm::dvec4(corners[2], 1.0);
  controls[0][1] = glm::dvec4(corners[3], 1.0);

  return NurbsSurface(1, 1, { 0, 0, 1, 1}, { 0, 0, 1, 1}, controls);
}
NurbsSurface NurbsSurface::make_sweep(Crv::NurbsCurve<3> const& profile, Crv::NurbsCurve<3> const& sweep)
{
  std::cout<<"Make sweep!\n";
  std::cout<<"Profile: "<<profile<<"\nSweep: "<<sweep<<"\n";
  std::vector<std::vector<vecw_t>> controls(profile.n_controls(), std::vector<vecw_t>(sweep.n_controls()));

  for(size_t i = 0; i < profile.n_controls(); ++i) {
    for(size_t j = 0; j < sweep.n_controls(); ++j) {
      controls[i][j] = vecw_t(vec_t(profile.control(i)) + vec_t(sweep.control(j)), 
                              profile.weight(i)*sweep.weight(j));
    }
  }
  return NurbsSurface(profile.degree, sweep.degree, profile.knots, sweep.knots, controls);
}

NurbsSurface::vec_t NurbsSurface::at(double u, double v) const
{
  size_t uspan = knot_span(degree_u, knots_u, u);
  auto basis_u = basis_fun(uspan, degree_u, u, knots_u);

  size_t vspan = knot_span(degree_v, knots_v, v);
  auto basis_v = basis_fun(vspan, degree_v, v, knots_v);

  size_t uind = uspan-degree_u;
  vecw_t out(0.0);

  for(size_t l = 0; l <= degree_v; ++l) {
    vecw_t temp(0.0);
    size_t vind = vspan-degree_v+l;
    for(size_t k = 0; k <= degree_u; ++k) {
      temp += basis_u[k]*multiply_by_weight(controls[uind+k][vind]);
    }
    out += basis_v[l] * temp;
  }
  return divide_by_weight(out);
}
std::vector<std::vector<NurbsSurface::vec_t>> NurbsSurface::derivatives(double u, double v, size_t up_to) const
{
  // A 3.6 b-spline surface derivative
  size_t du = std::min(up_to, degree_u);
  size_t dv = std::min(up_to, degree_v);
  std::vector<std::vector<vecw_t>> bspline_deriv(up_to+1, std::vector<vecw_t>(up_to+1, vecw_t(0.0)));

  size_t uspan = knot_span(degree_u, knots_u, u);
  Eigen::MatrixXd basis_deriv_u = basis_fun_deriv(uspan, degree_u, u, knots_u, du);

  size_t vspan = knot_span(degree_v, knots_v, v);
  Eigen::MatrixXd basis_deriv_v = basis_fun_deriv(vspan, degree_v, v, knots_v, dv);

  std::vector<vecw_t> temp(degree_v+1);
  for(size_t k = 0; k <= du; ++k) {
    for(size_t s = 0; s <= degree_v; ++s) {
      temp[s] = vecw_t(0.0);
      for(size_t r = 0; r <= degree_u; ++r) {
        temp[s] += basis_deriv_u(k, r) * multiply_by_weight(controls[uspan-degree_u+r][vspan-degree_v+s]);
      }
      size_t dd = std::min(up_to-k, dv);
      for(size_t l = 0; l <= dd; ++l) {
        for(size_t s = 0; s <= degree_v; ++s) {
          bspline_deriv[k][l] += basis_deriv_v(l, s) * temp[s];
        }
      }
    }
  }

  std::vector<std::vector<vec_t>> SKL(up_to+1, std::vector<vec_t>(up_to+1));

  for(size_t k = 0; k <= up_to; ++k) {
    for(size_t l = 0; l <= up_to-k; ++l) {
      vec_t v = bspline_deriv[k][l];
      for(size_t j = 1; j <= l; ++j) {
        v -= NurbsUtils::binomial(l, k) * weight(bspline_deriv[0][j])*SKL[k][l-j];
      }
      for(size_t i = 1; i <= k; ++i) {
        v -= NurbsUtils::binomial(k, i) * weight(bspline_deriv[i][0])*SKL[k-i][l];
        vec_t v2(0.0);
        for(size_t j = 1; j <= l; ++j) {
          v2 += NurbsUtils::binomial(l, j) * weight(bspline_deriv[i][j])*SKL[k-i][l-j];
        }
        v -= static_cast<double>(NurbsUtils::binomial(k, i))*v2;
      }
      SKL[k][l] = v / weight(bspline_deriv[0][0]);
    }
  }
  return SKL;
}
NurbsSurface::vec_t NurbsSurface::normal(double u, double v) const
{
  auto derivs = derivatives(u, v, 1); // First order derivatives

  return glm::normalize(glm::cross(derivs[1][0], derivs[0][1]));
}
std::pair<NurbsSurface::vec_t, NurbsSurface::vec_t> NurbsSurface::point_normal(double u, double v) const
{
  return { at(u, v), normal(u, v) };
}
bool NurbsSurface::is_flat(Direction dir) const
{
  switch(dir) {
  case Direction::U_dir:
    return degree_u == 1;
  case Direction::V_dir:
    return degree_v == 1;
  case Direction::Both:
    return degree_u == 1 && degree_v == 1;
  }
}
ParamFlat NurbsSurface::as_flat() const
{
  auto derivs = derivatives(0.0, 0.0, 1); // First order derivatives
  return ParamFlat { .origin=at(0.5, 0.5), .x_=glm::normalize(derivs[1][0]), .y_=glm::normalize(derivs[0][1]) };
}
Plane NurbsSurface::tangeant(double u, double v) const
{
  return Plane { .origin=at(u, v), .norm=normal(u, v) };
}
NurbsSurface NurbsSurface::insert_knot(double knot, size_t n_times, Direction dir) const
{
  if(dir == Direction::U_dir) {
    return insert_knot_u(knot, n_times);
  } else if(dir == Direction::V_dir) {
    return insert_knot_v(knot, n_times);
  }
  std::cerr<<"Cannot insert knot in both directions :("<<std::endl;
  return *this;
}
struct KnotInsertData {
  size_t span;
  size_t mult;
  NurbsUtils::KnotVec knots;
  Eigen::MatrixXd alpha;
};
KnotInsertData load_knotvec_for_insert(size_t degree, double knot, size_t n_times, NurbsUtils::KnotVec const& knots)
{  
  KnotInsertData out;

  out.span = knot_span(degree, knots, knot);
  out.mult = knot_multiplicity(degree, knots, knot);
  out.knots = KnotVec(knots.size() + n_times);
  out.alpha = Eigen::MatrixXd(degree-out.mult, n_times+1);

  for(size_t i = 0; i <= out.span; ++i) {
    out.knots[i] = knots[i];
  }
  for(size_t i = 1; i <= n_times; ++i) {
    out.knots[i+out.span] = knot;
  }
  for(size_t i = out.span+1; i < knots.size(); ++i) {
    out.knots[i+n_times] = knots.at(i);
  }

  for(size_t j = 1; j <= n_times; ++j) {
    size_t L = out.span-degree+j;
    for(size_t i = 0; i <= degree-j-out.mult; ++i) {
      out.alpha(i, j) = (knot - knots[L+i]) / (knots[i+out.span+1]-knots[L+i]);
    }
  }
  return out;
}

NurbsSurface NurbsSurface::insert_knot_u(double knot, size_t n_times) const
{
  auto insert_data = load_knotvec_for_insert(degree_u, knot, n_times, knots_u);

  size_t rows = controls.size(); 
  size_t cols = controls[0].size();
  std::vector<std::vector<vecw_t>> new_controls(rows, std::vector<vecw_t>(cols+n_times));
  std::vector<vecw_t> temp(degree_u+1);

  for(size_t col = 0; col < cols; ++col) {
    for(size_t i = 0; i <= insert_data.span - degree_u; ++i) {
      new_controls[i][col] = controls[i][col];
    }
    for(size_t i = insert_data.span - insert_data.mult; i < cols; ++i) {
      new_controls[i+n_times][col] = controls[i][col];
    }
    for(size_t i = 0; i <= degree_u - insert_data.mult; ++i) {
      temp[i] = controls[insert_data.span-degree_u+i][col];
    }
    size_t L = 0;
    for(size_t j = 1; j <= n_times; ++j) {
      L = insert_data.span - degree_u + j;
      for(size_t i = 0; i <= degree_u - j - insert_data.mult; ++i) {
        temp[i] = insert_data.alpha(i, j) * temp[i+1] + (1.0-insert_data.alpha(i, j) * temp[i]);
      }
      new_controls[L][col] = temp[0];
      new_controls[insert_data.span+n_times-j-insert_data.mult][col] = temp[degree_u-j-insert_data.mult];
    }
    for(size_t i = L + 1; i < insert_data.span - insert_data.mult; ++i) {
      new_controls[i][col] = temp[i-L];
    }
  }
  return NurbsSurface(degree_u, degree_v, insert_data.knots, knots_v, new_controls);
}
NurbsSurface NurbsSurface::insert_knot_v(double knot, size_t n_times) const
{
  auto insert_data = load_knotvec_for_insert(degree_v, knot, n_times, knots_v);

  size_t rows = controls.size(); 
  size_t cols = controls[0].size();
  std::vector<std::vector<vecw_t>> new_controls(rows+n_times, std::vector<vecw_t>(cols));
  std::vector<vecw_t> temp(degree_v+1);

  for(size_t row = 0; row < rows; ++row) {
    for(size_t i = 0; i <= insert_data.span - degree_v; ++i) {
      new_controls[row][i] = controls[row][i];
    }
    for(size_t i = insert_data.span - insert_data.mult; i < cols; ++i) {
      new_controls[row][i+n_times] = controls[row][i];
    }
    for(size_t i = 0; i <= degree_v - insert_data.mult; ++i) {
      temp[i] = controls[row][insert_data.span-degree_v+i];
    }
    size_t L = 0;
    for(size_t j = 1; j <= n_times; ++j) {
      L = insert_data.span - degree_v + j;
      for(size_t i = 0; i <= degree_v - j - insert_data.mult; ++i) {
        temp[i] = insert_data.alpha(i, j) * temp[i+1] + (1.0-insert_data.alpha(i, j) * temp[i]);
      }
      new_controls[row][L] = temp[0];
      new_controls[row][insert_data.span+n_times-j-insert_data.mult] = temp[degree_v-j-insert_data.mult];
    }
    for(size_t i = L + 1; i < insert_data.span - insert_data.mult; ++i) {
      new_controls[row][i] = temp[i-L];
    }
  }
  return NurbsSurface(degree_u, degree_v, knots_u, insert_data.knots, new_controls);
}

Crv::NurbsCurve<3> NurbsSurface::embed_curve(Crv::NurbsCurve<2> const& src) const
{
  std::vector<glm::dvec4> new_controls(src.n_controls());

  for(size_t i = 0; i < src.n_controls(); ++i) {
    new_controls[i] = glm::dvec4(at(glm::dvec2(src.control(i))), src.weight(i));
  }
  return Crv::NurbsCurve<3>(src.degree, src.knots, new_controls);
}
NurbsSurface NurbsSurface::transform(std::function<vec_t(vec_t)> fun) const
{
  std::vector<std::vector<vecw_t>> new_controls(controls.size(), std::vector<vecw_t>(controls[0].size()));

  for(size_t i = 0; i < controls.size(); ++i) {
    for(size_t j = 0; j < controls[i].size(); ++j) {
      new_controls[i][j] = vecw_t(fun(vec_t(controls[i][j])), weight(controls[i][j]));
    }
  }
  return NurbsSurface(degree_u, degree_v, knots_u, knots_v, new_controls);
}
NurbsSurface NurbsSurface::flip(Direction dir) const
{
  if(dir == Direction::U_dir) {
    std::vector<std::vector<vecw_t>> new_controls(controls.size(), std::vector<vecw_t>(controls[0].size()));
    std::reverse_copy(controls.begin(), controls.end(), new_controls.begin());

    return NurbsSurface(degree_u, degree_v, reverseknots(knots_u), knots_v, new_controls);
  } else if(dir == Direction::V_dir) {
    std::vector<std::vector<vecw_t>> new_controls(controls.size(), std::vector<vecw_t>(controls[0].size()));
    for(size_t i = 0; i < new_controls.size(); ++i) {
      std::reverse_copy(controls[i].begin(), controls[i].end(), new_controls[i].begin());
    }

    return NurbsSurface(degree_u, degree_v, knots_u, reverseknots(knots_v), new_controls);
  } else {
    return flip(Direction::U_dir).flip(Direction::V_dir);
  }
}
NurbsSurface NurbsSurface::offset_by(Crv::NurbsCurve<3> const& driving_curve) const
{
   // Todo make rotations as well
   glm::dvec3 offset = driving_curve.end_pt() - driving_curve.start_pt();

   return transform([&](glm::dvec3 ctrl) {
    return ctrl + offset;
   });
}

} // !Srf
} // !Core

Core::Srf::NurbsSurface operator*(glm::dmat4 mat, Core::Srf::NurbsSurface surf)
{
  return surf.transform([&](glm::dvec3 control) {
    return mat*glm::dvec4(control, 1.0);
  });
}
Core::Srf::NurbsSurface operator*(glm::dmat3 mat, Core::Srf::NurbsSurface surf)
{
  return surf.transform([&](glm::dvec3 control) {
    return mat*control;
  });
}