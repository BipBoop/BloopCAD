
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef BLOOP_CORE_SURF_NURBS_HPP_
#define BLOOP_CORE_SURF_NURBS_HPP_

#include <Core/Maths/NurbsUtils.hpp>
#include <Core/Curve/Nurbs.hpp>
#include "Flat_surface.hpp"

#include "Plane.hpp"

namespace Core {
namespace Srf {

class NurbsSurface {
public:
  using vec_t = glm::dvec3;
  using vecw_t = glm::dvec4;
  enum class Direction { U_dir, V_dir, Both };

private:
  size_t degree_u, degree_v;
  NurbsUtils::KnotVec knots_u, knots_v;
  std::vector<std::vector<vecw_t>> controls;

public:
  NurbsSurface() = default;
  NurbsSurface( size_t degree_u_, size_t degree_v_, NurbsUtils::KnotVec knots_u_, 
                NurbsUtils::KnotVec knots_v_, std::vector<std::vector<vecw_t>> controls_);

  // Makes a flat (1 degree) surface with 4 control points (corners)
  static NurbsSurface make_flat_rectangle(std::array<glm::dvec3, 4> corners);

  // P. 472
  // Creates a surface by sweeping a profile curve along another curve
  static NurbsSurface make_sweep(Crv::NurbsCurve<3> const& profile, Crv::NurbsCurve<3> const& sweep);

  // A 4.3
  // Returns the world position of at a parameter
  // space coordinate
  vec_t at(double u, double v) const;
  vec_t at(glm::dvec2 pt) const { return at(pt.x, pt.y); };

  // A 4.4
  // Computes n derivatives (including derivative 0)
  // at a point in parameter space
  std::vector<std::vector<vec_t>> derivatives(double u, double v, size_t up_to) const;
  std::vector<std::vector<vec_t>> derivatives(glm::dvec2 pt, size_t up_to) const { return derivatives(pt.x, pt.y, up_to); };

  // Returns the normal to the surface
  // at a parameter space coordinate
  vec_t normal(double u, double v) const;
  vec_t normal(glm::dvec2 pt) const { return normal(pt.x, pt.y); };

  // Returns both the position and the normal at a parametric position
  std::pair<vec_t, vec_t> point_normal(double u, double v) const;
  std::pair<vec_t, vec_t> point_normal(glm::dvec2 pt) const { return point_normal(pt.x, pt.y); }

  // Returns true if the surface is flat (degree 1)
  // in any or both directions
  bool is_flat(Direction dir = Direction::Both) const;

  // Create a flat surface from the nurbs surface
  // (assumes both directions are degee 1)
  ParamFlat as_flat() const;

  // Returns the tangeant plane at a coordinate
  Plane tangeant(double u, double v) const;
  Plane tangeant(glm::dvec2 pt) const { return tangeant(pt.x, pt.y); }

  // A 5.3
  // Insert a knot in a direction and adjusts control points accordingly
  NurbsSurface insert_knot(double knot, size_t n_times, Direction dir) const;
  NurbsSurface insert_knot_u(double knot, size_t n_times) const;
  NurbsSurface insert_knot_v(double knot, size_t n_times) const;

  // From a curve in 2d that lives in the surface's parameter space,
  // create a 3d curve that matches it in the world space
  Crv::NurbsCurve<3> embed_curve(Crv::NurbsCurve<2> const& src) const;

  // Arbitrary transform of the control points
  // but keeps the weights intact
  NurbsSurface transform(std::function<vec_t(vec_t)> fun) const;

  // Flip the control points and knots in a direction (or both)
  NurbsSurface flip(Direction dir) const;

  // Moves the surface according to a driving curve
  // the curve does not need to connect to the surface as only deltas
  // are used. Translates and rotates the controls point so that
  // the surface has moved the difference between the start and the
  // end of the curve and the curve is normal to it
  NurbsSurface offset_by(Crv::NurbsCurve<3> const& driving_curve) const;
};

} // !Srf
} // !Core

Core::Srf::NurbsSurface operator*(glm::dmat4 mat, Core::Srf::NurbsSurface surf);
Core::Srf::NurbsSurface operator*(glm::dmat3 mat, Core::Srf::NurbsSurface surf);

#endif