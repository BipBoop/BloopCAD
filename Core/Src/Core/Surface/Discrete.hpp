
#ifndef BLOOP_CORE_SURFACE_DISCREETE_HPP_
#define BLOOP_CORE_SURFACE_DISCREETE_HPP_

#include <Core/Curve/ClosedShape/Shape.hpp>
#include "Nurbs.hpp"

#include <Core/Discrete/Mesh.hpp>

#include <glm/gtx/vector_angle.hpp>

#include <functional>
#include <map>

// https://people.idsia.ch/~frank/kailfrank_thesis.pdf

namespace Core {
namespace Srf {

struct Point {
  glm::dvec2 uvPos; // 2d position in the parametric space
  glm::dvec3 worldPos; // 3d position in world space
  glm::dvec3 normal; // Normal to surface in world space at this point

  Point() = default;
  Point(glm::dvec2 surfPos, NurbsSurface cont);
  Point(glm::dvec2 surfPos1, glm::dvec2 surfPos2, NurbsSurface cont);
};

struct Node {
  std::array<Point, 5> points; // 4 corners + 1 middle
  double area;
  double aspect_ratio;
  bool aspect_ratio_inv;
  double warp;
  glm::dvec2 curvature;
  size_t u_divs { 0 }, v_divs { 0 };
  int parentNode { -1 };
  int ind;
  std::array<int, 4> children { -1, -1, -1, -1 };

  enum class SubPatch { SOUTH_WEST, SOUTH_EAST, NORTH_WEST, NORTH_EAST, NORTH, SOUTH, WEST, EAST };

  Node(std::array<glm::dvec2, 4> uvPoints, NurbsSurface cont);
  Node(Node parent, SubPatch subPatch, NurbsSurface cont);

  static double triangle_area(glm::dvec3 a, glm::dvec3 b, glm::dvec3 c);
  double comp_area() const;
  glm::dvec2 comp_curvature() const;
  std::pair<double, bool> comp_aspectRatio() const;
  double comp_warp() const;
};

struct Disc {
  std::vector<Node> nodes;
  dBox bbox;

  Disc() = default;
  Disc(NurbsSurface cont, std::function<std::pair<bool, bool>(Node)> subdivide_fun, dBox parambox);

  int add_node(Node node);
  void build_tree(int node_ind, NurbsSurface cont, std::function<std::pair<bool, bool>(Node)> subdivide_fun);

  std::vector<Node> leaves_nodes() const;

  // Returns a vector of points in the parameter space
  // that are inside a given triming
  size_t point_index(glm::dvec2 pt, size_t n_rows, size_t n_cols, dBox bbox) const;
  std::vector<glm::dvec2> paramspace_insidePoints(Crv::_2d::ShapeWithHoles const& trimming) const;
};

// Implements 2.37
std::pair<bool, bool> subdivide_quad_curvature(Node node, double target_curvature);
// Implements 2.38
std::pair<bool, bool> subdivide_hybrid_curvature(Node node, double target_curvature);

bool subdivide_area(Node node, double div, double target_area);
// Implements 2.10
std::pair<bool, bool> subdivide_quad_area(Node node, double target_area);
// Implements 2.11
std::pair<bool, bool> subdivide_hybrid_area(Node node, double target_area);

// Implements 2.44
std::pair<bool, bool> subdivide_quad_edgeArea(Node node, double target_area);
// Implements 2.45
std::pair<bool, bool> subdivide_hybrid_edgeArea(Node node, double target_area);

std::pair<bool, bool> subdivide(Node node, double target_area, double target_curvature, double warp_treshold);

} // !Srf
} // !Core

std::ostream& operator<<(std::ostream& os, Core::Srf::Point const& point);
std::ostream& operator<<(std::ostream& os, Core::Srf::Node const& node);

#endif
