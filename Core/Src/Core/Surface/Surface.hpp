
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef BLOOP_CORE_SURFACE_HPP_
#define BLOOP_CORE_SURFACE_HPP_

#include "Surface.hpp"
#include <Core/Curve/Curve_decl.hpp>
#include "Discrete.hpp"
#include "Nurbs.hpp"
#include <Core/Curve/Nurbs.hpp>

#include <variant>
#include <numbers>

namespace Core {
namespace Srf {

struct Surface {
  NurbsSurface support;
  Crv::_2d::ShapeWithHoles trimming;

	Surface() = default;
  Surface(NurbsSurface support_, Crv::_2d::ShapeWithHoles timming_);
  Surface(NurbsSurface support_, dBox trimbox);
  Surface(NurbsSurface support_);

  Surface invert() const;

  Mesh to_mesh() const;

  NurbsSurface* operator->()
  {
    return &support;
  }
  NurbsSurface const* operator->() const
  {
    return &support;
  }
};

} // !Srf
} // !Core

Core::Srf::Surface operator*(glm::dmat4 mat, Core::Srf::Surface surf);
Core::Srf::Surface operator*(glm::dmat3 mat, Core::Srf::Surface surf);

#endif