
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
#ifndef BLOOP_CORE_SPACE_2D_HPP_
#define BLOOP_CORE_SPACE_2D_HPP_

#include "Plane.hpp"

#include <glm/glm.hpp>

namespace Core {
namespace Srf {

// A 2d space embeded in a 3d space
struct ParamFlat {
	glm::dvec3 origin;
	glm::dvec3 x_;
	glm::dvec3 y_;

	glm::dvec3 to_3d(glm::dvec2 const& pos_2d) const
	{
		return origin + x_ * pos_2d.x + y_ * pos_2d.y;
	}
	glm::dvec2 to_2d(glm::dvec3 const& point) const
	{
		glm::dvec3 pt_around_origin = point - origin;
		glm::dvec2 point_2d(glm::dot(glm::normalize(x_), pt_around_origin), glm::dot(glm::normalize(y_), pt_around_origin));
		return point_2d;
	}
	Plane plane() const
	{
		return Plane { origin, glm::normalize(glm::cross(x_, y_)) };
	}
};

} // !Src
} // !Core

#endif