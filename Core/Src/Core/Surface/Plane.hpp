
#ifndef BLOOP_CORE_MATHPLANE_HPP_
#define BLOOP_CORE_MATHPLANE_HPP_

#include <glm/gtx/norm.hpp>
#include <glm/glm.hpp>

namespace Core {
namespace Srf {

struct Plane {
	glm::dvec3 origin;
	glm::dvec3 norm;

	glm::dvec3 closestPoint(glm::dvec3 point)
	{
		glm::dvec3 op = point-origin;
		glm::dvec3 op_proj = glm::dot(op, norm) * norm / glm::length2(norm);
		return point - op_proj;
	}
};

} // !Srf
} // !Core

#endif