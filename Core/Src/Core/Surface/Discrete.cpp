
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "Discrete.hpp"

#include <Core/Print/GlmOstream.hpp>
#include <Core/Precision.hpp>
#include <Core/Debug/Timer.hpp>

#include <map>
#include <unordered_set>

namespace Core {
namespace Srf {

Point::Point(glm::dvec2 surfPos, NurbsSurface cont)
  : uvPos(surfPos)
{
  std::tie(worldPos, normal) = cont.point_normal(surfPos);
}
Point::Point(glm::dvec2 surfPos1, glm::dvec2 surfPos2, NurbsSurface cont)
  : Point((surfPos1+surfPos2) / 2.0, cont)
{

}

Node::Node(std::array<glm::dvec2, 4> uvPoints, NurbsSurface cont)
{
  for(int i = 0; i < 4; ++i) {
    points[i] = Point(uvPoints[i], cont);

  }

  points[4] = Point(uvPoints[0], uvPoints[2], cont);


  area = comp_area();
  curvature = comp_curvature();
  std::tie(aspect_ratio, aspect_ratio_inv) = comp_aspectRatio();
  warp = comp_warp();
}
Node::Node(Node parent, SubPatch subPatch, NurbsSurface cont)
{
  // in the patch space points go (0, 0) -> (1, 0) -> (1, 1) -> (0, 1)
  // then center (0.5, 0.5)
  switch(subPatch) {
  case SubPatch::SOUTH_WEST:
    points[0] = parent.points[0];
    points[1] = Point(parent.points[0].uvPos, parent.points[1].uvPos, cont);
    points[2] = parent.points[4];
    points[3] = Point(parent.points[0].uvPos, parent.points[3].uvPos, cont);
    v_divs = parent.v_divs+1;
    u_divs = parent.u_divs+1;
    break;
  case SubPatch::SOUTH_EAST:
    points[0] = Point(parent.points[0].uvPos, parent.points[1].uvPos, cont);
    points[1] = parent.points[1];
    points[2] = Point(parent.points[1].uvPos, parent.points[2].uvPos, cont);
    points[3] = parent.points[4];
    v_divs = parent.v_divs+1;
    u_divs = parent.u_divs+1;
    break;
  case SubPatch::NORTH_EAST:
    points[0] = parent.points[4];
    points[1] = Point(parent.points[2].uvPos, parent.points[1].uvPos, cont);
    points[2] = parent.points[2];
    points[3] = Point(parent.points[2].uvPos, parent.points[3].uvPos, cont);
    v_divs = parent.v_divs+1;
    u_divs = parent.u_divs+1;
    break;
  case SubPatch::NORTH_WEST:
    points[0] = Point(parent.points[0].uvPos, parent.points[3].uvPos, cont);
    points[1] = parent.points[4];
    points[2] = Point(parent.points[2].uvPos, parent.points[3].uvPos, cont);
    points[3] = parent.points[3];
    v_divs = parent.v_divs+1;
    u_divs = parent.u_divs+1;
    break;
  case SubPatch::NORTH:
    points[0] = Point(parent.points[0].uvPos, parent.points[3].uvPos, cont);
    points[1] = Point(parent.points[1].uvPos, parent.points[2].uvPos, cont);
    points[2] = parent.points[2];
    points[3] = parent.points[3];
    v_divs = parent.v_divs+1;
    u_divs = parent.u_divs;
    break;
  case SubPatch::SOUTH:
    points[0] = parent.points[0];
    points[1] = parent.points[1];
    points[2] = Point(parent.points[1].uvPos, parent.points[2].uvPos, cont);
    points[3] = Point(parent.points[0].uvPos, parent.points[3].uvPos, cont);
    v_divs = parent.v_divs+1;
    u_divs = parent.u_divs;
    break;
  case SubPatch::EAST:
    points[0] = Point(parent.points[0].uvPos, parent.points[1].uvPos, cont);
    points[1] = parent.points[1];
    points[2] = parent.points[2];
    points[3] = Point(parent.points[3].uvPos, parent.points[2].uvPos, cont);
    u_divs = parent.u_divs+1;
    v_divs = parent.v_divs;
    break;
  case SubPatch::WEST:
    points[0] = parent.points[0];
    points[1] = Point(parent.points[0].uvPos, parent.points[1].uvPos, cont);
    points[2] = Point(parent.points[3].uvPos, parent.points[2].uvPos, cont);
    points[3] = parent.points[3];
    u_divs = parent.u_divs+1;
    v_divs = parent.v_divs;
    break;
  }

  points[4] = Point(points[0].uvPos, points[2].uvPos, cont);
  // std::cout<<"divisions: "<<u_divs<<",  "<<v_divs<<"\n";
  parentNode = parent.ind;
  area = comp_area();
  curvature = comp_curvature();
  std::tie(aspect_ratio, aspect_ratio_inv) = comp_aspectRatio();

  warp = comp_warp();
}

double Node::triangle_area(glm::dvec3 a, glm::dvec3 b, glm::dvec3 c)
{
  return glm::length(glm::cross(b-a, c-a)) / 2.0;
}
double Node::comp_area() const
{
  return  triangle_area(points[0].worldPos, points[1].worldPos, points[4].worldPos) + 
          triangle_area(points[1].worldPos, points[2].worldPos, points[4].worldPos) +
          triangle_area(points[2].worldPos, points[3].worldPos, points[4].worldPos) + 
          triangle_area(points[3].worldPos, points[0].worldPos, points[4].worldPos);
}
glm::dvec2 Node::comp_curvature() const
{
  // return {  curvature_3pts( (points[0].worldPos + points[1].worldPos) / 2.0, points[4].worldPos,
  //                           (points[2].worldPos + points[3].worldPos) / 2.0),
  //           curvature_3pts( (points[0].worldPos + points[3].worldPos) / 2.0, points[4].worldPos,
  //                           (points[1].worldPos + points[2].worldPos) / 2.0) };



  double max_angle = std::numeric_limits<double>::min();
  for(int i = 0; i < 5; ++i) {
    for(int j = i+1; j < 5; ++j) {
      double angle = glm::angle(points[i].normal, points[j].normal);
      if(angle > max_angle) {
        max_angle = angle;
      }
    }
  }
  return { std::cos(max_angle), std::cos(max_angle) };
}
std::pair<double, bool> Node::comp_aspectRatio() const
{
  double num = glm::distance(points[0].worldPos, points[1].worldPos) + glm::distance(points[2].worldPos, points[3].worldPos);
  double den = glm::distance(points[0].worldPos, points[3].worldPos) + glm::distance(points[1].worldPos, points[2].worldPos);

  bool inv = num < den;
  if(inv)
    std::swap(num, den);

  return { num / den, inv };
}
double Node::comp_warp() const
{
  double min_angle = std::numeric_limits<double>::max();
  for(int i = 0; i < 4; ++i) {
    double angle = glm::angle(points[i].worldPos-points[(i+1)%4].worldPos, points[i].worldPos-points[(i+3)%4].worldPos);
    if(angle < min_angle) {
      min_angle = angle;
    }
  }
  return std::cos(min_angle);
}

Disc::Disc(NurbsSurface cont, std::function<std::pair<bool, bool>(Node)> subdivide_fun, dBox bb)
  : bbox(bb)
{
  // Dbg::Timer tim(__FUNCTION__);
  
  int root_node = add_node(Node({{glm::dvec2(0.0, 0.0), glm::dvec2(1.0, 0.0), glm::dvec2(1.0, 1.0), glm::dvec2(0.0, 1.0)}}, cont));
  
  build_tree(root_node, cont, subdivide_fun);

  std::cout<<"Built discreete surface with "<<nodes.size()<<" nodes\n";
}

int Disc::add_node(Node node)
{
  node.ind = nodes.size();
  nodes.push_back(node);
  return node.ind;
}


void Disc::build_tree(int node_ind, NurbsSurface cont, std::function<std::pair<bool, bool>(Node)> subdivide_fun)
{
  // Dbg::Timer tim(__FUNCTION__);
  bool divU, divV;
  std::tie(divU, divV) = subdivide_fun(nodes[node_ind]);

  if(divU && divV) {
    nodes[node_ind].children[0] = add_node(Node(nodes[node_ind], Node::SubPatch::SOUTH_WEST, cont));
    nodes[node_ind].children[1] = add_node(Node(nodes[node_ind], Node::SubPatch::SOUTH_EAST, cont));
    nodes[node_ind].children[2] = add_node(Node(nodes[node_ind], Node::SubPatch::NORTH_WEST, cont));
    nodes[node_ind].children[3] = add_node(Node(nodes[node_ind], Node::SubPatch::NORTH_EAST, cont));

    build_tree(nodes[node_ind].children[0], cont, subdivide_fun);
    build_tree(nodes[node_ind].children[1], cont, subdivide_fun);
    build_tree(nodes[node_ind].children[2], cont, subdivide_fun);
    build_tree(nodes[node_ind].children[3], cont, subdivide_fun);
  } else if(divU) {
    nodes[node_ind].children[0] = add_node(Node(nodes[node_ind], Node::SubPatch::WEST, cont));
    nodes[node_ind].children[1] = add_node(Node(nodes[node_ind], Node::SubPatch::EAST, cont));

    build_tree(nodes[node_ind].children[0], cont, subdivide_fun);
    build_tree(nodes[node_ind].children[1], cont, subdivide_fun);
  } else if(divV) {
    nodes[node_ind].children[0] = add_node(Node(nodes[node_ind], Node::SubPatch::SOUTH, cont));
    nodes[node_ind].children[1] = add_node(Node(nodes[node_ind], Node::SubPatch::NORTH, cont));

    build_tree(nodes[node_ind].children[0], cont, subdivide_fun);
    build_tree(nodes[node_ind].children[1], cont, subdivide_fun);
  }
}
std::vector<Node> Disc::leaves_nodes() const
{
  std::vector<Node> out;
  for(Node node : nodes) {
    bool childless = true;
    for(int i = 0; i < 4; ++i) {
      if(node.children[i] != -1) {
        childless = false;
        break;
      }
    }
    if(childless) {
      out.push_back(node);
    }
  }
  return out;
}
size_t Disc::point_index(glm::dvec2 pt, size_t n_rows, size_t n_cols, dBox bbox) const
{
  size_t u_ind = round(map(pt.x, bbox.smoll.x, bbox.big.x, 0.0, static_cast<double>(n_rows-1)));
  size_t v_ind = round(map(pt.y, bbox.smoll.y, bbox.big.y, 0.0, static_cast<double>(n_cols-1)));

  return v_ind * n_rows + u_ind;
}
std::vector<glm::dvec2> Disc::paramspace_insidePoints(Crv::_2d::ShapeWithHoles const& trimming) const
{
  std::vector<Node> leaves = leaves_nodes();
  size_t u_divs = 0, v_divs = 0;
  for(Node const& leaf : leaves) {
    u_divs = std::max(u_divs, leaf.u_divs);
    v_divs = std::max(v_divs, leaf.v_divs);
  }

  size_t n_rows = std::pow(2, v_divs+1) + 1;
  size_t n_cols = std::pow(2, u_divs+1) + 1;

  std::unordered_set<size_t> checked_points;
  std::vector<glm::dvec2> out;
  for(auto const& leaf : leaves) {
    for(int j = 0; j < 5; ++j) {
      auto pt_index = point_index(leaf.points[j].uvPos, n_rows, n_cols, bbox);
      if(checked_points.find(pt_index) != checked_points.end()) {
        continue;
      }
      checked_points.insert(pt_index);
      if(!bbox.point_onborder(leaf.points[j].uvPos, Prc::kIntersection) && trimming.point_inside(leaf.points[j].uvPos)) {
        out.push_back(leaf.points[j].uvPos);
      }
    }
  }
  return out;
}

// Implements 2.37
std::pair<bool, bool> subdivide_quad_curvature(Node node, double target_curvature)
{
  if(node.curvature.x < target_curvature || node.curvature.y < target_curvature) {
    return { true, true };
  }
  return { false, false };
}
std::pair<bool, bool> subdivide_hybrid_curvature(Node node, double target_curvature)
{
  using std::numbers::sqrt2;

  if(node.curvature.x > target_curvature && node.curvature.y > target_curvature)
    return { false, false };

  bool divU = false;
  bool divV = false;

  if(node.aspect_ratio > sqrt2) {
    // Use binary subdivision
    if(!node.aspect_ratio_inv) {
      divU = true;
    } else {
      divV = true;
    }
  } else {
    divU = node.curvature.x < target_curvature;
    divV = node.curvature.y < target_curvature;
  }
  return { divU, divV };
}

bool subdivide_area(Node node, double div, double target_area)
{
  using std::pow;
  return pow(node.area-target_area, 2) > pow(node.area/div-target_area, 2);
}

// Implements 2.10
std::pair<bool, bool> subdivide_quad_area(Node node, double target_area)
{
  if(subdivide_area(node, 4.0, target_area))
    return { true, true };
  return { false, false };
}
// Implements 2.11
std::pair<bool, bool> subdivide_hybrid_area(Node node, double target_area)
{
  using std::numbers::sqrt2;

  bool divU = false;
  bool divV = false;
  if(node.aspect_ratio > sqrt2) {
    // Use binary subdivision
    if(subdivide_area(node, 2.0, target_area)) {
      if(!node.aspect_ratio_inv) {
        divU = true;
      } else {
        divV = true;
      }
    }
  } else if(subdivide_area(node, 4.0, target_area)) {
    // Use quaternary subdivision
    divU = true;
    divV = true;
  }
  return { divU, divV };
}
// Implements 2.44
std::pair<bool, bool> subdivide_quad_edgeArea(Node node, double target_area)
{
  if(subdivide_area(node, 4.0, target_area)) {
    return { true, true };
  }
  return { false, false };
}
// Implements 2.45
std::pair<bool, bool> subdivide_hybrid_edgeArea(Node node, double target_area)
{
  using std::numbers::sqrt2;

  if(node.parentNode == -1)
    return { true, true };

  bool divU = false;
  bool divV = false;

  if(node.aspect_ratio > sqrt2) {
    // Use binary subdivision
    if(subdivide_area(node, 2.0, target_area)) {
      if(!node.aspect_ratio_inv) {
        divU = true;
      } else {
        divV = true;
      }
    }
  } else if(subdivide_area(node, 4.0, target_area)) {
    // Use quaternary subdivision
    divU = true;
    divV = true;
  }
  return { divU, divV };
}
// std::pair<bool, bool> subdivide(Node node, double target_area, double target_curvature, double warp_treshold)
// {
//   bool divU = false;
//   bool divV = false;
//   using std::numbers::sqrt2;
//   using std::numbers::sqrt3;


//   if(node.curvature < target_curvature) {
//     if((parent.warp < warp_treshold && (node.aspect_ratio < sqrt2 || node.aspect_ratio > 4.0/3.0*sqrt3)) ||
//           node.aspect_ratio > sqrt2) {
      

//     }
//   }
// }

std::pair<bool, bool> subdivide(Node node, double target_area, double target_curvature, double warp_treshold)
{
  bool divU = false;
  bool divV = false;
  using std::numbers::sqrt2;
  using std::numbers::sqrt3;
  if(node.curvature.x < target_curvature || node.curvature.y < target_curvature) {
    if((node.warp < warp_treshold && (node.aspect_ratio < sqrt2 ||
      node.aspect_ratio > 4.0/3.0*sqrt3)) || node.aspect_ratio > sqrt2) {
      // Use binary subdivision
      if(subdivide_area(node, 2.0, target_area)) {
        if(!node.aspect_ratio_inv) {
          divU = true;
        } else {
          divV = true;
        }
      }
    } else if(subdivide_area(node, 4.0, target_area)) {
      // Use quaternary subdivision
      divU = true;
      divV = true;
    }
  }
  return { divU, divV };
}



} // !Srf
} // !Core

std::ostream& operator<<(std::ostream& os, Core::Srf::Point const& point)
{
  os<<"uvPos="<<point.uvPos.x<<",  "<<point.uvPos.y<<",  "<<"\n";
  os<<"worldPos="<<point.worldPos.x<<",  "<<point.worldPos.y<<",  "<<point.worldPos.z<<"\n";
  os<<"normal="<<point.normal.x<<",  "<<point.normal.y<<",  "<<point.normal.z<<"\n";
  
  return os;
}
std::ostream& operator<<(std::ostream& os, Core::Srf::Node const& node)
{
  os<<"-- Points --\n";
  for(size_t i = 0; i < 5; ++i) {
    os<<"------\n";
    os<<node.points[i]<<"\n";
  }
  os<<"area="<<node.area<<"\n";
  os<<"aspect_ratio="<<node.aspect_ratio<<(node.aspect_ratio_inv ? "[inv]" : "[str]")<<"\n";
  os<<"warp="<<node.warp<<"\n";
  os<<"curvature="<<node.curvature.x<<",  "<<node.curvature.y<<"\n";
  os<<"divs [u, v]="<<node.u_divs<<",  "<<node.v_divs<<"\n";
  os<<"ind="<<node.ind<<"\n";
  os<<"children="<<node.children[0]<<",  "<<node.children[1]<<",  "<<node.children[2]<<",  "<<node.children[3]<<"\n";

  return os;
}
