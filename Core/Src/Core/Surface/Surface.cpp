
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "Surface.hpp"
#include <Core/Curve/Line.hpp>
#include "Flat_surface.hpp"
#include "Surface.hpp"
#include <Core/Discrete/Triangulation/DelaunayTrig_2d.hpp>
#include <Core/Curve/Curve_2d.hpp>

#include <Core/Utils/Visitor.hpp>
#include <Core/Maths/Utils.hpp>

#include <glm/gtx/string_cast.hpp>

#include <map>

namespace Core {
namespace Srf {

using namespace Crv::_2d;

Surface::Surface(NurbsSurface support_, ShapeWithHoles trimming_)
  : support(support_)
  , trimming(trimming_.normalize())
  // , disc(cont_, [&](Node const& node) -> std::pair<bool, bool> {
  // 		// std::cout<<"Curvature: "<<node.curvature<<"\n";
  // 		bool div = node.curvature < 0.99;
  // 		// bool div = node.area > 10;
  // 		return { div, div };
  // })
  // , disc(cont_, std::bind(&subdivide_hybrid_area, std::placeholders::_1, 0.5))
{

}
Surface::Surface(NurbsSurface support_)
  : Surface(support_, ShapeWithHoles::NDC_square())
{

}
Surface::Surface(NurbsSurface support_, dBox trimbox)
  : Surface(support_, ShapeWithHoles(Shape({ 
                        to_nurbs(Line(trimbox.bottomLeft(), trimbox.bottomRight())),
                        to_nurbs(Line(trimbox.bottomRight(), trimbox.topRight())),
                        to_nurbs(Line(trimbox.topRight(), trimbox.topLeft())),
                        to_nurbs(Line(trimbox.topLeft(), trimbox.bottomLeft()))}), {}))
{

}
Surface Surface::invert() const
{
  return Surface(support.flip(NurbsSurface::Direction::U_dir), trimming.flip(Shape::Direction::X_dir));
}
Mesh Surface::to_mesh() const
{
  double target_curvature = 0.9;
  Disc disc(support, std::bind(&subdivide_hybrid_curvature, std::placeholders::_1, target_curvature), trimming.boundingBox());

  auto triginput = trimming.delaunayInput(target_curvature, true, [&, this](glm::dvec2 a, glm::dvec2 elbow, glm::dvec2 b) {
    glm::dvec3 a_3d     = support.at(trimming.boundingBox().normalize(a));
    glm::dvec3 elbow_3d = support.at(trimming.boundingBox().normalize(elbow));
    glm::dvec3 b_3d     = support.at(trimming.boundingBox().normalize(b));

    return curvature_3pts(a_3d, elbow_3d, b_3d);
  });
  triginput.pointCloud = trimming.boundingBox().normalize(disc.paramspace_insidePoints(trimming));

  // std::cout<<"N holes: "<<trimming.n_holes()<<"\n";
  auto trigout = DelaunayTrig().triangulate_to_data(triginput);

  // std::cout<<"Trigout:\n";
  // for(size_t i = 0; i < trigout.verts.size(); ++i) {
  //   std::cout<<"("<<trigout.verts[i].x<<", "<<trigout.verts[i].y<<")\n";
  // }
  return Mesh::from_trigdata(to_3dNormal(trigout, support));
}

} // !Srf
} // !Core

Core::Srf::Surface operator*(glm::dmat4 mat, Core::Srf::Surface surf)
{
  return Core::Srf::Surface(mat*surf.support, surf.trimming);
}
Core::Srf::Surface operator*(glm::dmat3 mat, Core::Srf::Surface surf)
{
  return Core::Srf::Surface(mat*surf.support, surf.trimming);
}