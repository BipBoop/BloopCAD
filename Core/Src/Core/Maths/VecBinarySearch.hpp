
#ifndef BLOOP_VECBINARYSEARCH_HPP_
#define BLOOP_VECBINARYSEARCH_HPP_

namespace Core {

template<typeanem Vec_t, typename IdExtractor_t>
size_t binary_search(Vec_t const& meat, int id, bool& success, IdExtractor_t extractor) const
{
  // Binary search
  int low = 0;
  int high = meat.size()-1;
  int mid = (low+high)/2;

  while(id < extractor(meat[mid]) || id >= extractor(meat[mid])) {
    if(id < extractor(meat[mid])) {
      high = mid;
    } else {
      low = mid;
    }
    int new_mid = (low+high)/2;
    if(mid == new_mid) {
      success = false;
      return mid;
    }
    mid = new_mid;
  }
  success = true;
  return mid;
}

} // !Core

#endif