
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef BLOOP_CORE_NURBSUTILS_HPP_
#define BLOOP_CORE_NURBSUTILS_HPP_

#include <Eigen/Core>
#include <glm/glm.hpp>

namespace Core {
namespace NurbsUtils {

using KnotVec = std::vector<double>;

// // Algorithm A1.1
// double Horner1(std::vector<double> const& knots, int degree, double t)
// {
//   double out = knots[degree];
//   for(int i = degree-1; i >= 0; i--) {
//     out = out * t + knots[i];
//   }
// }

double binomial(int n, int k) noexcept;

template<int dim>
double weight(glm::vec<dim, double> in)
{
  return in[dim-1];
}
template<int dim>
glm::vec<dim, double> divide_by_weight(glm::vec<dim, double> in)
{
  for(int i = 0; i < dim-1; ++i) {
    in[i] /= weight(in);
  }
  return in;
}

template<int dim>
glm::vec<dim, double> multiply_by_weight(glm::vec<dim, double> in)
{
  for(int i = 0; i < dim-1; ++i) {
    in[i] *= weight(in);
  }
  return in;
}

// Make weight control points (multiply the control point with it's weight)
template<int dim>
std::vector<glm::vec<dim, double>> weight_controls(std::vector<glm::vec<dim, double>> controls)
{
  for(int i = 0; i < controls.size(); ++i) {
    controls[i] = multiply_by_weight(controls[i]);
  }
  return controls;
}

// A 1.2
double Bernstein(int ind, int degree, double t);
// A1.3
std::vector<double> AllBernstein(int degree, double t);

// A2.1
int knot_span(int degree, KnotVec const& knots, double u);

int knot_multiplicity(int degree, KnotVec const& knots, double u);

// Number of identical knots on both sides of the knot vector
int knot_vec_degree(KnotVec const& knots);

int n_controls_for_knots(int degree, int n_knots);
int n_knots_for_controls(int degree, int n_controls);

int n_endOfCurve_knots(int degree);

KnotVec normalize(KnotVec vec);
KnotVec reverseknots(KnotVec vec);

// A2.2
std::vector<double> basis_fun(int ind, int degree, double u, KnotVec const& knots);

// A2.3
Eigen::MatrixXd basis_fun_deriv(int ind, int degree, double u, KnotVec const& knots, int up_to);
std::vector<std::vector<double>> cheat_basis_fun_deriv(int spanIndex, int degree,  
	int derivative, const std::vector<double>& knotVector, double paramT);

// A2.4
double one_basis_fun(int ind, int degree, KnotVec const& knots, double u);

// A2.5
std::vector<double> one_basis_fun_deriv(int ind, int degree, KnotVec knots, double u, int up_to);

} // !NurbsUtils
} // !Core

#endif