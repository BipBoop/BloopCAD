
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef MATHUTILS_HPP_
#define MATHUTILS_HPP_

#include <glm/glm.hpp>
#include <glm/gtx/norm.hpp>

#include <array>

namespace Core {

/*
	@function diff_angless computes the minimum difference between two angles

	@param current : 	The first angle to evaluate
	@param target : 	The angle that should be obtained if the result was added to current

	@return : The difference between two angle [-pi, pi]
*/
float diff_angles(float current, float target);

double ccw_diff_angles(double horiz_angl1, double horiz_angl2);
double horizontal_angle(glm::dvec2 normVec);
double ccw_angle_between(glm::dvec2 from, glm::dvec2 to);

template<typename T>
bool epsilon_equal(glm::vec<2, T, glm::defaultp> vec1, glm::vec<2, T, glm::defaultp> vec2, double epsilon)
{
	return glm::distance2(vec1, vec2) < (epsilon*epsilon);
}
template<typename T>
bool epsilon_equal(glm::vec<3, T, glm::defaultp> vec1, glm::vec<3, T, glm::defaultp> vec2, double epsilon)
{
	return glm::distance2(vec1, vec2) < (epsilon*epsilon);
}
template<typename T>
bool epsilon_equal(glm::vec<4, T, glm::defaultp> vec1, glm::vec<4, T, glm::defaultp> vec2, double epsilon)
{
	return glm::distance2(vec1, vec2) < (epsilon*epsilon);
}
template<typename T>
bool sepsilon_equal(T a, T b, T epsilon)
{
	return std::abs(a-b) < (epsilon);
}

template<typename T>
T clamp(T const& val, T const& min, T const& max)
{
	if(val < min)
		return min;
	if(val > max)
		return max;
	return val;
}

/*
	@function map maps a number in a linear fashion between two ranges

	@param x : 			The number to map
	@param in_min : 	The minimum value of the input range
	@param in_max : 	The maximum value of the input range
	@param out_min : 	The minimum value of the output range
	@param out_max : 	The maximum value of the output range

	@return : The mapped number

	@note : This function was taken from the arduino map page and templated
*/
template<typename T>
T map(T const& x, T const& in_min, T const& in_max, T const& out_min, T const& out_max)
{
	// https://www.arduino.cc/reference/en/language/functions/math/map/
	return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

// Maths (and corresponding unit test) from 
// https://gamedev.stackexchange.com/questions/70075/how-can-i-find-the-perpendicular-to-a-2d-vector
template<typename T>
glm::vec<2, T, glm::defaultp> perpendicular_cw(glm::vec<2, T, glm::defaultp> in)
{
	return glm::vec<2, T, glm::defaultp>(in.y, -in.x);
}
template<typename T>
glm::vec<2, T, glm::defaultp> perpendicular_ccw(glm::vec<2, T, glm::defaultp> in)
{
	return glm::vec<2, T, glm::defaultp>(-in.y, in.x);
}

template<typename T>
glm::vec<3, T> normal_ccw(std::array<glm::vec<3, T>, 3> triangle)
{
	return glm::normalize(glm::cross(triangle[1]-triangle[0], triangle[2]-triangle[0]));
}

bool ccw(glm::dvec2 A, glm::dvec2 B, glm::dvec2 C);

template<typename T>
bool colinear(glm::vec<2, T, glm::defaultp> A, glm::vec<2, T, glm::defaultp> B, glm::vec<2, T, glm::defaultp> C)
{
	// https://ncalculators.com/geometry/triangle-area-by-3-points.htm
	double triangle_area = 0.5 * (A.x*(B.y-C.y) + B.x*(C.y-A.y) + C.x*(A.y-B.y));
	return triangle_area == 0.0;
}

bool isAngleBetween(double angle, double a, double b);

template<int dim>
double curvature_3pts(glm::vec<dim, double> a, glm::vec<dim, double> elbow, glm::vec<dim, double> b)
{
	a = elbow-a;
	b = elbow-b;
	return std::abs(glm::dot(a, b) / (glm::length(a) * glm::length(b)));
}

template<typename Ta, typename Tb>
Ta cross_2d(glm::vec<2, Ta, glm::defaultp> a, glm::vec<2, Tb, glm::defaultp> b)
{
	return a.x * b.y - a.y * b.x;
}
template<typename Ta, typename Tb>
Ta cross(glm::vec<2, Ta, glm::defaultp> a, glm::vec<2, Tb, glm::defaultp> b)
{
	return cross_2d(a, b);
}
template<typename Ta, typename Tb>
Ta cross(glm::vec<3, Ta, glm::defaultp> a, glm::vec<3, Tb, glm::defaultp> b)
{
	return glm::cross(a, b);
}
template<typename Ta, typename Tb>
Ta cross(glm::vec<4, Ta, glm::defaultp> a, glm::vec<4, Tb, glm::defaultp> b)
{
	return glm::cross(a, b);
}


template<typename baseT, typename v1T, typename v2T>
void min_max(v1T const& v1, v2T const& v2, baseT& min_, baseT& max_)
{
	if(v1 > v2) {
		min_ = v2;
		max_ = v1;
	} else {
		min_ = v1;
		max_ = v2;
	}
}

template<typename T>
struct InclusiveRange {
	T start;
	T end;

	InclusiveRange(T a, T b)
		: start(a)
		, end(b)
	{
		if(start > b)
			std::swap(start, end);
	}

	bool intersects(InclusiveRange const& other)
	{
		if(other.end >= start && other.start <= end)
			return true; 
		return false;
	}
};

glm::vec3 as_3d(glm::dvec2 vert);

} // !Core

#endif
