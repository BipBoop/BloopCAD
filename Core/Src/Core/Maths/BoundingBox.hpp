
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef BLOOP_CORE_BOUNDINGBOX_HPP_
#define BLOOP_CORE_BOUNDINGBOX_HPP_

#include <Core/Curve/Line.hpp>

#include <glm/glm.hpp>

#include <vector>

namespace Core {

template<int dim>
struct BoundingBox {
  using vec_t = glm::vec<dim, double>;
  vec_t smoll, big;

  BoundingBox()
    : smoll(1)
    , big(-1)
  {
    
  }
  BoundingBox(vec_t point)
    : smoll(point)
    , big(point)
  {
  
  }
  BoundingBox(vec_t pt1, vec_t pt2)
  {
    for(int i = 0; i < dim; ++i) {
      smoll[i]  = std::min(pt1[i], pt2[i]);
      big[i]    = std::max(pt1[i], pt2[i]);
    }
  }
  // Creates a box with an origin and dimensions
  static BoundingBox<dim> make_dimensions(vec_t origin, vec_t dimensions)
  {
    return make_nosort(origin, origin+dimensions);
  }
  static BoundingBox<dim> make_nosort(vec_t smoll_, vec_t big_)
  {
    BoundingBox<dim> out;
    out.smoll = smoll_;
    out.big   = big_;
    return out;
  }
  bool is_point(double epsilon) const
  {
    for(int i = 0; i < dim; ++i) {
      if(!sepsilon_equal(smoll[i], big[i], epsilon))
        return false;
    }
    return true;
  }
  bool intersects(Core::Crv::LineAbst<dim> ln) const
  {
    return false;
  }
  bool contains(BoundingBox<dim> const& other) const
  {
    return contains(other.smoll) && contains(other.big);
  }
  bool overlaps(BoundingBox<dim> const& other) const
  {
    for(int i = 0; i < dim; ++i) {
      if(smoll[i] > other.big[i] || big[i] < other.smoll[i])
        return false;
    }
    return true;
  }
  // bool intersects(Crv::_2d::Line const& line) const;
  bool contains(vec_t const& point) const
  {
    for(int i = 0; i < dim; ++i) {
      if(point[i] <= smoll[i] || point[i] >= big[i])
        return false;
    }
    return true;
  }
  bool contains_incl(vec_t const& point) const
  {
    for(int i = 0; i < dim; ++i) {
      if(point[i] < smoll[i] || point[i] > big[i]) {
        return false;
      }
    }
    return true;
  }

  // Returns true if the point is on the border of the 
  // box within precision, only implemented for 2d for now
  bool point_onborder(vec_t const& point, double epsilon) const
  {
    return expand(epsilon).contains(point) && !expand(-epsilon).contains(point);
  }

  BoundingBox<dim> combine(BoundingBox<dim> const& other) const
  {
    if(!valid()) {
      return other;
    } else if(!other.valid()) {
      return *this;
    }
    return extend_unsafe(other.smoll).extend_unsafe(other.big);
  }
  BoundingBox<dim> extend(vec_t point) const
  {
    if(!valid()) {
      return BoundingBox<dim>(point);
    }
    return extend_unsafe(point);
  }
  BoundingBox<dim> extend_unsafe(vec_t point) const
  {
    BoundingBox<dim> out = *this;
    for(int i = 0; i < dim; ++i) {
      out.smoll[i] = std::min(out.smoll[i], point[i]);
      out.big[i]   = std::max(out.big[i], point[i]);
    }
    return out;
  }
  BoundingBox<dim> expand(double by) const
  {
    return BoundingBox<dim>::make_nosort(smoll-vec_t(by), big+vec_t(by));
  }
  vec_t center() const
  {
    return (smoll+big)/2.0;
  }

  // Normalize a vector of points within a box so each coordinate lends
  // in the [0, 1] range
  vec_t normalize(vec_t src) const
  {
    for(size_t j = 0; j < dim; ++j) {
      src[j] = clamp(map(src[j], smoll[j], big[j], 0.0, 1.0), 0.0, 1.0);
    }
    return src;
  }
  std::vector<vec_t> normalize(std::vector<vec_t> src) const
  {
    for(size_t i = 0; i < src.size(); ++i) {
      src[i] = normalize(src[i]);
    }
    return src;
  }

  // Could make sense in 3d, but currently not
  double area() const
  {
    return width() * height();
  }
  double width() const
  {
    return big.x - smoll.x;
  }
  double height() const
  {
    return big.y - smoll.y;
  }
  bool valid() const
  {
    for(int i = 0; i < dim; ++i) {
      if(smoll[i] > big[i])
        return false;
    }
    return true;
  }
  
  // Only really makes sense for 2d..
  glm::dvec2 topLeft() const
  {
    return glm::dvec2(smoll.x, big.y);
  }
  glm::dvec2 topRight() const
  {
    return glm::dvec2(big.x, big.y);
  }
  glm::dvec2 bottomLeft() const
  {
    return glm::dvec2(smoll.x, smoll.y);
  }
  glm::dvec2 bottomRight() const
  {
    return glm::dvec2(big.x, smoll.y);
  }
};

template<> bool BoundingBox<2>::intersects(Core::Crv::LineAbst<2> ln) const;

using dBox = BoundingBox<2>;

} // !Core

#endif
