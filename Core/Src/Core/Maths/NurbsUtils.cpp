
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "NurbsUtils.hpp"

#include "Utils.hpp"
#include <Core/Precision.hpp>

#include <iostream>

namespace Core {
namespace NurbsUtils {

constexpr int binom(int n, int k) noexcept
{
  // https://stackoverflow.com/questions/44718971/calculate-binomial-coffeficient-very-reliably
  return
    (        k> n  )? 0 :          // out of range
    (k==0 || k==n  )? 1 :          // edge
    (k==1 || k==n-1)? n :          // first
    (     k+k < n  )?              // recursive:
    (binom(n-1,k-1) * n)/k :       //  path to k=1   is faster
    (binom(n-1,k) * n)/(n-k);      //  path to k=n-1 is faster
}

constexpr int n_precomp_binom = 5;
constexpr int precomp_binom[n_precomp_binom][n_precomp_binom] = {
  { binom(0, 0), binom(0, 1), binom(0, 2), binom(0, 3), binom(0, 4) },
  { binom(1, 0), binom(1, 1), binom(1, 2), binom(1, 3), binom(1, 4) },
  { binom(2, 0), binom(2, 1), binom(2, 2), binom(2, 3), binom(2, 4) },
  { binom(3, 0), binom(3, 1), binom(3, 2), binom(3, 3), binom(3, 4) },
  { binom(4, 0), binom(4, 1), binom(4, 2), binom(4, 3), binom(4, 4) },
};

double binomial(int n, int k) noexcept
{
  if(n >= n_precomp_binom || k >= n_precomp_binom) {
    std::cerr<<"Big binomial.. ("<<n<<",  "<<k<<")"<<std::endl;
    return static_cast<double>(binom(n, k));
  }
  return static_cast<double>(precomp_binom[n][k]);
}

// A 1.2
double Bernstein(int ind, int degree, double t)
{
  if(degree == 0 || t < 0.0 || t > 1.0) {
    std::cerr<<__FUNCTION__<<", invalid parameters"<<std::endl;
    return 0.0;
  }
  if(ind > degree) {
    return 0.0;
  }
  if(ind == 0) {
    return 1.0;
  }

  KnotVec temp(degree+1, 0.0);
  temp[degree-ind] = 1.0;

  double t1 = 1.0-t;
  for(int k = ind; k <= degree; ++k) {
    for(int j = degree; j >= k; j--) {
      temp[j] = t1*temp[j] + t*temp[j-1];
    }
  }
  return temp[degree];
}

// A1.3
// std::vector<double> AllBernstein(int degree, double t)
// {
//   if(degree == 0 || t < 0.0 || t > 1.0) {
//     std::cerr<<__FUNCTION__<<", invalid parameters"<<std::endl;
//     return {};
//   }

//   std::vector<double> out(degree);
//   out[0] = 1.0;
//   double t1 = 1.0-t;
//   double saved;

//   for(int j = 1; j <= degree; ++j) {
//     for(int k = 0; k < j; ++k) {
//       double temp = out[k];
//       out[k] = saved + t1*temp;
//       saved = t * temp;
//     }
//     out[j] = saved;
//   }
//   return out;
// }

// A2.1
int knot_span(int degree, KnotVec const& knots, double u)
{
  int n = knots.size() - degree - 2;
  if(sepsilon_equal(u, knots[n+1], Prc::kNurbs)) {
    return n;
  }

  int low = degree;
  int high = n+1;
  int mid = (low+high) / 2;

  while(u < knots[mid] || u >= knots[mid+1]) {
    if(u < knots[mid]) {
      high = mid;
    } else {
      low = mid;
    }
    mid = (low+high) / 2;
  }
  return mid;
}
int knot_multiplicity(int degree, KnotVec const& knots, double u)
{
  int span = knot_span(degree, knots, u);
  int mult = 0;
  while(mult <= span && sepsilon_equal(knots[span-mult], u, Prc::kNurbs)) {
    mult++;
  }
  return mult;
}

int knot_vec_degree(KnotVec const& knots)
{
  if(knots.size() < 2 || knots[0] == knots.back()) {
    return 0;
  }
  if(knots.size() == 2) {
    return 1;
  }
  double first_knot = knots[0];
  int first_mult = 1;
  for(int i = 1; i < knots.size(); ++i, ++first_mult) {
    if(!sepsilon_equal(knots[i], first_knot, Prc::kNurbs)) {
      break;
    }
  }
  double last_knot = knots.back();
  int last_mult = 1;
  for(int i = knots.size()-2; i >= 0; --i, ++last_mult) {
    if(!sepsilon_equal(knots[i], last_knot, Prc::kNurbs)) {
      break;
    }
  }
  return std::min(last_mult, first_mult)-1;
}
int n_controls_for_knots(int degree, int n_knots)
{
  return n_knots - degree - 1;
}
int n_knots_for_controls(int degree, int n_controls)
{
  return n_controls + degree + 1;
}
int n_endOfCurve_knots(int degree)
{
  return degree+1;
}
KnotVec normalize(KnotVec vec)
{
  if(vec.size() < 2) {
    return vec;
  }
  std::sort(vec.begin(), vec.end());

  if(sepsilon_equal(vec[0], 0.0, Prc::kNurbs) && sepsilon_equal(vec.back(), 1.0, Prc::kNurbs))
    return vec;

  double map_start = vec[0];
  double map_end = vec.back();

  for(int i = 0; i < vec.size(); ++i) {
    vec[i] = map(vec[i], map_start, map_end, 0.0, 1.0);
  }
  return vec;
}
KnotVec reverseknots(KnotVec knots)
{
  KnotVec new_knots(knots.size());
  for(size_t i = 0; i < new_knots.size(); ++i) {
    new_knots[i] = map(knots[knots.size()-i-1], knots[0], knots.back(), knots.back(), knots[0]);
  }
  return new_knots;
}
// A2.2
std::vector<double> basis_fun(int ind, int degree, double u, KnotVec const& knots)
{
  std::vector<double> N(degree+1), left(degree+1), right(degree+1);
  N[0] = 1.0;

  double saved;
  for(int j = 1; j <= degree; ++j) {
    left[j] = u-knots[ind+1-j];
    right[j] = knots[ind+j]-u;
    double saved = 0.0;
    for(int r = 0; r < j; ++r) {
      double temp = N[r]/(right[r+1]+left[j-r]);
      N[r] = saved+right[r+1]*temp;
      saved=left[j-r]*temp;
    }
    N[j] = saved;
  }
  return N;
}
// A2.3
Eigen::MatrixXd basis_fun_deriv(int ind, int degree, double u, KnotVec const& knots, int up_to)
{
  Eigen::MatrixXd ndu(degree+1, degree+1);
  ndu(0, 0) = 1.0;

  Eigen::MatrixXd derivs(up_to+1, degree+1);
  Eigen::MatrixXd a(2, degree+1);

  std::vector<double> left(degree+1);
  std::vector<double> right(degree+1);

  double saved;
  for(int j = 1; j <= degree; ++j) {
    left[j] = u-knots[ind+1-j];
    right[j] = knots[ind+j]-u;
    saved = 0.0;
    for(int r = 0; r < j; ++r) {
      ndu(j, r) = right[r+1]+left[j-r];
      double temp = ndu(r, j-1) / ndu(j, r);

      ndu(r, j) = saved + right[r+1]*temp;
      saved = left[j-r]*temp;
    }
    ndu(j, j)= saved;
  }

  for(int j = 0; j <= degree; ++j) {
    derivs(0, j) = ndu(j, degree);
  }

  for(int r = 0; r <= degree; ++r) {
    int s1 = 0;
    int s2 = 1;
    a(0, 0) = 1.0;
    for(int k = 1; k <= up_to; ++k) {
      double d = 0.0;
      int rk = r-k;
      int pk = degree-k;
      int j1, j2;

      if(r >= k) {
        a(s2, 0)= a(s1, 0)/ndu(pk+1, rk);
        d = a(s2, 0) * ndu(rk, pk);
      }
      if(rk >= -1) {
        j1 = 1;
      } else {
        j1 = -rk;
      }
      if(r-1 <= pk) {
        j2 = k-1;
      } else {
        j2 = degree-r;
      }
      for(int j = j1; j <= j2; ++j) {
        a(s2, j) = (a(s1, j)-a(s1, j-1))/ndu(pk+1, rk+j);
        d += a(s2, j) * ndu(rk+j, pk);
      }
      if(r <= pk) {
        double a_s1_k1 = a(s1, k-1);
        double ndu_pk1_r = ndu(pk+1, r);
        double ndu_r_pk = ndu(r, pk);
        a(s2, k) = -a(s1, k-1)/ndu(pk+1, r);
        d += a(s2, k)*ndu(r, pk);
      }
      derivs(k, r) = d;
      std::swap(s1, s2);
    }
  }
  int r = degree;
  for(int k = 1; k <= up_to; ++k) {
    for(int j = 0; j <= degree; ++j) {
      derivs(k, j) *= r;
    }
    r *= (degree-k);
  }
  return derivs;
}

// A2.4
double one_basis_fun(int ind, int degree, KnotVec const& knots, double u)
{
  int m = knots.size() - 1;
  if( (ind == 0 && sepsilon_equal(u, knots[0], Prc::kNurbs)) || 
      (ind == degree && u == knots[m])) {
    return 1.0;
  }
  if(u < knots[ind] || u >= knots[ind+degree+1]) {
    return 0.0;
  }

  std::vector<double> N(degree+1);
  for(int j = 0; j <= degree; ++j) {
    if(u >= knots[ind+j] && u < knots[ind+j+1]) {
      N[j] = 1.0;
    } else {
      N[j] = 0.0;
    }
  }
  double saved;
  for(int k = 1; k <= degree; ++k) {
    if(N[0] == 0.0) {
      saved = 0.0;
    } else {
      saved = ((u-knots[ind]*N[0])/knots[ind+k]-knots[ind]);
    }
    for(int j = 0; j < degree-k+1; ++j) {
      double kLeft = knots[ind+j+1];
      double kRight = knots[ind+j+k+1];

      if(N[j+1] == 0.0) {
        N[j] = saved;
        saved = 0.0;
      } else {
        double temp = N[j+1] / (kRight - kLeft);
        N[j] = saved + (kRight-u) * temp;
        saved = (u-kLeft)*temp;
      }
    }
  }
  return N[0];
}

std::vector<double> one_basis_fun_deriv(int ind, int degree, KnotVec knots, double t, int up_to)
{
  std::vector<double> derivatives(up_to+1, 0.0);
  if(t < knots[ind] || t >= knots[ind+degree+1]) {
    return derivatives; // All zeroes
  }
  Eigen::MatrixXd N(up_to+1, degree+1);
  for(int j = 0; j <= degree; ++j) {
    if(t >= knots[ind+j] && t < knots[ind+j+1]) {
      N(j, 0) = 1.0;
    } else {
      N(j, 0) = 0.0;
    }
  }
  double saved = 0.0;
  for(int k = 1; k <= degree; ++k) {
    if(N(0, k-1) == 0.0) {
      saved = 0.0;
    } else {
      saved = ((t-knots[ind])*N(0, k-1))/(knots[ind+k]-knots[ind]);
    }

    for(int j = 0; j < degree-k+1; ++j) {
      double tLeft = knots[ind+j+1];
      double tRight = knots[ind+j+k+1];
      if(sepsilon_equal(N(j+1, k-1), 0.0, Prc::kNurbs)) {
        N(j, k) = saved;
        saved = 0.0;
      } else {
        double temp = N(j+1, k-1) / (tRight-tLeft);
        N(j, k) = saved+(tRight-t) * temp;
        saved = (t-tLeft) * temp;
      }
    }
  }
  derivatives[0] = N(0, degree);
  for(int k = 1; k <= up_to; ++k) {
    std::vector<double> ND(k+1, 0.0);

    for(int j = 0; j <= k; ++j) {
      ND[j] = N(j, degree-k);
    }
    for(int jj = 1; jj <= k; ++jj) {
      if(sepsilon_equal(ND[0], 0.0, Prc::kNurbs)) {
        saved = 0.0;
      } else {
        saved = ND[0]/(knots[ind+degree-k+jj] - knots[ind]);
      }
      for(int j = 0; j < k - jj + 1; ++j) {
        double tLeft = knots[ind+j+1];
        double tRight = knots[ind+j+degree+jj+1];

        if(sepsilon_equal(ND[j+1], 0.0, Prc::kNurbs)) {
          ND[j] = (degree-k+jj) * saved;
          saved = 0.0;
        } else {
          double temp = ND[j+1] / (tRight-tLeft);
          ND[j] = (degree-k+jj) * (saved-temp);
          saved = temp;
        }
      }
    }
    derivatives[k] = ND[0];
  }
  return derivatives;
}

} // !NurbsUtils
} // !Core