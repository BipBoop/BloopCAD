
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "Ray_3d.hpp"

namespace Core {

Ray Ray::transform(glm::dmat4 transf_mat) const
{
	return Ray{	.origin=glm::dvec3(glm::dvec4(origin, 1.0)*transf_mat),
							.dir=glm::dvec3(glm::dvec4(dir, 1.0)*transf_mat) };
}

glm::dvec3 intersection(Srf::Plane const& plane, Ray const& ray)
{
	// straight from https://en.wikipedia.org/wiki/Line%E2%80%93plane_intersection
	double t = (glm::dot(plane.norm, (plane.origin-ray.origin))) / (glm::dot(ray.dir, plane.norm));
	glm::dvec3 inter = ray.origin + ray.dir * t;
	return inter;
}

} // !Core