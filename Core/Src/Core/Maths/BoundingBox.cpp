
#include "BoundingBox.hpp"
#include "Core/Curve/Line.hpp"

namespace Core {

template<> bool BoundingBox<2>::intersects(Core::Crv::LineAbst<2> ln) const
{
  return contains(ln.pt1) || contains(ln.pt2) ||
  ln.intersection(Core::Crv::_2d::Line(smoll, {big.x, smoll.y})) || // horizontal down
  ln.intersection(Core::Crv::_2d::Line({big.x, smoll.y}, big)) || // vertical right
  ln.intersection(Core::Crv::_2d::Line(big, {smoll.x, big.y})) || // horizontal up
  ln.intersection(Core::Crv::_2d::Line({smoll.x, big.y}, smoll)); // vertical left
}

} // !Core
