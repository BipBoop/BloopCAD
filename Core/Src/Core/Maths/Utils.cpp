
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "Utils.hpp"

#include <glm/gtx/vector_angle.hpp>

#include <cmath>

namespace Core {

float diff_angles(float current, float target)
{
	float out = target - current + M_PI; // Add PI to simplify maths
				  // The floor term brings back the answer in a reasonnable range
	return (out - (std::floor(out / (M_PI * 2.0f)) * M_PI * 2.0f)) - M_PI; // Remove PI to cancel the added PI
}
double ccw_diff_angles(double horiz_angl1, double horiz_angl2)
{
	if(horiz_angl1 <= horiz_angl2) {
		return horiz_angl2 - horiz_angl1;
	}
	return 2.0*M_PI-horiz_angl1 + horiz_angl2;
}
double horizontal_angle(glm::dvec2 normVec)
{
	// Find out the angle of the three main components of the annotation
	// The angle is [0, 2PI] and is counter clockwise => (1, 0) is 0 and 2PI, (0.707, 0.707) is PI/4, etc.. 
	const glm::dvec2 horizontal(-1.0, 0.0); // Convenience horizontal vector
	return M_PI - glm::orientedAngle(normVec, horizontal);
}
double ccw_angle_between(glm::dvec2 from, glm::dvec2 to)
{
	return glm::orientedAngle(glm::normalize(from), glm::normalize(to));
}

bool ccw(glm::dvec2 A, glm::dvec2 B, glm::dvec2 C)
{
	return (C.y-A.y) * (B.x-A.x) > (B.y-A.y) * (C.x-A.x);
}

bool isAngleBetween(double angle, double a, double b)
{
	while(angle < 0.0) {
		angle += 2*M_PI;
	}
	while(angle > 2*M_PI) {
		angle -= 2*M_PI;
	}
	// angle = std::fmod(angle, 2*M_PI);

	if(a < b) {
		return angle >= a && angle <= b;
	}
	return angle >= a || angle <= b;
}

glm::vec3 as_3d(glm::dvec2 vert)
{
	return glm::vec3(vert.x, vert.y, 0.0);
}

} // !Core