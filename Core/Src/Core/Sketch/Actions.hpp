
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef BLOOP_CORE_SK_ACTIONS_HPP_
#define BLOOP_CORE_SK_ACTIONS_HPP_

#include "Kinds.hpp"
#include "Sketch.hpp"

#include <variant>
#include <optional>

namespace Core {
namespace Sk {

struct Move {
  glm::dvec2 delta;
  glm::dvec2 to;
  SibIdVec which;
};
struct SetRadius {
  SibId which;
  double newRad;
};
struct AddPoint {
  Point point;
};
struct AddLine {
  Line line;
};
struct AddCircle {
  Circle circle;
};
// struct AddArc {
//   Arc arc;
// };
struct Remove {
  SibIdVec which;
};

struct AddSymbolicConstraint {
  Kind::inner kind;
  SymbolicConstraint constr;
};
struct AddMeasureConstraint {
  Kind::inner kind;
  MeasureConstraint constr;
};
struct SetConstraintValue {
  SibId which;

  std::string expression;
  double value;
};

using Action = std::variant<Move, 
                            SetRadius, 
                            AddPoint, 
                            AddLine, 
                            AddCircle, 
                            AddSymbolicConstraint, 
                            AddMeasureConstraint, 
                            SetConstraintValue, 
                            Remove>;

Action name_action(Action act, Sketch const& currentSketch);

} // !Sk
} // !Core

#endif