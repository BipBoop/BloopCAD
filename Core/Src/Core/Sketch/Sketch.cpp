
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "Sketch.hpp"

#include <Core/Print/IDOstream.hpp>
#include <Core/Debug/Timer.hpp>
#include <Core/Utils/Paths.hpp>

namespace Core {
namespace Sk {

Sketch::Sketch(WorkpieceId wpid)
  : self_id(wpid)
  , origin_x(Point(-1.0, 0.0), Point(1.0, 0.0))
  , origin_y(Point(0.0, -1.0), Point(0.0, 1.0))
  , origin_point(0.0)
{

}

IdVec Sketch::children() const
{
  IdVec out;

  out.push_back({ .wpid=self_id, .entId=SibId{.kind=Kind::OriginLine,   .index=0 }});
  out.push_back({ .wpid=self_id, .entId=SibId{.kind=Kind::OriginLine,   .index=1 }});
  out.push_back({ .wpid=self_id, .entId=SibId{.kind=Kind::OriginPoint,  .index=0 }});

  for(auto point : points) {
    out.push_back({ .wpid=self_id, .entId=SibId{.kind=Kind::Point, .index=point.first.index }});
  }
  for(auto line : lines) {
    out.push_back({ .wpid=self_id, .entId=SibId{.kind=Kind::Line, .index=line.first.index }});
  }
  for(auto circle : circles) {
    out.push_back({ .wpid=self_id, .entId=SibId{.kind=Kind::Circle, .index=circle.first.index }});
  }

  sprites.ids(self_id, out);
  
  for(auto lLength : lLengths) {
    out.push_back({ .wpid=self_id, .entId={.kind=Kind::LineLength, .index=lLength.first.index}});
  }
  for(auto cDiameter : cDiameters) {
    out.push_back({ .wpid=self_id, .entId={.kind=Kind::CircleDiameter, .index=cDiameter.first.index}});
  }
  for(auto ppHdist : ppHdists) {
    out.push_back({ .wpid=self_id, .entId={.kind=Kind::PointPointHdist, .index=ppHdist.first.index}});
  }
  for(auto ppVdist : ppVdists) {
    out.push_back({ .wpid=self_id, .entId={.kind=Kind::PointPointVdist, .index=ppVdist.first.index}});
  }
  for(size_t i = 0; i < shapeBasis.shapes.size(); ++i) {
    out.push_back({ .wpid=self_id, .entId={.kind=Kind::Loopdiloop, .index=i}});
  }
  return out;
}
std::vector<Mesh> Sketch::meshes(SibId id) const
{
  switch(id.kind) {
  case Kind::Point:
    return { point_mesh(*condensed_get(id.index, points)) };
  case Kind::Line: {
    Line line = *condensed_get(id.index, lines);
    return {  line_mesh(line),
              point_mesh(line.pt1),
              point_mesh(line.pt2) };
  }
  case Kind::Circle: {
    Circle circle = *condensed_get(id.index, circles);
    return {  nurbs_mesh(to_nurbs(circle)),
              point_mesh(circle.center) };
  }
  case Kind::OriginLine:
    if(id.index == 0) {
      return { line_mesh(origin_x) };
    } else {
      return { line_mesh(origin_y) };
    }
  case Kind::OriginPoint:
    return { point_mesh(origin_point) };
  case Kind::LineLength: {
    auto constr = *condensed_get(id.index, lLengths);
    return { Mesh(), distanceAnnot_mesh(constr, id.kind), text_mesh(constr, id.kind) };
  }
  case Kind::PointPointHdist: {
    auto constr = *condensed_get(id.index, ppHdists);
    return { Mesh(), distanceAnnot_mesh(constr, id.kind), text_mesh(constr, id.kind) };
  }
  case Kind::PointPointVdist: {
    auto constr = *condensed_get(id.index, ppVdists);
    return { Mesh(), distanceAnnot_mesh(constr, id.kind), text_mesh(constr, id.kind) };
  }
  case Kind::CircleDiameter: {
    auto constr = *condensed_get(id.index, cDiameters);
    return { Mesh(), distanceAnnot_mesh(constr, id.kind), text_mesh(constr, id.kind) };
  }
  case Kind::Loopdiloop: {
    return { loop_mesh(id.index) };
  }
  case Kind::PointPointCoincident:
  case Kind::LineHorizontal:
  case Kind::LineVertical:
  case Kind::PointPointHorizontal:
  case Kind::PointPointVertical:
  case Kind::PointCircleCoincident:
  case Kind::LineLineParallel:
  case Kind::LineCircleTangeant:
  case Kind::PointLineCoincident:
  case Kind::LineLinePerpendicular: {
    
    auto specific_sprites = sprites.get(id.kind, id.index);
    if(specific_sprites.empty()) {
      return {};
    }
    std::vector<Mesh> meshes;
    meshes.push_back(Mesh()); // Empty mesh for constraint
    // sprite meshes for handles
    for(size_t i = 0; i < specific_sprites.size(); ++i) {
      meshes.push_back(offsetPointTexture_mesh(*specific_sprites[i]));
    }
    return meshes;
  }
  }
  std::cerr<<"Sketch could not resolve sibid "<<id<<" for mesh generation"<<std::endl;
  return {};
}
std::string Sketch::name_of(SibId id) const
{
  std::string name = "";
  switch(id.kind) {
  case Kind::Point:
    name = condensed_get(id.index, points).name;
    break;
  case Kind::Line:
    name = condensed_get(id.index, lines).name;
    break;    
  case Kind::Circle: {
    name = condensed_get(id.index, circles).name;
    break;
  }
  case Kind::OriginLine:
    name = id.index == 0 ? "origin_x" : "origin_y";
    break;
  case Kind::OriginPoint:
    name = "origin_point";
    break;
  case Kind::LineLinePerpendicular:
    name = "lineLinePerp";
    break;
  case Kind::LineLength:
    name = "lineLen";
    break;
  case Kind::PointPointHdist:
    name = "pointPoinHdist";
    break;
  case Kind::PointPointVdist:
    name = "pointPoinVdist";
    break;
  case Kind::CircleDiameter:
    name = "lineLen";
    break;
  case Kind::Loopdiloop:
    name = shapesNames[id.index];
    break;
  default:
    name = "no_matched_entity";
    break;
  }

  // Formats the name correctly if the handle_num
  // is not -1
  return handleName(name, id.handle_num);
}

Wp::VecsSnapshot Sketch::idsSnapshot() const
{
  using namespace Wp;
  VecsSnapshot snapshot;
  snapshot.push_back(Kind_indexVec{.kind=Kind::Point,   .indexVec=strip_inner(points)});
  snapshot.push_back(Kind_indexVec{.kind=Kind::Line,    .indexVec=strip_inner(lines)});
  snapshot.push_back(Kind_indexVec{.kind=Kind::Circle,  .indexVec=strip_inner(circles)});

  sprites.add_to_snapshot(snapshot);

  snapshot.push_back(Kind_indexVec{.kind=Kind::LineLength, .indexVec=strip_inner(lLengths)});
  snapshot.push_back(Kind_indexVec{.kind=Kind::PointPointHdist, .indexVec=strip_inner(ppHdists)});
  snapshot.push_back(Kind_indexVec{.kind=Kind::PointPointVdist, .indexVec=strip_inner(ppVdists)});
  snapshot.push_back(Kind_indexVec{.kind=Kind::CircleDiameter, .indexVec=strip_inner(cDiameters)});

  // std::cout<<"Sketchversion: "<<version<<"\n";
  Kind_indexVec looops { .kind=Kind::Loopdiloop };
  for(size_t i = 0; i < shapeBasis.shapes.size(); ++i) {
    looops.indexVec.push_back(VersionedIndex{.index=i, .version=version});
  }
  snapshot.push_back(looops);

  return snapshot;
}

DistanceAnnotSqueleton Sketch::measureConstraint_squeletonLine(MeasureConstraint constr, size_t kind) const
{
  switch(kind) {
  case Kind::LineLength: {
    auto geom_line = *get<Line>(*constr.first);
    glm::dvec2 normal_offset = geom_line.normal() * constr.annotDesc.locus_dist;
    return DistanceAnnotSqueleton { .pt1=geom_line.pt1, .pt2=geom_line.pt2, 
            .refLine=geom_line,
            .dimLine=Line(geom_line.pt1+normal_offset, geom_line.pt2+normal_offset)};
  }
  case Kind::PointPointHdist: {
    Point pt1 = *get<Point>(*constr.first);
    Point pt2 = *get<Point>(*constr.second);
    glm::dvec2 offset = glm::dvec2(0.0, 1.0) * (constr.annotDesc.locus_dist);
    double mid_y = (pt1.y + pt2.y) / 2.0;

    return DistanceAnnotSqueleton { .pt1=pt1, .pt2=pt2,
            .refLine=Line(glm::dvec2(pt1.x, mid_y), glm::dvec2(pt2.x, mid_y)),
            .dimLine=Line(glm::dvec2(pt1.x, mid_y)+offset, glm::dvec2(pt2.x, mid_y)+offset)};
  }
  case Kind::PointPointVdist: {
    Point pt1 = *get<Point>(*constr.first);
    Point pt2 = *get<Point>(*constr.second);
    glm::dvec2 offset = glm::dvec2(-1.0, 0.0) * (constr.annotDesc.locus_dist);
    double mid_x = (pt1.x + pt2.x) / 2.0;

    return DistanceAnnotSqueleton { .pt1=pt1, .pt2=pt2,
            .refLine=Line(glm::dvec2(mid_x, pt1.y), glm::dvec2(mid_x, pt2.y)),
            .dimLine=Line(glm::dvec2(mid_x, pt1.y)+offset, glm::dvec2(mid_x, pt2.y)+offset)};
  }
  case Kind::CircleDiameter: {
    Circle circ = *get<Circle>(*constr.first);
    Line centerLine(circ.center-glm::dvec2(circ.radius, 0.0), circ.center+glm::dvec2(circ.radius, 0.0));
    return DistanceAnnotSqueleton { .pt1=centerLine.pt1, .pt2=centerLine.pt2,
              .refLine=centerLine,
              .dimLine=Line(centerLine.pt1+glm::dvec2(0.0, constr.annotDesc.locus_dist), 
                            centerLine.pt2+glm::dvec2(0.0, constr.annotDesc.locus_dist))};
  }
  }
  std::cerr<<"Should not get here,, "<<__FUNCTION__<<",  "<<__LINE__<<std::endl;
  return DistanceAnnotSqueleton{};
}
// Get the measurement text position in the plane
glm::dvec2 Sketch::get_textpos(MeasureConstraint constr, size_t kind) const
{
  switch(kind) {
  case Kind::LineLength: 
  case Kind::PointPointHdist:
  case Kind::PointPointVdist:
  case Kind::CircleDiameter: {
    return measureConstraint_squeletonLine(constr, kind).dimLine.at(constr.annotDesc.text_lerp);
  }
  }
  return glm::dvec2(0.0);
}
// Creates the annot desc from a text position
MeasureConstraint::AnnotDesc Sketch::set_textpos(MeasureConstraint constr, size_t kind, glm::dvec2 pos) const
{
  switch(kind) {
  case Kind::LineLength: 
  case Kind::PointPointHdist:
  case Kind::PointPointVdist:
  case Kind::CircleDiameter: {
    auto squel = measureConstraint_squeletonLine(constr, kind);

    return MeasureConstraint::AnnotDesc{  .text_lerp=squel.refLine.closest_lerp(pos),
                                          .locus_dist=-squel.refLine.dist_signed(pos) };
  }
  }
  return MeasureConstraint::AnnotDesc{};
}
Srf::Plane Sketch::plane()
{
  return Srf::Plane { .origin=glm::dvec3(0.0), .norm=glm::dvec3(0.0, 0.0, 1.0) };
}
Srf::NurbsSurface Sketch::surface(dBox bbox)
{
  return Srf::NurbsSurface::make_flat_rectangle({ glm::dvec3(bbox.smoll, 0.0),
                                                  glm::dvec3(bbox.big.x, bbox.smoll.y, 0.0),
                                                  glm::dvec3(bbox.big, 0.0),
                                                  glm::dvec3(bbox.smoll.x, bbox.big.y, 0.0) });
}

Mesh Sketch::point_mesh(Point point_data) const
{
  return Mesh(Mesh::Kind::Point, {  static_cast<float>(point_data.x), 
                                    static_cast<float>(point_data.y), 
                                    0.0f }, { 0 });
}
Mesh Sketch::line_mesh(Line line_data) const
{
  return Mesh(Mesh::Kind::Curve, {  static_cast<float>(line_data.pt1.x), 
                                    static_cast<float>(line_data.pt1.y),
                                    0.0f,
                                    
                                    static_cast<float>(line_data.pt2.x),
                                    static_cast<float>(line_data.pt2.y),
                                    0.0f}, {0, 1});
}

Mesh Sketch::nurbs_mesh(Crv::_2d::Nurbs const& nurbs) const
{
  std::vector<float> vertices_data;
  std::vector<unsigned int> indices;

  auto disc_data = nurbs.discretize_n(100);

  for(size_t i = 0; i < disc_data.size(); ++i) {
    vertices_data.push_back(disc_data[i].x);
    vertices_data.push_back(disc_data[i].y);
    vertices_data.push_back(0.0f);
    indices.push_back(i);
  }

  return Mesh(Mesh::Kind::Curve, vertices_data, indices);
}

Mesh Sketch::offsetPointTexture_mesh(SpriteHandle handle) const
{
  Mesh mesh(Mesh::Kind::OffsetPointTexture, {
                                                static_cast<float>(handle.pos.x),
                                                static_cast<float>(handle.pos.y),
                                                0.0f,
                                                static_cast<float>(handle.offset_px.x),
                                                static_cast<float>(handle.offset_px.y)}, 
                                                {0});
  mesh.textureKind = Mesh::TextureKind::Texture;
  mesh.texture = handle.sprite;
  return mesh;
}
Mesh Sketch::distanceAnnot_mesh(MeasureConstraint constr, size_t kind) const
{
  auto squel = measureConstraint_squeletonLine(constr, kind);

  glm::dvec2 text = squel.dimLine.at(constr.annotDesc.text_lerp);

  return Mesh(Mesh::Kind::AnnotLine, {
                                        static_cast<float>(squel.pt1.x),
                                        static_cast<float>(squel.pt1.y),
                                        0.0f,
                                        -30.0f,
                                        
                                        static_cast<float>(squel.dimLine.pt1.x),
                                        static_cast<float>(squel.dimLine.pt1.y),
                                        0.0f,
                                        30.0f,

                                        static_cast<float>(squel.dimLine.pt1.x),
                                        static_cast<float>(squel.dimLine.pt1.y),
                                        0.0f,
                                        0.0f,

                                        static_cast<float>(text.x),
                                        static_cast<float>(text.y),
                                        0.0f,
                                        -60.0f,

                                        static_cast<float>(squel.dimLine.pt2.x),
                                        static_cast<float>(squel.dimLine.pt2.y),
                                        0.0f,
                                        0.0f,

                                        static_cast<float>(squel.dimLine.pt2.x),
                                        static_cast<float>(squel.dimLine.pt2.y),
                                        0.0f,
                                        30.0f,

                                        static_cast<float>(squel.pt2.x),
                                        static_cast<float>(squel.pt2.y),
                                        0.0f,
                                        -30.0f,


                        }, {0, 1, 2, 3, 3, 4, 5, 6});

}
Mesh Sketch::text_mesh(MeasureConstraint constr, size_t kind) const
{
  glm::dvec2 pos = get_textpos(constr, kind);
  Mesh mesh(Mesh::Kind::OffsetPointTexture, {
                                                static_cast<float>(pos.x),
                                                static_cast<float>(pos.y),
                                                0.0f,
                                                0.0f,
                                                0.0f}, 
                                                {0});
  mesh.textureKind = Mesh::TextureKind::Text;
  mesh.texture = std::to_string(constr.value);
  return mesh;
}
Mesh Sketch::loop_mesh(int shape_index) const
{
  Dbg::Timer tim(std::string(__FUNCTION__) + std::to_string(shape_index));
  Crv::_2d::ShapeWithHoles shape = shapeBasis.shapeWithHoles(shape_index);
  
  return Srf::Surface(surface(shape.boundingBox()), shape).to_mesh();
}
Wp::Diff Sketch::account_relationships(Wp::Diff diff) const
{
  diff.modified = add_workpiece(account_relationships(strip_workpiece(diff.modified)), self_id);
  return diff;
}
SibIdVec Sketch::account_relationships(SibIdVec ids) const
{
  size_t n_mod = ids.size();
  for(size_t i = 0; i < n_mod; ++i) {
    auto constr_associated = geom_to_constraints.find(ids[i]);
    if(constr_associated == geom_to_constraints.end())
      continue;
    for(auto constr : constr_associated->second) {
      ids.push_back(constr);
    }
  }
  // Remove potential duplicates,, unlikely but whatevs
  auto it = std::unique(ids.begin(), ids.end()); 
  ids.erase(it, ids.end()); 
  
  return ids;
}

template<>
std::optional<Point> Sketch::get(SibId sid) const
{
  if(sid.kind == Kind::OriginPoint) {
    return origin_point;
  } else if(sid.kind == Kind::Point) {
    return *condensed_get(sid.index, points);

    std::cerr<<"index "<<sid.index<<" invalid for get<Point>()"<<std::endl;
    return std::nullopt;
  }
  if(sid.handle_num != -1 && sid.kind == Kind::Line) {
    auto ln = get<Line>(sid);
    if(ln) {
      if(sid.handle_num == 0) {
        return ln->pt1;
      } else {
        return ln->pt2;
      }
    }
  }
  if(sid.handle_num != -1 && sid.kind == Kind::Circle) {
    auto circle = get<Circle>(sid);
    if(circle) {
      return circle->center;
    }
  }
  std::cerr<<"Id "<<sid<<" invalid for cast into point"<<std::endl;
  return std::nullopt;
}
template<>
std::optional<Line> Sketch::get(SibId sid) const
{ 
  if(sid.kind == Kind::OriginLine) {
    return sid.index == 0 ? origin_x : origin_y;
  } else if(sid.kind == Kind::Line) {
    return *condensed_get(sid.index, lines);

    std::cerr<<"index "<<sid.index<<" invalid for get<Line>()"<<std::endl;
    return std::nullopt;
  }
  std::cerr<<"Id "<<sid<<" invalid for cast into line"<<std::endl;
  return std::nullopt;
}
template<>
std::optional<Circle> Sketch::get(SibId sid) const
{ 
  if(sid.kind == Kind::Circle) {
    return *condensed_get(sid.index, circles);

    std::cerr<<"index "<<sid.index<<" invalid for get<Circle>()"<<std::endl;
    return std::nullopt;
  }
  std::cerr<<"Id "<<sid<<" invalid for cast into circle"<<std::endl;
  return std::nullopt;
}
template<> 
std::optional<Srf::Surface> Sketch::get(SibId sid) const
{
  if(sid.kind == Kind::Loopdiloop) {
    Crv::_2d::ShapeWithHoles shape = shapeBasis.shapeWithHoles(sid.index);
  
    return Srf::Surface(surface(shape.boundingBox()), shape);
  }
  std::cerr<<"Id "<<sid<<" invalid for cast into surface"<<std::endl;
  return std::nullopt;
}

std::optional<Id> Sketch::find(std::string name, Kind::inner hint) const
{
  std::optional<SibId> maybe_sib = find_sib(name, hint);

  if(maybe_sib) {
    return Id{.wpid=self_id, .entId=*maybe_sib};
  }
  return std::nullopt;
}
std::optional<Id> Sketch::find(std::string name) const
{
  std::optional<SibId> maybe_sib = find_sib(name);

  if(maybe_sib) {
    return Id{.wpid=self_id, .entId=*maybe_sib};
  }
  return std::nullopt;
}
std::optional<SibId> Sketch::find_sib(std::string name, Kind::inner hint) const
{
  auto parsed_name = entityName_and_handleNum(name);

  std::optional<SibId> maybe_ent = find_sib_impl(parsed_name.first, hint);

  if(maybe_ent) {
    maybe_ent->handle_num = parsed_name.second;
  }
  return maybe_ent;
}
std::optional<SibId> Sketch::find_sib_impl(std::string name, Kind::inner hint) const
{
  size_t find_index;

  switch(hint) {
  case Kind::OriginPoint:
    if(name == "origin_point") {
      return SibId{.kind=Kind::OriginPoint, .index=0};
    }
    break;
  case Kind::OriginLine:
    if(name == "origin_x") {
      return SibId{.kind=Kind::OriginLine, .index=0};
    } else if(name == "origin_y") {
      return SibId{.kind=Kind::OriginLine, .index=1};
    }
    break;
  case Kind::Point:
    find_index = find_by_name(points, name);
    if(find_index != invalid_index()) {
      return SibId{.kind=Kind::Point, .index=find_index};
    }
    break;
  case Kind::Line:
    find_index = find_by_name(lines, name);
    if(find_index != invalid_index()) {
      return SibId{.kind=Kind::Line, .index=find_index};
    }
    break;
  case Kind::Circle:
    find_index = find_by_name(circles, name);
    if(find_index != invalid_index()) {
      return SibId{.kind=Kind::Circle, .index=find_index};
    }
  case Kind::Loopdiloop:
    for(size_t i = 0; i < shapesNames.size(); ++i) {
      if(name == shapesNames[i]) {
        return SibId{.kind=Kind::Loopdiloop, .index=i};
      }
      break;
    }
    break;
  default:
    break;
  }
  return std::nullopt;
}
std::optional<SibId> Sketch::find_sib(std::string name) const
{
  std::optional<SibId> out;
  auto parsed_name = entityName_and_handleNum(name);

  out = find_sib_impl(parsed_name.first, Kind::OriginPoint);
  if(out) { out->handle_num = parsed_name.second; return out; }

  out = find_sib_impl(parsed_name.first, Kind::OriginLine);
  if(out) { out->handle_num = parsed_name.second; return out; }

  out = find_sib_impl(parsed_name.first, Kind::OriginLine);
  if(out) { out->handle_num = parsed_name.second; return out; }

  out = find_sib_impl(parsed_name.first, Kind::Line);
  if(out) { out->handle_num = parsed_name.second; return out; }

  out = find_sib_impl(parsed_name.first, Kind::Point);
  if(out) { out->handle_num = parsed_name.second; return out; }

  out = find_sib_impl(parsed_name.first, Kind::Circle);
  if(out) { out->handle_num = parsed_name.second; return out; }

  out = find_sib_impl(parsed_name.first, Kind::Loopdiloop);
  if(out) { out->handle_num = parsed_name.second; return out; }

  std::cerr<<"Could not resolve name "<<name<<std::endl;
  return std::nullopt;
}

SpriteHandleRegistry::SpriteHandleRegistry()
{
  for(size_t i = 0; i < kind_starts.size(); ++i) {
    kind_starts[i] = { invalid_index(), invalid_index() };
  }
}
void SpriteHandleRegistry::add_to_snapshot(Wp::VecsSnapshot& snap) const
{
  for(size_t i = 0; i < kind_starts.size(); ++i) {
    Wp::Kind_indexVec::indexvec_t indexvec;

    for(size_t j = kind_starts[i].first; j < kind_starts[i].second; ++j) {
      if(sprites[j].second->twin == 0 || sprites[j].second->twin == 1) {
        indexvec.push_back(sprites[j].first);
      }
    }
    snap.push_back(Wp::Kind_indexVec { .kind=symbolicConstraint_for_index(i), .indexVec=indexvec });
  }
}
void SpriteHandleRegistry::ids(WorkpieceId sk_id, IdVec& ids) const
{
  for(size_t i = 0; i < kind_starts.size(); ++i) {
    Wp::Kind_indexVec::indexvec_t indexvec;
    
    for(size_t j = kind_starts[i].first; j < kind_starts[i].second; ++j) {
      if(sprites[j].second->twin == 0 || sprites[j].second->twin == 1) {
        ids.push_back(Id { .wpid=sk_id, .entId=SibId{ .kind=symbolicConstraint_for_index(i), .index=sprites[j].first.index}});
      }
    }
  }
}
std::vector<Named<SpriteHandle>> SpriteHandleRegistry::get(size_t kind, size_t index) const
{
  auto range = kind_starts[index_of_symbolicConstraint(kind)];

  // Looping over all sprites of a kind and taking
  // all matching indices makes twins work, but is
  // not really elegant..
  std::vector<Named<SpriteHandle>> out;
  for(size_t i = range.first; i < range.second; ++i) {
    if(sprites[i].first.index == index) {
      out.push_back(sprites[i].second);
    }
  }
  return out;
}

} // !Sk
} // !Core