
#include "Kinds.hpp"

namespace Core {
namespace Sk {

// these two functions must stay synced
size_t index_of_symbolicConstraint(Kind::inner symb_constraint)
{
  switch(symb_constraint) {
  case Kind::PointPointCoincident:  return 0;
  case Kind::LineCircleTangeant:    return 1;
  case Kind::LineHorizontal:        return 2;
  case Kind::LineVertical:          return 3;
  case Kind::PointCircleCoincident: return 4;
  case Kind::LineLinePerpendicular: return 5;
  case Kind::LineLineParallel:      return 6;
  case Kind::PointPointHorizontal:  return 7;
  case Kind::PointPointVertical:    return 8;
  default:
    return invalid_index();
  }
}
size_t index_of_symbolicConstraint(size_t symb_constraint)
{
  return index_of_symbolicConstraint(static_cast<Kind::inner>(symb_constraint));
}
Kind::inner symbolicConstraint_for_index(size_t index)
{
  switch(index) {
  case 0: return Kind::PointPointCoincident;
  case 1: return Kind::LineCircleTangeant;
  case 2: return Kind::LineHorizontal;
  case 3: return Kind::LineVertical;
  case 4: return Kind::PointCircleCoincident;
  case 5: return Kind::LineLinePerpendicular;
  case 6: return Kind::LineLineParallel;
  case 7: return Kind::PointPointHorizontal;
  case 8: return Kind::PointPointVertical;
  default:
    return Kind::inner{};
  }  
}
std::string name_kind(Kind::inner kind)
{
  switch(kind) {
  case Kind::Point: return "Point";
  case Kind::Line: return "Line";
  case Kind::Circle: return "Circle";
  case Kind::Arc: return "Arc";
  case Kind::OriginLine: return "OriginLine";
  case Kind::OriginPoint: return "OriginPoint";
  case Kind::Loopdiloop: return "Loopdiloop";
  case Kind::PointPointHdist: return "PointPointHdist";
  case Kind::PointPointVdist: return "PointPointVdist";
  case Kind::PointLineDist: return "PointLineDist";
  case Kind::PointLineCoincident: return "PointLineCoincident";
  case Kind::LineLineAngle: return "LineLineAngle";
  case Kind::LineLineDist: return "LineLineDist";
  case Kind::LineLength: return "LineLength";
  case Kind::CircleRadius: return "CircleRadius";
  case Kind::CircleDiameter: return "CircleDiameter";
  case Kind::PointCircleDist: return "PointCircleDist";
  case Kind::PointPointCoincident: return "PointPointCoincident";
  case Kind::LineCircleTangeant: return "LineCircleTangeant";
  case Kind::LineHorizontal: return "LineHorizontal";
  case Kind::LineVertical: return "LineVertical";
  case Kind::PointPointHorizontal: return "PointPointHorizontal";
  case Kind::PointPointVertical: return "PointPointVertical";
  case Kind::PointCircleCoincident: return "PointCircleCoincident";
  case Kind::LineLinePerpendicular: return "LineLinePerpendicular";
  case Kind::LineLineParallel: return "LineLineParallel";
  }
}
bool is_geom(Kind::inner kind)
{
  return is_geom(static_cast<size_t>(kind));
}
bool is_geom(size_t kind)
{
  switch(kind) {
  case Kind::Point:
  case Kind::Line:
  case Kind::Circle:
  case Kind::Arc:
    return true;
  default:
    return false;
  }
}
bool is_geom(SibId id)
{
  return is_geom(id.kind);
}
bool is_point(SibId id)
{
  return id.kind == Kind::Point || (is_geom(id) && id.handle_num != -1);
}
bool is_line(SibId id)
{
  return !is_point(id) && id.kind == Kind::Line;
}
bool is_circle(SibId id)
{
  return !is_point(id) && id.kind == Kind::Circle;
}
bool is_curve(SibId id)
{
  return !is_point(id) && is_curve(id.kind);
}
bool is_curve(size_t kind)
{
  return kind == Kind::Line || Kind::Circle || Kind::Arc;
}

// Orders the geometries in the order of Kind
// Point, Line, Circle, Arc, 
// It also considers handles as point
IdVec order_geometries(IdVec src)
{
  std::sort(src.begin(), src.end(), [](Core::Id const& ida, Core::Id const& idb) { 
    size_t kind_a = ida.entId.handle_num == -1 ? ida.entId.kind : Kind::Point;
    size_t kind_b = idb.entId.handle_num == -1 ? idb.entId.kind : Kind::Point;
    return kind_a < kind_b; 
  });
  return src;
}

} // !Sk
} // !Core