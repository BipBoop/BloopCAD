
#ifndef BLOOP_CORE_SK_IO_TOSVG_HPP_
#define BLOOP_CORE_SK_IO_TOSVG_HPP_

#include <Core/Sketch/Factory/Factory.hpp>

#include <string>

namespace Core {
namespace Sk {

std::string to_svg(Sk::Factory const& sk_factory);

} // !Sk
} // !Core

#endif