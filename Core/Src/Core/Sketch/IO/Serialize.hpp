
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
#ifndef BLOOP_CORE_SK_IO_SERIALIZE_HPP_
#define BLOOP_CORE_SK_IO_SERIALIZE_HPP_

#include <Core/Sketch/Factory/Factory.hpp>
#include <Core/Serialize/Serialize_glm.hpp>

#include <yaml-cpp/yaml.h>

namespace YAML {

using namespace Core;

using Point = glm::dvec2;
using Crv::_2d::Line;
using Crv::_2d::Circle;

template<>
struct convert<Named<Point>> {
  static Node encode(Named<Point> const& rhs) 
  {
    Node node;
    node["pos"] = *rhs;
    node["name"] = rhs.name;
    
    return node;
  }

  static bool decode(Node const& node, Named<Point>& rhs) 
  {
    if(!node.IsMap() || node.size() != 2 && node["pos"] && node["name"]) {
      return false;
    }
    rhs = Named<Point>(node["pos"].as<Point>(), node["name"].as<std::string>());
    // std::cout<<"Found point name: "<<rhs.name<<"\n";
    return true;
  }
};
template<>
struct convert<Named<Line>> {
  static Node encode(Named<Line> const& rhs) 
  {
    Node node;
    node["pos"].push_back(rhs->pt1);
    node["pos"].push_back(rhs->pt2);
    node["name"] = rhs.name;
    return node;
  }

  static bool decode(Node const& node, Named<Line>& rhs) 
  {
    Node posNode, nameNode;
    if(!(posNode = node["pos"]) || !posNode.IsSequence() || posNode.size() != 2 || !(nameNode = node["name"])) {
      return false;
    }
    
    rhs.name = nameNode.as<std::string>();
    // std::cout<<"Found line name: "<<rhs.name<<"\n";
    rhs->pt1 = posNode[0].as<glm::dvec2>();
    rhs->pt2 = posNode[1].as<glm::dvec2>();
    return true;
  }
};
template<>
struct convert<Named<Circle>> {
  static Node encode(Named<Circle> const& rhs) 
  {
    Node node;
    node["pos"] = rhs->center;
    node["radius"] = rhs->radius;
    node["name"] = rhs.name;
    return node;
  }

  static bool decode(Node const& node, Named<Circle>& rhs) 
  {
    if(!node.IsMap() || node.size() != 3) {
      return false;
    }
    rhs->center = node["pos"].as<glm::dvec2>();
    rhs->radius = node["radius"].as<double>();
    rhs.name = node["name"].as<std::string>();
    return true;
  }
};
template<>
struct convert<Sk::SymbolicConstraint::HandleDesc> {
  static Node encode(Sk::SymbolicConstraint::HandleDesc const& annot)
  {
    Node node;

    node["slot"] = annot.geom_slot;
    node["offset"] = annot.planeoffset;

    return node;
  }
  static bool decode(Node const& node, Sk::SymbolicConstraint::HandleDesc& rhs)
  {
    if(!node.IsMap() || node.size() != 2) {
      return false;
    }

    rhs.geom_slot = node["slot"].as<int>();
    rhs.planeoffset = node["offset"].as<glm::dvec2>();

    return true;
  }
};

template<>
struct convert<Named<Sk::SymbolicConstraint>> {
  static Node encode(Named<Sk::SymbolicConstraint> const& constr)
  {
    Node node;

    node["first"] = constr->first.name;
    node["first_h"] = constr->first_hdesc;
    if(constr->second->valid()) {
      node["second"] = constr->second.name;
      node["second_h"] = constr->second_hdesc;
    }
    node["name"] = constr.name;

    return node;
  }
  static bool decode(Node const& node, Named<Sk::SymbolicConstraint>& rhs)
  {
    if(!node.IsMap()) {
      return false;
    }

    rhs->first.name = node["first"].as<std::string>();
    rhs->first_hdesc = node["first_h"].as<Sk::SymbolicConstraint::HandleDesc>();

    if(node["second"]) {
      rhs->second.name = node["second"].as<std::string>();
    }
    if(node["second_h"]) {
      rhs->second_hdesc = node["second_h"].as<Sk::SymbolicConstraint::HandleDesc>();
    }

    rhs.name = node["name"].as<std::string>();

    return true;
  }
};
template<>
struct convert<Named<Sk::MeasureConstraint>> {
  static Node encode(Named<Sk::MeasureConstraint> const& constr)
  {
    Node node;

    node["first"] = constr->first.name;
    if(constr->second->valid()) {
      node["second"] = constr->second.name;
    }
    node["val"] = constr->value;
    node["exp"] = constr->expression;

    node["txt_lerp"] = constr->annotDesc.text_lerp;
    node["locus"] = constr->annotDesc.locus_dist;

    node["name"] = constr.name;

    return node;
  }
  static bool decode(Node const& node, Named<Sk::MeasureConstraint>& rhs)
  {
    if(!node.IsMap()) {
      return false;
    }

    rhs->first.name = node["first"].as<std::string>();
    if(node["second"]) {
      rhs->second.name = node["second"].as<std::string>();
    }
    rhs->expression = node["exp"].as<std::string>();
    rhs->value = node["val"].as<double>();

    rhs->annotDesc.text_lerp = node["txt_lerp"].as<double>();
    rhs->annotDesc.locus_dist = node["locus"].as<double>();
    
    rhs.name = node["name"].as<std::string>();

    return true;
  }
};
}  // !YAML

namespace Core {

YAML::Node encode(Sk::Factory const& sk_factory, std::map<WorkpieceId, std::string> const& wpids_to_locanames);

bool decode(YAML::Node const& node, Sk::Factory& skfact, WorkpieceId prt_id, std::string filename, 
            std::unordered_map<std::string, WorkpieceId> const& locanames_to_wpids);

} // !Core

#endif