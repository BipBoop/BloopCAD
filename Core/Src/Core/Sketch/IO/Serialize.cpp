
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "Serialize.hpp"

namespace Core {

YAML::Node encode(Sk::Factory const& sk_factory, std::map<WorkpieceId, std::string> const& wpids_to_locanames)
{
  YAML::Node node;
  node["kind"]      = "sketch";
  node["points"]    = sk_factory.geometries.points.all_valid();
  node["lines"]     = sk_factory.geometries.lines.all_valid();
  node["circles"]   = sk_factory.geometries.circles.all_valid();
  node["llPerps"]     = sk_factory.constraints.llPerps.all_valid();
  node["lHorizs"]     = sk_factory.constraints.lHorizs.all_valid();
  node["lVerts"]      = sk_factory.constraints.lVerts.all_valid();
  node["ppHorizs"]     = sk_factory.constraints.ppHorizs.all_valid();
  node["ppVerts"]      = sk_factory.constraints.ppVerts.all_valid();
  node["ppCoincids"]  = sk_factory.constraints.ppCoincids.all_valid();
  node["lLengths"]    = sk_factory.constraints.lLengths.all_valid();
  node["ppHdists"]    = sk_factory.constraints.ppHdists.all_valid();
  node["ppVdists"]    = sk_factory.constraints.ppVdists.all_valid();
  node["cDiameters"]    = sk_factory.constraints.cDiameters.all_valid();

  return node;
}

template<typename cT>
void fill_constraint_geomIds(std::vector<Named<cT>>& constraints, Sk::Sketch const& sketch_skeleton, bool two_geoms)
{
  for(size_t i = 0; i < constraints.size(); ++i) {
    auto found_id = sketch_skeleton.find(constraints[i]->first.name);
    if(!found_id) {
      std::cerr<<"Could not decode constraint geometry "<<constraints[i]->first.name;
      continue;
    }
    *constraints[i]->first = *found_id;

    if(!two_geoms)
      continue;

    found_id = sketch_skeleton.find(constraints[i]->second.name);
    if(!found_id) {
      std::cerr<<"Could not decode constraint geometry "<<constraints[i]->second.name;
      continue;
    }
    *constraints[i]->second = *found_id;

  }
}
bool decode(YAML::Node const& node, Sk::Factory& skfact, WorkpieceId sk_id, std::string filename, 
            std::unordered_map<std::string, WorkpieceId> const& locanames_to_wpids)
{
  using symbvec_t = std::vector<Named<Sk::SymbolicConstraint>>;

  try {
    auto points  = node["points"].as<std::vector<Named<glm::dvec2>>>();
    auto lines   = node["lines"].as<std::vector<Named<Sk::Line>>>();
    auto circles = node["circles"].as<std::vector<Named<Sk::Circle>>>();

    skfact = Sk::Factory(Sk::Geometries(points, lines, circles), Sk::Constraints(), sk_id);
    bool gargbage;
    Sk::Sketch tmp_sketch = skfact.build(gargbage).first;

    // symbvec_t llPerps, lHorizs, lVerts, ppHorizs, ppVerts, ppCoincids;

    auto llPerps = node["llPerps"].as<symbvec_t>();
    fill_constraint_geomIds(llPerps, tmp_sketch, true);

    auto lHorizs = node["lHorizs"].as<symbvec_t>();
    fill_constraint_geomIds(lHorizs, tmp_sketch, false);

    auto lVerts = node["lVerts"].as<symbvec_t>();
    fill_constraint_geomIds(lVerts, tmp_sketch, false);

    auto ppHorizs = node["ppHorizs"].as<symbvec_t>();
    fill_constraint_geomIds(ppHorizs, tmp_sketch, true);

    auto ppVerts = node["ppVerts"].as<symbvec_t>();
    fill_constraint_geomIds(ppVerts, tmp_sketch, true);

    auto ppCoincids = node["ppCoincids"].as<symbvec_t>();
    fill_constraint_geomIds(ppCoincids, tmp_sketch, true);

    auto lLengths = node["lLengths"].as<std::vector<Named<Sk::MeasureConstraint>>>();
    fill_constraint_geomIds(lLengths, tmp_sketch, false);

    auto ppHdists = node["ppHdists"].as<std::vector<Named<Sk::MeasureConstraint>>>();
    fill_constraint_geomIds(ppHdists, tmp_sketch, true);

    auto ppVdists = node["ppVdists"].as<std::vector<Named<Sk::MeasureConstraint>>>();
    fill_constraint_geomIds(ppVdists, tmp_sketch, true);

    auto cDiameters = node["cDiameters"].as<std::vector<Named<Sk::MeasureConstraint>>>();
    fill_constraint_geomIds(cDiameters, tmp_sketch, false);


    skfact = Sk::Factory(skfact.geometries, Sk::Constraints(llPerps, lHorizs, lVerts, ppHorizs, ppVerts, ppCoincids, lLengths, ppHdists, ppVdists, cDiameters), sk_id);
    return true;
  } catch (std::exception& e) {
    std::cerr<<"Failed to decode sketch factory "<<e.what()<<std::endl;
    return false;
  }
}

}  // !Core