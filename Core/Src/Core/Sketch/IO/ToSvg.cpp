
#include "ToSvg.hpp"

#include <simple-svg/simple_svg_1.0.0.hpp>

namespace Core {
namespace Sk {

std::string to_svg(Sk::Factory const& sk_factory)
{
  double stroke_width = 0.1; // mm;
  auto box = sk_factory.curveBox().expand(stroke_width / 2.0);
  glm::dvec2 offset = box.bottomLeft();// - glm::dvec2(stroke_width / 2.0);

  svg::Dimensions dimensions(box.width(), box.height());
  svg::Document doc(svg::Layout("mm", dimensions, svg::Layout::BottomLeft));
  svg::Stroke stroke(stroke_width, svg::Color(svg::Color::Black));

  // sk_factory.geometries.points.for_each([&](Named<Point> point, size_t i, size_t version) {
  //   doc<<svg::Point(point->x, point->y);
  //   return true;
  // });

  sk_factory.geometries.lines.for_each([&](Named<Line> line, size_t i, size_t version) {
    svg::Point pt1(line->pt1.x - offset.x, line->pt1.y - offset.y);
    svg::Point pt2(line->pt2.x - offset.x, line->pt2.y - offset.y);
    doc<<svg::Line(pt1, pt2, stroke);
    return true;
  });
  sk_factory.geometries.circles.for_each([&](Named<Circle> circle, size_t i, size_t version) {
    svg::Point center(circle->center.x - offset.x, circle->center.y - offset.y);
    doc<<svg::Circle(center, circle->radius * 2, svg::Fill(), stroke);
    return true;
  });
  return doc.toString();
}

} // !Sk
} // !Core