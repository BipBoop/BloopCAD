
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef BLOOP_CORE_SK_KINDS_HPP_
#define BLOOP_CORE_SK_KINDS_HPP_

#include <Core/Id.hpp>

#include <Core/Curve/Line.hpp>
#include <Core/Curve/Circle.hpp>

#include <glm/glm.hpp>

namespace Core {
namespace Sk {

using Point = glm::dvec2;
using Crv::_2d::Line;
using Crv::_2d::Circle;

struct SpriteAnnot {
  glm::dvec2 pos; // Position on plane
  glm::dvec2 offset_px;
  std::string sprite;
  SibId annotOf;
};

// Handle which is a sprite
struct SpriteHandle {
  glm::dvec2 pos;
  glm::dvec2 offset_px;
  std::string sprite;
  int twin { 0 }; // 1 twin is after in the list, -1 twin is before in the list
};
struct SymbolicConstraint {
  struct HandleDesc {
    int geom_slot;
    glm::dvec2 planeoffset;
  };

  Named<SibId> first;
  Named<SibId> second;

  HandleDesc first_hdesc;
  HandleDesc second_hdesc;
};
struct MeasureConstraint {
  struct AnnotDesc {
    double text_lerp;
    double locus_dist;
  };

  Named<SibId> first;
  Named<SibId> second;

  AnnotDesc annotDesc;

  double value;
  std::string expression;
};
struct DistanceAnnotSqueleton {
  Point pt1, pt2;
  Line refLine;
  Line dimLine;
};

struct Kind {
  enum inner {  
                // Geometries
                Point, Line, Circle, Arc, OriginLine, OriginPoint, Loopdiloop,

                // Dimension constraints
                PointPointHdist, PointPointVdist, 
                PointLineDist, PointLineCoincident,
                LineLineAngle, 
                LineLineDist, LineLength,
                CircleRadius, CircleDiameter,
                PointCircleDist,
                // symbolic constraints
                PointPointCoincident, LineCircleTangeant, LineHorizontal, LineVertical, 
                PointCircleCoincident,
                LineLinePerpendicular, LineLineParallel,
                PointPointHorizontal, PointPointVertical};
};
size_t index_of_symbolicConstraint(Kind::inner symb_constraint);
size_t index_of_symbolicConstraint(size_t symb_constraint);
Kind::inner symbolicConstraint_for_index(size_t index);
std::string name_kind(Kind::inner kind);
bool is_geom(Kind::inner kind);
bool is_geom(size_t kind);
bool is_geom(SibId id);
bool is_point(SibId id);
bool is_line(SibId id);
bool is_circle(SibId id);
bool is_curve(SibId id);
bool is_curve(size_t kind);

// Orders the geometries in the order of Kind
// Point, Line, Circle, Arc, 
// It also considers handles as point
IdVec order_geometries(IdVec src);

} // !Sk
} // !Core

#endif