
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef BLOOP_CORE_SKETCH_HPP_
#define BLOOP_CORE_SKETCH_HPP_

#include "Kinds.hpp"

#include <Core/Utils/Visitor.hpp>
#include <Core/Discrete/Mesh.hpp>
#include <Core/Surface/Plane.hpp>
#include <Core/Surface/Flat_surface.hpp>

#include <Core/Maths/Ray_3d.hpp>
#include <Core/Id.hpp>
#include <Core/UpdReport.hpp>
#include <Core/Curve/ClosedShape/MinShapeBasis.hpp>

#include <glm/glm.hpp>

#include <immer/vector.hpp>
#include <immer/vector_transient.hpp>
#include <immer/flex_vector.hpp>
#include <immer/flex_vector_transient.hpp>
#include <immer/map.hpp>

#include <variant>
#include <tuple>
#include <string>
#include <vector>
#include <variant>
#include <iostream>
#include <optional>
#include <map>

namespace Core {
namespace Sk {

struct SpriteHandleRegistry {

  CondensedList<Named<SpriteHandle>> sprites;
   // Indexed on symbolic constraint kind first->first sprite, second->last sprite +1
  std::array<std::pair<size_t, size_t>, 9> kind_starts;

  SpriteHandleRegistry();

  void add_to_snapshot(Wp::VecsSnapshot& snap) const;
  void ids(WorkpieceId sk_id, IdVec& dst) const;
  std::vector<Named<SpriteHandle>> get(size_t kind, size_t index) const;
};

struct Sketch {
  CondensedList<Named<Point>> points;
  CondensedList<Named<Line>> lines;
  CondensedList<Named<Circle>> circles;

  // CondensedList<Named<MeasureConstraint>> measConstraints;

  SpriteHandleRegistry sprites;
  
  CondensedList<Named<MeasureConstraint>> lLengths;
  CondensedList<Named<MeasureConstraint>> ppHdists;
  CondensedList<Named<MeasureConstraint>> ppVdists;
  CondensedList<Named<MeasureConstraint>> cDiameters;

  std::map<SibId, std::vector<SibId>> geom_to_constraints;

  Crv::_2d::MinShapeBasis shapeBasis;
  std::vector<std::string> shapesNames;

  Point origin_point;
  Line origin_x, origin_y;

  WorkpieceId self_id;

  size_t version { 0 };

  Sketch() = default;
  Sketch(WorkpieceId wpid);

  IdVec children() const;

  std::vector<Mesh> meshes(SibId childId) const;

  std::string name_of(SibId id) const;

  Wp::VecsSnapshot idsSnapshot() const;

  Mesh point_mesh(Point point_data) const;
  Mesh line_mesh(Line line_data) const;
  Mesh nurbs_mesh(Crv::_2d::Nurbs const& nurbs) const;
  Mesh offsetPointTexture_mesh(SpriteHandle handle) const;
  Mesh distanceAnnot_mesh(MeasureConstraint constr, size_t kind) const;
  Mesh text_mesh(MeasureConstraint constr, size_t kind) const;
  Mesh loop_mesh(int shape_index) const;

  Wp::Diff account_relationships(Wp::Diff diff) const;
  SibIdVec account_relationships(SibIdVec ids) const;

  DistanceAnnotSqueleton measureConstraint_squeletonLine(MeasureConstraint constr, size_t kind) const;

  // Get the measurement text position in the plane
  glm::dvec2 get_textpos(MeasureConstraint constr, size_t kind) const;
  // Creates the annot desc from a text position
  MeasureConstraint::AnnotDesc set_textpos(MeasureConstraint constr, size_t kind, glm::dvec2 pos) const;

  template<typename T>
  std::optional<T> get(SibId sid) const
  { 
    std::cerr<<"Type not handled in Sketch::get()"<<std::endl;
    return std::nullopt;
  }
  template<typename T>
  std::optional<T> get(Named<SibId> sid) const
  {
    if(*sid == SibId{}) { // No id
      std::optional<SibId> found = find_sib(sid.name);
      if(!found) {
        std::cerr<<"Could not resolve named index \""<<sid.name<<"\""<<std::endl;
        return std::nullopt;
      }
      return get<T>(*found);
    } else if(sid.name == "") { // No name 
      return get<T>(*sid);
    } else if(*sid != SibId{} && sid.name != "") { // Has both a name and an id
      std::optional<T> try_out = get<T>(*sid); // Try id first because it's simpler
      if(try_out)
        return try_out;
      
      return get<T>(Named<SibId>(SibId{}, sid.name)); // Recursive call as to not duplicate logic
    }

    std::cerr<<"Could not resolve invalid named index"<<std::endl;
    return std::nullopt;
  }
  template<typename T>
  std::optional<T> get(std::string name) const { return get<T>(Named<SibId>(SibId{}, name)); }

  std::optional<Id> find(std::string name, Kind::inner hint) const;
  std::optional<Id> find(std::string name) const;
  std::optional<SibId> find_sib(std::string name, Kind::inner hint) const;
  std::optional<SibId> find_sib_impl(std::string name, Kind::inner hint) const;
  std::optional<SibId> find_sib(std::string name) const;

  static Srf::Plane plane();
  static Srf::NurbsSurface surface(dBox bbox);
};

template<> std::optional<Point> Sketch::get(SibId sid) const;
template<> std::optional<Line> Sketch::get(SibId sid) const;
template<> std::optional<Circle> Sketch::get(SibId sid) const;
template<> std::optional<Srf::Surface> Sketch::get(SibId sid) const;

} // !Sk
} // !Core

#endif