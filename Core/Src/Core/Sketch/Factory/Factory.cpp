
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "Factory.hpp"

#include <Core/Print/IDOstream.hpp>

namespace Core {
namespace Sk {

Factory::Factory(WorkpieceId wpid_)
  : wpid(wpid_)
{

}
Factory::Factory(Geometries const& geometries_, Constraints const& constraints_, WorkpieceId wpid_, std::vector<SibId> drag_)
  : geometries(geometries_)
  , constraints(constraints_)
  , wpid(wpid_)
  , drag(drag_)
{

}

std::pair<Sketch, Factory> Factory::build(bool& success) const
{
  std::cout<<"Build sketch "<<wpid<<" ["<<0<<"]\n";

  Sketch out(wpid);

  // Try to solve
  auto new_geometries = geometries.unvectorize(
                          constraints.make_system(geometries).solve(
                            geometries.vectorize(), geometries.draggedParams_from_draggedGeoms(drag)));

  // update the geometries

  out.points  = new_geometries.crystalize_points();
  out.lines   = new_geometries.crystalize_lines();
  out.circles = new_geometries.crystalize_circles();

  // Set the constraints' handles positions
  constraints.make_symbolic_registry(out, new_geometries);

  constraints.lLengths.for_each([&](auto constr, size_t index, size_t version) {
    SibId constr_id{.kind=Kind::LineLength, .index=index};
    out.geom_to_constraints[constr->first->no_handle()].push_back(constr_id);
    return true; // Keep going in the foreach
  });
  out.lLengths = constraints.lLengths.condense();

  constraints.ppHdists.for_each([&](auto constr, size_t index, size_t version) {
    SibId constr_id{.kind=Kind::PointPointHdist, .index=index};
    out.geom_to_constraints[constr->first->no_handle()].push_back(constr_id);
    out.geom_to_constraints[constr->second->no_handle()].push_back(constr_id);
    return true; // Keep going in the foreach
  });
  out.ppHdists = constraints.ppHdists.condense();

  constraints.ppVdists.for_each([&](auto constr, size_t index, size_t version) {
    SibId constr_id{.kind=Kind::PointPointVdist, .index=index};
    out.geom_to_constraints[constr->first->no_handle()].push_back(constr_id);
    out.geom_to_constraints[constr->second->no_handle()].push_back(constr_id);
    return true; // Keep going in the foreach
  });
  out.ppVdists = constraints.ppVdists.condense();

  constraints.cDiameters.for_each([&](auto constr, size_t index, size_t version) {
    SibId constr_id{.kind=Kind::CircleDiameter, .index=index};
    out.geom_to_constraints[constr->first->no_handle()].push_back(constr_id);
    return true; // Keep going in the foreach
  });
  out.cDiameters = constraints.cDiameters.condense();

  std::tie(out.shapeBasis, out.shapesNames) = find_loops(out);

  out.version = version;

  success = true;
  return { out, Factory(new_geometries, constraints, wpid) };
}

std::pair<Crv::_2d::MinShapeBasis, std::vector<std::string>> Factory::find_loops(Sketch const& sk) const
{
  size_t line_offset = 0, circle_offset;
  std::vector<Crv::_2d::Curve> curves;
  for(auto named_line : sk.lines) {
    curves.push_back(*named_line.second);
  }
  circle_offset = curves.size();
  for(auto named_circle : sk.circles) {
    curves.push_back(*named_circle.second);
  }

  Crv::_2d::MinShapeBasis shapeBasis(curves);

  // Generate names
  std::vector<std::string> names;
  for(size_t i = 0; i < shapeBasis.shapes_desc.size(); ++i) {
    std::vector<int> cycle = shapeBasis.shapes_desc[i];
    
    auto min_elem = std::min_element(cycle.begin(), cycle.end());
    std::rotate(cycle.begin(), min_elem, cycle.end()); // Uniformizes cycles, but keep the geometries order

    std::string name = "";
    for(int c : cycle) {
      // c--;// quirk of the cycle finding system
      if(c >= circle_offset) {
        name += sk.circles[c-circle_offset].second.name;
      } else {
        name += sk.lines[c].second.name;
      }
    }
    // std::cout<<"shapename: "<<name<<"\n";
    names.push_back(name);
  }
  // std::cout<<"found "<<names.size()<<" loops\n";
  return { shapeBasis, names };
}
dBox Factory::curveBox() const
{
  dBox box;
  geometries.lines.for_each([&](Named<Line> line, size_t i, size_t version) {
    box = box.extend(line->pt1);
    box = box.extend(line->pt2);
    return true;
  });
  geometries.circles.for_each([&](Named<Circle> circle, size_t i, size_t version) {
    box = box.combine(dBox(circle->center-glm::dvec2(circle->radius), circle->center+glm::dvec2(circle->radius)));
    return true;
  });
  return box;
}

EntityList<Named<Point>> update(EntityList<Named<Point>> points, WorkpieceId skId, Action action);
EntityList<Named<Line>> update(EntityList<Named<Line>> lines, WorkpieceId skId, Action action);
EntityList<Named<Circle>> update(EntityList<Named<Circle>> circles, WorkpieceId skId, Action action);
EntityList<Named<SymbolicConstraint>> update(EntityList<Named<SymbolicConstraint>> constraints, 
                                                Sketch sk, Action action, Kind::inner kind);
EntityList<Named<MeasureConstraint>> update( EntityList<Named<MeasureConstraint>> constraints, 
                                                Sketch sk, Action action, Kind::inner kind);

Factory next(Factory current, Action action, Sketch const& currsketch)
{
  action = name_action(action, currsketch);

  if(std::holds_alternative<Remove>(action)) {
    auto rem_act = std::move(std::get<Remove>(action));
    rem_act.which = currsketch.account_relationships(rem_act.which);
    action = std::move(rem_act);
  }

  std::vector<SibId> drag;
  if(std::holds_alternative<Move>(action)) {
    auto move = std::get<Move>(action);
    for(SibId id : move.which) {
      if(!is_geom(id))
        continue;
      drag.push_back(id);
    }
  }

  current = Factory(  Geometries( 
                        update(std::move(current.geometries.points), current.wpid, action),
                        update(std::move(current.geometries.lines), current.wpid, action),
                        update(std::move(current.geometries.circles), current.wpid, action)),
                      Constraints(
                        update(std::move(current.constraints.llPerps), currsketch, action, Kind::LineLinePerpendicular),
                        update(std::move(current.constraints.lHorizs), currsketch, action, Kind::LineHorizontal),
                        update(std::move(current.constraints.lVerts), currsketch, action, Kind::LineVertical),
                        update(std::move(current.constraints.ppHorizs), currsketch, action, Kind::PointPointHorizontal),
                        update(std::move(current.constraints.ppVerts), currsketch, action, Kind::PointPointVertical),
                        update(std::move(current.constraints.ppCoincids), currsketch, action, Kind::PointPointCoincident),
                        update(std::move(current.constraints.lLengths), currsketch, action, Kind::LineLength),
                        update(std::move(current.constraints.ppHdists), currsketch, action, Kind::PointPointHdist),
                        update(std::move(current.constraints.ppVdists), currsketch, action, Kind::PointPointVdist),
                        update(std::move(current.constraints.cDiameters), currsketch, action, Kind::CircleDiameter)),
                      current.wpid, drag);
  current.version++;
            
  return current;
}

EntityList<Named<Point>> update(EntityList<Named<Point>> points, WorkpieceId skId, Action action)
{
  std::visit(Visitor {
    [&](Move move) {
      for(SibId id : move.which) {
        if(id.kind != Kind::Point)
          continue;
        points = std::move(points.transform(id.index, 
                  [move](Named<Point> pt) { *pt += move.to; return pt; }).first);
      }
    },
    [&](AddPoint add) {
      points = std::move(points.add(Named<Point>(add.point, "point" + std::to_string(points.next()))).first);
    },
    [&](Remove remove) {
      for(SibId id : remove.which) {
        if(id.kind != Kind::Point)
          continue;

        points = std::move(points.remove(id.index));
      }
    },
    [&](auto& act) {
      // std::cerr<<"Cannot apply action with index "<<action.index()<<" to points"<<std::endl;
    }
  }, action);
  return points;
}
EntityList<Named<Line>> update(EntityList<Named<Line>> lines, WorkpieceId skId, Action action)
{
  std::visit(Visitor {
    [&](Move move) {
      size_t last_moved_whole = invalid_index();
      std::sort(move.which.begin(), move.which.end());
      for(auto id : move.which) {
         // Assumes the ids vector is sorted with non-handle first 
        if(id.kind != Kind::Line || id.index == last_moved_whole)
          continue;

        if(id.handle_num == 0) {
          lines = std::move(lines.transform(id.index, 
            [move](Named<Line> ln) { ln->pt1 = move.to; return ln; }).first);
        } else if(id.handle_num == 1) {
          lines = std::move(lines.transform(id.index, 
            [move](Named<Line> ln) { ln->pt2 = move.to; return ln; }).first);
        } else {
          lines = std::move(lines.transform(id.index, 
            [move](Named<Line> ln) { ln->pt1+=move.delta; ln->pt2+=move.delta; return ln; }).first);
          last_moved_whole = id.index;
        }
      }
    },
    [&](AddLine add) {
      // std::cout<<"Addline!\n";
      lines = std::move(lines.add(Named<Line>(add.line, "line" + std::to_string(lines.next()))).first);
    },
    [&](Remove remove) {
      
      for(SibId id : remove.which) {
        if(id.kind != Kind::Line)
          continue;

        lines = std::move(lines.remove(id.index));
      }
    },
    [&](auto& act) {
      // std::cerr<<"Cannot apply action with index "<<action.index()<<" to lines"<<std::endl;
    }
  }, action);
  return lines;
}
EntityList<Named<Circle>> update(EntityList<Named<Circle>> circles, WorkpieceId skId, Action action)
{
  std::visit(Visitor {
    [&](Move move) {
      for(SibId id : move.which) {
        if(id.kind != Kind::Circle)
          continue;
        circles = std::move(circles.transform(id.index, 
                  [move](Named<Circle> circ) { circ->center += move.delta; return circ; }).first);
      }
    },
    [&](SetRadius set) {
      circles = std::move(circles.transform(set.which.index, 
                  [set](Named<Circle> circ) { circ->radius = set.newRad; return circ; }).first);
    },
    [&](AddCircle add) {
      circles = std::move(circles.add(Named<Circle>(add.circle, "circle" + std::to_string(circles.next()))).first);
    },
    [&](Remove remove) {
      for(SibId id : remove.which) {
        if(id.kind != Kind::Circle)
          continue;

        circles = std::move(circles.remove(id.index));
      }
    },
    [&](auto& act) {
      // std::cerr<<"Cannot apply action with index "<<action.index()<<" to points"<<std::endl;
    }
  }, action);
  return circles;
}

EntityList<Named<SymbolicConstraint>> update(EntityList<Named<SymbolicConstraint>> constraints, 
                                                Sketch sk, Action action, Kind::inner kind)
{
  std::visit(Visitor {
    [&](Move move) {
      for(SibId id : move.which) {
        if(id.kind != kind)
          continue;
        
        if(id.handle_num == 0) {
          constraints = std::move(constraints.transform(id.index, 
                          [move](Named<SymbolicConstraint> constr) { constr->first_hdesc.planeoffset = move.to; return constr; }).first);
        } else if(id.handle_num == 1) {
          constraints = std::move(constraints.transform(id.index, 
                          [move](Named<SymbolicConstraint> constr) { constr->second_hdesc.planeoffset = move.to; return constr; }).first);
        }
      }
    },
    [&](AddSymbolicConstraint add) {
      if(add.kind != kind)
        return;
      
      constraints = std::move(constraints.add(Named<SymbolicConstraint>(add.constr, name_kind(kind) + std::to_string(constraints.next()))).first);
    },
    [&](Remove remove) {
      
      for(SibId id : remove.which) {
        if(id.kind != kind)
          continue;

        constraints = std::move(constraints.remove(id.index));
      }
    },
    [&](auto& act) {
      // std::cerr<<"Cannot apply action with index "<<action.index()<<" to points"<<std::endl;
    }
  }, action);
  return constraints;
}
EntityList<Named<MeasureConstraint>> update( EntityList<Named<MeasureConstraint>> constraints, 
                                                Sketch sk, Action action, Kind::inner kind)
{
  std::visit(Visitor {
    [&](Move move) {
      for(SibId id : move.which) {
        if(id.kind != kind)
          continue;
        
        if(id.handle_num == 0) {
          
          constraints = std::move(constraints.transform(id.index, 
                          [&](Named<MeasureConstraint> constr) { 
                            // glm::dvec2 currPos = sk.get_textpos(*constr, kind);
                            constr->annotDesc = sk.set_textpos(*constr, kind, move.to);
                            return constr; }).first);
        }
      }
    },
    [&](AddMeasureConstraint add) {
      if(add.kind != kind)
        return;
      
      constraints = std::move(constraints.add(Named<MeasureConstraint>(add.constr, name_kind(kind) + std::to_string(constraints.next()))).first);
    },
    [&](SetConstraintValue set) {
      if(set.which.kind != kind) 
        return;

      constraints = std::move(constraints.transform(set.which.index, [set](Named<MeasureConstraint> constr)
          { constr->expression = set.expression;
            constr->value = set.value;
            return constr; }).first);
    },
    [&](Remove remove) {
      
      for(SibId id : remove.which) {
        if(id.kind != kind)
          continue;

        constraints = std::move(constraints.remove(id.index));
      }
    },
    [&](auto& act) {
      // std::cerr<<"Cannot apply action with index "<<action.index()<<" to points"<<std::endl;
    }
  }, action);
  return constraints;
}

} // !Sk
} // !Core