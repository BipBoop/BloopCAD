
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "Geometries.hpp"

#include <Core/Print/IDOstream.hpp>

namespace Core {
namespace Sk {

Geometries::Geometries( EntityList<Named<Point>> points_, 
                        EntityList<Named<Line>> lines_,
                        EntityList<Named<Circle>> circles_)
  : points(points_)
  , lines(lines_)
  , circles(circles_)
{

}
Geometries::Geometries( std::vector<Named<Point>> points_, 
                        std::vector<Named<Line>> lines_,
                        std::vector<Named<Circle>> circles_)
  : Geometries( EntityList<Named<Point>>::make(points_), 
                EntityList<Named<Line>>::make(lines_),
                EntityList<Named<Circle>>::make(circles_))
{

}
size_t Geometries::version_of(SibId geom) const
{
  switch(geom.kind) {
  case Kind::Point:   return points.version(geom.index);
  case Kind::Line:    return lines.version(geom.index);
  case Kind::Circle:  return circles.version(geom.index);
  default: return invalid_index();
  }
}
size_t Geometries::n_vectorized(SibId geom) const
{
  switch(geom.kind) {
  case Kind::Point:   return point_vectorized_size();
  case Kind::Line:    return geom.handle_num == -1 ? line_vectorized_size() : point_vectorized_size();
  case Kind::Circle:  return geom.handle_num == -1 ? circle_vectorized_size() : point_vectorized_size();
  default: return invalid_index();
  }
}
Point Geometries::get_point(SibId id) const
{
  if(id.kind == Kind::Point) {
    return *points[id.index];
  } else if(id.handle_num == -1) {
    std::cerr<<"Could not extract point from "<<id<<std::endl;
    return Point {};
  }
  if(id.kind == Kind::Line) {
    return id.handle_num == 0 ? lines[id.index]->pt1 : lines[id.index]->pt2;
  }
  if(id.kind == Kind::Circle) {
    return circles[id.index]->center;
  }
  
  std::cerr<<"Could not extract point from "<<id<<std::endl;
  return Point{};
}
std::unordered_set<size_t> Geometries::draggedParams_from_draggedGeoms(std::vector<SibId> geoms) const
{
  std::unordered_set<size_t> out;
  for(auto geom : geoms) {
    std::cout<<"dragged: "<<geom<<"\n";
    size_t start = vectorized_index(geom);
    size_t n = n_vectorized(geom);
    for(size_t i = 0; i < n; ++i) {
      out.insert(i+start);
    }
  }
  return out;
}
size_t Geometries::vectorized_index(SibId geom) const
{
  switch(geom.kind) {
  case Kind::Point:
    return points.count_up_to(geom.index) * point_vectorized_size();
  case Kind::Line:
    if(geom.handle_num == -1) {
      return  points.size() * point_vectorized_size() 
              + lines.count_up_to(geom.index) * line_vectorized_size();
    } else {
      return  points.size() * point_vectorized_size() 
              + lines.count_up_to(geom.index) * line_vectorized_size()
              + (geom.handle_num) * point_vectorized_size();
    }
  case Kind::Circle:
    // if(geom.handle_num == -1) {
      return  points.size() * point_vectorized_size() 
              + lines.size() * line_vectorized_size()
              + circles.count_up_to(geom.index) * circle_vectorized_size();
    // } else {
    //   return  points.size() * point_vectorized_size() 
    //           + lines.size() * line_vectorized_size();
    //           + circles.count_up_to(geom.index) * circle_vectorized_size();
    // }
  default: return invalid_index();
  }
}

std::vector<double> Geometries::vectorize() const
{
  std::vector<double> vectorized;
  points.for_each([&](Named<Point> point, size_t i, size_t version) {
    vectorized.push_back(point->x);
    vectorized.push_back(point->y);
    return true;
  });
  lines.for_each([&](Named<Line> line, size_t i, size_t version) {
    vectorized.push_back(line->pt1.x);
    vectorized.push_back(line->pt1.y);
    vectorized.push_back(line->pt2.x);
    vectorized.push_back(line->pt2.y);
    return true;
  });
  circles.for_each([&](Named<Circle> circle, size_t i, size_t version) {
    vectorized.push_back(circle->center.x);
    vectorized.push_back(circle->center.y);
    vectorized.push_back(circle->radius);
    return true;
  });

  return vectorized;
}
Geometries Geometries::unvectorize(std::vector<double> vectorized) const
{
  size_t index = 0;

  auto new_points = points.for_each_transform([&](Named<Point> point, size_t i, size_t version) {
    point->x = vectorized[index++];
    point->y = vectorized[index++];
    return point;
  });
  auto new_lines = lines.for_each_transform([&](Named<Line> line, size_t i, size_t version) {
    line->pt1.x = vectorized[index++];
    line->pt1.y = vectorized[index++];
    line->pt2.x = vectorized[index++];
    line->pt2.y = vectorized[index++];
    return line;
  });
  auto new_circles = circles.for_each_transform([&](Named<Circle> circle, size_t i, size_t version) {
    circle->center.x  = vectorized[index++];
    circle->center.y  = vectorized[index++];
    circle->radius    = vectorized[index++];
    return circle;
  });
  return Geometries(new_points, new_lines, new_circles);
}

CondensedList<Named<Point>> Geometries::crystalize_points() const
{
  return points.condense();
}
CondensedList<Named<Line>> Geometries::crystalize_lines() const
{
  return lines.condense();
}
CondensedList<Named<Circle>> Geometries::crystalize_circles() const
{
  return circles.condense();
}

} // !Sk
} // !Core