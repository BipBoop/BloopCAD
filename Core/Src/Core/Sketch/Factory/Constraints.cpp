
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "Constraints.hpp"

namespace Core {
namespace Sk {

Constraints::Constraints( EntityList<Named<SymbolicConstraint>> llPerps_,
                          EntityList<Named<SymbolicConstraint>> lHorizs_,
                          EntityList<Named<SymbolicConstraint>> lVerts_,
                          EntityList<Named<SymbolicConstraint>> ppHorizs_,
                          EntityList<Named<SymbolicConstraint>> ppVerts_,
                          EntityList<Named<SymbolicConstraint>> ppCoincids_,
                          EntityList<Named<MeasureConstraint>> lLengths_,
                          EntityList<Named<MeasureConstraint>> ppHdists_,
                          EntityList<Named<MeasureConstraint>> ppVdists_,
                          EntityList<Named<MeasureConstraint>> cDiameters_)
  : llPerps(llPerps_)
  , lHorizs(lHorizs_)
  , lVerts(lVerts_)
  , ppHorizs(ppHorizs_)
  , ppVerts(ppVerts_)
  , ppCoincids(ppCoincids_)
  , lLengths(lLengths_)
  , ppHdists(ppHdists_)
  , ppVdists(ppVdists_)
  , cDiameters(cDiameters_)
{
  
}
Constraints::Constraints( std::vector<Named<SymbolicConstraint>> const& llPerps_,
                          std::vector<Named<SymbolicConstraint>> const& lHorizs_,
                          std::vector<Named<SymbolicConstraint>> const& lVerts_,
                          std::vector<Named<SymbolicConstraint>> const& ppHorizs_,
                          std::vector<Named<SymbolicConstraint>> const& ppVerts_,
                          std::vector<Named<SymbolicConstraint>> const& ppCoincids_,
                          std::vector<Named<MeasureConstraint>> const& lLengths_,
                          std::vector<Named<MeasureConstraint>> const& ppHdists_,
                          std::vector<Named<MeasureConstraint>> const& ppVdists_,
                          std::vector<Named<MeasureConstraint>> const& cDiameters_)
  : Constraints(EntityList<Named<SymbolicConstraint>>::make(llPerps_),
                EntityList<Named<SymbolicConstraint>>::make(lHorizs_),
                EntityList<Named<SymbolicConstraint>>::make(lVerts_),
                EntityList<Named<SymbolicConstraint>>::make(ppHorizs_),
                EntityList<Named<SymbolicConstraint>>::make(ppVerts_),
                EntityList<Named<SymbolicConstraint>>::make(ppCoincids_),
                EntityList<Named<MeasureConstraint>>::make(lLengths_),
                EntityList<Named<MeasureConstraint>>::make(ppHdists_),
                EntityList<Named<MeasureConstraint>>::make(ppVdists_),
                EntityList<Named<MeasureConstraint>>::make(cDiameters_))
{

}

enum class LineSubstitudness { Horizontal, Vertical, None };
LineSubstitudness substitudness(size_t line_start, Slv::Subst::Map const& substMap)
{
  size_t x1 = line_start;
  size_t y1 = x1+1;
  size_t x2 = x1+2;
  size_t y2 = x2+1;

  auto offset = substMap.offset(x1, x2);
  if(offset && *offset == 0.0) {
    return LineSubstitudness::Vertical;
  }
  offset = substMap.offset(y1, y2);
  if(offset && *offset == 0.0) {
    return LineSubstitudness::Horizontal;
  }
  return LineSubstitudness::None;  
}
Slv::Subst::Relation line_vertical(size_t line_start)
{
  size_t x1 = line_start;
  size_t x2 = x1+2;
  return Slv::Subst::Relation { .first=x1, .second=x2 };
}
Slv::Subst::Relation line_horizontal(size_t line_start)
{
  size_t y1 = line_start+1;
  size_t y2 = y1+2;
  return Slv::Subst::Relation { .first=y1, .second=y2 };
}

Slv::System Constraints::make_system(Geometries geometries) const
{
  Slv::Subst::Map substMap;
  lHorizs.for_each([&](Named<SymbolicConstraint> horiz, size_t i, size_t version) {
    substMap.add_relation(line_horizontal(geometries.vectorized_index(*horiz->first)));
    return true;
  });
  lVerts.for_each([&](Named<SymbolicConstraint> vert, size_t i, size_t version) {
    substMap.add_relation(line_vertical(geometries.vectorized_index(*vert->first)));
    return true;
  });
  ppHorizs.for_each([&](Named<SymbolicConstraint> horiz, size_t i, size_t version) {
    size_t y1 = geometries.vectorized_index(*horiz->first)+1;
    size_t y2 = geometries.vectorized_index(*horiz->second)+1;
    substMap.add_relation(Slv::Subst::Relation { 
      .first=y1,
      .second=y2
    });
    return true;
  });
  ppVerts.for_each([&](Named<SymbolicConstraint> vert, size_t i, size_t version) {
    size_t x1 = geometries.vectorized_index(*vert->first);
    size_t x2 = geometries.vectorized_index(*vert->second);
    substMap.add_relation(Slv::Subst::Relation { 
      .first=x1,
      .second=x2
    });
    return true;
  });
  ppCoincids.for_each([&](Named<SymbolicConstraint> ppCoincids, size_t i, size_t version) {
    size_t x1 = geometries.vectorized_index(*ppCoincids->first);
    size_t y1 = x1+1;
    size_t x2 = geometries.vectorized_index(*ppCoincids->second);
    size_t y2 = x2+1;
    substMap.add_relation(Slv::Subst::Relation { .first=x1, .second=x2 });
    substMap.add_relation(Slv::Subst::Relation { .first=y1, .second=y2 });
    return true;
  });
  ppHdists.for_each([&](Named<MeasureConstraint> hdist, size_t i, size_t version) {
    size_t x1 = geometries.vectorized_index(*hdist->first);
    size_t x2 = geometries.vectorized_index(*hdist->second);
    auto pt1 = geometries.get_point(*hdist->first);
    auto pt2 = geometries.get_point(*hdist->second);

    substMap.add_relation(Slv::Subst::Relation { 
        .first=x1, 
        .second=x2,
        .offset=pt1.x > pt2.x ? -hdist->value : hdist->value
    });
    return true;
  });
  ppVdists.for_each([&](Named<MeasureConstraint> hdist, size_t i, size_t version) {
    size_t y1 = geometries.vectorized_index(*hdist->first)+1;
    size_t y2 = geometries.vectorized_index(*hdist->second)+1;
    auto pt1 = geometries.get_point(*hdist->first);
    auto pt2 = geometries.get_point(*hdist->second);

    substMap.add_relation(Slv::Subst::Relation { 
        .first=y1, 
        .second=y2,
        .offset=pt1.y > pt2.y ? -hdist->value : hdist->value
    });
    return true;
  });
  cDiameters.for_each([&](Named<MeasureConstraint> cDiameter, size_t i, size_t version) {
    size_t r = geometries.vectorized_index(*cDiameter->first)+2; // Radius is stored after the center (x, y, r)

    substMap.add_relation(Slv::Subst::Relation { 
        .first=r, 
        .offset=cDiameter->value/2.0,
        .is_assignement=true
    });
    return true;
  });


  // For ambiguous constraints tm 
  // try to make them substitution until
  // it doesn't work anymore
  bool added_one = false;
  std::unordered_set<size_t> subst_llPerp;
  std::unordered_set<size_t> subst_lLength;
  do {
    added_one = false;
    size_t initial_subst_size = subst_llPerp.size();
    llPerps.for_each([&](Named<SymbolicConstraint> perp, size_t i, size_t version) {
      // Avoid getting stuck in a loop
      if(subst_llPerp.find(i) != subst_llPerp.end()) {
        return true;
      }
      auto l1_start = geometries.vectorized_index(*perp->first);
      auto l2_start = geometries.vectorized_index(*perp->second);

      // If the first line is vertical or horizontal, we
      // can define the second line as horizontal or verical
      auto l1_subtn = substitudness(l1_start, substMap);
      if(l1_subtn == LineSubstitudness::Vertical) {
        substMap.add_relation(line_horizontal(l2_start));
        subst_llPerp.insert(i);
        return true;
      } else if(l1_subtn == LineSubstitudness::Horizontal) {
        substMap.add_relation(line_vertical(l2_start));
        subst_llPerp.insert(i);
        return true;
      }

      // If the second line is vertical or horizontal, we
      // can define the first line as horizontal or verical
      auto l2_subtn = substitudness(l2_start, substMap);
      if(l2_subtn == LineSubstitudness::Vertical) {
        substMap.add_relation(line_horizontal(l1_start));
        subst_llPerp.insert(i);
        return true;
      } else if(l2_subtn == LineSubstitudness::Horizontal) {
        substMap.add_relation(line_vertical(l1_start));
        subst_llPerp.insert(i);
        return true;
      }
      
      return true;
    });
    if(subst_llPerp.size() > initial_subst_size) {
      added_one = true;
    }
    initial_subst_size = subst_lLength.size();
    lLengths.for_each([&](Named<MeasureConstraint> length, size_t i, size_t version) {
      // Avoid getting stuck in a loop
      if(subst_lLength.find(i) != subst_lLength.end()) {
        return true;
      }
      auto l_start = geometries.vectorized_index(*length->first);

      // If the line is vertical or horizontal, it's length
      // can be expressed as a simple substitution offset
      auto l_subtn = substitudness(l_start, substMap);
      if(l_subtn == LineSubstitudness::Vertical) {
        size_t y1 = l_start+1;
        size_t y2 = y1+2;
        Line ln = *geometries.lines[length->first->index];
        substMap.add_relation(Slv::Subst::Relation {
          .first=y1,
          .second=y2,
          .offset=ln.pt1.y > ln.pt2.y ? -length->value : length->value
        });
        subst_lLength.insert(i);
        return true;
      } else if(l_subtn == LineSubstitudness::Horizontal) {
        size_t x1 = l_start;
        size_t x2 = x1+2;
        Line ln = *geometries.lines[length->first->index];
        substMap.add_relation(Slv::Subst::Relation {
          .first=x1,
          .second=x2,
          .offset=ln.pt1.x > ln.pt2.x ? -length->value : length->value
        });
        subst_lLength.insert(i);
        return true;
      }
      
      return true;
    });
    if(subst_llPerp.size() > initial_subst_size) {
      added_one = true;
    }

  } while(added_one);

  return Slv::System(substMap);
}

void Constraints::make_symbolic_registry(Sketch& sk, Geometries const& geometries) const
{
  symbolic_registry_twinhelper(sk, Kind::LineLinePerpendicular, "perp", llPerps, geometries);
  symbolic_registry_twinhelper(sk, Kind::PointPointHorizontal, "horiz", ppHorizs, geometries);
  symbolic_registry_twinhelper(sk, Kind::PointPointVertical, "vert", ppVerts, geometries);
  symbolic_registry_singlehelper(sk, Kind::LineHorizontal, "horiz", lHorizs, geometries);
  symbolic_registry_singlehelper(sk, Kind::LineVertical, "vert", lVerts, geometries);
  symbolic_registry_coincidhelper(sk, Kind::PointPointCoincident, "coinc", ppCoincids, geometries);
}
void Constraints::symbolic_registry_twinhelper( Sketch& sk, Kind::inner kind, std::string spritename, 
                                                EntityList<Named<SymbolicConstraint>> const& list, Geometries const& geometries)
{
  if(list.size() > 0) {
    size_t size_atStart = sk.sprites.sprites.size();
    list.for_each([&](auto constr, size_t index, size_t version) {
      sk.sprites.sprites.push_back({ VersionedIndex{.index=index, .version=geometries.version_of(*constr->first)}, 
          make_sprite_handle(constr->first_hdesc, *constr->first, geometries, spritename, 1)});
      sk.sprites.sprites.push_back({ VersionedIndex{.index=index, .version=geometries.version_of(*constr->second)}, 
          make_sprite_handle(constr->second_hdesc, *constr->second, geometries, spritename, -1)});

      SibId constr_id{.kind=kind, .index=index};
      sk.geom_to_constraints[constr->first->no_handle()].push_back(constr_id);
      sk.geom_to_constraints[constr->second->no_handle()].push_back(constr_id);
  
      return true; // Keep going in the foreach
    });
    sk.sprites.kind_starts[index_of_symbolicConstraint(kind)] = { size_atStart, sk.sprites.sprites.size() };
  }
}
void Constraints::symbolic_registry_singlehelper( Sketch& sk, Kind::inner kind, std::string spritename, 
                                                  EntityList<Named<SymbolicConstraint>> const& list, Geometries const& geometries)
{
  if(list.size() > 0) {
    size_t size_atStart = sk.sprites.sprites.size();
    list.for_each([&](auto constr, size_t index, size_t version) {
      sk.sprites.sprites.push_back({ VersionedIndex{.index=index, .version=geometries.version_of(*constr->first)}, 
          make_sprite_handle(constr->first_hdesc, *constr->first, geometries, spritename, 0)});

      SibId constr_id{.kind=kind, .index=index};
      sk.geom_to_constraints[constr->first->no_handle()].push_back(constr_id);

      return true; // Keep going in the foreach
    });
    sk.sprites.kind_starts[index_of_symbolicConstraint(kind)] = { size_atStart, sk.sprites.sprites.size() };
  }
}
void Constraints::symbolic_registry_coincidhelper(Sketch& sk, Kind::inner kind, std::string spritename, 
                                                  EntityList<Named<SymbolicConstraint>> const& list, Geometries const& geometries)
{
  if(list.size() > 0) {
    size_t size_atStart = sk.sprites.sprites.size();
    list.for_each([&](auto constr, size_t index, size_t version) {
      sk.sprites.sprites.push_back({ VersionedIndex{.index=index, .version=geometries.version_of(*constr->first)}, 
          make_sprite_handle(constr->first_hdesc, *constr->first, geometries, spritename, 0)});

      SibId constr_id{.kind=kind, .index=index};
      sk.geom_to_constraints[constr->first->no_handle()].push_back(constr_id);
      sk.geom_to_constraints[constr->second->no_handle()].push_back(constr_id);

      return true; // Keep going in the foreach
    });
    sk.sprites.kind_starts[index_of_symbolicConstraint(kind)] = { size_atStart, sk.sprites.sprites.size() };
  }
}

SpriteHandle Constraints::make_sprite_handle( SymbolicConstraint::HandleDesc handDesc, SibId geom, 
                                              Geometries const& geometries, std::string sprite, int twinness)
{
  switch(geom.kind) {
  case Kind::Point: {
    Point pt = *geometries.points.get(geom.index);
    return SpriteHandle{.pos=pt, .offset_px=glm::dvec2(20, 20), .sprite=sprite, .twin=twinness };
  }
  case Kind::Line: {
    Line ln = *geometries.lines.get(geom.index);
    if(geom.handle_num == -1) {
      return SpriteHandle{.pos=ln.at(0.5), .offset_px=glm::dvec2(20, 20), .sprite=sprite, .twin=twinness };
    } else if(geom.handle_num == 0) {
      return SpriteHandle{.pos=ln.at(0.0), .offset_px=glm::dvec2(20, 20), .sprite=sprite, .twin=twinness };
    } else {
      return SpriteHandle{.pos=ln.at(1.0), .offset_px=glm::dvec2(20, 20), .sprite=sprite, .twin=twinness };
    }
  }
  case Kind::Circle: {
    Circle circ = *geometries.circles.get(geom.index);
    return SpriteHandle{.pos=circ.center, .offset_px=glm::dvec2(20, 20), .sprite=sprite, .twin=twinness };
  }
  }
  return SpriteHandle{};
}

} // !Sk
} // !Core