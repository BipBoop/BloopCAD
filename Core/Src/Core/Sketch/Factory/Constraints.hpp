
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef BLOOP_CORE_SKETCH_FACTORY_CONSTRAINTS_HPP_
#define BLOOP_CORE_SKETCH_FACTORY_CONSTRAINTS_HPP_

#include "Geometries.hpp"
#include <Core/Sketch/Sketch.hpp>
#include <Core/Solver/System.hpp>
#include <Core/Id.hpp>

namespace Core {
namespace Sk {

struct Constraints {
  EntityList<Named<SymbolicConstraint>> llPerps;
  EntityList<Named<SymbolicConstraint>> llParals;
  EntityList<Named<SymbolicConstraint>> lHorizs;
  EntityList<Named<SymbolicConstraint>> lVerts;
  EntityList<Named<SymbolicConstraint>> ppHorizs;
  EntityList<Named<SymbolicConstraint>> ppVerts;
  EntityList<Named<SymbolicConstraint>> ppCoincids;

  EntityList<Named<MeasureConstraint>> lLengths;
  EntityList<Named<MeasureConstraint>> ppHdists;
  EntityList<Named<MeasureConstraint>> ppVdists;
  EntityList<Named<MeasureConstraint>> cDiameters;

  Constraints() = default;
  Constraints(EntityList<Named<SymbolicConstraint>> llPerps_,
              EntityList<Named<SymbolicConstraint>> lHorizs_,
              EntityList<Named<SymbolicConstraint>> lVerts_,
              EntityList<Named<SymbolicConstraint>> ppHorizs_,
              EntityList<Named<SymbolicConstraint>> ppVerts_,
              EntityList<Named<SymbolicConstraint>> ppCoincids_,
              EntityList<Named<MeasureConstraint>> lLengths_,
              EntityList<Named<MeasureConstraint>> ppHdists_,
              EntityList<Named<MeasureConstraint>> ppVdists_,
              EntityList<Named<MeasureConstraint>> cDiameters_);

  Constraints(std::vector<Named<SymbolicConstraint>> const& llPerps_,
              std::vector<Named<SymbolicConstraint>> const& lHorizs_,
              std::vector<Named<SymbolicConstraint>> const& lVerts_,
              std::vector<Named<SymbolicConstraint>> const& ppHorizs_,
              std::vector<Named<SymbolicConstraint>> const& ppVerts_,
              std::vector<Named<SymbolicConstraint>> const& ppCoincids_,
              std::vector<Named<MeasureConstraint>> const& lLengths_,
              std::vector<Named<MeasureConstraint>> const& ppHdists_,
              std::vector<Named<MeasureConstraint>> const& ppVdists_,
              std::vector<Named<MeasureConstraint>> const& cDiameters_);

  Slv::System make_system(Geometries geometries) const;

  void make_symbolic_registry(Sketch& sk, Geometries const& geometries) const;
  static SpriteHandle make_sprite_handle( SymbolicConstraint::HandleDesc handDesc, SibId geom, 
                                          Geometries const& geometries, std::string sprite, int twinness);

private:
  static void symbolic_registry_twinhelper(Sketch& sk, Kind::inner kind, std::string spritename, 
                                           EntityList<Named<SymbolicConstraint>> const& list, Geometries const& geometries);
  static void symbolic_registry_singlehelper(Sketch& sk, Kind::inner kind, std::string spritename, 
                                             EntityList<Named<SymbolicConstraint>> const& list, Geometries const& geometries);
  static void symbolic_registry_coincidhelper(Sketch& sk, Kind::inner kind, std::string spritename, 
                                              EntityList<Named<SymbolicConstraint>> const& list, Geometries const& geometries);

};

} // !Sk
} // !Core

#endif