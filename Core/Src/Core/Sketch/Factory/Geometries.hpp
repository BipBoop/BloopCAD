
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef BLOOP_CORE_SK_FACTORY_GEOMETRIES_HPP_
#define BLOOP_CORE_SK_FACTORY_GEOMETRIES_HPP_

#include <Core/Sketch/Kinds.hpp>

#include <unordered_set>

namespace Core {
namespace Sk {

struct Geometries {
  EntityList<Named<Point>> points; 
  EntityList<Named<Line>> lines; 
  EntityList<Named<Circle>> circles;

  Geometries() = default;
  Geometries( EntityList<Named<Point>> points_, 
              EntityList<Named<Line>> lines_,
              EntityList<Named<Circle>> circles_);
  Geometries( std::vector<Named<Point>> points_, 
              std::vector<Named<Line>> lines_,
              std::vector<Named<Circle>> circles_);

  static constexpr size_t point_vectorized_size() { return 2; }
  static constexpr size_t line_vectorized_size() { return 4; }
  static constexpr size_t circle_vectorized_size() { return 3; }

  size_t version_of(SibId geom) const;
  size_t n_vectorized(SibId geom) const;
  
  // Returns the index of the start of the 
  // id-ed geometry in the vectorized version of the 
  // geometries
  size_t vectorized_index(SibId geom) const;

  std::unordered_set<size_t> draggedParams_from_draggedGeoms(std::vector<SibId> geoms) const;

  std::vector<double> vectorize() const;
  Geometries unvectorize(std::vector<double> params) const;

  Point get_point(SibId id) const;

  CondensedList<Named<Point>>   crystalize_points() const;
  CondensedList<Named<Line>>    crystalize_lines() const;
  CondensedList<Named<Circle>>  crystalize_circles() const;
};

} // !Sk
} // !Core

#endif