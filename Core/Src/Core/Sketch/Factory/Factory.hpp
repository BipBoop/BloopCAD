
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef BLOOP_CORE_SK_FACTORY_HPP_
#define BLOOP_CORE_SK_FACTORY_HPP_

#include "Geometries.hpp"
#include "Constraints.hpp"

#include <Core/Sketch/Kinds.hpp>
#include <Core/Sketch/Sketch.hpp>
#include <Core/Sketch/Actions.hpp>

#include <Core/Id.hpp>

namespace Core {
namespace Sk {

struct Factory {
  using action_t = Action;

  Geometries geometries;
  // EntityList<Named<SymbolicConstraint>> symbConstraints;
  Constraints constraints;
  WorkpieceId wpid;
  int mod_struct { -1 };
  size_t version { 0 };
  std::vector<SibId> drag {};

  Factory() = default;
  Factory(WorkpieceId wpid_);
  Factory(Geometries const& geometries_, Constraints const& constraints_, WorkpieceId wpid_, std::vector<SibId> drag_ = {});


  std::pair<Sketch, Factory> build(bool& success) const;

  size_t n_steps() const { return 1; }

  std::pair<Crv::_2d::MinShapeBasis, std::vector<std::string>> find_loops(Sketch const& sk) const;

  dBox curveBox() const;
};

// Generates the next version of the factory
// given the action
Factory next(Factory current, Action action, Sketch const& currsketch);

} // !Sk
} // !Core

#endif