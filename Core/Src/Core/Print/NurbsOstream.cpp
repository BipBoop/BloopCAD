
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "NurbsOstream.hpp"

std::ostream& operator<<(std::ostream& os, Core::Crv::NurbsCurve<2> curve)
{
  os<<"degree="<<curve.degree<<"\nknots=[";
  for(size_t i = 0; i < curve.knots.size(); ++i) {
    os<<curve.knots[i];
    if(i < curve.knots.size() - 1) {
      os<<", ";
    }
  }
  os<<"]\ncontrols=[";
  for(size_t i = 0; i < curve.controls.size(); ++i) {
    os<<"("<<curve.controls[i].x<<", "<<curve.controls[i].y<<", w"<<curve.controls[i].z<<")";
    if(i < curve.controls.size() - 1) {
      os<<", ";
    }
  }
  os<<"]";
  return os;
}
std::ostream& operator<<(std::ostream& os, Core::Crv::NurbsCurve<3> curve)
{
  os<<"degree="<<curve.degree<<"\nknots=[";
  for(size_t i = 0; i < curve.knots.size(); ++i) {
    os<<curve.knots[i];
    if(i < curve.knots.size() - 1) {
      os<<", ";
    }
  }
  os<<"]\ncontrols=[";
  for(size_t i = 0; i < curve.controls.size(); ++i) {
    os<<"("<<curve.controls[i].x<<", "<<curve.controls[i].y<<", "<<curve.controls[i].z<<",  w"<<curve.controls[i].w<<")";
    if(i < curve.controls.size() - 1) {
      os<<", ";
    }
  }
  os<<"]";
  return os;
}
