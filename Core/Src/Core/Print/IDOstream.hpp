
#ifndef BLOOP_CORE_IDOSTREAM_HPP_
#define BLOOP_CORE_IDOSTREAM_HPP_

#include <Core/Id.hpp>

std::ostream& operator<<(std::ostream& os, Core::WorkpieceId workpieceId);
std::ostream& operator<<(std::ostream& os, Core::SibId workpieceId);
std::ostream& operator<<(std::ostream& os, Core::Id workpieceId);

#endif