
#include "IDOstream.hpp"

std::ostream& operator<<(std::ostream& os, Core::WorkpieceId workpieceId)
{
  os<<"<"<<workpieceId.kind<<" ; "<<workpieceId.index<<">";
  return os;
}
std::ostream& operator<<(std::ostream& os, Core::SibId sibId)
{
  if(sibId.handle_num == -1) {
    os<<"#"<<sibId.kind<<" ; "<<sibId.index<<"#";
  } else {
    os<<"#"<<sibId.kind<<" ; "<<sibId.index<<" ; "<<sibId.handle_num<<"#";
  }
  return os;
}
std::ostream& operator<<(std::ostream& os, Core::Id id)
{
  os<<"["<<id.wpid<<id.entId<<"]";
  return os;
}