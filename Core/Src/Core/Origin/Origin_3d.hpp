
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef BLOOP_CORE_ORIGIN_3D_HPP_
#define BLOOP_CORE_ORIGIN_3D_HPP_

#include <Core/Surface/Surface.hpp>
#include <Core/Discrete/Mesh.hpp>
#include <Core/Id.hpp>

#include <array>

namespace Core {

struct Origin_3d {
  std::array<Srf::Surface, 3> planes; // xy, yz, zx
  glm::dvec3 origin;

  Origin_3d() = default;
  Origin_3d(glm::dvec3 origin_, glm::dvec3 x_, glm::dvec3 y_, glm::dvec3 z_)
    : origin(origin_)
    , planes{{  Srf::Surface(Srf::NurbsSurface::make_flat_rectangle({ origin_ - x_ - y_, origin_ + x_ - y_, origin_ + x_ + y_, origin_ - x_ + y_ })), 
                Srf::Surface(Srf::NurbsSurface::make_flat_rectangle({ origin_ - y_ - z_, origin_ + y_ - z_, origin_ + y_ + z_, origin_ - y_ + z_ })), 
                Srf::Surface(Srf::NurbsSurface::make_flat_rectangle({ origin_ - z_ - x_, origin_ + z_ - x_, origin_ + z_ + x_, origin_ - z_ + x_ }))}}
  {

  }
  Mesh plane_mesh(int which) const
  {
    return planes[which].to_mesh();
  }
  Mesh point_mesh() const
  {
    return Mesh(Mesh::Kind::Point, {  static_cast<float>(origin.x), 
                                      static_cast<float>(origin.y), 
                                      static_cast<float>(origin.z) }, { 0 });
  }

  
};

} // !Core

#endif