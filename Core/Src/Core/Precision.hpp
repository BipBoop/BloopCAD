
#ifndef BLOOP_CORE_PRECISION_HPP_
#define BLOOP_CORE_PRECISION_HPP_

namespace Core {
namespace Prc {

double constexpr kIntersection = 1e-6;
double constexpr kNurbs = 1e-6;
double constexpr kStraightEnough = 1e-6; // Pseudocurvature straigth enough to be considered a line
double constexpr kSolver = 1e-6;

} // !Prc
} // !Core

#endif
