
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "UpdReport.hpp"

namespace Core {

namespace Wp {

Diff Diff::make(WorkpieceId wpid, VecsSnapshot const& prev, VecsSnapshot const& curr)
{
  Diff out;
  if(prev.size() != curr.size()) {
    std::cerr<<"Number of dimensions mismatch on Diffing"<<std::endl;
    return out;
  }
  for(size_t i = 0; i < prev.size(); ++i) {
    if(prev[i].kind != curr[i].kind) {
      std::cerr<<"Kind mismatch on diffing at index "<<i<<" => "
        <<prev[i].kind<<" vs "<<curr[i].kind<<std::endl;
      continue;
    }
    out.add(wpid, prev[i].kind, prev[i].indexVec, curr[i].indexVec);
  }
  return out;
}
void Diff::add(WorkpieceId wpid, size_t kind, std::vector<VersionedIndex> const& prev, std::vector<VersionedIndex> const& curr)
{
  std::vector<VersionedIndex> tmp;
  auto added_it = std::set_difference(curr.begin(), curr.end(), prev.begin(), prev.end(), std::back_inserter(tmp));
  for(size_t i = 0; i < tmp.size(); ++i) {
    added.push_back(Id{.wpid=wpid, .entId={.kind=kind, .index=tmp[i].index}});
  }
  tmp.clear();

  auto removed_it = std::set_difference(prev.begin(), prev.end(), curr.begin(), curr.end(), std::back_inserter(tmp));
  for(size_t i = 0; i < tmp.size(); ++i) {
    removed.push_back(Id{.wpid=wpid, .entId={.kind=kind, .index=tmp[i].index}});
  }

  for(size_t i = 0; i < curr.size(); ++i) {
    for(size_t j = 0; j < prev.size(); ++j) {
      if(curr[i].index < prev[j].index) {
        break;
      } else if(curr[i].index == prev[j].index) {
        if(curr[i].version != prev[j].version)
          modified.push_back(Id{.wpid=wpid, .entId={.kind=kind, .index=curr[i].index}});
        break;
      }
    }
  }
}

void Diff::add_listdiff(std::vector<std::pair<size_t, int>> const& listdiff, size_t kind, WorkpieceId wpid)
{
  for(int i = 0; i < listdiff.size(); ++i) {
    switch(listdiff[i].second) {
    case 0:
      modified.push_back(Id{.wpid=wpid, .entId=SibId{.kind=kind, .index=listdiff[i].first}});
    case 1:
      added.push_back(Id{.wpid=wpid, .entId=SibId{.kind=kind, .index=listdiff[i].first}});
    case -1:
      removed.push_back(Id{.wpid=wpid, .entId=SibId{.kind=kind, .index=listdiff[i].first}});
    }
  }
}
void Diff::add_wplistdiff(std::vector<std::pair<size_t, int>> const& listdiff, size_t kind)
{
  for(int i = 0; i < listdiff.size(); ++i) {
    switch(listdiff[i].second) {
    case 0:
      modified.push_back(Id{.wpid={.kind=kind, .index=listdiff[i].first}});
    case 1:
      added.push_back(Id{.wpid={.kind=kind, .index=listdiff[i].first}});
    case -1:
      removed.push_back(Id{.wpid={.kind=kind, .index=listdiff[i].first}});
    }
  }
}
} // !Wp


UpdReport UpdReport::from_diff(WorkpieceId wpid, Wp::Diff const& diff)
{
  UpdReport out;
  out.wp_diffs.push_back({wpid, diff});
  return out;
}
void UpdReport::combine(UpdReport const& other)
{
  wp_diffs.insert(wp_diffs.end(), other.wp_diffs.begin(), other.wp_diffs.end());
  wp_buildSequence_mod.insert(wp_buildSequence_mod.end(), other.wp_buildSequence_mod.begin(), other.wp_buildSequence_mod.end());
  toolmsg += other.toolmsg;
}

} // !Core