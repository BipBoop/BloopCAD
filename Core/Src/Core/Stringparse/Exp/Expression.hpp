
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
#ifndef BLOOP_CORE_STR_EXPRESSION_HPP_
#define BLOOP_CORE_STR_EXPRESSION_HPP_

#include "Expression_forward.hpp"

#include <memory>
#include <string>
#include <vector>
#include <functional>
#include <map>

namespace Core {

/*
	@file This file describes expressions that interact with each other, currently they are all dynamic
	there might eventualy be a static (template based) version.
	Raw expressions are rarelly dealt with, the use of shared_ptr to wrap every expression simplifies allocation

	Expressions are created with functions such as cos, sin or pow with expressions as arguments as well as with
	overloaded operators such as +, -, *, etc..

	Eventually expression will have to be created by parsing a string (for useful dimensioning for instance), but it
	is not currently a thing
*/

/*
	@class Expression_abstr is a mathematical that does not implement most of it's functions,
	it describes the framework (value getter, derivative getter etc). It also has a debug id
*/
class Expression_abstr {
private:
	static int n_exp; // Number of abstract expression created
	int mID; // Debug ID of this expression
public:
	/*
		@constructor assigns the debug ID
	*/
	Expression_abstr();
	virtual ~Expression_abstr();

	/*
		@functiond eval computes the expression

		@return The value of the expression
	*/
	virtual double eval() = 0;
	/*
		@function derivative computes the derivative of the expression

		@return The derivative of the expression which is a smart pointer
	*/
	virtual exp_ptr derivative() = 0;
	/*
		@function d simple wrapped for @function derivative

		@return The derivative
	*/
	exp_ptr d();

	/*
		@function to_string prints the expression (currently it gives a very crude and ugly 
		string most of the time but it's purpose is to debug so no stress)
	*/
	virtual std::string to_string() = 0;
	/*
		@function id 

		@return The debug id of the expression
	*/
	int id() { return mID; }

	/*
		@function is_var 

		@return Wheter or not this expression is a variable

		@Note Not sure if it is a good practice, it is still manageable since it is
		a single function to check an instance type..
	*/
	virtual bool is_var() { return false; }

	int n_allowedArgs() { return n_allowedArgs_impl(); } // negative means variadic
	void set_arg(int ind, exp_ptr arg);

protected:
	virtual int n_allowedArgs_impl() = 0;
	virtual void set_arg_impl(int ind, exp_ptr arg) = 0;
};

/*
	@class Expression defines a framework to reuse derivatives and not compute them
	every time @function derivative is called

	@Note This class is the ancestor of all
*/
class Expression : public Expression_abstr {
public:
protected:
	exp_ptr mDerivative; 	// Pointer to derivative of expression, 
							// only calculated on demand to avoid recursive derivative calculations
public:
	Expression();
	virtual ~Expression();

	/*
		@function derivative generates a derivative if it is the first
		time it is called

		@return The derivative of the expression
	*/
	exp_ptr derivative() override;
	/*
		@function generate_derivative creates the derivative of the expression

		@return The newly created derivative
	*/
	virtual exp_ptr generate_derivative() = 0;
};


/*
	@class UnaryExp describes an expression that has 1 parameter (e.g. sin, cos, tan)
*/
class UnaryExp : public Expression {
protected:
	exp_ptr mOperand; // Parameter of the expression onto which the expression acts

public:
	UnaryExp();
	/*
		@constructor

		@param operand The parameter of the expression
	*/
	UnaryExp(exp_ptr operand);

protected:
	int n_allowedArgs_impl() { return 1; }
	void set_arg_impl(int ind, exp_ptr arg);
};

/*
	@class UnaryExp describes an expression that has 2 parameter (e.g. addition, substraction, multiplication)
*/
class BinaryExp : public Expression {
protected:
	exp_ptr mLeft, mRight; // Operands

public:
	BinaryExp();
	/*
		@constructor

		@param left		The left hand side of the expression
		@param right	The right hand side of the expression
	*/
	BinaryExp(exp_ptr left, exp_ptr right);

protected:
	int n_allowedArgs_impl() { return 2; }
	void set_arg_impl(int ind, exp_ptr arg);
};

class VariadicExpression : public Expression {
protected:
	std::vector<exp_ptr> mArgs;

public:
	VariadicExpression() = default;

	std::string to_string();
protected:
	int n_args_current() { return mArgs.size(); }
	int n_allowedArgs_impl() { return -1; }
	void set_arg_impl(int ind, exp_ptr arg);
};

/*
	@class ExpConst is an expression that is a numeric constant or a coefficient
*/
class ExpConst : public Expression {
public:
	// Some useful constants, no need to create *zero* multiple time for instance
	static std::shared_ptr<ExpConst> zero;
	static std::shared_ptr<ExpConst> one_half;
	static std::shared_ptr<ExpConst> one;
	static std::shared_ptr<ExpConst> two;
	static std::shared_ptr<ExpConst> e;
	static std::shared_ptr<ExpConst> pi;
	static std::shared_ptr<ExpConst> pi2;
	static std::shared_ptr<ExpConst> phi;

private:
	double mVal; // Value of the constant

public:
	/*
		@constructor 

		@param val The value of the constant, cannot be changed, ever
	*/
	ExpConst(double val);
	/*
		@function make is a light wrapper around std::make_shared<ExpConst>(val)
		
		@param val The value of the constant expression to create
		@return The created expression pointer

		@static
	*/
	static exp_ptr make(double val);

	double eval() override;
	exp_ptr generate_derivative() override;

	std::string to_string() override;

protected:
	int n_allowedArgs_impl() override { return 1; }
	void set_arg_impl(int ind, exp_ptr arg) override;
};


// class ExpCoeff : public Expression {
// protected:
// 	double mVal;
// 	int mUnit;
// 	double mUnit_to_internalFormat, mInternalFormat_to_unit;
// public:
// 	ExpCoeff();

// 	double eval() override;
// 	exp_ptr generate_derivative() override;

// 	std::string to_string() override;

// 	void set_unit(int unit_, bool convert_currentValue_to_newUnit);
// 	int unit();
// 	void set_val(double val_);
// 	double val(); // not to confuse with eval. val() returns the value in the current unit
// protected:
// 	virtual std::string unit_symbol(int unit_) { return ""; }
// 	virtual double unit_to_internalFormat(int unit_) { return 1.0; }
// 	virtual double internalFormat_to_unit(int unit_) { return 1.0 / unit_to_internalFormat(unit_); }
// };

// class ExpCoeffAngle : public ExpCoeff {
// public:
// 	enum units { RAD, DEG };
// public:
// 	ExpCoeffAngle(double angle, int unit_ = units::RAD);

// 	double eval() override;

// 	static std::shared_ptr<ExpCoeffAngle> make(double angle, int unit_ = units::RAD);
// protected:
// 	virtual std::string unit_symbol(int unit_);
// 	virtual double unit_to_internalFormat(int unit_);
// };
// class ExpCoeffLength : public ExpCoeff {
// public:
// 	enum units { MM, CM, DM, M, IN, FT };
// public:
// 	ExpCoeffLength(double length, int unit_ = units::CM);

// 	static std::shared_ptr<ExpCoeffLength> make(double length, int unit_ = units::CM);
// protected:
// 	virtual std::string unit_symbol(int unit_);
// 	virtual double unit_to_internalFormat(int unit_);
// };


class ExpPlus : public UnaryExp {
public:
	ExpPlus(exp_ptr operand);

	double eval() override;
	exp_ptr generate_derivative() override;

	std::string to_string() override;
};

class ExpMinus : public UnaryExp {
public:
	ExpMinus(exp_ptr operand);

	double eval() override;
	exp_ptr generate_derivative() override;

	std::string to_string() override;
};

class ExpSin : public UnaryExp {
public:
	ExpSin() = default;
	ExpSin(exp_ptr operand);

	double eval() override;
	exp_ptr generate_derivative() override;

	std::string to_string() override;
};
class ExpAsin : public UnaryExp {
public:
	ExpAsin() = default;
	ExpAsin(exp_ptr operand);

	double eval() override;
	exp_ptr generate_derivative() override;

	std::string to_string() override;
};
class ExpCsc : public UnaryExp {
public:
	ExpCsc() = default;
	ExpCsc(exp_ptr operand);

	double eval() override;
	exp_ptr generate_derivative() override;

	std::string to_string() override;
};


class ExpCos : public UnaryExp {
public:
	ExpCos() = default;
	ExpCos(exp_ptr operand);

	double eval() override;
	exp_ptr generate_derivative() override;

	std::string to_string() override;
};
class ExpAcos : public UnaryExp {
public:
	ExpAcos() = default;
	ExpAcos(exp_ptr operand);

	double eval() override;
	exp_ptr generate_derivative() override;

	std::string to_string() override;
};
class ExpSec : public UnaryExp {
public:
	ExpSec() = default;
	ExpSec(exp_ptr operand);

	double eval() override;
	exp_ptr generate_derivative() override;

	std::string to_string() override;
};

class ExpTan : public UnaryExp {
public:
	ExpTan() = default;
	ExpTan(exp_ptr operand);

	double eval() override;
	exp_ptr generate_derivative() override;

	std::string to_string() override;
};
class ExpAtan2 : public BinaryExp {
public:
	ExpAtan2() = default;
	ExpAtan2(exp_ptr left, exp_ptr right);

	double eval() override;
	exp_ptr generate_derivative() override;

	std::string to_string() override;
};
class ExpCot : public UnaryExp {
public:
	ExpCot() = default;
	ExpCot(exp_ptr operand);

	double eval() override;
	exp_ptr generate_derivative() override;

	std::string to_string() override;
};

/*
	@class ExpAbsDerivative is the derivative of an absolute value,
	it needs a separate class because the derivative of an absolute value depends on
	the value of it's opperand.
*/
class ExpAbs;
class ExpAbsDerivative : public Expression {
private:
	ExpAbs* mAbs; // Variable that this expression is the derivative of
public:
	/*
		@constructor

		@param var The ExpAbs that this expression is the derivative of
	*/
	ExpAbsDerivative(ExpAbs* abs);

	/*
		@function eval checks if the variable is a coefficient

		@return 0 if the variable is in coefficient mode and 1 if not
	*/
	double eval() override;
	exp_ptr generate_derivative() override;

	virtual std::string to_string() override { return "[abs derivative]"; }

protected:
	int n_allowedArgs_impl() override { return 0; }
	void set_arg_impl(int ind, exp_ptr) override {}
};

class ExpAbs : public UnaryExp { 
public:
	friend ExpAbsDerivative;
	ExpAbs() = default;
	ExpAbs(exp_ptr operand);

	double eval() override;
	exp_ptr generate_derivative() override;

	std::string to_string() override;
};
// This one is tricky, it won't have a derivative so to speak, 
// it is a unary Expression, but has two component (one is fixed)
class ExpMod : public UnaryExp {
private:
	double mMod;
public:
	ExpMod(exp_ptr operand, double modulo);

	double eval() override;
	exp_ptr generate_derivative() override;

	std::string to_string() override;
};

class ExpAdd : public BinaryExp {
public:
	ExpAdd(exp_ptr left, exp_ptr right);

	double eval() override;
	exp_ptr generate_derivative() override;

	std::string to_string() override;
};

class ExpSubstr : public BinaryExp {
public:
	ExpSubstr(exp_ptr left, exp_ptr right);

	double eval() override;
	exp_ptr generate_derivative() override;

	std::string to_string() override;
};

class ExpMult : public BinaryExp {
public:
	ExpMult(exp_ptr left, exp_ptr right);

	double eval() override;
	exp_ptr generate_derivative() override;

	std::string to_string() override;
};

class ExpDiv : public BinaryExp {
public:
	ExpDiv(exp_ptr numerator, exp_ptr denominator);

	double eval() override;
	exp_ptr generate_derivative() override;

	std::string to_string() override;
};

class ExpPow : public BinaryExp {
public:
	/*
		@constructor ExpPow

		@param base 	The base of the exponent
		@param power 	The power of the exponent, it is assumed to not change after creation
	*/
	ExpPow(exp_ptr base, exp_ptr power);

	double eval() override;
	exp_ptr generate_derivative() override;

	std::string to_string() override;
};

class ExpMax : public VariadicExpression {
public:
	ExpMax() = default;

	double eval() override;
	exp_ptr generate_derivative() override;

	std::string to_string() override;
};

class ExpMin : public VariadicExpression {
public:
	ExpMin() = default;

	double eval() override;
	exp_ptr generate_derivative() override;

	std::string to_string() override;
};

/*
	Many operators and function are defined to create organic 
	expression writing
*/

/*
	Overloaded operators
*/
exp_ptr operator+(exp_ptr left, exp_ptr right);
exp_ptr operator-(exp_ptr left, exp_ptr right);
exp_ptr operator*(exp_ptr left, exp_ptr right);
exp_ptr operator/(exp_ptr left, exp_ptr right);
exp_ptr operator+(double left, exp_ptr right);
exp_ptr operator-(double left, exp_ptr right);
exp_ptr operator*(double left, exp_ptr right);
exp_ptr operator/(double left, exp_ptr right);
exp_ptr operator+(exp_ptr left, double right);
exp_ptr operator-(exp_ptr left, double right);
exp_ptr operator*(exp_ptr left, double right);
exp_ptr operator/(exp_ptr left, double right);
exp_ptr operator+(exp_ptr expr);
exp_ptr operator-(exp_ptr expr);

/*
	Functions using exp_ptr
*/
exp_ptr pow(exp_ptr base, exp_ptr power);
exp_ptr pow(exp_ptr base, double power);
exp_ptr sqrt(exp_ptr expr);
exp_ptr sin(exp_ptr expr);
exp_ptr asin(exp_ptr expr);
exp_ptr csc(exp_ptr expr);
exp_ptr cos(exp_ptr expr);
exp_ptr acos(exp_ptr expr);
exp_ptr sec(exp_ptr expr);
exp_ptr tan(exp_ptr expr);
exp_ptr atan2(exp_ptr left, exp_ptr right);
exp_ptr cot(exp_ptr expr);
exp_ptr abs(exp_ptr expr);
exp_ptr mod(exp_ptr expr, double modulo);

exp_ptr dot(exp_ptr x1, exp_ptr y1, exp_ptr x2, exp_ptr y2);

} // !Core

/*
	Print functions (never used as of now)
*/
std::ostream& operator <<(std::ostream& os, Core::exp_ptr expr);

#endif