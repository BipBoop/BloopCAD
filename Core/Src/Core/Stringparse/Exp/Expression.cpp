
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
#include "Expression.hpp"

#include <cmath>
#include <algorithm>
#include <iostream>
#include <stack>

namespace Core {

/*
	@define SIMPLIFY_EXP_AT_OPERATOR enables simple simplifications
	at run time for expression creation (e.g x + 0 will return x),
	it will also catch invalid expressions such as division by zeroget_single_var
*/
// #define SIMPLIFY_EXP_AT_OPERATOR

/*
	Useful constants
*/
std::shared_ptr<ExpConst> ExpConst::zero(new ExpConst(0.0));
std::shared_ptr<ExpConst> ExpConst::one_half(new ExpConst(0.5));
std::shared_ptr<ExpConst> ExpConst::one(new ExpConst(1.0));
std::shared_ptr<ExpConst> ExpConst::two(new ExpConst(2.0));
std::shared_ptr<ExpConst> ExpConst::e(new ExpConst(M_E));
std::shared_ptr<ExpConst> ExpConst::pi(new ExpConst(M_PI));
std::shared_ptr<ExpConst> ExpConst::pi2(new ExpConst(M_PI_2));
std::shared_ptr<ExpConst> ExpConst::phi(new ExpConst(1.618033988749));

int Expression_abstr::n_exp = 0;

Expression_abstr::Expression_abstr()
	: mID(n_exp++) // ID for debug purposes
{

}
Expression_abstr::~Expression_abstr()
{

}

exp_ptr Expression_abstr::d()
{
	return derivative();
}

void Expression_abstr::set_arg(int ind, exp_ptr arg)
{
	if((ind < n_allowedArgs() || n_allowedArgs() < 0) && ind >= 0)
		set_arg_impl(ind, arg);
}

Expression::Expression()
	: mDerivative(nullptr)
{

}
Expression::~Expression()
{
	
}
exp_ptr Expression::derivative()
{
	if(!mDerivative)
		mDerivative = generate_derivative();
	return mDerivative;
}

UnaryExp::UnaryExp()
	: mOperand(nullptr)
{

}
UnaryExp::UnaryExp(exp_ptr operand)
	: mOperand(operand)
{

}

void UnaryExp::set_arg_impl(int ind, exp_ptr arg)
{
	mOperand = arg;
}

BinaryExp::BinaryExp()
	: mLeft(nullptr)
	, mRight(nullptr)
{

}
BinaryExp::BinaryExp(exp_ptr left, exp_ptr right)
	: mLeft(left)
	, mRight(right)
{

}

void BinaryExp::set_arg_impl(int ind, exp_ptr arg)
{
	if(ind == 0) {
		mLeft = arg;
	} else {
		mRight = arg;
	}
}

/* -------------- VariadicExpression -------------- */

std::string VariadicExpression::to_string()
{
	std::string out = "(";

	for(int i = 0; i < n_args_current(); ++i) {
		out += mArgs[i]->to_string();
		
		if(i != n_args_current() - 1)
			out += ", ";
	}
	out += ")";
	return out;
}

void VariadicExpression::set_arg_impl(int ind, exp_ptr arg)
{
	if(ind < 0)
		return;
	if(ind >= n_args_current()) {
		mArgs.resize(ind+1);
	}
	mArgs[ind] = arg;
}

/* -------------- End VariadicExpression -------------- */

/* -------------- Const -------------- */
ExpConst::ExpConst(double val)
{
	mVal = val;
}

exp_ptr ExpConst::make(double val)
{
	return exp_ptr(new ExpConst(val));
}

double ExpConst::eval()
{
	return mVal;
}
exp_ptr ExpConst::generate_derivative()
{
	return ExpConst::zero;
}

std::string ExpConst::to_string()
{
	return std::to_string(mVal);
}

void ExpConst::set_arg_impl(int ind, exp_ptr arg)
{
	mVal = arg->eval();
}

/* -------------- End const -------------- */
/* -------------- Coefficient -------------- */
// ExpCoeff::ExpCoeff()
// {

// }

// double ExpCoeff::eval()
// {
// 	return mVal;
// }
// exp_ptr ExpCoeff::generate_derivative()
// {
// 	return ExpConst::zero;
// }
// std::string ExpCoeff::to_string()
// {
// 	return std::to_string(val()) + unit_symbol(mUnit);
// }
// void ExpCoeff::set_unit(int unit_, bool convert_currentValue_to_newUnit)
// {
// 	if(convert_currentValue_to_newUnit) {
// 		double oldUnit_val = val();
// 		mUnit = unit_;
// 		set_val(oldUnit_val);
// 	} else {
// 		mUnit = unit_;
// 	}
// 	mUnit_to_internalFormat = unit_to_internalFormat(unit_);
// 	mInternalFormat_to_unit = internalFormat_to_unit(unit_);
// }
// int ExpCoeff::unit()
// {
// 	return mUnit;
// }
// void ExpCoeff::set_val(double val)
// {
// 	mVal = val * mUnit_to_internalFormat;
// }
// double ExpCoeff::val()
// {
// 	return mVal * mInternalFormat_to_unit;
// }

// /* -------------- End coefficient -------------- */
// /* -------------- Coefficient angle -------------- */
// ExpCoeffAngle::ExpCoeffAngle(double angle, int unit_)
// {
// 	set_unit(unit_, false);
// 	set_val(angle);
// }
// double ExpCoeffAngle::eval()
// {
// 	return std::fmod(ExpCoeff::eval(), M_PI * 2.0);
// }
// std::shared_ptr<ExpCoeffAngle> ExpCoeffAngle::make(double angle, int unit_)
// {
// 	return std::make_shared<ExpCoeffAngle>(angle, unit_);
// }
// std::string ExpCoeffAngle::unit_symbol(int unit_)
// {
// 	switch(mUnit) {
// 	case units::DEG:
// 		return " deg";
// 	default:
// 		return " rad";
// 	};
// }
// double ExpCoeffAngle::unit_to_internalFormat(int unit_)
// {
// 	switch(mUnit) {
// 	case units::DEG:
// 		return M_PI / 180.0;
// 	default:
// 		return 1.0;
// 	};
// }
// /* -------------- End coefficient angle -------------- */
// /* -------------- Coefficient length -------------- */
// ExpCoeffLength::ExpCoeffLength(double length, int unit_)
// {
// 	set_unit(unit_, false);
// 	set_val(length);
// }
// std::shared_ptr<ExpCoeffLength> ExpCoeffLength::make(double length, int unit_)
// {
// 	return std::make_shared<ExpCoeffLength>(length, unit_);
// }
// std::string ExpCoeffLength::unit_symbol(int unit_)
// {
// 	switch(mUnit) {
// 	case units::MM:
// 		return " mm";
// 	case units::CM:
// 		return " cm";
// 	case units::DM:
// 		return " dm";
// 	case units::M:
// 		return " m";
// 	case units::IN:
// 		return " in";
// 	case units::FT:
// 		return " ft";
// 	default:
// 		return " cm";
// 	};
// }
// double ExpCoeffLength::unit_to_internalFormat(int unit_)
// {
// 	switch(mUnit) {
// 	case units::MM:
// 		return 1.0e-1;
// 	case units::CM:
// 		return 1.0;
// 	case units::DM:
// 		return 1.0e1;
// 	case units::M:
// 		return 1.0e2;
// 	case units::IN:
// 		return 2.54;
// 	case units::FT:
// 		return 30.48;
// 	default:
// 		return 1.0;
// 	};
// }
/* -------------- End coefficient length -------------- */

/* -------------- Plus -------------- */
ExpPlus::ExpPlus(exp_ptr operand)
	: UnaryExp(operand)
{

}

double ExpPlus::eval()
{
	return mOperand->eval();
}
exp_ptr ExpPlus::generate_derivative()
{
	return mOperand->derivative();
}

std::string ExpPlus::to_string()
{
	return  "+" + mOperand->to_string();
}
/* -------------- End plus -------------- */

/* -------------- Minus -------------- */
ExpMinus::ExpMinus(exp_ptr operand)
	: UnaryExp(operand)
{

}

double ExpMinus::eval()
{
	return -mOperand->eval();
}
exp_ptr ExpMinus::generate_derivative()
{
	return -mOperand->derivative();
}

std::string ExpMinus::to_string()
{
	return  "-" + mOperand->to_string();
}
/* -------------- End minus -------------- */

/* -------------- Sin -------------- */
ExpSin::ExpSin(exp_ptr operand)
	: UnaryExp(operand)
{

}

double ExpSin::eval()
{
	return std::sin(mOperand->eval());
}
exp_ptr ExpSin::generate_derivative()
{
	return cos(mOperand) * mOperand->derivative();
}

std::string ExpSin::to_string()
{
	return  "sin(" + mOperand->to_string() + ")";
}
/* -------------- End sin -------------- */
/* -------------- Asin -------------- */
ExpAsin::ExpAsin(exp_ptr operand)
	: UnaryExp(operand)
{

}

double ExpAsin::eval()
{
	return std::asin(mOperand->eval());
}
exp_ptr ExpAsin::generate_derivative()
{
	return ExpConst::one / sqrt(ExpConst::one - pow(mOperand, 2)) * mOperand->derivative();
}

std::string ExpAsin::to_string()
{
	return  "asin(" + mOperand->to_string() + ")";
}
/* -------------- End asin -------------- */
/* -------------- Csc -------------- */
ExpCsc::ExpCsc(exp_ptr operand)
	: UnaryExp(operand)
{

}

double ExpCsc::eval()
{
	return 1.0f / std::sin(mOperand->eval());
}
exp_ptr ExpCsc::generate_derivative()
{
	return -cot(mOperand) * csc(mOperand) * mOperand->derivative();
}

std::string ExpCsc::to_string()
{
	return  "csc(" + mOperand->to_string() + ")";
}
/* -------------- End asin -------------- */

/* -------------- Cos -------------- */
ExpCos::ExpCos(exp_ptr operand)
	: UnaryExp(operand)
{

}

double ExpCos::eval()
{
	return std::cos(mOperand->eval());
}
exp_ptr ExpCos::generate_derivative()
{
	return -sin(mOperand) * mOperand->derivative();
}

std::string ExpCos::to_string()
{
	return  "cos(" + mOperand->to_string() + ")";
}
/* -------------- End cos -------------- */
/* -------------- Acos -------------- */
ExpAcos::ExpAcos(exp_ptr operand)
	: UnaryExp(operand)
{

}

double ExpAcos::eval()
{
	return std::acos(mOperand->eval());
}
exp_ptr ExpAcos::generate_derivative()
{
	return -(ExpConst::one / sqrt(ExpConst::one - pow(mOperand, 2.0f))) * mOperand->derivative();
}

std::string ExpAcos::to_string()
{
	return  "acos(" + mOperand->to_string() + ")";
}
/* -------------- End acos -------------- */
/* -------------- Sec -------------- */
ExpSec::ExpSec(exp_ptr operand)
	: UnaryExp(operand)
{

}

double ExpSec::eval()
{
	return 1.0f / std::cos(mOperand->eval());
}
exp_ptr ExpSec::generate_derivative()
{
	return -sec(mOperand) * tan(mOperand) * mOperand->derivative();
}

std::string ExpSec::to_string()
{
	return  "sec(" + mOperand->to_string() + ")";
}
/* -------------- End sec -------------- */

/* -------------- Tan -------------- */
ExpTan::ExpTan(exp_ptr operand)
	: UnaryExp(operand)
{

}

double ExpTan::eval()
{
	return std::tan(mOperand->eval());
}
exp_ptr ExpTan::generate_derivative()
{
	return pow(sec(mOperand), 2.0f) * mOperand->derivative();
}

std::string ExpTan::to_string()
{
	return  "tan(" + mOperand->to_string() + ")";
}
/* -------------- End tan -------------- */
/* -------------- Atan2 -------------- */
ExpAtan2::ExpAtan2(exp_ptr left, exp_ptr right)
	: BinaryExp(left, right)
{

}

double ExpAtan2::eval()
{
	return std::atan2(mLeft->eval(), mRight->eval());
}
exp_ptr ExpAtan2::generate_derivative()
{
	return mRight / (pow(mLeft, 2.0f) + pow(mRight, 2.0f)) * mLeft->derivative() * mRight->derivative();
}

std::string ExpAtan2::to_string()
{
	return  "atan2(" + mLeft->to_string() + ", " + mRight->to_string() + ")";
}
/* -------------- End atan2 -------------- */
/* -------------- Cot -------------- */
ExpCot::ExpCot(exp_ptr operand)
	: UnaryExp(operand)
{

}

double ExpCot::eval()
{
	return 1.0f / std::tan(mOperand->eval());
}
exp_ptr ExpCot::generate_derivative()
{
	return -pow(csc(mOperand), 2.0f) * mOperand->derivative();
}

std::string ExpCot::to_string()
{
	return  "cot(" + mOperand->to_string() + ")";
}
/* -------------- End cot -------------- */
/* -------------- Abs -------------- */
ExpAbsDerivative::ExpAbsDerivative(ExpAbs* var)
	: mAbs(var)
{

}

double ExpAbsDerivative::eval()
{
	if(mAbs->mOperand->eval() > 0) 
		return mAbs->mOperand->derivative()->eval();
	return -mAbs->mOperand->derivative()->eval(); // not legit if the operands evaluates at 0 because abs'() is not defined at 0
}
exp_ptr ExpAbsDerivative::generate_derivative()
{
	std::cerr<<"This part should not be reached aaaaaaaaa >.<"<<std::endl;
	return ExpConst::zero; // Not true but it should not be reached..
}

ExpAbs::ExpAbs(exp_ptr operand)
	: UnaryExp(operand)
{

}

double ExpAbs::eval()
{
	return std::abs(mOperand->eval());
}
exp_ptr ExpAbs::generate_derivative()
{
	return std::make_shared<ExpAbsDerivative>(this);	
}

std::string ExpAbs::to_string()
{
	return  "|" + mOperand->to_string() + "|";
}
/* -------------- End abs -------------- */
/* -------------- Mod -------------- */
ExpMod::ExpMod(exp_ptr operand, double modulo)
	: UnaryExp(operand)
	, mMod(modulo)
{

}

double ExpMod::eval()
{
	return std::fmod(mOperand->eval(), mMod);
}
exp_ptr ExpMod::generate_derivative()
{
	return mOperand->derivative(); // reeeeeeeeeeeeee
}

std::string ExpMod::to_string()
{
	return  mOperand->to_string() + " % " + std::to_string(mMod);
}
/* -------------- End mod -------------- */

/* -------------- Add -------------- */
ExpAdd::ExpAdd(exp_ptr left, exp_ptr right)
	: BinaryExp(left, right)
{

}

double ExpAdd::eval()
{
	return mLeft->eval() + mRight->eval();
}
exp_ptr ExpAdd::generate_derivative()
{
	return mLeft->derivative() + mRight->derivative();
}

std::string ExpAdd::to_string()
{
	return "(" + mLeft->to_string() + " + " + mRight->to_string() + ")";
}
/* -------------- End add -------------- */

/* -------------- Substr -------------- */
ExpSubstr::ExpSubstr(exp_ptr left, exp_ptr right)
	: BinaryExp(left, right)
{

}

double ExpSubstr::eval()
{
	return mLeft->eval() - mRight->eval();
}
exp_ptr ExpSubstr::generate_derivative()
{
	return mLeft->derivative() - mRight->derivative();
}

std::string ExpSubstr::to_string()
{
	return "(" + mLeft->to_string() + " - " + mRight->to_string() + ")";
}
/* -------------- End substr -------------- */


/* -------------- Mult -------------- */
ExpMult::ExpMult(exp_ptr left, exp_ptr right)
	: BinaryExp(left, right)
{

}

double ExpMult::eval()
{
	return mLeft->eval() * mRight->eval();
}
exp_ptr ExpMult::generate_derivative()
{
	return mLeft->derivative() * mRight + mLeft * mRight->derivative();
}

std::string ExpMult::to_string()
{
	return mLeft->to_string() + " * " + mRight->to_string();
}
/* -------------- End mult -------------- */

/* -------------- Div -------------- */
ExpDiv::ExpDiv(exp_ptr left, exp_ptr right)
	: BinaryExp(left, right)
{

}

double ExpDiv::eval()
{
	double right_val = mRight->eval();
	if(std::abs(right_val) < 1e-5)
		right_val = 1.0f;
	return mLeft->eval() / right_val;
}
exp_ptr ExpDiv::generate_derivative()
{
	return (mLeft->derivative() * mRight - mLeft * mRight->derivative()) / pow(mRight, 2.0);
}

std::string ExpDiv::to_string()
{
	return mLeft->to_string() + " / " + mRight->to_string();
}
/* -------------- End div -------------- */

/* -------------- Pow -------------- */
ExpPow::ExpPow(exp_ptr base, exp_ptr power)
	: BinaryExp(base, power)
{

}

double ExpPow::eval()
{
	return std::pow(mLeft->eval(), mRight->eval());
}
exp_ptr ExpPow::generate_derivative()
{
	return mRight->eval() * pow(mLeft, mRight->eval() - 1.0) * mLeft->derivative(); // This assumes that the right opperand will not change dynamically
}

std::string ExpPow::to_string()
{
	return  "(" + mLeft->to_string() + "^" + mRight->to_string() + ")";
}
/* -------------- End pow -------------- */

/* -------------- Max -------------- */

double ExpMax::eval()
{
	double maxVal = std::numeric_limits<double>::min();
	for(int i = 0; i < n_args_current(); ++i) {
		double eval = mArgs[i]->eval();
		if(eval > maxVal) {
			maxVal = eval;
		}
	}

	return maxVal;
}
exp_ptr ExpMax::generate_derivative()
{
	return ExpConst::one;
}

std::string ExpMax::to_string()
{
	return "max" + VariadicExpression::to_string();
}

/* -------------- End Max -------------- */

/* -------------- Min -------------- */

double ExpMin::eval()
{
	double minVal = std::numeric_limits<double>::max();
	for(int i = 0; i < n_args_current(); ++i) {
		double eval = mArgs[i]->eval();
		if(eval < minVal) {
			minVal = eval;
		}
	}

	return minVal;
}
exp_ptr ExpMin::generate_derivative()
{
	return ExpConst::one;
}

std::string ExpMin::to_string()
{
	return "min" + VariadicExpression::to_string();
}

/* -------------- End Min -------------- */

exp_ptr operator+(exp_ptr left, exp_ptr right)
{
#ifdef SIMPLIFY_EXP_AT_OPERATOR
	if(left == ExpConst::zero) {
		return right;
	} else if(right == ExpConst::zero) {
		return left;
	}
#endif
	return exp_ptr(new ExpAdd(left, right));
}
exp_ptr operator-(exp_ptr left, exp_ptr right)
{
#ifdef SIMPLIFY_EXP_AT_OPERATOR
	if(left == ExpConst::zero) {
		return -right;
	} else if(right == ExpConst::zero) {
		return left;
	}
#endif
	return exp_ptr(new ExpSubstr(left, right));
}
exp_ptr operator*(exp_ptr left, exp_ptr right)
{
#ifdef SIMPLIFY_EXP_AT_OPERATOR
	if(left == ExpConst::zero || right == ExpConst::zero) {
		return ExpConst::zero;
	} else if(left == ExpConst::one) {
		return right;
	} else if(right == ExpConst::one) {
		return left;
	}
#endif
	return exp_ptr(new ExpMult(left, right));
}
exp_ptr operator/(exp_ptr left, exp_ptr right)
{
#ifdef SIMPLIFY_EXP_AT_OPERATOR
	if(left == ExpConst::zero) {
		return ExpConst::zero;
	} else if(right == ExpConst::zero) {
		std::cerr<<"Division by zero."<<__FILE__<<__LINE__<<std::endl;
	} else if(right == ExpConst::one) {
		return left;
	}
#endif
	return exp_ptr(new ExpDiv(left, right));
}

exp_ptr operator+(double left, exp_ptr right)
{
#ifdef SIMPLIFY_EXP_AT_OPERATOR
	if(left == 0.0)
		return right;
#endif
	return ExpConst::make(left) + right;
}
exp_ptr operator-(double left, exp_ptr right)
{
#ifdef SIMPLIFY_EXP_AT_OPERATOR
	if(left == 0.0)
		return -right;
#endif
	return ExpConst::make(left) - right;
}
exp_ptr operator*(double left, exp_ptr right)
{
#ifdef SIMPLIFY_EXP_AT_OPERATOR
	if(left == 0.0) {
		return ExpConst::zero;
	} else if(left == 1.0) {
		return right;
	}
#endif
	return ExpConst::make(left) * right;
}
exp_ptr operator/(double left, exp_ptr right)
{
	if(left == 0.0) {
		return ExpConst::zero;
	}
	return ExpConst::make(left) / right;
}
exp_ptr operator+(exp_ptr left, double right)
{
#ifdef SIMPLIFY_EXP_AT_OPERATOR
	if(right == 0.0) {
		return left;
	}
#endif
	return left + ExpConst::make(right);
}
exp_ptr operator-(exp_ptr left, double right)
{
#ifdef SIMPLIFY_EXP_AT_OPERATOR
	if(right == 0.0) {
		return left;
	}
#endif
	return left - ExpConst::make(right);
}
exp_ptr operator*(exp_ptr left, double right)
{
#ifdef SIMPLIFY_EXP_AT_OPERATOR
	if(right == 0.0) {
		return ExpConst::zero;
	} else if(right == 1.0) {
		return left;
	}
#endif
	return left * ExpConst::make(right);
}
exp_ptr operator/(exp_ptr left, double right)
{
#ifdef SIMPLIFY_EXP_AT_OPERATOR
	if(right == 0.0) {
		std::cerr<<"Division by 0."<<__FILE__<<__LINE__<<std::endl;
	} else if(right == 1.0) {
		return left;
	}
#endif
	return left / ExpConst::make(right);
}

exp_ptr operator+(exp_ptr expr)
{
	return exp_ptr(new ExpPlus(expr));
}
exp_ptr operator-(exp_ptr expr)
{
	return exp_ptr(new ExpMinus(expr));
}

exp_ptr pow(exp_ptr base, exp_ptr power)
{
	return exp_ptr(new ExpPow(base, power));
}
exp_ptr pow(exp_ptr base, double power)
{
	return pow(base, exp_ptr(new ExpConst(power)));
}
exp_ptr sqrt(exp_ptr expr)
{
	return pow(expr, ExpConst::one_half);
}

exp_ptr sin(exp_ptr expr)
{
	return exp_ptr(new ExpSin(expr));
}
exp_ptr asin(exp_ptr expr)
{
	return exp_ptr(new ExpAsin(expr));
}
exp_ptr csc(exp_ptr expr)
{
	return exp_ptr(new ExpCsc(expr));
}
exp_ptr cos(exp_ptr expr)
{
	return exp_ptr(new ExpCos(expr));
}
exp_ptr acos(exp_ptr expr)
{
	return exp_ptr(new ExpAcos(expr));
}
exp_ptr sec(exp_ptr expr)
{
	return exp_ptr(new ExpSec(expr));
}
exp_ptr tan(exp_ptr expr)
{
	return exp_ptr(new ExpTan(expr));
}
exp_ptr atan2(exp_ptr left, exp_ptr right)
{
	return exp_ptr(new ExpAtan2(left, right));
}
exp_ptr cot(exp_ptr expr)
{
	return exp_ptr(new ExpCot(expr));
}
exp_ptr abs(exp_ptr expr)
{
	return exp_ptr(new ExpAbs(expr));
}
exp_ptr mod(exp_ptr expr, double modulo)
{
	return exp_ptr(new ExpMod(expr, modulo));
}

exp_ptr dot(exp_ptr x1, exp_ptr y1, exp_ptr x2, exp_ptr y2)
{
	return x1 * x2 + y1 * y2;
}

} // !Core

std::ostream& operator <<(std::ostream& os, Core::exp_ptr expr)
{
	os<<expr->to_string();
	return os;
}