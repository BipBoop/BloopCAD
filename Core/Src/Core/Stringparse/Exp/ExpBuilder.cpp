
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
#include "ExpBuilder.hpp"

#include "Parselets/PrefixOperatorParselet.hpp"
#include "Parselets/NumericParselet.hpp"
#include "Parselets/BinaryOperatorParselet.hpp"
#include "Parselets/CallParselet.hpp"
#include "Parselets/NameParselet.hpp"
#include "Parselets/GroupParselet.hpp"

#include <cmath>

namespace Core {

ExpBuilder::ExpBuilder()
{
	mParser.register_parselet(Token::NAME, std::make_shared<NameParselet>());
	mParser.register_parselet(Token::MINUS, std::make_shared<PrefixOperatorParselet<ExpMinus>>());
	mParser.register_parselet(Token::PLUS, std::make_shared<PrefixOperatorParselet<ExpPlus>>());
	mParser.register_parselet(Token::NUMERIC, std::make_shared<NumericParselet>());
	mParser.register_parselet(Token::LEFT_PARENT, std::make_shared<GroupParselet>());

	mParser.register_parselet(Token::PLUS, std::make_shared<BinaryOperatorParselet<ExpAdd, ExpParselet::PrecedenceVals::SUM>>());
	mParser.register_parselet(Token::MINUS, std::make_shared<BinaryOperatorParselet<ExpSubstr, ExpParselet::PrecedenceVals::SUM>>());
	mParser.register_parselet(Token::MULTIPLY, std::make_shared<BinaryOperatorParselet<ExpMult, ExpParselet::PrecedenceVals::PRODUCT>>());
	mParser.register_parselet(Token::DIVIDE, std::make_shared<BinaryOperatorParselet<ExpDiv, ExpParselet::PrecedenceVals::PRODUCT>>());
	mParser.register_parselet(Token::EXPONENT, std::make_shared<BinaryOperatorParselet<ExpPow, ExpParselet::PrecedenceVals::EXPONENT>>());
	mParser.register_parselet(Token::LEFT_PARENT, std::make_shared<CallParselet>());

	mParser.register_expression("pi", ExpConst::pi);
	mParser.register_expression("Pi", ExpConst::pi);
	mParser.register_expression("PI", ExpConst::pi);
	mParser.register_expression("e", ExpConst::e);
	mParser.register_expression("phi", ExpConst::phi);
	mParser.register_expression("Phi", ExpConst::phi);
	mParser.register_expression("PHI", ExpConst::phi);
	mParser.register_expression("tau", ExpConst::make(2*M_PI));
	mParser.register_expression("Tau", ExpConst::make(2*M_PI));
	mParser.register_expression("TAU", ExpConst::make(2*M_PI));

	// angle
	mParser.register_unit("rad", 1.0);
	mParser.register_unit("deg", M_PI/180.0);

	// length (metric)
	mParser.register_unit("mm", 	1.0e-3);
	mParser.register_unit("cm", 	1.0e-2);
	mParser.register_unit("dm", 	1.0e-1);
	mParser.register_unit("m", 		1.0);
	mParser.register_unit("dam", 	1.0e2);
	mParser.register_unit("hm", 	1.0e2);
	mParser.register_unit("km", 	1.0e3);

	// length (imperial)
	mParser.register_unit("in", 0.0254);
	mParser.register_unit("ft", 0.3048);
}

double ExpBuilder::eval(std::string expStr)
{
	mParser.set_expression(mLexer.generateTokens(expStr));
	mParser.substitute_units();
	
	exp_ptr out = mParser.parse_exp();

	if(mParser.currentToken() < mParser.n_tokens()) {
		throw std::invalid_argument("Unexpected token \"" + mParser.lookAhead(0).text() + "\" stop parsing.");
	}

	if(!out) {
		throw(std::invalid_argument("Unknown error while parsing"));
		return 0.0;
	}
	return out->eval();
}

bool ExpBuilder::isNumber()
{
	if(mParser.n_tokens() == 1 && mParser.getToken(0).type() == Token::NUMERIC)
		return true;
	return false;
}

} // !Core