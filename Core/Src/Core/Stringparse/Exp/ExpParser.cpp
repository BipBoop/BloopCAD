
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
#include "ExpParser.hpp"

#include "ExpParselet.hpp"

#include <algorithm>
#include <iostream>

namespace Core {

ExpParser::ExpParser()
	: Parser()
{

}


void ExpParser::substitute_units()
{
	for(int i = 0; i < n_tokens(); ++i) {
		Token token = lookAhead(i);
		if(token.type() != Token::NAME)
			continue;
		auto find = mUnitConversions.find(token.text());
		if(find == mUnitConversions.end())
			continue;
		replace(Token(Token::MULTIPLY, "*"), i);
		insert(Token(Token::NUMERIC, find->second), i+1);
	}
}
void ExpParser::register_parselet(int tokentype, std::shared_ptr<PrefixParselet> parselet)
{
	mPrefixParselets[tokentype] = parselet;
}
void ExpParser::register_parselet(int tokentype, PrefixParselet* parselet)
{
	register_parselet(tokentype, std::shared_ptr<PrefixParselet>(parselet));
}
void ExpParser::register_parselet(int tokentype, std::shared_ptr<InfixParselet> parselet)
{
	mInfixParselets[tokentype] = parselet;
}
void ExpParser::register_parselet(int tokentype, InfixParselet* parselet)
{
	register_parselet(tokentype, std::shared_ptr<InfixParselet>(parselet));
}
void ExpParser::register_expression(std::string name, exp_ptr exp)
{
	mKnownNames[name] = exp;
}
void ExpParser::register_unit(std::string name, double convToMainFactor)
{
	mUnitConversions[name] = convToMainFactor;
}

exp_ptr ExpParser::parse_exp(int precedence)
{
	Token token = consume();
	auto prefixParselet = getPrefixParselet(token.type());

	if(prefixParselet == nullptr) {
		throw(std::invalid_argument("Unexpected token \"" + token.text() + "\", stop parsing"));
		return nullptr;
	}
	
	exp_ptr left = prefixParselet->parse(this, token);

	while(precedence < getPrecedence()) {
		token = consume();

		auto infixParselet = getInfixParselet(token.type());
		left = infixParselet->parse(this, token, left);
	}

	return left;
}



std::shared_ptr<PrefixParselet> ExpParser::getPrefixParselet(int tokenType)
{
	auto find = mPrefixParselets.find(tokenType);
	if(find == mPrefixParselets.end())
		return nullptr;
	return find->second;
}
std::shared_ptr<InfixParselet> ExpParser::getInfixParselet(int tokenType)
{
	auto find = mInfixParselets.find(tokenType);
	if(find == mInfixParselets.end())
		return nullptr;
	return find->second;
}

exp_ptr ExpParser::getExpByName(std::string name)
{
	auto find = mKnownNames.find(name);
	if(find == mKnownNames.end())
		return nullptr;

	return find->second;
}
int ExpParser::getPrecedence()
{
	auto infixParselet = getInfixParselet(lookAhead(0).type());

	if(infixParselet == nullptr)
		return 0;
	return infixParselet->precedence();
}

} // !Core