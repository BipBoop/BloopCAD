
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
#ifndef BLOOP_CORE_STR_EXPPARSER_HPP_
#define BLOOP_CORE_STR_EXPPARSER_HPP_

#include <Core/Stringparse/Parser.hpp>
#include <Core/Stringparse/Token.hpp>
#include "Expression.hpp"

#include <string>
#include <map>
#include <vector>

namespace Core {

class PrefixParselet;
class InfixParselet;

class ExpParser : public Parser {
private:
	std::map<std::string, exp_ptr> mKnownNames;
	std::map<int, std::shared_ptr<PrefixParselet>> mPrefixParselets;
	std::map<int, std::shared_ptr<InfixParselet>> mInfixParselets;
	std::map<std::string, double> mUnitConversions; // TODO: change when proper unit system will exist

public:
	ExpParser();

	void substitute_units();

	void register_parselet(int tokentype, std::shared_ptr<PrefixParselet> parselet);
	void register_parselet(int tokentype, PrefixParselet* parselet);
	void register_parselet(int tokentype, std::shared_ptr<InfixParselet> parselet);
	void register_parselet(int tokentype, InfixParselet* parselet);
	void register_expression(std::string name, exp_ptr exp);
	void register_unit(std::string name, double convToMainFactor);

	std::shared_ptr<PrefixParselet> getPrefixParselet(int tokenType);
	std::shared_ptr<InfixParselet> getInfixParselet(int tokenType);

	exp_ptr parse_exp(int precedence = 0);

	exp_ptr getExpByName(std::string name);
	int getPrecedence();
};

} // !Core

#endif