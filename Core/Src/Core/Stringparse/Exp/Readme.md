
# Exp

## This directory contains the code for a system that translate string input into a mathematical representation

It uses the Pratt parser algorithm and was created following this blog post: http://journal.stuffwithstuff.com/2011/03/19/pratt-parsers-expression-parsing-made-easy/


It supports:

* Named expressions
* Numbers
* Priority of operation
* Operands (+, -, *, /, ^)
* A few function calls (trigonometric, min, max, abs)
* Units conversion (base units are radians and meters)
* A few constants (pi, tau, e and phi)

## How it works:

1. A lexer translates the string to a list of tokens (parenthesis, plus sign, minus sign, number, name, etc...)
2. A parser iterates through the tokens list
3. At each token, the parser tries to find a corresponding prefix parselet (left parenthesis, plus sign, number, etc...)
4. If no parselet is found, the parser exits with an error, if one is found, the parselet describes the rules for the parsing of this token, it can for instance, call the parser to find the next token, or even call the parser for the next token until a condition
5. Once an expression is outputed by the prefix parselet, the parser tries to find an infix parselet for the next token, to do so, it checks the precedence of the potential infix parselets to comply with operator priority
6. If no infix parselet is found, the output of the prefix parselet is the output of the parser, if it is found, the output is the output of the infix parselet