
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
#include "NumericParselet.hpp"

#include <iostream>
#include <sstream>

namespace Core {

exp_ptr NumericParselet::parse_impl(ExpParser* parser, Token const& token)
{
	// Should be taken of by the lexer
	// if(!is_valid(token.text())) {
	// 	std::cout<<"Error, \""<<token.text()<<"\" is not a number.\n";
	// 	return nullptr;
	// }

	double val;

	if(token.has_val()) {
		val = token.val();
	} else {
		std::stringstream ss(token.text());

		ss >> val;
	}
	
	return ExpConst::make(val);
}


bool NumericParselet::is_valid(std::string const& numStr)
{
	return numStr.find_first_not_of("012345678.") == std::string::npos;
}

} // !Core