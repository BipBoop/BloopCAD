
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
#ifndef BLOOP_CALLPARSELET_HPP_
#define BLOOP_CALLPARSELET_HPP_

#include <Core/Stringparse/Exp/ExpParselet.hpp>
#include <Core/Stringparse/Exp/ExpParser.hpp>

#include <iostream>

namespace Core {

class CallParselet : public InfixParselet {
protected:
	exp_ptr parse_impl(ExpParser* parser, Token const& token, exp_ptr left)
	{
		std::vector<exp_ptr> args;

		Token nameToken = parser->lookAhead(-2); // -2 because '(' has been consumed
		if(nameToken.type() != Token::NAME) {
			throw(std::invalid_argument("Expected \"NAME\" type [" + std::to_string(Token::NAME) + "], got " + 
			std::to_string(nameToken.type()) + " for \"" + nameToken.text() + "\""));
		}
		std::string name = nameToken.text();

		if(!parser->match(Token::RIGHT_PARENT)) {
			do {
				args.push_back(parser->parse_exp());
			} while(parser->match(Token::COMMA));
			
			if(parser->consume().type() != Token::RIGHT_PARENT) {
				throw(std::invalid_argument("Expected matching \")\", stop parsing"));
			}
		}

		exp_ptr func = create_emptyFunc(name);
		fill_arguments(func, args);

		return func;
	}

	int precedence_impl()
	{
		return ExpParselet::PrecedenceVals::CALL;
	}

	exp_ptr create_emptyFunc(std::string const& name)
	{
		if(name == "sin") {
			return std::make_shared<ExpSin>();
		} else if(name == "asin") {
			return std::make_shared<ExpAsin>();
		} else if(name == "csc") {
			return std::make_shared<ExpCsc>();
		} else if(name == "cos") {
			return std::make_shared<ExpCos>();
		} else if(name == "acos") {
			return std::make_shared<ExpAcos>();
		} else if(name == "sec") {
			return std::make_shared<ExpSec>();
		} else if(name == "tan") {
			return std::make_shared<ExpTan>();
		} else if(name == "atan2") {
			return std::make_shared<ExpAtan2>();
		} else if(name == "cot") {
			return std::make_shared<ExpCot>();
		} else if(name == "max") {
			return std::make_shared<ExpMax>();
		} else if(name == "min") {
			return std::make_shared<ExpMin>();
		}

		throw(std::invalid_argument("Unknown function \"" + name + "\" stop parsing"));
		return nullptr;
	}

	void fill_arguments(exp_ptr func, std::vector<exp_ptr> args)
	{
		int n_args = func->n_allowedArgs();

		if(n_args == -1) {
			n_args = args.size();

			if(n_args == 0) {
				throw(std::invalid_argument("Too few arguments given, expected at least 1 argument, got "
					+ std::to_string(args.size()) + ", stop parsing"));
			}
		}

		if(n_args > args.size()) {
			throw(std::invalid_argument("Too few arguments given, expected " + 
				std::to_string(func->n_allowedArgs()) + ", got " + std::to_string(args.size()) + 
				", stop parsing"));
		} else if(n_args < args.size()) {
			throw(std::invalid_argument("Too many arguments given, expected " + 
				std::to_string(func->n_allowedArgs()) + ", got " + std::to_string(args.size()) +
				", stop parsing"));
		}

		for(int i = 0; i < args.size(); ++i) {
			func->set_arg(i, args[i]);
		}
	}
};

} // !Core

#endif