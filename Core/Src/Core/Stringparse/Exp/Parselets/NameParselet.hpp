
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
#ifndef BLOOP_NAMEPARSELET_HPP_
#define BLOOP_NAMEPARSELET_HPP_

#include <Core/Stringparse/Exp/ExpParselet.hpp>

namespace Core {

class NameParselet : public PrefixParselet {
public:

protected:
	exp_ptr parse_impl(ExpParser* parser, Token const& token)
	{
		exp_ptr namedExp = parser->getExpByName(token.text());
		
		if(namedExp == nullptr && parser->lookAhead(0).type() != Token::LEFT_PARENT) {
			// It is not a function and it is not a named expression
			throw(std::invalid_argument("Unknown expression \"" + token.text() + "\", stop parsing"));
		}

		return namedExp;
	}
};

} // !Core

#endif