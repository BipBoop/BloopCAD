
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
#ifndef BLOOP_BINARYOPERATORPARSELET_HPP_
#define BLOOP_BINARYOPERATORPARSELET_HPP_

#include <Core/Stringparse/Exp/ExpParselet.hpp>

namespace Core {

template<typename ExpType, int Prec>
class BinaryOperatorParselet : public InfixParselet {
public:

protected:
	exp_ptr parse_impl(ExpParser* parser, Token const& token, exp_ptr left)
	{
		exp_ptr right = parser->parse_exp(precedence());

		if(!right)
			throw(std::invalid_argument("Invalid operand, stop parsing."));

		return std::make_shared<ExpType>(left, right);
	}

	int precedence_impl()
	{
		return Prec;
	}
};

} // !Core

#endif 