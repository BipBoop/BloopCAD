
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
#ifndef BLOOP_EXPPARSELET_HPP_
#define BLOOP_EXPPARSELET_HPP_

#include <Core/Stringparse/Token.hpp>

#include "Expression.hpp"

namespace Core {

class ExpParser;

class ExpParselet {
public:
	enum PrecedenceVals {
		SUM = 3,
		PRODUCT = 4,
		EXPONENT = 5,
		PREFIX = 6,
		CALL = 7
	};

public:
	ExpParselet() = default;
	virtual ~ExpParselet() { }

	int precedence() { return precedence_impl(); };

protected:
	virtual int precedence_impl() = 0;

};

class PrefixParselet : public ExpParselet {
public:
	PrefixParselet() = default;
	virtual ~PrefixParselet() { }

	exp_ptr parse(ExpParser* parser, Token const& token) { return parse_impl(parser, token); }

protected:
	virtual exp_ptr parse_impl(ExpParser* parser, Token const& token) = 0;

	int precedence_impl() override { return ExpParselet::PrecedenceVals::PREFIX; }
};

class InfixParselet : public ExpParselet {
public:
	InfixParselet() = default;
	virtual ~InfixParselet() { }

	exp_ptr parse(ExpParser* parser, Token const& token, exp_ptr left) { return parse_impl(parser, token, left); }

protected:
	virtual exp_ptr parse_impl(ExpParser* parser, Token const& token, exp_ptr left) = 0;
	
};

} // !Core

#endif