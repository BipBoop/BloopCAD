
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
#ifndef BLOOP_CORE_STR_LEXER_HPP_
#define BLOOP_CORE_STR_LEXER_HPP_

#include "Token.hpp"

#include <vector>
#include <string>
#include <map>

namespace Core {

class Lexer {
private:
	std::map<char, int> mPonctuators;

	bool mHas_point, mHas_digit, mHas_e, mHas_plusMinus; // Number stuff;
public:
	Lexer();

	std::vector<Token> generateTokens(std::string str);

	bool update_number(char c);
	void reset_number();

	static bool isLetter(char c);
	static bool isNumber(char c);
	static bool isBlank(char c);
};

} // !Core

#endif