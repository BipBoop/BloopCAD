
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
#include "Parser.hpp"

#include <iostream>

namespace Core {

Parser::Parser()
	: mTokens({})
	, mCurrToken(0)
{

}
Parser::Parser(std::vector<Token> tokens)
	: mTokens(tokens)
	, mCurrToken(0)
{

}

void Parser::set_expression(std::vector<Token> tokens)
{
	mTokens = tokens;
	mCurrToken = 0;
}

Token Parser::consume()
{
	mCurrToken++;
	return getToken(mCurrToken - 1);
}
Token Parser::lookAhead(int howFar)
{
	return getToken(mCurrToken+howFar);
}

Token Parser::getToken(int ind)
{
	if(ind >= mTokens.size() || ind < 0)
		return Token(Token::END, "");
	return mTokens[ind];
}
bool Parser::match(int tokenType)
{
	if(lookAhead(0).type() != tokenType)
		return false;

	consume();
	return true;
}

void Parser::replace(Token tok, int ind)
{
	if(ind < 0 || ind > mTokens.size())
		throw(std::invalid_argument("Index out of bounds."));

	mTokens[ind] = tok;
}
void Parser::insert(Token tok, int ind)
{
    if(ind < 0 || ind > mTokens.size())
        throw(std::invalid_argument("Index out of bounds."));
	
	mTokens.insert(mTokens.begin()+ind, tok);
}

} // !Core