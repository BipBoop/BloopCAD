
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
#ifndef BLOOP_CORE_TOKEN_HPP_
#define BLOOP_CORE_TOKEN_HPP_

#include <string>

namespace Core {

class Token {
public:
	enum TokenTypes { 
		PLUS,
		MINUS,
		MULTIPLY,
		DIVIDE,
		EXPONENT,
		LEFT_PARENT,
		RIGHT_PARENT,
		COMMA,
		NAME,
		NUMERIC,
		LEFT_BRACKET,
		RIGHT_BRACKET,
		LEFT_SQUAREBRACKET,
		RIGHT_SQUAREBRACKET,
		QUOTATION,
		DOUBLEQUOTATION,
		COLON,
		SEMICOLON,
		END
	};

private:
	int mType;
	std::string mText;
	double mVal;
	bool mHasVal { false };

public:
	Token(int ty, std::string txt);
	Token(int ty, double txt);

	int type() const { return mType; }
	std::string text() const { return mText; }
	bool has_val() const { return mHasVal; }
	double val() const { return mVal; }

	static bool get_char(int ty, char& ch);
};

} // !Core

#endif