
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
#include <Core/Stringparse/Token.hpp>

namespace Core {

Token::Token(int ty, std::string txt)
	: mType(ty)
	, mText(txt)
{

}
Token::Token(int ty, double txt)
	: mType(ty)
	, mText(std::to_string(txt))
	, mVal(txt)
	, mHasVal(txt)
{

}

bool Token::get_char(int ty, char& ch)
{
	switch(ty) {
	case PLUS:					ch = '+';   return true;
	case MINUS:					ch = '-';   return true;
	case MULTIPLY:				ch = '*';   return true;
	case DIVIDE:				ch = '/';   return true;
	case EXPONENT:				ch = '^';   return true;
	case LEFT_PARENT:			ch = '(';   return true;
	case RIGHT_PARENT:			ch = ')';   return true;
	case COMMA:					ch = ','; 	return true;
	case LEFT_BRACKET: 			ch = '{';	return true;
	case RIGHT_BRACKET:			ch = '}'; 	return true;
	case LEFT_SQUAREBRACKET:	ch = '[';	return true;
	case RIGHT_SQUAREBRACKET:	ch = ']'; 	return true;
	case QUOTATION:				ch = '\'';	return true;
	case DOUBLEQUOTATION:		ch = '"'; 	return true;
	case COLON:					ch = ':';	return true;
	case SEMICOLON:				ch = ';';	return true;
	case NAME:
	case NUMERIC:
	default:                        return false;
	}
}

} // !Core