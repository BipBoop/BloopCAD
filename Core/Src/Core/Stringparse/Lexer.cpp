
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
#include "Lexer.hpp"

#include <algorithm>
#include <iostream>
#include <string>

namespace Core {


Lexer::Lexer()
{
	for(int i = 0; i < Token::TokenTypes::END; ++i) {
		char c;
		if(Token::get_char(i, c))
			mPonctuators[c] = i;
	}
}

std::vector<Token> Lexer::generateTokens(std::string str)
{
	std::vector<Token> tokens;
	reset_number();

	for(int i = 0; i < str.size(); ++i) {
		char currChar = str[i];

		if(mPonctuators.find(currChar) != mPonctuators.end()) {
			tokens.push_back(Token(mPonctuators[currChar], std::string(1, currChar)));
			continue;
		}

		if(isLetter(currChar)) {
			int start_ind = i;
			i++;
			
			if(i < str.size()) {	
				char lookAheadChar = str[i];

				while((isLetter(lookAheadChar) || isNumber(lookAheadChar) || isBlank(lookAheadChar)) && i < str.size()) {
					i++;
					
					if(i < str.size()) {
						lookAheadChar = str[i];
					} else {
						break;
					}
				}
				i--;
			}
			tokens.push_back(Token(Token::NAME, str.substr(start_ind, i-start_ind+1)));
			continue;
		}

		if(update_number(currChar)) {	
			int start_ind = i;
			i++;

			if(i < str.size()) {
				char lookAheadChar = str[i];

				while(update_number(lookAheadChar)) {
					i++;

					if(i < str.size()) {
						lookAheadChar = str[i];
					} else {
						break;
					}
				}
				i--;

				// number can't end with exponent notation unclosed (ex: 1e or 1e-)
				lookAheadChar = str[i];
				while(lookAheadChar == 'e' || lookAheadChar == 'E' || lookAheadChar == '-' || lookAheadChar == '+') {
					i--;
					if(i > 0) {
						lookAheadChar = str[i];
					} else {
						break;
					}
				}
			}

			tokens.push_back(Token(Token::NUMERIC, str.substr(start_ind, i-start_ind+1)));
			reset_number();
			continue;
		}
	}

	return tokens;
}

bool Lexer::update_number(char c)
{
	if(c >= '0' && c <= '9') {
		mHas_digit = true;
		return true;
	} else if(c == '.' && !mHas_e) {
		mHas_point = true;
		return true;
	} else if((c == 'e' || c == 'E') && (mHas_digit)) {
		mHas_e = true;
		return true;
	} else if((c == '+' || c == '-') && mHas_e && !mHas_plusMinus) {
		mHas_plusMinus = true;
		return true;
	}
	return false;
}
void Lexer::reset_number()
{
	mHas_point = false;
	mHas_digit = false;
	mHas_e = false;
	mHas_plusMinus = false;
}

bool Lexer::isLetter(char c)
{
	return (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z');
}
bool Lexer::isNumber(char c)
{
	return (c >= '0' && c <= '9') || c == '.';
}
bool Lexer::isBlank(char c)
{
	return c == ' ' || c == '\t';
}

} // !Core