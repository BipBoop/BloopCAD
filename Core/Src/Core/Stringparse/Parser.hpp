
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
#ifndef BLOOP_CORE_STR_PARSER_HPP_
#define BLOOP_CORE_STR_PARSER_HPP_

#include "Token.hpp"

#include <vector>

namespace Core {

class Parser {
private:
	std::vector<Token> mTokens;
	size_t mCurrToken;

public:
    Parser();
    Parser(std::vector<Token> tokens);

	void set_expression(std::vector<Token> tokens);

	size_t currentToken() { return mCurrToken; }
	size_t n_tokens() { return mTokens.size(); }

    Token consume();
	Token lookAhead(int howFar);
	Token getToken(int ind);
	bool match(int tokenType);

    void replace(Token tok, int ind);
    void insert(Token tok, int ind);
};

} // !Core

#endif