
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef BLOOP_CORE_UPDRESULT_HPP_
#define BLOOP_CORE_UPDRESULT_HPP_

#include <Core/Id.hpp>

#include <optional>

namespace Core {

namespace Wp {

// A vevs snapshot is a structure that contains all named ids of a
// workpiece, it is used to make the diff of a workpiece
struct Kind_indexVec {
  using indexvec_t = std::vector<VersionedIndex>;

  size_t kind;
  indexvec_t indexVec;
};
using VecsSnapshot = std::vector<Kind_indexVec>;

struct Diff {
  IdVec added, removed, modified;

  static Diff make(WorkpieceId wpid, VecsSnapshot const& prev, VecsSnapshot const& curr);

  void add(WorkpieceId wpid, size_t kind, std::vector<VersionedIndex> const& prev, std::vector<VersionedIndex> const& curr);

  void add_listdiff(std::vector<std::pair<size_t, int>> const& listdiff, size_t kind, WorkpieceId wpid);
  void add_wplistdiff(std::vector<std::pair<size_t, int>> const& listdiff, size_t kind);

  bool empty() const { return added.empty() && removed.empty() && modified.empty(); }
};

} // !Wp


struct UpdReport {
  std::vector<std::pair<WorkpieceId, Wp::Diff>> wp_diffs; // Modifications of resulting entities of the workpiece
  std::vector<std::pair<WorkpieceId, int>> wp_buildSequence_mod; // Which workpieces' buildsequence have been modified (could be more interesting with diffs and such)
  std::string toolmsg { "" }; // Additional info from the tool (or upddesc sender)

  static UpdReport from_diff(WorkpieceId wpid, Wp::Diff const& diff);
  void combine(UpdReport const& other);
  bool empty() const { return wp_diffs.empty(); }
};

} // !Core

#endif