
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef BLOOP_CORE_ID_HPP_
#define BLOOP_CORE_ID_HPP_

#include <immer/vector.hpp>
#include <immer/vector_transient.hpp>
#include <immer/box.hpp>

#include <queue>
#include <stack>
#include <bitset>
#include <iostream>
#include <compare>
#include <limits>

namespace Core {

static constexpr size_t invalid_index()
{
  return std::numeric_limits<size_t>::max();
}

using b_string = immer::box<std::string>;
template<typename T>
struct Named {
  T value;
  std::string name;

  Named() = default;
  Named(T const& t, std::string name_)
    : value(t)
    , name(name_)
  {
    
  }
  Named(Named<T> const& other)
    : value(other.value)
    , name(other.name)
  {
    
  }
  Named(T const& t)
    : value(t)
    , name("")
  {

  }
  Named(std::string name_)
    : value(T())
    , name(name_)
  {

  }
  void operator=(Named<T> && other)
  {
    value = std::move(other.value);
    name = std::move(other.name);
  }
  void operator=(T && other_val)
  {
    value = std::move(other_val);
  }
  void operator=(T const& other_val)
  {
    value = other_val;
  }
  T& operator*()
  {
    return value;
  }
  T const& operator*() const
  {
    return value;
  }
  T* operator->()
  {
    return &value;
  }
  T const* operator->() const
  {
    return &value;
  }
  operator T()
  {
    return value;
  }
};
template<typename T>
std::vector<Named<T>> unamed(std::vector<T> const& vec)
{
  std::vector<Named<T>> out(vec.size());
  for(size_t i = 0; i < vec.size(); ++i) {
    out[i] = Named<T>(vec[i]);
  }
  return out;
}

struct WorkpieceId {
  size_t kind { invalid_index() };
  size_t index { invalid_index() };

  static WorkpieceId from_string(std::string str)
  {
    std::cerr<<"Creating workpiece from "<<str<<std::endl;
    return WorkpieceId{};
  }

  bool operator<(WorkpieceId const& other) const
  {
    if(kind == other.kind) {
      return index < other.index;
    }
    return kind < other.kind;
  }
  bool operator==(WorkpieceId const& other) const { return kind == other.kind && index == other.index; }
  bool operator!=(WorkpieceId const& other) const { return !(*this == other); }

  bool valid() const { return index != invalid_index(); }
};
// Sibling ID
struct SibId {
  size_t kind { invalid_index() };
  size_t index { invalid_index() };
  size_t version { 0 };
  int handle_num { -1 };

  bool operator==(SibId const& other) const { return index == other.index && kind == other.kind && handle_num == other.handle_num; }
  bool operator!=(SibId const& other) const { return !(*this == other); }
  bool operator<(SibId const& other) const
  {
    if(kind == other.kind) {
      if(index == other.index) {
        return handle_num < other.handle_num;
      }
      return index < other.index;
    }
    return kind < other.kind;
  }

  SibId make_handle(int num) const
  {
    SibId hand = *this;
    hand.handle_num = num;
    return hand;
  }
  SibId no_handle() const
  {
    SibId out = *this;
    out.handle_num = -1;
    return out;
  }
  bool valid() const { return index != invalid_index(); }
};
struct Id {
  WorkpieceId wpid;
  SibId entId;

  Id make_handle(int num) const
  {
    return Id { .wpid=wpid, .entId=entId.make_handle(num) };
  }
  operator SibId() const { return entId; }
  bool operator==(Id const& other) const { return entId == other.entId && wpid == other.wpid; }
  bool operator!=(Id const& other) const { return !(*this == other); }
  bool operator<(Id const& other) const
  {
    if(wpid == other.wpid) {
      return entId < other.entId;
    }
    return wpid < other.wpid;
  }
  bool is_workpiece() const
  {
    return entId == SibId() && wpid != WorkpieceId();
  }
  bool is_handle_of(Id other) const
  {
    return other.wpid == wpid && other.entId.kind == entId.kind && other.entId.index == entId.index 
        && other.entId.handle_num == -1 && entId.handle_num != -1;
  }
  bool valid() const { return wpid.valid() && entId.valid(); }
};

struct VersionedIndex {
  size_t index;
  size_t version { 0 };

  // SibId to_sibId(size_t kind, int handle_num=-1) const
  // {
  //   return SibId{.kind=kind, .index=index, .version=version, .handle_num=handle_num };
  // }
  bool operator<(VersionedIndex const& other) const 
  {
    return index < other.index;
  }
};


// To use in workpieces
// Output of the condense function, it's static data which can easily be diffed
// the indexed can be used with a kind to make a SibId in a workpiece
// algorithms will assume that it is sorted by growing index
template<typename inner_t>
using CondensedList = std::vector<std::pair<VersionedIndex, inner_t>>;


template<typename inner_t>
inner_t condensed_get(size_t index, CondensedList<inner_t> const& cond)
{
  for(auto elem : cond) {
    if(elem.first.index == index)
      return elem.second;
  }
  std::cerr<<"Could not find condensed element at index "<<index<<std::endl;
  return inner_t();
}
template<typename inner_t>
std::vector<VersionedIndex> strip_inner(CondensedList<inner_t> const& cond)
{
  std::vector<VersionedIndex> out;
  for(auto elem : cond) {
    VersionedIndex vind = elem.first;
    out.push_back(vind);
  }
  return out;
}

template<typename T>
size_t find_by_name(std::vector<Named<T>> const& thingies, std::string name)
{
  for(size_t i = 0; i < thingies.size(); ++i) {
    if(thingies[i].name == name) {
      return i;
    }
  }
  // std::cerr<<"Could not find by name "<<name<<std::endl;
  return invalid_index();
}
template<typename T>
size_t find_by_name(CondensedList<Named<T>> const& thingies, std::string name)
{
  for(size_t i = 0; i < thingies.size(); ++i) {
    if(thingies[i].second.name == name) {
      return i;
    }
  }
  // std::cerr<<"Could not find by name "<<name<<std::endl;
  return invalid_index();
}


template<typename inner_t>
struct EntityList {
  using self_t = EntityList<inner_t>;

  struct EntityBundle {
    size_t next_free { invalid_index() };
    inner_t entity;
    size_t version { 0 };
  };

  immer::vector<EntityBundle> entities;
  size_t first_free { invalid_index() };
  size_t true_size { 0 };

  size_t size() const { return true_size; }

  // Returns the number of elements before the index
  // in the list (should be index- number of invalid indices in the range [0, index])
  size_t count_up_to(size_t index) const
  {
    size_t count = 0;
    for(size_t i = 0; i < index; ++i) {
      if(valid(i)) {
        count++;
      }
    }
    return count;
  }
  static EntityList<inner_t> make(std::vector<inner_t> const& values)
  {
    EntityList<inner_t> out;
    auto entities_trans = out.entities.transient();

    for(auto value : values) {
      entities_trans.push_back(EntityBundle { .entity=value });
    }

    out.entities = entities_trans.persistent();
    out.true_size = values.size();
    return out;
  }

  std::pair<self_t, VersionedIndex> add(inner_t entity) const
  {
    if(first_free == invalid_index()) {
      return {  self_t {  .entities=entities.push_back(EntityBundle{ .entity=entity }), 
                          .first_free=invalid_index(), .true_size=true_size+1 }, 
                VersionedIndex{.index=entities.size(), .version=0} };
    }
    size_t new_first_free = entities[first_free].next_free;
    return { self_t { .entities=entities.set(first_free, EntityBundle{ .entity=entity }), 
                      .first_free=new_first_free, .true_size=true_size+1}, 
              VersionedIndex{.index=first_free, .version=0} };
  }
  self_t remove(size_t index) const
  {
    warn_index(index);
  
    size_t new_first_free;
    auto new_ent = entities.update(index, [&](EntityBundle eb) {
                            eb.version=invalid_index();
                            eb.next_free=first_free; 
                            new_first_free=index; 
                            return eb; 
                          });

    return self_t { .entities=new_ent, .first_free=new_first_free, .true_size=true_size-1};
  }
  std::pair<self_t, VersionedIndex> set(size_t index, inner_t entity) const
  {
    warn_index(index);
    size_t new_version;
    
    return {  self_t { .entities=entities.update(index, [&](EntityBundle eb) { 
                            eb.entity = std::move(entity); 
                            new_version = eb.version++;
                            return eb; }), .first_free=first_free, .true_size=true_size}, 
              VersionedIndex{.index=index, .version=new_version}};
  }
  template<typename transFun_t>
  std::pair<self_t, VersionedIndex> transform(size_t index, transFun_t fun) const
  {
    warn_index(index);
    size_t new_version;
    return { self_t { .entities=entities.update(index, [&](EntityBundle eb) { 
                            eb.entity = fun(eb.entity);
                            new_version = eb.version++; 
                            return eb; }), .first_free=first_free, .true_size=true_size},
              VersionedIndex{.index=index, .version=new_version}};
  }

  inner_t const& get(size_t index) const
  {
    warn_index(index);
    return entities[index].entity;
  }
  inner_t const& back() const
  {
    if(size() == 0) {
      std::cerr<<"Accessing back of empty list"<<std::endl;
    }
    return entities.back().entity;
  }
  template<typename fun_t>
  void for_each(fun_t fun) const
  {
    // It is a const function so it could be parallelized, perhaps
    for(size_t i = 0; i < entities.size(); ++i) {
      if(!valid(i)) 
        continue;
      if(!fun(entities[i].entity, i, entities[i].version)) {
        break;
      }
    }
  }
  template<typename transFun_t>
  self_t for_each_transform(transFun_t fun) const
  {
    auto trans_entities = entities.transient();
    for(size_t i = 0; i < entities.size(); ++i) {
      if(!valid(i)) 
        continue;
      auto eb = trans_entities[i];

      // Update the entity bundle, keep next_free list metadata
      // and increment the version
      trans_entities.set(i, EntityBundle{ .next_free=eb.next_free,
                                          .entity=fun(eb.entity, i, eb.version), 
                                          .version=eb.version+1});
    }
    return self_t { .entities=trans_entities.persistent(), .first_free=first_free, .true_size=true_size};
  }
  // std::pair<inner_t const&, b_string> get_named(size_t index) const
  // {
  //   warn_index(index);
  //   auto bundle = entities[index];
  //   return { bundle.entity, bundle.name };
  // }
  inner_t const& operator[](size_t index) const
  {
    return get(index);
  }
  size_t version(size_t index) const
  {
    warn_index(index);
    return entities[index].version;
  }
  void warn_index(size_t index) const
  {
    if(index > entities.size()) {
      std::cerr<<"Accessing index out of range "<<index<<" vs "<<entities.size()<<std::endl;
    }
    if(!valid(index)) {
      std::cerr<<"Accessing deleted entity "<<index<<std::endl;
    }
  }
  size_t next() const
  {
    if(first_free == invalid_index()) {
      return entities.size();
    }
    return first_free;
  }
  bool valid(size_t index) const
  {
    return entities[index].version != invalid_index();
  }
  // Returns a std vector of all non-deleted elements
  std::vector<inner_t> all_valid() const
  {
    std::vector<inner_t> out;
    out.reserve(entities.size());

    for(int i = 0; i < entities.size(); ++i) {
      if(valid(i)) {
        out.push_back(entities[i].entity);
      }
    }
    return out;
  }

  std::vector<VersionedIndex> versIndices() const
  {
    std::vector<VersionedIndex> out;
    for(size_t i = 0; i < entities.size(); ++i) {
      if(valid(i)) {
        out.push_back(VersionedIndex{.index=i, .version=entities[i].version});
      }
    }
    return out;    
  }
  CondensedList<inner_t> condense() const
  {
    CondensedList<inner_t> out;
    for(size_t i = 0; i < entities.size(); ++i) {
      if(valid(i)) {
        out.push_back({VersionedIndex{.index=i, .version=entities[i].version}, entities[i].entity});
      }
    }
    return out;
  }
  /*
    Returns a vector of paired values: the index of the entity in question, and an int -1 => deleted in next, 0 => modified in next, 1 => created in next
  */
  std::vector<std::pair<size_t, int>> diff(EntityList<inner_t> const& next) const
  {
    if(next.size() > size()) {
      return diff_impl(entities, next.entities);
    } else {
      std::vector<std::pair<size_t, int>> tmp = diff_impl(next.entities, entities);
      for(size_t i = 0; i < tmp.size(); ++i) {
        tmp[i].second = -tmp[i].second;
      }
      return tmp;
    }
  }
private:
  // Impl assumes that next is longer or of equal size to prev
  static std::vector<std::pair<size_t, int>> diff_impl(immer::vector<EntityBundle> prev, immer::vector<EntityBundle> next)
  {
    std::vector<std::pair<size_t, int>> out;

    for(size_t i = 0; i < prev.size(); ++i) {
      bool del_prev = prev[i].next_free != invalid_index();
      bool del_next = next[i].next_free != invalid_index();

      if(del_prev && !del_next) {
        out.push_back({i, 1});
      } else if(!del_prev && del_next) {
        out.push_back({i, -1});
      } else if(!del_prev && !del_next && prev[i].version != next[i].version) {
        out.push_back({i, 0});
      }
    }
    for(size_t i = prev.size(); i < next.size(); ++i) {
      bool del = next[i].next_free != invalid_index();
      if(!del) {
        out.push_back({i, 1});
      }
    }
    return out;
  }
};

using SibIdVec = std::vector<SibId>; 
using IdVec = std::vector<Id>;

SibIdVec strip_workpiece(IdVec const& vec);
IdVec add_workpiece(SibIdVec const& vec, WorkpieceId wpid);

} // !Core

#endif