
#ifndef BLOOP_CORE_SERIALIZE_GLM_HPP_
#define BLOOP_CORE_SERIALIZE_GLM_HPP_

#include <yaml-cpp/yaml.h>

namespace YAML {
template<typename inner_t, int dim>
struct convert<glm::vec<dim, inner_t>> {
  static Node encode(glm::vec<dim, inner_t> const& rhs) {
    Node node;
    for(size_t i = 0; i < dim; ++i) {
      node.push_back(rhs[i]);
    }
    return node;
  }

  static bool decode(Node const& node, glm::vec<dim, inner_t>& rhs) {
    if(!node.IsSequence() || node.size() != dim) {
      return false;
    }
    for(size_t i = 0; i < dim; ++i) {
      rhs[i] = node[i].as<double>();
    }
    return true;
  }
};
template<typename inner_t, int dimC, int dimR>
struct convert<glm::mat<dimC, dimR, inner_t>> {
  static Node encode(glm::mat<dimC, dimR, inner_t> const& rhs) {
    Node node;
    for(size_t i = 0; i < dimC; ++i) {
      for(size_t j = 0; j < dimR; ++j) {
        node.push_back(rhs[i][j]);
      }
    }
    return node;
  }

  static bool decode(Node const& node, glm::mat<dimC, dimR, inner_t>& rhs) {
    if(!node.IsSequence() || node.size() != dimC*dimR) {
      return false;
    }
    size_t n = 0;
    for(size_t i = 0; i < dimC; ++i) {
      for(size_t j = 0; j < dimR; ++j) {
        rhs[i][j] = node[n++].as<double>();
      }
    }
    return true;
  }
};
}

#endif