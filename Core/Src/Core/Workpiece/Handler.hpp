
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef BLOOP_CORE_WP_HANDLER_HPP_
#define BLOOP_CORE_WP_HANDLER_HPP_

#include <Core/Sketch/Sketch.hpp>
#include <Core/Sketch/Factory/Factory.hpp>
#include <Core/Part/Part.hpp>
#include <Core/Part/Factory.hpp>
#include <Core/Maths/Ray_3d.hpp>

#include "DepTree.hpp"
#include "Wrapper.hpp"
#include <Core/UpdReport.hpp>
#include "Kinds.hpp"
#include "Actions.hpp"

#include <vector>
#include <ostream>

namespace Core {
namespace Wp {

class Handler {
private:
  WrapperVec<Wrapper<Sk::Sketch, Sk::Factory>, Kind::Sketch> sketches;
  WrapperVec<Wrapper<Prt::Part, Prt::Factory>, Kind::Part> parts;
  DepTree deptree;

public:
  // Creates a new empty workpiece that lives in
  // a temp directory and has an "external" flag
  // set. Returns the handle of the newly created workpiece
  WorkpieceId create_empty(CreateEmpty act);

  UpdReport branchItUp(WorkpieceId wpid, BranchAction act);
  UpdReport update(UpdDesc updDesc);
  UpdReport update(WorkpieceId wpid, HdAction act);
  UpdReport undo(WorkpieceId wpid);
  UpdReport redo(WorkpieceId wpid);
  UpdReport rebuild(WorkpieceId wpid);

  void set_nameSuffix(WorkpieceId wpid, std::string name_suffix);

  // Loads the workpiece at the file and sets the "external" 
  // flag to true
  std::optional<WorkpieceId> find_or_load(std::string filepath);
  std::optional<WorkpieceId> load(std::string filepath);
  

  // write_on_stream writes the content of the file 
  // return and a vector of external dependencies
  std::vector<WorkpieceId> write_on_stream(WorkpieceId wpid, std::ostream& os) const;
  void save(WorkpieceId wpid, bool save_extDeps) const;
  void save(std::string filepath, WorkpieceId wpid, bool save_extDeps) const;

  void export_wp(WorkpieceId wpid, std::string filepath) const;

  // Removes the external load flag of the workpiece
  // and might unload it from memory
  void external_unload(WorkpieceId wpid);

  std::vector<Mesh> meshes(Id id) const;

  // returns all constituents of a workpiece
  // that is dependent workpieces AND entities
  // contained
  // if recursive, it will return the ids of 
  // the constituents of dependency workpieces
  // recursively 
  IdVec constituents_of(WorkpieceId wpid, bool recursive = true) const;

  // returns all dependencies of a workpiece
  // if recursive, it will return the dependencies'
  // dependencies and so on
  std::vector<WorkpieceId> dependencies_of(WorkpieceId wpid, bool recursive = true) const;

  // Returns the name (suffix) of
  // a workpiece in the deptree
  std::string name_of(WorkpieceId wpid) const;

  // Returns the name or suffix of an entity
  // be it a workpiece or a workpiece's constituent
  std::string name_of(Id id) const;

  // // Point on a workpiece from a ray 
  // // the ray is expected to be transformed 
  // // matching the app context (e.g. if a sketch is on an 
  // // arbitrary surface on a part, the ray must act as though it was on the xy plane)
  // // since there are no transformation stored 
  // glm::dvec3 point( WorkpieceId wpid, Ray ray, 
  //                   glm::dmat4 wp_transform = glm::dmat4(1)) const;

  // // Ids of entities in the workpiece which intersect
  // // with a ray. Since workpieces are stored in their "normal"
  // // transformation (identity matrix), the caller can provide
  // // a transformation matrix so that the ray can be transformed
  // // from the editor's workpiece transform to the "normal" workpiece transform
  // // Returns a vector of ids paired with the intersection point in the space of
  // // the caller, ordered from closest to farthest
  // std::vector<std::pair<Id, glm::dvec3>> touched( WorkpieceId wpid, Ray ray, 
  //                                                 glm::dmat4 wp_transform = glm::dmat4(1)) const

  // // Similar to previous, except the intersecting shape is an n-point pyramid instead of a single ray
  // // std::vector<std::pair<Id, glm::dvec3>> touched( WorkpieceId wpid, RayPyramid ray, 
  // //                                                 glm::dmat4 wp_transform = glm::dmat4(1)) const

  // Get the transformation of the descendant workpiece
  // from an ancestor with the deptree
  // (each dependency has a transform associated, and this
  // function accumulates them down the tree)
  std::optional<glm::dmat4> transform_descendant_to_ancestor(WorkpieceId wpid, WorkpieceId root) const;

  // Returns the locaname of a workpiece
  // it querries the deptree to do so
  std::string locaname(WorkpieceId wpid) const;

  // Sets the name_suffix of a workpiece, all descendents
  // will be updated accordingly
  void set_namesuffix(WorkpieceId wpid, std::string name_suffix);

  std::optional<std::pair<Sk::Sketch, glm::dmat4>> sketch_access(WorkpieceId parent, WorkpieceId sk_id) const;
  std::optional<std::pair<Prt::Part, glm::dmat4>> part_access(WorkpieceId parent, WorkpieceId prt_id) const;

  template<typename T>
  std::optional<T> get_wp(WorkpieceId wpid) const { return std::nullopt; }

  template<typename T>
  std::optional<T> get(Id id) const
  {
    if(id.wpid.kind == Kind::Sketch) {
      auto maybe_sketch = get_wp<Sk::Sketch>(id.wpid);
      if(maybe_sketch) {
        return maybe_sketch->get<T>(id.entId);
      }
    } else if(id.wpid.kind == Kind::Part) {
      auto maybe_part = get_wp<Prt::Part>(id.wpid);
      if(maybe_part) {
        return maybe_part->get<T>(id.entId);
      }
    }
    return std::nullopt;
  }

  WorkpieceId next(Kind::inner) const;
  std::optional<WorkpieceId> parent(WorkpieceId wpid) const { return deptree.parent(wpid); }

private:
  // Serializes any workpiece with a locaname- relative path
  // to the root workpiece being serialized
  YAML::Node encode_workpiece(WorkpieceId wpid, std::string relname, 
                              std::map<WorkpieceId, std::string>& wpids_to_locanames) const;

  // Loads the file workpiece and it's dependencies and sets the
  // "external" flag accordingly
  std::optional<WorkpieceId> find_or_load_impl(std::string filepath, bool external);
  std::optional<WorkpieceId> load_impl(std::string filepath, bool external);
  void load_workpiece(YAML::Node workpieceyaml, std::string filepath, 
                      std::unordered_map<std::string, WorkpieceId>& locanames_to_wpids);

  void unload_impl(WorkpieceId wpid);

  template<typename fun_t>
  void fwd_wrapper(WorkpieceId wpid, fun_t fun)
  {
    switch(wpid.kind) {
    case Wp::Kind::Sketch:
      fun(sketches.get(wpid.index));
      return;
    case Wp::Kind::Part:
      fun(parts.get(wpid.index));
      return;
    }
    std::cerr<<"Could not resolve fwd_wrapper"<<std::endl;
  }
  template<typename fun_t>
  void fwd_wrapper(WorkpieceId wpid, fun_t fun) const
  {
    switch(wpid.kind) {
    case Wp::Kind::Sketch:
      fun(sketches.get(wpid.index));
      return;
    case Wp::Kind::Part:
      fun(parts.get(wpid.index));
      return;
    }
    std::cerr<<"Could not resolve fwd_wrapper"<<std::endl;
  }
};


template<> std::optional<Prt::Part> Handler::get_wp(WorkpieceId wpid) const;
template<> std::optional<Prt::Factory> Handler::get_wp(WorkpieceId wpid) const;
template<> std::optional<Sk::Sketch> Handler::get_wp(WorkpieceId wpid) const;
template<> std::optional<Sk::Factory> Handler::get_wp(WorkpieceId wpid) const;

} // !Wp
} // !Core

#endif