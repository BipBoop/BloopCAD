
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "DepTree.hpp"

#include <Core/Utils/Paths.hpp>
#include <Core/Print/IDOstream.hpp>

namespace Core {
namespace Wp {

glm::dmat4 Node::dep_transform(WorkpieceId dep_wpid) const
{
  for(auto dep : dependencies) {
    if(dep.wpid == dep_wpid) {
      return dep.transform;
    }
  }
  std::cerr<<dep_wpid<<" is not a dependency of workpiece"<<std::endl;
  return glm::dmat4(1.0);
}

void DepTree::load_yaml(YAML::Node const& yamlnode, std::string root_locaname)
{
  WorkpieceId wpid, root;
  for(auto yaml_int : yamlnode["internal"]) {
    if(yaml_int["loc"].as<std::string>() == "&root") {
      wpid = locanames_to_wpid[root_locaname];
    } else {
      wpid = locanames_to_wpid[yaml_int["loc"].as<std::string>()];
    }
    for(auto yaml_dep : yaml_int["depends"]) {
      std::string true_locaname = absolute_locaname(root_locaname, yaml_dep["rel_loc"].as<std::string>());
      WorkpieceId dep_wpid = locanames_to_wpid[true_locaname];
      add_dependency(Dependency{.wpid=dep_wpid, 
                                .is_child=yaml_dep["child"].as<bool>(), 
                                .build_step=yaml_dep["bstep"].as<size_t>()}, wpid);
    }
  }
}

YAML::Node yaml_depNode(size_t buildstep, bool is_child, std::string rel_locaname)
{
  YAML::Node node;
  node["bstep"] = buildstep;
  node["rel_loc"] = rel_locaname;
  node["child"] = is_child;
  return node;
}
YAML::Node yaml_nodeNode(std::string rel_locaname)
{
  YAML::Node node;
  node["loc"] = rel_locaname;
  return node;
}
std::pair<YAML::Node, SaveList> DepTree::saveInfo(WorkpieceId wpid) const
{
  std::vector<YAML::Node> yamlinternal, yamlexternal;
  SaveList savelist;

  wpid = root_of(wpid);
  std::string file_locaname = wp_nodes.at(wpid).locaname;

  // Workpiece id paired with an index in yamlinternal
  // to connect the internal parent to it's dependencies
  std::stack<std::pair<WorkpieceId, size_t>> dep_to_explore;
  dep_to_explore.push({ wpid, 0 });
  yamlinternal.push_back(yaml_nodeNode("&root"));
  savelist.internal_deps.push_back({wpid, "&root"});
  
  while(!dep_to_explore.empty()) {
    Node const& currNode = wp_nodes.at(dep_to_explore.top().first);
    size_t currInternalParent = dep_to_explore.top().second;
    dep_to_explore.pop();
    for(auto dep_dep : currNode.dependencies) {
      Node const& depNode = wp_nodes.at(dep_dep.wpid);
      std::string rel_locaname = relative_locaname(file_locaname, depNode.locaname);
      yamlinternal[currInternalParent]["depends"].push_back(
            yaml_depNode(dep_dep.build_step, dep_dep.is_child, rel_locaname));
      if(dep_dep.is_child) {
        // We only want to explore children and be aware of externals without going deep in their dependencies
        if(!depNode.dependencies.empty()) {
          dep_to_explore.push({ dep_dep.wpid, yamlinternal.size() });
          yamlinternal.push_back(yaml_nodeNode(rel_locaname));
        }
        savelist.internal_deps.push_back({dep_dep.wpid, rel_locaname});
      } else {
        savelist.external_deps.push_back({dep_dep.wpid, rel_locaname});
        yamlexternal.push_back(yaml_nodeNode(rel_locaname));
      }
      
    }
  }

  std::reverse(savelist.internal_deps.begin(), savelist.internal_deps.end());

  YAML::Node depyaml;
  depyaml["internal"] = yamlinternal;
  depyaml["external"] = yamlexternal;

  return { depyaml, savelist };
}

bool DepTree::add_node(WorkpieceId wpid, Wp::Node node)
{
  if(wp_nodes.find(wpid) != wp_nodes.end()) { // This workpiece has already been loaded
    return false;
  }

  for(auto dep : node.dependencies) {
    add_dependency(dep, wpid);
  }
  wp_nodes[wpid] = node;
  set_locaname(wpid, gen_locaname(wpid));

  return true;
}
void DepTree::add_nodes(std::unordered_map<std::string, WorkpieceId> new_wps)
{
  for(auto new_wp : new_wps) {
    locanames_to_wpid.insert(new_wp);
    wp_nodes[new_wp.second] = Node{ .name_suffix=get_suffix(new_wp.first),
                                    .locaname=new_wp.first};
  }
}
bool DepTree::add_dependency(Dependency dep, WorkpieceId to)
{
  wp_iterator found_node = wp_nodes.find(dep.wpid);
  if(found_node == wp_nodes.end()) {
    std::cerr<<"Error: Could not add dependency "<<dep.wpid<<" to "<<to<<" : dependency does not exist"<<std::endl;
    return false;
  }
  Node& dep_node = found_node->second;
  if(dep.is_child && dep_node.has_parent) {
    std::cerr<<"Warning: Setting "<<dep.wpid<<" as child of "<<to<<", but dependency has parent"<<std::endl;
  }
  if(dep.is_child) {
    dep_node.has_parent = true;
    dep_node.ext_refs.insert(dep_node.ext_refs.begin(), to); // Add ref at the start
    set_locaname(dep.wpid, gen_locaname(dep.wpid));
  } else {
    add_ref(dep.wpid, to);
  }
  wp_nodes[to].dependencies.push_back(dep);
  return true;
}
void DepTree::set_nameSuffix(WorkpieceId wpid, std::string suffix)
{
  Node& node = wp_nodes[wpid];
  if(suffix == node.name_suffix) {
    return;
  }

  node.name_suffix = suffix;
  reassign_locanames(wpid, gen_locaname(wpid));
}
void DepTree::set_locaname(WorkpieceId wpid, std::string locaname)
{
  Node& node = wp_nodes[wpid];

  locanames_to_wpid.erase(node.locaname);
  node.locaname = locaname;
  locanames_to_wpid[node.locaname] = wpid;
}

std::vector<WorkpieceId> DepTree::external_unload(WorkpieceId wpid)
{
  if(wp_nodes.find(wpid) == wp_nodes.end()) {
    std::cerr<<"Error: Cannot external-unload "<<wpid<<" : not found"<<std::endl;
    return {};
  }
  if(!wp_nodes[wpid].external_load) {
    std::cerr<<"Warning: external-unload "<<wpid<<" : but was not external-load"<<std::endl;
  }
  wp_nodes[wpid].external_load = false;

  return unload_impl(wpid);
}
std::vector<WorkpieceId> DepTree::unload_impl(WorkpieceId root)
{
  if(any_referenced(root)) {
    return {};
  }
  return erase_recursive(root);
}
std::vector<WorkpieceId> DepTree::erase_recursive(WorkpieceId root)
{
  std::vector<WorkpieceId> erased;
  for(auto dep : wp_nodes[root].dependencies) {
    std::vector<WorkpieceId> tmp_erased;
    if(dep.is_child) {
      erase_recursive(root);
    } else {
      remove_ref(root, dep.wpid);
    }
    erased.insert(erased.end(), tmp_erased.begin(), tmp_erased.end());
  }
  wp_nodes.erase(root);
  erased.push_back(root);
  return erased;
}
std::vector<WorkpieceId> DepTree::remove_ref(WorkpieceId from, WorkpieceId ref)
{
  // Try to remove the referenced node's memory
  // of the from node
  if(std::erase(wp_nodes[ref].ext_refs, from) != 0) {
    return unload_impl(root_of(ref)); // Try to unload 
                                      // (it will check if there are other valid references)
                                      // to the referenced node
  }
  return {};
}
void DepTree::add_ref(WorkpieceId to, WorkpieceId ref)
{
  Node& referenced = wp_nodes[ref];
  if(std::find(referenced.ext_refs.begin(), referenced.ext_refs.end(), to) != referenced.ext_refs.end()) {
    referenced.ext_refs.push_back(ref);
  }
}

bool DepTree::any_referenced(WorkpieceId wpid)
{
  Node& node = wp_nodes[wpid];
  if(node.external_load || node.refs_count() > 0)
    return true;
  for(auto dep : node.dependencies) {
    if(!dep.is_child)
      continue;
    if(any_referenced(dep.wpid))
      return true;
  }
  return false;
}
WorkpieceId DepTree::root_of(WorkpieceId wpid) const
{
  auto currNode = wp_nodes.at(wpid);

  while(currNode.has_parent) {
    wpid = currNode.ext_refs[0];
    currNode = wp_nodes.at(wpid);
  }
  return wpid;
}

std::string DepTree::gen_locaname(WorkpieceId wpid) const
{
  Node currNode = wp_nodes.at(wpid);
  std::string locaname = currNode.name_suffix;

  while(currNode.has_parent) {
    wpid = currNode.ext_refs[0];
    currNode = wp_nodes.at(wpid);
    locaname = currNode.name_suffix + ":" + locaname;
  }
  return locaname;
}
void DepTree::reassign_locanames(WorkpieceId root, std::string rootlocaname)
{
  Node& root_node = wp_nodes[root];
  set_locaname(root, rootlocaname);

  for(auto dep : root_node.dependencies) {
    if(!dep.is_child) {
      continue;
    }
    reassign_locanames(dep.wpid, rootlocaname+":"+wp_nodes[dep.wpid].name_suffix);
  }
}
std::string DepTree::locaname_of(WorkpieceId wpid) const
{
  return wp_nodes.at(wpid).locaname;
}

std::vector<WorkpieceId> DepTree::dependencies_ids(WorkpieceId wpid) const
{
  std::vector<WorkpieceId> out;
  
  Node const& node = wp_nodes.at(wpid);

  for(auto dep : node.dependencies) {
    out.push_back(dep.wpid);
  }
  return out;
}

std::vector<WorkpieceId> DepTree::dependencies_ids_recursive(WorkpieceId wpid) const
{
  std::vector<WorkpieceId> out = dependencies_ids(wpid);
  size_t exploration_ind = 0;
  
  while(exploration_ind != out.size()) {
    std::vector<WorkpieceId> tmp = dependencies_ids(out[exploration_ind]);

    for(auto tmp_dep : tmp) {
      if(std::find(out.begin(), out.end(), tmp_dep) == out.end()) {
        out.push_back(tmp_dep);
      }
    }
    exploration_ind++;
  }

  return out; 
}

std::optional<glm::dmat4> DepTree::transform(WorkpieceId of, WorkpieceId with_respect_to) const
{
  std::optional<glm::dmat4> maybe_descendant = transform_upstream(of, with_respect_to);
  if(maybe_descendant) {
    return maybe_descendant;
  }
  std::optional<glm::dmat4> maybe_ancestor = transform_upstream(with_respect_to, of);

  if(maybe_ancestor) {
    return glm::inverse(*maybe_ancestor);
  }
  return std::nullopt;
}

std::optional<glm::dmat4> DepTree::transform_upstream(WorkpieceId upstream, WorkpieceId downstream) const
{
  // Explore dependencies recursively until wpid "of" is found
  std::vector<WorkpieceId> path_2_dep = path_to_dependency(upstream, downstream);
  if(path_2_dep.empty())
    return std::nullopt;

  glm::dmat4 out(1.0);
  for(size_t i = 0; i < path_2_dep.size()-1; ++i) {
    upstream = path_2_dep[i+1];
    downstream = path_2_dep[i];
    out = out * wp_nodes.at(downstream).dep_transform(upstream);
  }
  return out;
}
std::vector<WorkpieceId> DepTree::path_to_dependency(WorkpieceId target, WorkpieceId root) const
{
  if(root == target)
    return { root };

  std::map<WorkpieceId, WorkpieceId> parent_map;
  std::stack<WorkpieceId> to_explore;

  to_explore.push(root);
  bool found = false;

  while(!to_explore.empty()) {
    WorkpieceId wpid = to_explore.top();
    to_explore.pop();
    Node const& node = wp_nodes.at(wpid);
    for(auto dep : node.dependencies) {
      parent_map[dep.wpid] = wpid;
      if(dep.wpid == target) {
        found = true;
        break;
      }
      to_explore.push(dep.wpid);
    }
  }
  std::vector<WorkpieceId> out;
  if(found) {
    std::map<WorkpieceId, WorkpieceId>::iterator found_parent = parent_map.find(target);
    while(found_parent != parent_map.end()) {
      out.push_back(found_parent->second);
      found_parent = parent_map.find(found_parent->second);
    }
    std::reverse(out.begin(), out.end());
    out.push_back(target);
  }
  return out;
}
void DepTree::external_load(WorkpieceId wpid, bool external)
{
  if(!external)
    return;

  Node& node = wp_nodes[wpid];

  if(node.external_load) {
    std::cerr<<"Wpid "<<wpid<<" is already externaly loaded.."<<std::endl;
    return;
  }
  node.external_load = true;  
}
std::optional<WorkpieceId> DepTree::parent(WorkpieceId wpid) const
{
  try {
    Node node = wp_nodes.at(wpid);
    if(node.has_parent) {
      return node.ext_refs[0];
    }
    return std::nullopt;
  } catch(std::exception &e) {
    return std::nullopt;
  }
}

} // !Wp
} // !Core