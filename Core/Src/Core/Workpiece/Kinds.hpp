
#ifndef BLOOP_CORE_WP_KINDS_HPP_
#define BLOOP_CORE_WP_KINDS_HPP_

namespace Core {

namespace Wp {

struct Kind {
  enum inner { Sketch, Part, Assembly };
};

} // !Wp
} // !Core

#endif