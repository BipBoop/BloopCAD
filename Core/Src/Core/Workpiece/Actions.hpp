
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef BLOOP_CORE_WP_ACTIONS_HPP_
#define BLOOP_CORE_WP_ACTIONS_HPP_

#include <Core/Sketch/Actions.hpp>
#include <Core/Part/Actions.hpp>
#include "Anyworkpiece.hpp"
#include "Kinds.hpp"

#include <variant>

namespace Core {
namespace Wp {

using WpAction = std::variant<Sk::Action, Prt::Action>;

struct Undo {

};
struct Redo {

};
struct Attach {
  WorkpieceId which, to;
  WpAction attachAction;
  bool as_child;
};
struct CreateEmpty {
  Kind::inner kind;
  std::string name_suffix;
  bool external_load;
  bool is_file;

  std::optional<Attach> attach { std::nullopt };
};
using HdAction = std::variant<Undo, Redo, Attach, CreateEmpty>;

using Action = std::variant<Sk::Action, Prt::Action, HdAction>;

Action attach_action(WorkpieceId which, WpAction wpact);

// Branch command to preserve a state and try stuff (ex: the state must
// be restored if an extrusion is canceled, but it's still nice to have 
// interactive feedback so actions are stored on a staging branch until 
// accepted by the user)
// Branch actions will be executed before the tool's main action
enum class BranchAction { Create, Merge, MergeCreate, Remove, RemoveCreate, None };

} // !Wp

struct UpdDesc {
  WorkpieceId wpid;
  Wp::BranchAction branchaction { Wp::BranchAction::None }; // Always updated first
  std::optional<Wp::Action> action { std::nullopt };
  std::string toolmsg { "" };
};

} // !Core

#endif