
#ifndef BLOOP_CORE_ANYWORKPIECE_HPP_
#define BLOOP_CORE_ANYWORKPIECE_HPP_

#include <Core/Sketch/Sketch.hpp>
#include <Core/Part/Part.hpp>

#include <variant>

namespace Core {

using AnyWorkpiece = std::variant<Sk::Sketch, Prt::Part>;


} // !Core

#endif