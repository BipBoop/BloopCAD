
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef BLOOP_CORE_WP_WRAPPER_HPP_
#define BLOOP_CORE_WP_WRAPPER_HPP_

#include "Anyworkpiece.hpp"
#include <Core/UpdReport.hpp>
#include "Actions.hpp"

#include <Core/Sketch/Factory/Factory.hpp>
#include <Core/Part/Factory.hpp>

#include <immer/vector.hpp>

namespace Core {
namespace Wp {

using GetSkFun  = std::function<std::optional<std::pair<Sk::Sketch, glm::dmat4>>(WorkpieceId)>;
using GetPrtFun = std::function<std::optional<std::pair<Prt::Part, glm::dmat4>>(WorkpieceId)>;

template<typename wp_factory>
struct Branch {
  immer::vector<wp_factory> herstory;
  size_t herstory_pos { 0 };
  size_t attach_pos { invalid_index() }; // Point at which this branch attach to the parent branch
  size_t build_pos { 0 };

  // Pushes a new inner workpiece state onto the herstory stack
  // if the herstory stack is not at it's head, this becomes the new head
  bool push(wp_factory factory)
  {
    herstory = herstory.take(herstory_pos+1).push_back(factory);
    herstory_pos = herstory.size()-1;
    std::cout<<"Herstory: pos="<<herstory_pos<<", size="<<herstory.size()<<"\n";
    return factory.mod_struct != -1;
  }
  wp_factory get() const
  {
    return herstory[herstory_pos];
  }
  bool is_build_upToDate() const { return build_pos == herstory_pos; }

  // Sets a new value for the factory at the 
  // herstory pos, use with care
  void set(wp_factory fact)
  {
    herstory = herstory.set(herstory_pos, fact);
  }
  // Moves the herstory stack pointer back one position if possible
  // (returns a pair of the new Workpiece and a success bool)
  bool undo()
  {
    if(herstory_pos == 0) { // Already at tail
      std::cout<<"At tail\n";
      return false;
    }
    herstory_pos--;

    std::cout<<"Herstory: pos="<<herstory_pos<<", size="<<herstory.size()<<"\n";
    return true;
  }
  // Moves the herstory stack pointer forward one position if possible
  // (returns a pair of the new Workpiece and a success bool)
  bool redo()
  {
    if(herstory_pos == herstory.size()-1) { // Already at head
      std::cout<<"At head\n";
      return false;
    }
    herstory_pos++;

    std::cout<<"Herstory: pos="<<herstory_pos<<", size="<<herstory.size()<<"\n";
    return true;
  }

};

template<typename wp_kind, typename wp_factory>
struct Wrapper {
  using action_t = typename wp_factory::action_t;

  wp_kind current;

  Branch<wp_factory> main_branch;
  std::optional<Branch<wp_factory>> staging_branch { std::nullopt };

  Wrapper() = default;
  Wrapper(WorkpieceId wpid)
    : current(wpid)
    , main_branch(Branch<wp_factory>{ .herstory={wp_factory(wpid)}} )
  {

  }
  Wrapper(wp_factory fact)
    : current(fact.wpid)
    , main_branch(Branch<wp_factory>{ .herstory={fact} })
  {

  }

  // Use the factory to build current from a build step to another
  // used by the dependency tree to build make sure dependencies are
  // built in between "inner" build step and to ensure children dependencies
  // have access to the partialy built parent to project geometries and such
  // 
  // returns the step at which the process was halted 
  // (to_step-1 => success, less => problem at returned step)
  bool partial_build(size_t step, GetSkFun getsk, GetPrtFun getprt)
  {
    std::cerr<<"Wrapper partial build for workpiece kind "<<wpkind_str()<<std::endl;
    return invalid_index();
  }
  glm::dmat4 transform_for_step(size_t step) const
  {
    std::cerr<<"No such thing as transform for workpiece kind "<<wpkind_str()<<std::endl;
    return glm::dmat4(1.0);
  }

  Diff diff(AnyWorkpiece const& prev) const
  {
    if(!std::holds_alternative<wp_kind>(prev)) {
      std::cerr<<"Diff workpiece kind mismatch, expected "<<wpkind_str()<<std::endl;
    }

    return std::get<wp_kind>(prev).diff(current);
  }
  // modifies the herstory branches
  // returns true if the change affects the wrapper
  // as seen by an external observer (e.g. if the call to
  // factory() would return a different value)
  bool branchMeUp(BranchAction what_do)
  {
    bool did_do { false };
    switch(what_do) {
    case BranchAction::Create:
      staging_branch = Branch<wp_factory>{ .herstory={factory()} };
      break;
    case BranchAction::Merge:
      // If size is one, no change was made..
      if(staging_branch && staging_branch->herstory.size() > 1) {
        main_branch.push(staging_branch->herstory.back());
        did_do = true;
      }
      staging_branch = std::nullopt;
      break;
    case BranchAction::Remove:
      did_do = staging_branch && staging_branch->herstory.size() > 1;
      staging_branch = std::nullopt;
      break;
    case BranchAction::MergeCreate:
      did_do = branchMeUp(BranchAction::Merge);
      branchMeUp(BranchAction::Create);
      break;
    case BranchAction::RemoveCreate:
      did_do = branchMeUp(BranchAction::Remove);
      branchMeUp(BranchAction::Create);
      break;
    case BranchAction::None:
      break;
    }
    return did_do;
  }

  wp_factory factory() const { return staging_branch ? staging_branch->get() : main_branch.get(); }

  bool is_build_upToDate() const { return staging_branch ? staging_branch->is_build_upToDate() : main_branch->is_build_upToDate(); }

  template<typename anyaction_t>
  bool push_action(anyaction_t action, GetSkFun getsk, GetPrtFun getprt)
  {
    return push_action(std::get<action_t>(action), getsk, getprt);
  }
  bool push_action(action_t action, GetSkFun getsk, GetPrtFun getprt)
  {
    if(staging_branch) {
      return staging_branch->push(make_next(action, getsk, getprt));
    } else {
      return main_branch.push(make_next(action, getsk, getprt));
    }
  }
  void set_current_factory(wp_factory fact)
  {
    if(staging_branch) {
      staging_branch->set(fact);
    } else {
      main_branch.set(fact);
    }
  }
  wp_factory make_next(action_t action, GetSkFun getsk, GetPrtFun getprt)
  {
    std::cerr<<"Wrapper partial build for workpiece kind "<<wpkind_str()<<std::endl;
    return wp_factory();
  }
  size_t main_herstory_pos() const
  {
    return main_branch.herstory_pos;
  }

  // Moves the herstory stack pointer back one position if possible
  // (returns a pair of the new Workpiece and a success bool)
  bool undo()
  {
    return staging_branch ? staging_branch->undo() : main_branch.undo();
  }
  // Moves the herstory stack pointer forward one position if possible
  // (returns a pair of the new Workpiece and a success bool)
  bool redo()
  {
    return staging_branch ? staging_branch->redo() : main_branch.redo();
  }

  std::string wpkind_str() const
  {
    return "unknownkind";
  }
};

template<> bool Wrapper<Sk::Sketch, Sk::Factory>::partial_build(size_t step, GetSkFun getsk, GetPrtFun getprt);
template<> bool Wrapper<Prt::Part, Prt::Factory>::partial_build(size_t step, GetSkFun getsk, GetPrtFun getprt);
template<> glm::dmat4 Wrapper<Prt::Part, Prt::Factory>::transform_for_step(size_t step) const;

template<> Sk::Factory Wrapper<Sk::Sketch, Sk::Factory>::make_next(Sk::Action action, GetSkFun getsk, GetPrtFun getprt);
template<> Prt::Factory Wrapper<Prt::Part, Prt::Factory>::make_next(Prt::Action action, GetSkFun getsk, GetPrtFun getprt);

template<> std::string Wrapper<Sk::Sketch, Sk::Factory>::wpkind_str() const;
template<> std::string Wrapper<Prt::Part, Prt::Factory>::wpkind_str() const;


template<typename Wrapper_t, int kind_id>
class WrapperVec {
private:
  std::vector<std::pair<size_t, Wrapper_t>> wrappers;
  size_t current_head { 0 };

public:
  // Adds the workpiece wrapper to the vector
  // and returns the workpiece index linked to it
  size_t add(Wrapper_t wrap)
  {
    wrappers.push_back({current_head, std::move(wrap)});
    current_head++;
    return current_head-1;
  }

  // Removes the workpiece wrapper linked
  // to the workpiece index
  // Returns true if it was found and false
  // otherwise
  bool remove(size_t wp_index)
  {
    size_t true_ind = find(wp_index);
    if(true_ind == invalid_index()) {
      return false;
    }
    wrappers.erase(wrappers.begin() + true_ind);
    return true;
  }

  Wrapper_t& get(size_t wp_index)
  {
    size_t true_ind = find(wp_index);
    if(true_ind == invalid_index()) {
      std::cerr<<"Trying to access wrapper at index "<<wp_index<<" : not found"<<std::endl;
    }
    return wrappers[true_ind].second;
  }
  Wrapper_t const& get(size_t wp_index) const
  {
    size_t true_ind = find(wp_index);
    if(true_ind == invalid_index()) {
      std::cerr<<"Trying to access wrapper at index "<<wp_index<<" : not found"<<std::endl;
    }
    return wrappers[true_ind].second;
  }
  size_t find(size_t wp_index) const
  {
    for(size_t i = 0; i < wrappers.size(); ++i) {
      if(wrappers[i].first == wp_index) 
        return i;
    }
    return invalid_index();
  }
  size_t next() const { return current_head; }
};

} // !Wp
} // !Core

#endif