
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "Handler.hpp"

#include <Core/Utils/Visitor.hpp>
#include <Core/Print/IDOstream.hpp>
#include <Core/Sketch/IO/Serialize.hpp>
#include <Core/Sketch/IO/ToSvg.hpp>
#include <Core/Part/IO/ToSTL.hpp>
#include <Core/Part/IO/Serialize.hpp>
#include <Core/Utils/Paths.hpp>

#include <glm/gtx/string_cast.hpp>

#include <fstream>
#include <sstream>

namespace Core {
namespace Wp {

WorkpieceId Handler::create_empty(CreateEmpty act)
{
  WorkpieceId wpid;
  switch(act.kind) {
  case Kind::Sketch:
    wpid = WorkpieceId{.kind=act.kind, .index=sketches.next()};
    sketches.add(Wrapper<Sk::Sketch, Sk::Factory>(wpid));
    break;
  case Kind::Part:
    wpid = WorkpieceId{.kind=act.kind, .index=parts.next()};
    parts.add(Wrapper<Prt::Part, Prt::Factory>(wpid));
    break;
  default:
    return WorkpieceId{};
  }
  deptree.add_node(wpid, Wp::Node { .name_suffix=act.name_suffix,
                                    .is_file=act.is_file,
                                    .external_load=act.external_load,
                                    .has_parent=false });
  
  return wpid;
}

UpdReport Handler::branchItUp(WorkpieceId wpid, BranchAction act)
{
  bool do_rebuild = false;
  fwd_wrapper(wpid, [&](auto& wrapper) {
    do_rebuild = wrapper.branchMeUp(act);
  });
  if(do_rebuild) {
    return rebuild(wpid);
  }
  return UpdReport{};
}
UpdReport Handler::update(UpdDesc updDesc)
{
  UpdReport report;
  report.toolmsg = updDesc.toolmsg;

  if(updDesc.branchaction != BranchAction::None) {
    report = branchItUp(updDesc.wpid, updDesc.branchaction);
  }
  if(!updDesc.action) {
    return report;
  }
  size_t init_n_steps;
  fwd_wrapper(updDesc.wpid, [&](auto const& wrapper) {
    init_n_steps = wrapper.factory().n_steps();
  });

  if(std::holds_alternative<HdAction>(*updDesc.action)) {
    report.combine(update(updDesc.wpid, std::get<HdAction>(*updDesc.action)));
  } else {
    fwd_wrapper(updDesc.wpid, [&](auto& wrapper) {
      // if(std::holds_alternative<decltype(wrapper)::action_t>(updDesc.action)) {
      //   std::cerr<<"Action - workpiece mismatch trying to execute action of wp-kind "
      //               <<updDesc.action.index()<<" on "<<updDesc.wpid<<std::endl;
      //   return;
      // }
      wrapper.push_action(*updDesc.action,  std::bind(&Handler::sketch_access, this, updDesc.wpid, std::placeholders::_1), 
                                            std::bind(&Handler::part_access, this, updDesc.wpid, std::placeholders::_1));
    });
    report.combine(rebuild(updDesc.wpid));
  }

  size_t final_n_steps;
  fwd_wrapper(updDesc.wpid, [&](auto const& wrapper) {
    final_n_steps = wrapper.factory().n_steps();
  });
  if(init_n_steps != final_n_steps) {
    report.wp_buildSequence_mod.push_back({ updDesc.wpid, 1 });
  }

  return report;
}
UpdReport Handler::update(WorkpieceId wpid, HdAction act)
{
  return std::visit(Visitor {
    [&](Undo) { return undo(wpid); },
    [&](Redo) { return redo(wpid); },
    [&](CreateEmpty act) { 
      wpid = create_empty(act);
      UpdReport out{.wp_diffs={{wpid, Diff{.added=constituents_of(wpid)}}}};
      if(act.attach) {
        act.attach->which = wpid;
        out.combine(update(act.attach->to, *act.attach));
      }
      return out;
    },
    [&](Attach act) {
      UpdReport out = update(UpdDesc{.wpid=wpid, .action=attach_action(act.which, act.attachAction)});
      size_t buildstep;
      size_t factory_herstory_pos;
      glm::dmat4 transform;
      fwd_wrapper(wpid, [&](auto const& wrapper) {
        buildstep = wrapper.factory().n_steps()-1;
        factory_herstory_pos = wrapper.main_herstory_pos();
        transform = wrapper.transform_for_step(buildstep);
        std::cout<<"Transform: "<<glm::to_string(transform)<<"\n";
      });
      
      deptree.add_dependency(Dependency{.wpid=act.which, 
                                        .is_child=act.as_child,
                                        .transform=transform,
                                        .build_step=buildstep,
                                        .factory_version=factory_herstory_pos}, wpid);
      return out;
    }
  }, act);
}
UpdReport Handler::undo(WorkpieceId wpid)
{
  fwd_wrapper(wpid, [](auto& wrapper) {
    wrapper.undo();
  });
  return rebuild(wpid);
}
UpdReport Handler::redo(WorkpieceId wpid)
{
  fwd_wrapper(wpid, [](auto& wrapper) {
    wrapper.redo();
  });
  return rebuild(wpid);
}
UpdReport Handler::rebuild(WorkpieceId wpid)
{
  std::cout<<"Rebuild wpid: "<<wpid<<"\n";
  UpdReport out;
  VecsSnapshot init_list;
  size_t n_steps_total;
  fwd_wrapper(wpid, [&](auto const& wrapper) {
    init_list = wrapper.current.idsSnapshot();
    n_steps_total = wrapper.factory().n_steps();
  });

  std::vector<Dependency>& dependencies = deptree.wp_nodes[wpid].dependencies;
  
  // Number of buildsteps to produce de workpiece

  // Offset in the dependencies vector to find a match dependency-buildstep
  size_t curr_dep_offset = 0;

  for(size_t i = 0; i < n_steps_total; ++i) {
    for(size_t j = curr_dep_offset; j < dependencies.size(); ++j) {
      if(dependencies[j].build_step == i) {
        // reatach
        fwd_wrapper(wpid, [&](auto const& wrapper) {
          dependencies[j].transform = wrapper.transform_for_step(i);
        });

        // rebuild the dependency
        out.combine(rebuild(dependencies[j].wpid));
      }
      if(dependencies[j].build_step > i) {
        break;
      }
    }
    bool success;
    fwd_wrapper(wpid, [&](auto& wrapper) {
      success = wrapper.partial_build(i, 
                            std::bind(&Handler::sketch_access, this, wpid, std::placeholders::_1), 
                            std::bind(&Handler::part_access, this, wpid, std::placeholders::_1));
    });
    if(!success) {
      std::cerr<<"Failed at buildstep "<<i<<std::endl;
      break;
    }
  }
  fwd_wrapper(wpid, [&](auto const& wrapper) {
    out.combine(UpdReport::from_diff(wpid, 
      wrapper.current.account_relationships(Wp::Diff::make(wpid, init_list, wrapper.current.idsSnapshot()))));
    int mod = wrapper.factory().mod_struct;
    if(mod != -1) {
      out.wp_buildSequence_mod.push_back({ wpid, mod });
    }
  });
  return out;
}
void Handler::set_nameSuffix(WorkpieceId wpid, std::string name_suffix)
{
  deptree.set_nameSuffix(wpid, name_suffix);
}

std::optional<WorkpieceId> Handler::find_or_load(std::string filepath)
{
  return find_or_load_impl(filepath, true);
}
std::optional<WorkpieceId> Handler::load(std::string filepath)
{
  return load_impl(filepath, true);
}
std::vector<WorkpieceId> Handler::write_on_stream(WorkpieceId wpid, std::ostream& os) const
{
  SaveList list;
  YAML::Node depsNode;
  std::tie(depsNode, list) = deptree.saveInfo(wpid);

  std::map<WorkpieceId, std::string> wpids_to_locanames;
  YAML::Node filenode;

  filenode["deps"] = depsNode;
  for(auto intdep : list.internal_deps) {
    filenode["internal"].push_back(encode_workpiece(intdep.first, intdep.second, wpids_to_locanames));
  }
  std::vector<WorkpieceId> out_deps;
  for(auto extdep : list.external_deps) {
    out_deps.push_back(extdep.first);
    filenode["external"].push_back(extdep.second);
  }
  os<<filenode;
  return out_deps;
}
void Handler::save(WorkpieceId wpid, bool save_extDeps) const
{
  std::ofstream filestream(deptree.gen_locaname(deptree.root_of(wpid)));
  auto ext_deps = write_on_stream(wpid, filestream);

  if(save_extDeps) {
    for(auto extdep : ext_deps) {
      save(extdep, true);
    }
  }
}
void Handler::export_wp(WorkpieceId wpid, std::string filepath) const
{
  size_t last_point = filepath.find_last_of('.');
  if(last_point == std::string::npos) {
    std::cerr<<"Cannot deduce filetype, no extension for file \""<<filepath<<"\""<<std::endl;
    return;
  }

  std::string extension = filepath.substr(last_point);
  std::string content = "no content, export error";
  switch(wpid.kind) {
  case Kind::Sketch:
    if(extension == ".svg") {
      auto maybe_fact = get_wp<Sk::Factory>(wpid);
      if(!maybe_fact) {
        std::cerr<<"Error getting sketch at "<<wpid<<std::endl;
        break;
      }
      content = Sk::to_svg(*maybe_fact);
    } else {
      std::cerr<<"Extension not handled for sketch: \""<<extension<<"\""<<std::endl;
      return;
    }
    break;
  case Kind::Part:
    std::cerr<<"No export routine for part workpiece :("<<std::endl;
    if(extension == ".stl") {
      auto maybe_part = get_wp<Prt::Part>(wpid);
      if(!maybe_part) {
        std::cerr<<"Error getting part at "<<wpid<<std::endl;
        break;
      }
      content = Prt::to_stl(*maybe_part);
    } else {
      std::cerr<<"Extension not handled for part: \""<<extension<<"\""<<std::endl;
      return;
    }
    break;
  }
  
  std::ofstream filestream(filepath);
  filestream<<content;
}

void Handler::external_unload(WorkpieceId wpid)
{
  std::vector<WorkpieceId> to_unload = deptree.external_unload(wpid);
  for(WorkpieceId wpid : to_unload) {
    unload_impl(wpid);
  }
}

std::vector<Mesh> Handler::meshes(Id id) const
{
  std::vector<Mesh> out;
  fwd_wrapper(id.wpid, [&](auto const& wrapper) {
    out = wrapper.current.meshes(id.entId);
  });
  return out;
}
IdVec Handler::constituents_of(WorkpieceId wpid, bool recursive) const
{
  IdVec out_ids;
  std::vector<WorkpieceId> wpids = dependencies_of(wpid, recursive);
  wpids.push_back(wpid);

  for(auto currwipd : wpids) {
     // Add the workpiece id as entity
    out_ids.push_back(Id{.wpid=currwipd});

    // Add the workpiece's entities
    IdVec children;
    fwd_wrapper(currwipd, [&](auto& wrapper) {
      children = wrapper.current.children();
    });
  
    out_ids.insert(out_ids.end(), children.begin(), children.end());
  }
  return out_ids;
}
std::vector<WorkpieceId> Handler::dependencies_of(WorkpieceId wpid, bool recursive) const
{
  if(recursive) {
    return deptree.dependencies_ids_recursive(wpid);
  } else {
    return deptree.dependencies_ids(wpid);
  }
}
std::string Handler::name_of(WorkpieceId wpid) const
{
  return deptree.wp_nodes.at(wpid).name_suffix;
}
// glm::dvec3 Handler::point(WorkpieceId wpid, Ray ray, 
//                           glm::dmat4 wp_transform) const
// {
//   glm::dvec3 pt(0.0);
//   fwd_wrapper(wpid, [&](auto& wrapper) {
//     pt = wrapper.current.point(ray.transform(glm::inverse(wp_transform)));
//   });
//   return glm::dvec3(glm::dvec4(pt, 1.0)*wp_transform);;
// }

// std::vector<std::pair<Id, glm::dvec3>> Handler::touched(WorkpieceId wpid, Ray ray, 
//                                                         glm::dmat4 wp_transform) const
// {
//   std::vector<std::pair<Id, glm::dvec3>> out;
//   fwd_wrapper(wpid, [&](auto const& wrapper) {
//     out = wrapper.current.touched(ray.transform(glm::inverse(wp_transform)));
//   });
//   for(size_t i = 0; i < out.size(); ++i) {
//     out[i].second = glm::dvec3(glm::dvec4(out[i].second, 1.0)*wp_transform);
//   }

//   std::sort(out.begin(), out.end(), 
//         [&](std::pair<Id, glm::dvec3> lhs, std::pair<Id, glm::dvec3> rhs) -> bool {
//           double ldist = glm::distance2(ray.origin, lhs.second);
//           double rdist = glm::distance2(ray.origin, rhs.second);

//           return ldist < rdist;
//   });
//   return out;
// }
// std::vector<std::pair<Id, glm::dvec3>> Handler::touched(WorkpieceId wpid, RayPyramid ray, 
//                                                         glm::dmat4 wp_transform) const
// {

// }
std::optional<glm::dmat4> Handler::transform_descendant_to_ancestor(WorkpieceId wpid, WorkpieceId root) const
{
  return deptree.transform(wpid, root);
}
std::string Handler::locaname(WorkpieceId wpid) const
{
  return deptree.wp_nodes.at(wpid).locaname;
}
void Handler::set_namesuffix(WorkpieceId wpid, std::string name_suffix)
{
  deptree.set_nameSuffix(wpid, name_suffix);
}

YAML::Node Handler::encode_workpiece( WorkpieceId wpid, std::string relname, 
                                      std::map<WorkpieceId, std::string>& wpids_to_locanames) const
{
  YAML::Node out;
  wpids_to_locanames[wpid] = deptree.wp_nodes.at(wpid).locaname;

  fwd_wrapper(wpid, [&](auto& wrapper) {
    out = encode(wrapper.factory(), wpids_to_locanames);
  });
  out["relname"] = relname;
  return out;
}
std::optional<WorkpieceId> Handler::find_or_load_impl(std::string filepath, bool external)
{
  using found_it = std::unordered_map<std::string, WorkpieceId>::iterator;
  found_it found = deptree.locanames_to_wpid.find(filepath);

  if(found != deptree.locanames_to_wpid.end()) {
    deptree.external_load(found->second, external);
    return found->second;
  }
  return load_impl(filepath, external);
}
std::optional<WorkpieceId> Handler::load_impl(std::string filepath, bool external)
{
  std::unordered_map<std::string, WorkpieceId> locanames_to_wpids;

  YAML::Node filenode;
  try {
    filenode = YAML::LoadFile(filepath);
  } catch(std::exception& e) {
    return std::nullopt;
  }

  YAML::Node externalyaml = filenode["external"];

  // Maps locaname of dependent to locaname of ext_dep
  std::vector<std::pair<std::string, std::string>> ext_locanames;
  for(auto extdep : externalyaml) {
    std::string dependent_relname = extdep["ent"].as<std::string>();
    std::string dependency_relname = extdep["ency"].as<std::string>();
    std::string locaname = absolute_locaname(filepath, dependency_relname);

    if(deptree.locanames_to_wpid.find(locaname) == deptree.locanames_to_wpid.end()) {
      // If not already loaded, load workpiece
      std::optional<WorkpieceId> maybe_loaded = load_impl(locaname, false);

      if(!maybe_loaded) {
        std::cerr<<"Could not load "<<locaname<<std::endl;
        continue;
      }
      locanames_to_wpids[locaname] = *maybe_loaded;
    }
    ext_locanames.push_back({dependent_relname, dependency_relname});
  }
  
  YAML::Node internalyaml = filenode["internal"];
  for(auto intdep : internalyaml) {
    load_workpiece(intdep, filepath, locanames_to_wpids);
  }

  YAML::Node depyaml = filenode["deps"];
  deptree.add_nodes(locanames_to_wpids);
  deptree.load_yaml(depyaml, filepath);


  WorkpieceId loaded_wpid = locanames_to_wpids[filepath];
  rebuild(loaded_wpid);
  return loaded_wpid;
}
void Handler::load_workpiece( YAML::Node workpieceyaml, std::string filepath, 
                              std::unordered_map<std::string, WorkpieceId>& locanames_to_wpids)
{
  std::string relname = workpieceyaml["relname"].as<std::string>();
  std::string locaname;
  
  if(relname == "&root") {
    locaname = filepath;
  } else {
    locaname = absolute_locaname(filepath, relname);
  }

  std::string kind = workpieceyaml["kind"].as<std::string>();

  if(kind == "sketch") {
    WorkpieceId new_wpid = next(Kind::Sketch);
    locanames_to_wpids[locaname] = new_wpid;
    Sk::Factory fact;
    decode(workpieceyaml, fact, new_wpid, filepath, locanames_to_wpids);
    sketches.add(Wrapper<Sk::Sketch, Sk::Factory>(fact));
  } else if(kind == "part") {
    WorkpieceId new_wpid = next(Kind::Part);
    locanames_to_wpids[locaname] = new_wpid;
    Prt::Factory fact;
    decode(workpieceyaml, fact, new_wpid, filepath, locanames_to_wpids);
    parts.add(Wrapper<Prt::Part, Prt::Factory>(fact));
  }
}
void Handler::unload_impl(WorkpieceId wpid)
{
  switch(wpid.kind) {
  case Kind::Sketch:
    sketches.remove(wpid.index);
    break;
  case Kind::Part:
    parts.remove(wpid.index);
    break;
  }
}

WorkpieceId Handler::next(Kind::inner kind) const
{
  switch(kind) {
  case Kind::Sketch:
    return WorkpieceId{ .kind=kind, .index=sketches.next() };
  case Kind::Part:
    return WorkpieceId{ .kind=kind, .index=parts.next() };
  default:
    break;
  }
  std::cerr<<"Unknown wp kind: "<<kind<<std::endl;
  return WorkpieceId{};
}
std::optional<std::pair<Sk::Sketch, glm::dmat4>> Handler::sketch_access(WorkpieceId parent, WorkpieceId sk_id) const
{
  auto maybe_sk = get_wp<Sk::Sketch>(sk_id);
  if(!maybe_sk) {
    return std::nullopt;
  }
  auto maybe_transform = transform_descendant_to_ancestor(sk_id, parent);
  if(!maybe_transform) { 
    return std::nullopt;
  }
  return std::make_pair(*maybe_sk, *maybe_transform);
}
std::optional<std::pair<Prt::Part, glm::dmat4>> Handler::part_access(WorkpieceId parent, WorkpieceId prt_id) const
{
  auto maybe_prt = get_wp<Prt::Part>(prt_id);
  if(!maybe_prt) {
    return std::nullopt;
  }
  auto maybe_transform = transform_descendant_to_ancestor(prt_id, parent);
  if(!maybe_transform) { 
    return std::nullopt;
  }
  return std::make_pair(*maybe_prt, *maybe_transform);
}

template<>
std::optional<Prt::Part> Handler::get_wp(WorkpieceId wpid) const
{
  if(wpid.kind != Kind::Part) {
    std::cerr<<"Cannot get part out of wpid "<<wpid<<std::endl;
    return std::nullopt;
  }
  return parts.get(wpid.index).current;
}
template<>
std::optional<Prt::Factory> Handler::get_wp(WorkpieceId wpid) const
{
  if(wpid.kind != Kind::Part) {
    std::cerr<<"Cannot get part factory out of wpid "<<wpid<<std::endl;
    return std::nullopt;
  }
  return parts.get(wpid.index).factory();
}
template<>
std::optional<Sk::Sketch> Handler::get_wp(WorkpieceId wpid) const
{
  if(wpid.kind != Kind::Sketch) {
    std::cerr<<"Cannot get sketch out of wpid "<<wpid<<std::endl;
    return std::nullopt;
  }
  return sketches.get(wpid.index).current;
}
template<>
std::optional<Sk::Factory> Handler::get_wp(WorkpieceId wpid) const
{
  if(wpid.kind != Kind::Sketch) {
    std::cerr<<"Cannot get sketch factory out of wpid "<<wpid<<std::endl;
    return std::nullopt;
  }
  return sketches.get(wpid.index).factory();
}


} // !Wp
} // !Core