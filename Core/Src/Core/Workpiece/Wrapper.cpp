
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "Wrapper.hpp"

#include <Core/Sketch/Factory/Factory.hpp>
#include <Core/Part/Factory.hpp>

namespace Core {
namespace Wp {

template<>
bool Wrapper<Sk::Sketch, Sk::Factory>::partial_build(size_t step, GetSkFun getsk, GetPrtFun getprt)
{
  bool step_success = true;
  Sk::Factory upd_factory;
  std::tie(current, upd_factory) = std::move(factory().build(step_success));
  set_current_factory(upd_factory);

  return step_success;
}

template<>
bool Wrapper<Prt::Part, Prt::Factory>::partial_build(size_t step, GetSkFun getsk, GetPrtFun getprt)
{
  if(step == 0) {
    current = Prt::Part(factory().wpid);
  }
  bool step_success = true;
  Prt::Factory upd_factory;
  std::tie(current, upd_factory) = std::move(factory().build(current, step, getsk, step_success));
  set_current_factory(upd_factory);
  
  return step_success;
}

template<>
glm::dmat4 Wrapper<Prt::Part, Prt::Factory>::transform_for_step(size_t step) const
{
  return factory().transform_for_step(step, current);
}

template<> 
Sk::Factory Wrapper<Sk::Sketch, Sk::Factory>::make_next(Sk::Action action, GetSkFun getsk, GetPrtFun getprt)
{
  return next(factory(), action, current);
}
template<> 
Prt::Factory Wrapper<Prt::Part, Prt::Factory>::make_next(Prt::Action action, GetSkFun getsk, GetPrtFun getprt)
{
  return next(factory(), action, current, getsk);
}


template<>
std::string Wrapper<Sk::Sketch, Sk::Factory>::wpkind_str() const
{
  return "Sketch";
}
template<>
std::string Wrapper<Prt::Part, Prt::Factory>::wpkind_str() const
{
  return "Part";
}

} // !Wp
} // !Core