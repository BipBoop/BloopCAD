
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef BLOOP_CORE_WP_DEPTREE_HPP_
#define BLOOP_CORE_WP_DEPTREE_HPP_

/*
  Defines the relationship between workpieces (external dependency, child, parent)
*/

#include <Core/Id.hpp>

#include <yaml-cpp/yaml.h>

#include <glm/glm.hpp>

#include <map>
#include <unordered_map>
#include <string>
#include <optional>

namespace Core {

namespace Wp {

struct SaveList {
  // Dependencies as workpiece ids + their relative locaname
  // (see Core/Utils/Paths.hpp)
  std::vector<std::pair<WorkpieceId, std::string>> internal_deps;
  std::vector<std::pair<WorkpieceId, std::string>> external_deps;
};

struct Dependency {
  WorkpieceId wpid;
  bool is_child { true };
  glm::dmat4 transform;

  // Build step into the dependant
  // when the dependency is added 
  size_t build_step { invalid_index() };

  // Factory version of the dependency workpiece
  // if this value is not equal to the workpiece
  // wrapper's current factory version (herstory_index)
  // then the dependency has changed since last build
  // of the dependant, which is now invalidated 
  size_t factory_version { invalid_index() };
};

struct Node {
  // Name of the workpiece, if it's a file, it will be the fs path, otherwise it's the locaname suffix
  std::string name_suffix;
  std::string locaname;

  bool is_file { false }; // If true, the workpiece is the main workpiece of a file 
                          // (it can have dependencies stored along but is the parent to all of them)
  bool external_load { false }; // If true, the workpiece is requested by an outside application layer
  bool has_parent { false }; // Parent node

  std::vector<Dependency> dependencies; // Nodes on which this node depends (with additional flags)

  // Nodes which have this node as a dependency (including parent)
  // this is not an information to save, it's purely to update the 
  // structure (dealocate and update upstream workpieces)
  std::vector<WorkpieceId> ext_refs;

  size_t refs_count() const { return ext_refs.size(); }
  bool is_floating() { return !is_file && !has_parent; }
  glm::dmat4 dep_transform(WorkpieceId dep_wpid) const;
};

struct DepTree {
  std::map<WorkpieceId, Wp::Node> wp_nodes;
  std::unordered_map<std::string, WorkpieceId> locanames_to_wpid;
  using wp_iterator = std::map<WorkpieceId, Wp::Node>::iterator;

  // Load yaml node into deptree
  // it will keep it's current state and add new wpnodes
  // it assumes locanames_to_wpid has already been updated
  // so that the yamlnode can refer to a locaname and the deptree
  // can interpret this as a valid wpid
  // Returns the root workpiece of the yamlnode
  void load_yaml(YAML::Node const& yamlnode, std::string root_locaname);

  // Create a yaml node describing the dependency tree
  // for a single workpiece from it's root workpiece
  // it will include all dependencies (children and external)
  // it returns the yamlnode as well as a savelist for the handler
  // to use
  std::pair<YAML::Node, SaveList> saveInfo(WorkpieceId wpid) const;

  bool add_node(WorkpieceId wpid, Wp::Node node);
  void add_nodes(std::unordered_map<std::string, WorkpieceId> new_wps);
  bool add_dependency(Dependency dep, WorkpieceId to);
  
  void set_nameSuffix(WorkpieceId wpid, std::string suffix);
  void set_locaname(WorkpieceId wpid, std::string locaname);

  // Removes the external-load flag from the given
  // wpid and attempts to unload it (might be blocked if
  // some other loaded workpiece requires it or a descendant)
  // Returns all workpieces which were unloaded
  std::vector<WorkpieceId> external_unload(WorkpieceId wpid);

  // Unloads a whole dependency tree starting at root if 
  // no part of it is being referenced (internaly or externaly)
  // idealy root is a file node.
  // Returns all workpieces which were unloaded
  std::vector<WorkpieceId> unload_impl(WorkpieceId root);
  std::vector<WorkpieceId> erase_recursive(WorkpieceId root);

  // Remove a reference from a workpiece to another
  // used if the workpiece from is removed
  // the referenced workpiece is referenced one less time
  std::vector<WorkpieceId> remove_ref(WorkpieceId from, WorkpieceId ref);
  void add_ref(WorkpieceId to, WorkpieceId ref);
 
  // Returns true if this workpiece's node or one of it's children' 
  // is referenced (internaly or externaly)
  bool any_referenced(WorkpieceId wpid);

  WorkpieceId root_of(WorkpieceId wpid) const;

  // Generates the location of the workpiece 
  // (filesystem path + workpiece path)
  // A sketch sk1 children of file-part prt3.bpart located at ~/Document
  // would have a locaname "~/Document/prt3.bpart:sk1"
  // it can then be used with the locanames_to_wpid map
  std::string gen_locaname(WorkpieceId wpid) const;
  // Regenerates the locaname of all children of root 
  void reassign_locanames(WorkpieceId root, std::string rootlocaname);

  std::string locaname_of(WorkpieceId wpid) const;

  // Returns all dependencies of a workpiece
  std::vector<WorkpieceId> dependencies_ids(WorkpieceId wpid) const;
  // Returns all dependencies of a workpiece as well as 
  // the dependencies' dependencies and so on
  std::vector<WorkpieceId> dependencies_ids_recursive(WorkpieceId wpid) const;

  // Returns the transformation of a workpiece 
  // with respect to another workpiece, 
  // or nullopt if there is no dep link
  std::optional<glm::dmat4> transform(WorkpieceId of, WorkpieceId with_respect_to) const;

  // Helper functions to get transform of an upstream node relative to a downstream node
  // E.g. the transform of a sketch relative to a part
  std::optional<glm::dmat4> transform_upstream(WorkpieceId upstream, WorkpieceId downstream) const;

  // Returns a path from a root workpiece to a dependency
  // if the returned vector is empty, no path was found
  std::vector<WorkpieceId> path_to_dependency(WorkpieceId dep, WorkpieceId root) const;

  // Sets the external load flag on the workpiece's root workpiece
  // it does not unset it if the "external" parameter is false
  // it also warns if the flag is already up and is requested
  // to be set high
  void external_load(WorkpieceId wpid, bool external);

  std::optional<WorkpieceId> parent(WorkpieceId wpid) const;
};

} // !Wp

} // !Core

#endif