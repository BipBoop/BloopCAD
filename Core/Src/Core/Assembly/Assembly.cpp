
#include "Assembly.hpp"

namespace Core {
namespace Ass {

std::vector<WorkpieceId> Assembly::direct_dependencies() const
{
  std::vector<WorkpieceId> used;

  parts.for_each([&](std::pair<WorkpieceId, glm::dmat4> entity_transform, size_t index, size_t version) {
    used.push_back(entity_transform.first);
    return true;
  });
  assemblies.for_each([&](std::pair<WorkpieceId, glm::dmat4> entity_transform, size_t index, size_t version) {
    used.push_back(entity_transform.first);
    return true;
  });

  return used;
}

} // !Ass
} // !Core