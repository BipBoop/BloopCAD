
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef BLOOP_CORE_ASSEMBLY_HPP_
#define BLOOP_CORE_ASSEMBLY_HPP_

#include <Core/Id.hpp>

#include <glm/glm.hpp>

#include <variant>
#include <vector>
#include <tuple>

namespace Core {
namespace Ass {

struct Constraint {
  enum class Kind { None, PlanePlaneCoincidence, PlanePlaneParallel, AxisAligned };
};

struct Name {
  enum class SymKind { None = 0, kPart = 1, kAssembly = 2, kConstraint = 4 };
  SymKind kind;
  size_t id;
};
using NameList = std::vector<Name>;

struct Assembly {
  EntityList<std::pair<WorkpieceId, glm::dmat4>> parts;
  EntityList<std::pair<WorkpieceId, glm::dmat4>> assemblies;

  EntityList<Constraint> constraints;
  WorkpieceId self_id;
  
  std::vector<WorkpieceId> direct_dependencies() const;
};

struct AddPart {
  size_t id;
};
struct AddAssembly {
  size_t id;
};
struct AddSymbolicConstraint {
  Constraint constr;
};
struct Remove {

};
struct Rebuild {

};

using Action = std::variant<AddPart, AddAssembly, AddSymbolicConstraint, Remove, Rebuild>;



} // !Ass
} // !Core

#endif