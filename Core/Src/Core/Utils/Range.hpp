
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef BLOOP_CORE_RANGE_HPP_
#define BLOOP_CORE_RANGE_HPP_

#include <vector>

namespace Core {

template<typename Type, bool inclusive_in = true, bool inclusive_out = false>
struct ContRange {
	Type first, last;

    ContRange() = default;
    ContRange(Type first_, Type last_) {
        first = std::min(first_, last_);
        last = std::max(first_, last_);
    }

	Type size() const { return last - first + inclusive_in + inclusive_out - 1; }
    bool contains(Type val) const
    {
        bool start, end;

        start = val > first || (inclusive_in && val == first);
        end = val < last || (inclusive_out && val == last);

        return start && end;
    }
};

using IndRange = ContRange<size_t, true, true>;

} // !Core

#endif