
#ifndef BLOOP_CORE_UTILS_STLSHENANIGANS_HPP_
#define BLOOP_CORE_UTILS_STLSHENANIGANS_HPP_

#include <vector>

namespace Core {
  
template<typename T>
void insert_back(std::vector<T>& aggr, std::vector<T> to_add)
{
  aggr.insert(aggr.end(), to_add.begin(), to_add.end());
}

template<typename T>
std::vector<std::pair<T, T>> swap_twinvec(std::vector<std::pair<T, T>> vec)
{
  for(size_t i = 0; i < vec.size(); ++i) {
    std::swap(vec[i].first, vec[i].second);
  }
  return vec;
}
template<typename T>
std::vector<T> select_from_vec(std::vector<T> const& src, std::vector<size_t> const indices)
{
  std::vector<T> out;
  for(auto index : indices) {
    out.push_back(src[index]);
  }
  return out;
}
  
} // !Core

#endif
