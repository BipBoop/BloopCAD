
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef BLOOP_CORE_PATHS_HPP_
#define BLOOP_CORE_PATHS_HPP_

#include <string>
#include <vector>


/*
  A BloopPath is the junction of a filepath and a workpiece path
  workpiece paths are used if workpieces are owned by other workpieces

  ex: file/location/on/computer/apart.bpart:sketch0
  Would point to sketch0, a sketch which is owned by apart.bpart which has a file system location
*/

namespace Core {

// Relative filepath between two files
std::string relative_path(std::string absolute_from, std::string absolute_to, char separator = '/');
// Absolute path from a starting point and a relative path
std::string absolute_path(std::string absolute_from, std::string relative_to, char separator = '/');

// Breaks the path into components with an arbitrary separator
std::vector<std::string> break_path(std::string path, std::string separator, size_t start_pos = 0);
// Create a path from components with an arbitrary serparator
std::string constitute_path(std::vector<std::string> broken_path, std::string separator);

// Remove the file path from a BloopPath
std::string remove_filepath(std::string path);
// Get the file path from a BloopPath
std::string get_filepath(std::string path);
// Extract the filename from the filepath from a BloopPath
std::string get_filename(std::string path);
std::pair<std::string, std::string> get_folderpath_and_bloopath(std::string locaname);

// Will give the path between two workpiece from their locanames
// they can be in the same file or in two different files

// Internal dependencies that will be saved alongside the 
// main workpiece in the actual file
// vector of workpieces ids + their relative locaname to the main workpiece
// e.g. if the file is "~/Documents/apart.bpart" and the
// locaname of a dependency is "~/Documents/apart.bpart:sketch1"
// the relative locaname would be ":sketch1"

// External dependencies that will not be saved in the same file
// as the main workpiece but that will be referenced
// vector of workpiece ids + their full locaname with the path 
// relative to the file
// e.g. if the file is "~/Documents/apart.bpart" and the external
// dependency is "~/Documents/folder/skeletonpart.bpart:sketch2"
// the stored locaname would be "folder/sketletonpart.bpart:sketch2"
std::string relative_locaname(std::string src, std::string dst);

std::string absolute_locaname(std::string absolute_from, std::string relative_to);

std::string get_suffix(std::string locaname);

// Returns the name of the handle of an entity 
std::string handleName(std::string entityName, int handle_num);
// Decodes the name of an entity as the name of the main entity
// and the handle number (can be -1 if the compound name is the name
// of the main entity)
std::pair<std::string, int> entityName_and_handleNum(std::string compoundName); 

} // !Core

#endif