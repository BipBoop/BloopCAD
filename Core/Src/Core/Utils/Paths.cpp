
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "Paths.hpp"

#include <iostream>

namespace Core {

std::string relative_path(std::string absolute_from, std::string absolute_to, char separator)
{
  std::vector<std::string> broken_from = break_path(absolute_from, std::string(1, separator));
  std::vector<std::string> broken_to = break_path(absolute_to, std::string(1, separator));

  size_t diff_ind = 0;
  for(; diff_ind < broken_from.size() && diff_ind < broken_to.size(); ++diff_ind) {
    if(broken_from[diff_ind] != broken_to[diff_ind]) {
      break;
    }
  }
  std::string rel_path(1, separator);

  for(size_t i = 0; i < broken_from.size() - diff_ind; ++i) {
    rel_path += ".." + std::string(1, separator);
  }
  for(size_t i = diff_ind; i < broken_to.size()-1; ++i) {
    rel_path += broken_to[i];
    rel_path += std::string(1, separator);
  }
  if(diff_ind <= broken_to.size() - 1) {
    rel_path += broken_to.back();
  }
  return rel_path;
}

std::string absolute_path(std::string absolute_from, std::string relative_to, char separator)
{
  std::vector<std::string> broken_from = break_path(absolute_from, std::string(1, separator));
  std::vector<std::string> broken_to = break_path(relative_to, std::string(1, separator));

  for(auto to_seg : broken_to) {
    if(to_seg == ".") {

    } else if(to_seg == "..") {
      broken_from.pop_back();
    } else {
      broken_from.push_back(to_seg);
    }
  }
  return constitute_path(broken_from, std::string(1, separator));
}

std::vector<std::string> break_path(std::string path, std::string separator, size_t start_pos)
{
  std::vector<std::string> broken_path;
  size_t next = 0;
  size_t last = start_pos;
  while((next = path.find(separator, last)) != std::string::npos) {
    size_t len = next-last;
    if(len != 0) {
      broken_path.push_back(path.substr(last, len));
    }
    last = next+1;
  }
  if(last != path.size()) {
    broken_path.push_back(path.substr(last));
  }
  return broken_path;
}
std::string constitute_path(std::vector<std::string> broken_path, std::string separator)
{
  if(broken_path.empty())
    return "";

  std::string path_str;
  
  for(size_t i = 0; i < broken_path.size(); ++i) {
    path_str += separator;
    path_str += broken_path[i];
  }
  return path_str;
}

std::string remove_filepath(std::string path)
{
  size_t separator_pos = path.find(':');
  if(separator_pos == std::string::npos) {
    return "";
  }  
  return path.substr(separator_pos);
}
std::string get_filepath(std::string path)
{
  size_t separator_pos = path.find(':');
  if(separator_pos == std::string::npos)
    return path;

  return path.substr(0, separator_pos);
}
std::string get_filename(std::string path)
{
  size_t colon_pos = path.find(':');
  size_t last_slash_pos = path.find_last_of('/');

  if(colon_pos == std::string::npos && last_slash_pos != std::string::npos) {
    return path.substr(last_slash_pos+1, path.size() - last_slash_pos-1);
  } else if(colon_pos != std::string::npos && last_slash_pos != std::string::npos) {
    return path.substr(last_slash_pos+1, colon_pos - last_slash_pos - 1);
  } else if(colon_pos != std::string::npos && last_slash_pos == std::string::npos) {
    return path.substr(0, colon_pos);
  } else { // if(colon_pos == std::string::npos && last_slash_pos == std::string::npos) {
    return path;
  }
}
std::pair<std::string, std::string> get_folderpath_and_bloopath(std::string locaname)
{
  size_t separator_pos = locaname.find_last_of('/');
  if(separator_pos == std::string::npos)
    return { "", locaname };
  return { locaname.substr(0, separator_pos), locaname.substr(separator_pos+1) };
}

std::string relative_locaname(std::string src, std::string dst)
{
  std::string file_1 = get_filepath(src);
  std::string file_2 = get_filepath(dst);

  if(file_1 == file_2) {
    return relative_path(remove_filepath(src), remove_filepath(dst), ':');
  }
  return relative_path(file_1, file_2) + remove_filepath(dst);
}
std::string absolute_locaname(std::string absolute_from, std::string relative_to)
{
  if(relative_to.empty())
    return absolute_from;

  if(relative_to[0] == ':') {
    return get_filepath(absolute_from) + absolute_path(remove_filepath(absolute_from), relative_to, ':');
  } else {
    return absolute_path(get_filepath(absolute_from), get_filepath(relative_to)) + remove_filepath(relative_to);
  }
}

std::string get_suffix(std::string locaname)
{
  if(locaname.empty())
    return "";
  return break_path(locaname, ":").back();
}

std::string handleName(std::string entityName, int handle_num)
{
  if(handle_num == -1) {
    return entityName;
  }
  return entityName + "#" + std::to_string(handle_num);
}

std::pair<std::string, int> entityName_and_handleNum(std::string compoundName)
{
  size_t markerpos = compoundName.find('#');
  if(markerpos == std::string::npos) {
    return { compoundName, -1 };
  }

  try {
    return { compoundName.substr(0, markerpos), std::stoi(compoundName.substr(markerpos+1)) };
  } catch(std::exception& e) {
    std::cerr<<"Warning, fixing incorrect handle id "<<compoundName<<std::endl;
    return { compoundName.substr(0, markerpos), -1 };
  }
}

} // !Core