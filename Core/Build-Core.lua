project "Core"
   kind "StaticLib"
   language "C++"
   cppdialect "C++20"
   targetdir "build/bin/%{cfg.buildcfg}"
   staticruntime "off"

   files { "Src/**.h", "Src/**.cpp" }

   includedirs
   {
      "Src",
      "../Vendor/include/"
   }

   links { "yaml-cpp" }

   targetdir ("../build/bin/" .. OutputDir .. "/%{prj.name}")
   objdir ("../build/bin/Intermediates/" .. OutputDir .. "/%{prj.name}")

   filter "system:windows"
       systemversion "latest"
       defines { }

   filter "configurations:Debug"
       defines { "DEBUG" }
       runtime "Debug"
       symbols "On"

   filter "configurations:Release"
       defines { "RELEASE" }
       runtime "Release"
       optimize "On"
       symbols "On"

   filter "configurations:Dist"
       defines { "DIST" }
       runtime "Release"
       optimize "On"
       symbols "Off"