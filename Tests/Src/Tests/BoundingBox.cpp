
#include "Core/Curve/Line.hpp"
#include <Core/Maths/BoundingBox.hpp>

#include <catch2/catch.hpp>

TEST_CASE("Overlap2d", "[boundingbox]")
{
  {
    auto boxA = Core::dBox::make_dimensions(glm::dvec2(59.065, 8.104), glm::dvec2(63.821, 180.267));
    auto boxB = Core::dBox::make_dimensions(glm::dvec2(25.005, 53.56), glm::dvec2(144.836, 83.649));
    auto boxC = Core::dBox::make_dimensions({68.76, 65.899}, {54.126, 62.116});
    REQUIRE(boxA.overlaps(boxB));
    REQUIRE(!boxA.contains(boxB));
    REQUIRE(!boxC.contains(boxB));
    REQUIRE(boxB.contains(boxC));
  }
  {
    auto boxA = Core::dBox::make_dimensions({25.005, 53.56}, {144.836, 40232});
    auto boxB = Core::dBox::make_dimensions({29.18, -8.596}, {59.617, 77.822});

    REQUIRE(boxA.overlaps(boxB));
    REQUIRE(boxB.overlaps(boxA));
    REQUIRE(!boxA.contains(boxB));
    REQUIRE(!boxB.contains(boxA));
  }
}
TEST_CASE("Contains2d", "[boundingbox]")
{
  {
    auto boxA = Core::dBox::make_dimensions({25.005, 53.56}, {144.836, 40232});
    auto boxB = Core::dBox::make_dimensions({29.18, -8.596}, {59.617, 77.822});

    REQUIRE(boxA.contains({124.426, 68.744}));
    REQUIRE(!boxB.contains({124.426, 68.744}));

    REQUIRE(!boxA.contains({98.523, 41.067}));
    REQUIRE(!boxB.contains({98.523, 41.067}));
    
    REQUIRE(!boxA.contains({71.059, 42.451}));
    REQUIRE(boxB.contains({71.059, 42.451}));

    REQUIRE(boxA.contains({50.074, 56.536}));
    REQUIRE(boxB.contains({50.074, 56.536}));
  }
}
TEST_CASE("Cross2d", "[boundingbox]")
{
  {
    auto box = Core::dBox::make_dimensions({31.604, 105.381}, {144.836, 40.232});
    auto ln = Core::Crv::_2d::Line({29.521, 152.029}, {18.73, 92.352});
    
    REQUIRE(!box.intersects(ln));
  }
  {
    auto box = Core::dBox::make_dimensions({8.507, 106.820}, {144.836, 40.232});
    auto ln = Core::Crv::_2d::Line({29.521, 152.029}, {18.73, 92.352});
    
    REQUIRE(box.intersects(ln));
  }
  {
    auto box = Core::dBox::make_dimensions({8.507, 106.820}, {144.836, 40.232});
    auto ln = Core::Crv::_2d::Line({94.238, 119.916}, {18.73, 92.352});
    
    REQUIRE(box.intersects(ln));
  }
  {
    auto box = Core::dBox::make_dimensions({85.203, 106.82}, {68.140, 96.626});
    auto lnA = Core::Crv::_2d::Line({25.935, 16.389}, {150.724, 211.675});
    auto lnB = Core::Crv::_2d::Line({25.935, 16.389}, {118.192, 83.841});
    
    REQUIRE(box.intersects(lnA));
    REQUIRE(!box.intersects(lnB));
  }
}
