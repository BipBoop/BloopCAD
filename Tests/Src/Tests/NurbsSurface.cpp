
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include <Core/Surface/Nurbs.hpp>
#include <Core/Precision.hpp>
#include <Core/Maths/Utils.hpp>
#include <Core/Origin/Origin_3d.hpp>
#include <Core/Curve/Curve_2d.hpp>

#include <set>
#include <iostream>

#include <catch2/catch.hpp>

using namespace Core;
using namespace Core::Srf;
using namespace Core::Crv;

TEST_CASE("NurbsSurface", "[Nurbs]")
{
  {
    auto flat = NurbsSurface::make_flat_rectangle({ glm::dvec3(0.0, 0.0, 0.0), glm::dvec3(1.0, 0.0, 0.0),
                                                    glm::dvec3(1.0, 1.0, 0.0), glm::dvec3(0.0, 1.0, 0.0)});

    REQUIRE(epsilon_equal(flat.at(0.2, 0.3), glm::dvec3(0.2, 0.3, 0.0), Prc::kNurbs));
    REQUIRE(epsilon_equal(flat.at(0.6, 0.0), glm::dvec3(0.6, 0.0, 0.0), Prc::kNurbs));
  }
  {
    auto flat = NurbsSurface::make_flat_rectangle({ glm::dvec3(0.5, 0.0, 0.0), glm::dvec3(1.5, 0.0, 0.0),
                                                    glm::dvec3(1.5, 1.0, 0.0), glm::dvec3(0.5, 1.0, 0.0)});

    REQUIRE(epsilon_equal(flat.at(0.2, 0.3), glm::dvec3(0.7, 0.3, 0.0), Prc::kNurbs));
    REQUIRE(epsilon_equal(flat.at(0.6, 0.0), glm::dvec3(1.1, 0.0, 0.0), Prc::kNurbs));

    auto tan = flat.as_flat();
    std::cout<<"tan: "<<glm::to_string(tan.x_)<<" ; "<<glm::to_string(tan.y_)<<"\n";
    REQUIRE(epsilon_equal(tan.x_, glm::dvec3(1.0, 0.0, 0.0), Prc::kNurbs));
    REQUIRE(epsilon_equal(tan.y_, glm::dvec3(0.0, 1.0, 0.0), Prc::kNurbs));
  }
  {
    auto flat = NurbsSurface::make_flat_rectangle({ glm::dvec3(0.0, 0.0, 0.0), glm::dvec3(1.0, 0.0, 0.0),
                                                    glm::dvec3(1.0, 1.0, 0.0), glm::dvec3(0.0, 1.0, 0.0)});
    NurbsCurve<2> nurbs_circle = _2d::to_nurbs(_2d::Circle{ .center={0.5, 0.5}, .radius=0.4 });
    NurbsCurve<3> nurbs_circle_embed = flat.embed_curve(nurbs_circle);

    size_t i = 0;
    size_t n_samples = 100;
    for(; i < n_samples; ++i) {
      double t = static_cast<double>(i) / static_cast<double>(n_samples);
      auto circ_at = nurbs_circle.at(t);
      auto emb_at = glm::dvec2(nurbs_circle_embed.at(t));
      if(!epsilon_equal(circ_at, emb_at, Prc::kNurbs)) {
        break;
      }
    }
    REQUIRE(i == n_samples);

    auto cylinder = Srf::NurbsSurface::make_sweep(
                            nurbs_circle_embed,
                            Crv::_3d::to_nurbs(Crv::_3d::Line(glm::dvec3(0.0), glm::dvec3(0.0, 0.0, 1.0))));
    for(i = 0; i < n_samples; ++i) {
      double t = static_cast<double>(i) / static_cast<double>(n_samples);
      auto circ_at = nurbs_circle.at(t);
      auto surf_at = glm::dvec2(cylinder.at(t, 0.3));
      if(!epsilon_equal(circ_at, surf_at, Prc::kNurbs)) {
        break;
      }
    }
    REQUIRE(i == n_samples);

    auto norm = cylinder.normal(0.0, 0.0);
    REQUIRE(epsilon_equal(norm, glm::dvec3(1.0, 0.0, 0.0), Prc::kNurbs));

    norm = cylinder.normal(0.25, 0.0);
    REQUIRE(epsilon_equal(norm, glm::dvec3(0.0, 1.0, 0.0), Prc::kNurbs));
  }
}
