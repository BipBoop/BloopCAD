
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include <Core/Id.hpp>

#include <set>

#include <catch2/catch.hpp>

TEST_CASE("Id", "[base]")
{
  Core::Id line {.wpid = {.kind=0, .index=0}, .entId = {.kind=1, .index=0, .version=0, .handle_num=-1}};
  Core::Id han0 {.wpid = {.kind=0, .index=0}, .entId = {.kind=1, .index=0, .version=0, .handle_num=0}};
  Core::Id han1 {.wpid = {.kind=0, .index=0}, .entId = {.kind=1, .index=0, .version=0, .handle_num=1}};

  std::set<Core::Id> idset { han1 };

  REQUIRE(idset.find(line) == idset.end());
  REQUIRE(idset.find(han0) == idset.end());
  REQUIRE(idset.find(han1) != idset.end());
}
