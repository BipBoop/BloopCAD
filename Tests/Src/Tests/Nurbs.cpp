
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "Core/Maths/NurbsUtils.hpp"
#include "Core/Maths/Utils.hpp"
#include "glm/fwd.hpp"
#include <Core/Curve/Nurbs.hpp>
#include <Core/Precision.hpp>
#include <Core/Curve/Circle.hpp>
#include <Core/Curve/Line.hpp>
#include <Core/Curve/DiscreteCurve.hpp>
#include <Core/Curve/Curve_2d.hpp>

#include <catch2/catch.hpp>

#include <iostream>

using namespace Core;
using namespace NurbsUtils;
using namespace Core::Crv::_2d;
using std::numbers::pi;

TEST_CASE("Knot vector", "[nurbs]")
{
  KnotVec knots { 0.0, 0.0, 0.0, pi/2.0, pi/2.0, pi, pi, 3.0*pi/2.0, 3.0*pi/2.0, 2*pi, 2*pi, 2*pi };

  int span = knot_span(2, knots, 0.1);
  REQUIRE(span == 2);

  // between 3pi/2 and 2pi
  span = knot_span(2, knots, 6.0);
  REQUIRE(span == 8);
  
  span = knot_span(2, knots, 2*pi);
  REQUIRE(span == 8);

  // Knots that are already present give the span on the
  // right of the already present knot
  span = knot_span(2, knots, pi);
  REQUIRE(span == 6);
  span = knot_span(2, knots, pi/2.0);
  REQUIRE(span == 4);

  int mult = knot_multiplicity(2, knots, 0.0);
  REQUIRE(mult == 3);

  mult = knot_multiplicity(2, knots, pi/2.0);
  REQUIRE(mult == 2);

  mult = knot_multiplicity(2, knots, 0.5);
  REQUIRE(mult == 0);

  int degree = knot_vec_degree(knots);
  REQUIRE(degree == 2);

  int n_controls = n_controls_for_knots(2, knots.size());
  REQUIRE(n_controls == 9);

  knots = normalize({ -5, -3, 1, 2, 3, 4, 5, 6, 7 });
  for(int i = 0; i < knots.size(); ++i) {
    REQUIRE((knots[i] <= 1.0 && knots[i] >= 0.0));
  }
}
TEST_CASE("Circle", "[nurbs]")
{
  Circle circ{ .center=glm::dvec2(0.0, 0.0), .radius=1 };
  Nurbs nurbs_circ = to_nurbs(circ);

  const int n_tries = 100;
  int i = 0;
  for(; i < n_tries; ++i) {
    double t = static_cast<double>(i) / static_cast<double>(n_tries);

    glm::dvec2 pos = nurbs_circ.at(t);
    double dist = glm::distance(pos, circ.center);
    if(!sepsilon_equal(dist, circ.radius, Prc::kNurbs))
      break;
  }
  REQUIRE(i == n_tries);

  REQUIRE(epsilon_equal(nurbs_circ.at(0.0), glm::dvec2(1.0, 0.0), Prc::kNurbs));
}
TEST_CASE("Line", "[nurbs]")
{
  Line line(glm::dvec2(0.0, 0.0), glm::dvec2(0.8, 10));
  auto nurbs_line = Nurbs::from_basecontrols(1, {0, 0, 1, 1}, { line.pt1, line.pt2 });

  const int n_tries = 100;
  int i;
  for(i = 0; i < n_tries; ++i) {
    double t = static_cast<double>(i) / static_cast<double>(n_tries);

    if(!epsilon_equal(nurbs_line.at(t), line.at(t), Prc::kNurbs))
      break;
  }
  REQUIRE(i == n_tries);
}
TEST_CASE("Split", "[nurbs]")
{
  {
    Circle circ{ .center=glm::dvec2(0.0, 0.0), .radius=0.6 };
    Nurbs nurbs_circ = to_nurbs(circ);
  
    std::pair<std::optional<Nurbs>, std::optional<Nurbs>> 
        split_res = nurbs_circ.split(0.5, true);
    REQUIRE((split_res.first));
    REQUIRE((split_res.second));
  
    const int n_tries = 100;
    int i;
    for(i = 0; i < n_tries; ++i) {
      double t = static_cast<double>(i) / static_cast<double>(n_tries);
  
      glm::dvec2 pos_1 = split_res.first->at(t);
      double dist1 = glm::distance(pos_1, circ.center);
  
      if(!sepsilon_equal(dist1, circ.radius, Prc::kNurbs))
        break;
    }
    REQUIRE(i == n_tries);
  
    for(i = 0; i < n_tries; ++i) {
      double t = static_cast<double>(i) / static_cast<double>(n_tries);
  
      glm::dvec2 pos_2 = split_res.second->at(t);
      double dist2 = glm::distance(pos_2, circ.center);
  
      if(!sepsilon_equal(dist2, circ.radius, Prc::kNurbs))
        break;
    }
    REQUIRE(i == n_tries);
  }
  {
    Line ln({143.651, 19.156}, {43.842, 135.74});
    Nurbs nurbs_ln = to_nurbs(ln);
    
    REQUIRE(epsilon_equal(ln.pt1, nurbs_ln.control(0), Prc::kNurbs));
    REQUIRE(epsilon_equal(ln.pt2, nurbs_ln.control(1), Prc::kNurbs));
    
    auto split_norm = nurbs_ln.split(0.25, 0.75, true);
    REQUIRE(split_norm);
    
    REQUIRE(epsilon_equal(nurbs_ln.at(0.25), split_norm->at(0.0), Prc::kNurbs));
    REQUIRE(epsilon_equal(nurbs_ln.at(0.75), split_norm->at(1.0), Prc::kNurbs));
    REQUIRE(sepsilon_equal( Line(nurbs_ln.control(0), nurbs_ln.control(1)).length() / 2.0, 
                            Line(split_norm->control(0), split_norm->control(1)).length(), Prc::kNurbs));
    
    auto split_keep = nurbs_ln.split(0.25, 0.75, false);
    REQUIRE(split_keep);
    
    REQUIRE(epsilon_equal(nurbs_ln.at(0.25), split_keep->at(0.25), Prc::kNurbs));
    REQUIRE(epsilon_equal(nurbs_ln.at(0.75), split_keep->at(0.75), Prc::kNurbs));  
    REQUIRE(sepsilon_equal( Line(nurbs_ln.control(0), nurbs_ln.control(1)).length() / 2.0, 
                            Line(split_keep->control(0), split_keep->control(1)).length(), Prc::kNurbs));
  }
  // {
  //   Circle circ{ .center=glm::dvec2(0.0, 0.0), .radius=0.6 };
  //   Nurbs nurbs_circ = to_nurbs(circ);

  //   auto split_a = nurbs_circ.split(0.0, 0.1, false);
  //   auto split_b = nurbs_circ.split(0.9, 1.0, false);

  //   REQUIRE(false);
  // }
}

TEST_CASE("Intersection", "[nurbs]")
{
  {
    Line l1(glm::dvec2(-0.78, 2.27), glm::dvec2(-3.16,-3.04));
    Line l2(glm::dvec2(1.07, -2.76), glm::dvec2(-3.79,-2.17));

    Crv::DiscreteCurve<2> ld1(to_nurbs(l1));
    Crv::DiscreteCurve<2> ld2(to_nurbs(l2));
    
    REQUIRE(ld1.intersections(ld2).size() == 1);
  }
  {
    Line l1(glm::dvec2(37.15, 11.36), glm::dvec2(34.8, -6.3));
    Line l2(glm::dvec2(34.35, 13.0), glm::dvec2(-3.79, -2.17));

    Crv::DiscreteCurve<2> ld1(to_nurbs(l1));
    Crv::DiscreteCurve<2> ld2(to_nurbs(l2));
    
    REQUIRE(ld1.intersections(ld2).size() == 0);
  }
  {
    Line l1(glm::dvec2(-14.32, -14.32), glm::dvec2(0.6, 0.6));
    Circle c1 { .center=glm::dvec2(1.3, 6.1), .radius=10.0 };

    Crv::DiscreteCurve<2> ld1(to_nurbs(l1));
    Crv::DiscreteCurve<2> ld2(to_nurbs(c1));
    
    auto intersections = ld1.intersections(ld2);
    REQUIRE(intersections.size() == 1);
  }
  {
    Line l1(glm::dvec2(-14.32, -14.32), glm::dvec2(13.17, 13.5));
    Circle c1 { .center=glm::dvec2(1.3, 6.1), .radius=10.0 };

    Crv::DiscreteCurve<2> ld1(to_nurbs(l1));
    Crv::DiscreteCurve<2> ld2(to_nurbs(c1));
    
    auto intersections = ld1.intersections(ld2);
    REQUIRE(intersections.size() == 2);
  }
  {
    Line l1(glm::dvec2(34.25, 13.0), glm::dvec2(-10.27, 18.16));
    Circle c1 { .center=glm::dvec2(-2.7, 7.2), .radius=8.7 };

    Crv::DiscreteCurve<2> ld1(to_nurbs(l1));
    Crv::DiscreteCurve<2> ld2(to_nurbs(c1));
    
    REQUIRE(ld1.intersections(ld2).size() == 0);
  }
  {
    Line l1(glm::dvec2(-0.78, 2.27), glm::dvec2(-3.16,-3.04));
    Line l2(glm::dvec2(1.07, -2.76), glm::dvec2(-3.79,-2.17));

    auto l1_nurbs = to_nurbs(l1);
    auto l2_nurbs = to_nurbs(l2);
    
    auto inter_an = l1.intersection(l2);
    REQUIRE(inter_an);
    
    auto inters_num = l1_nurbs.intersections(l2_nurbs);
    
    REQUIRE(inters_num.size() == 1);
    
    REQUIRE(sepsilon_equal(inter_an->first, inters_num[0].first.t, Prc::kIntersection));
  }
  {
    Line l1(glm::dvec2(-10.0, 0.5), glm::dvec2(10.0, 0.5));
    Circle c1 { .center=glm::dvec2(0.0, 0.0), .radius=1.0 };
    
    auto l1_nurbs = to_nurbs(l1);
    auto c1_nurbs = to_nurbs(c1);
    
    auto inters_num = l1_nurbs.intersections(c1_nurbs);
    
    REQUIRE(inters_num.size() == 2);
    
    if(inters_num[0].first.pos.x > inters_num[1].first.pos.x) {
      std::swap(inters_num[0], inters_num[1]);
    }
    std::cout<<"intersections: "<<glm::to_string(inters_num[0].first.pos)<<" :: "<<glm::to_string(inters_num[1].first.pos)<<"\n";

    REQUIRE(epsilon_equal(inters_num[0].first.pos, glm::dvec2(-0.866025, 0.5), Prc::kIntersection));
    REQUIRE(epsilon_equal(inters_num[1].first.pos, glm::dvec2(0.866025, 0.5), Prc::kIntersection));
  }

  // Test for intersections at seams points
  {
    auto ray = Line::from_posDir(glm::dvec2(0.0, 0.0), glm::dvec2(-10.0, 0.0));
    Circle circ { .center={0.0, 0.0}, .radius=5.0 };

    REQUIRE(to_nurbs(circ).intersections(ray).size() == 1);
  }
  {
    auto ray = Line::from_posDir(glm::dvec2(0.0, 0.0), glm::dvec2(10.0, 0.0));
    Circle circ { .center={0.0, 0.0}, .radius=5.0 };

    REQUIRE(to_nurbs(circ).intersections(ray).size() == 1);
  }
}
// TEST_CASE("Derivatives", "[nurbs]")
// {
//   auto cheat_derivs = NurbsUtils::cheat_basis_fun_deriv(2, 2, 2, { 0, 0, 0, 0.25, 0.25, 0.5, 0.5, 0.75, 0.75, 1, 1, 1}, 0.22222222222222221);

//   std::cout<<"Cheat: \n";
//   for(size_t i = 0; i < cheat_derivs.size(); ++i) {
//     for(size_t j = 0; j < cheat_derivs[i].size(); ++j) {
//       std::cout<<cheat_derivs[i][j]<<",  ";
//     }
//     std::cout<<"\n";
//   }

//     auto derivs = NurbsUtils::basis_fun_deriv(2, 2, 0.22222222222222221, { 0, 0, 0, 0.25, 0.25, 0.5, 0.5, 0.75, 0.75, 1, 1, 1}, 2);

//   std::cout<<"Attempt: \n";
//   for(size_t i = 0; i < derivs.rows(); ++i) {
//     for(size_t j = 0; j < derivs.cols(); ++j) {
//       std::cout<<derivs(i, j)<<",  ";
//     }
//     std::cout<<"\n";
//   }
//   REQUIRE(false);

// }
TEST_CASE("ClosestPoint", "nurbs")
{
  Circle c1 { .center=glm::dvec2(0.0, 0.0), .radius=2.0 };
  auto c1_nurbs = to_nurbs(c1);
  
  auto [closest, dist2] = c1_nurbs.closestPoint(glm::dvec2(0.0, 6.0));
  std::cout<<"Closest: "<<glm::to_string(closest)<<"\n";
  REQUIRE(epsilon_equal(closest, glm::dvec2(0.0, 2.0), Prc::kNurbs));
}
