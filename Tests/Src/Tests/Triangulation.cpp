
#include <Core/Surface/Surface.hpp>
#include <Core/Workpiece/Handler.hpp>

#include <catch2/catch.hpp>

using namespace Core;

TEST_CASE("Triangulate an axis-aligned square on a plane") 
{
  // Srf::ParamFlat flat { .origin=glm::dvec3(0.0), .x_=glm::dvec3(1.0, 0.0, 0.0), .y_=glm::dvec3(0.0, 1.0, 0.0) };
  // Srf::Surface surf(flat);

  // // REQUIRE(surf.disc.nodes.size() == 1);
  // REQUIRE(true);
}
TEST_CASE("Are nodes close to the edge arbitrarly small")
{
  // Wp::Handler handler;

  // std::optional<WorkpieceId> wpid = handler.load("onetoobig");

  // REQUIRE(wpid);

  // std::optional<Sk::Sketch> sk = handler.get_wp<Sk::Sketch>(Core::WorkpieceId{.kind=Core::Wp::Kind::Sketch, .index=0});
  
  // REQUIRE(sk);
  
  // REQUIRE(sk->shapeBasis.shapes.size() == 1);

  // Crv::_2d::ShapeWithHoles triangle = sk->shapeBasis.shapeWithHoles(0);
  // // Crv::_2d::DiscShapeWithHoles disc_triangle = triangle.to_disc();

  // Srf::Surface surf(Core::Sk::Sketch::surface(), triangle);

  // double max_area = 0.0;
  // std::vector<Core::Srf::Node> leaves = surf.disc.leaves_nodes();
  // for(auto leaf : leaves) {
  //   if(leaf.area > max_area) {
  //     max_area = leaf.area;
  //   }
  // }
  // std::cout<<"Max area="<<max_area<<"\n";
  // for(auto leaf : leaves) {
    // if(leaf.area == max_area) {
      // std::cout<<"Leaf at max area!\n\n"<<leaf;
    // }
  // }
  // REQUIRE(false);
}
TEST_CASE("Is a point inside a triangle formed by lines")
{
  // Wp::Handler handler;

  // std::optional<WorkpieceId> wpid = handler.load("points_in_triangle");

  // REQUIRE(wpid);

  // std::optional<Sk::Sketch> sk = handler.get_wp<Sk::Sketch>(*wpid);
  
  // REQUIRE(sk);
  
  // REQUIRE(sk->shapeBasis.shapes.size() == 1);

  // Crv::_2d::ShapeWithHoles triangle = sk->shapeBasis.shapeWithHoles(0);
  // Crv::_2d::DiscShapeWithHoles disc_triangle = triangle.to_disc();

  // auto pout0 = sk->get<Sk::Point>("pout0");
  // auto pout1 = sk->get<Sk::Point>("pout1");
  // auto pout2 = sk->get<Sk::Point>("pout2");
  // auto pout3 = sk->get<Sk::Point>("pout3");
  // auto pout4 = sk->get<Sk::Point>("pout4");
  // auto pout5 = sk->get<Sk::Point>("pout5");
  // auto pout6 = sk->get<Sk::Point>("pout6");
  // auto pout7 = sk->get<Sk::Point>("pout7");

  // auto pin0 = sk->get<Sk::Point>("pin0");
  // auto pin1 = sk->get<Sk::Point>("pin1");
  // auto pin2 = sk->get<Sk::Point>("pin2");
  // auto pin3 = sk->get<Sk::Point>("pin3");

  // REQUIRE(pout0);
  // REQUIRE(pout1);
  // REQUIRE(pout2);
  // REQUIRE(pout3);
  // REQUIRE(pout4);
  // REQUIRE(pout5);
  // REQUIRE(pout6);
  // REQUIRE(pout7);

  // REQUIRE(!disc_triangle.contains(*pout0));
  // REQUIRE(!disc_triangle.contains(*pout1));
  // REQUIRE(!disc_triangle.contains(*pout2));
  // REQUIRE(!disc_triangle.contains(*pout3));
  // REQUIRE(!disc_triangle.contains(*pout4));
  // REQUIRE(!disc_triangle.contains(*pout5));
  // REQUIRE(!disc_triangle.contains(*pout6));
  // REQUIRE(!disc_triangle.contains(*pout7));

  // REQUIRE(pin0);
  // REQUIRE(pin1);
  // REQUIRE(pin2);
  // REQUIRE(pin3);

  // REQUIRE(disc_triangle.contains(*pin0));
  // REQUIRE(disc_triangle.contains(*pin1));
  // REQUIRE(disc_triangle.contains(*pin2));
  // REQUIRE(disc_triangle.contains(*pin3));
}