
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include <Core/Solver/Substitution.hpp>
#include <Core/Maths/Utils.hpp>
#include <Core/Precision.hpp>

#include <catch2/catch.hpp>

using namespace Core;
using namespace Core::Slv;

TEST_CASE("SimpleSubstitutions", "[solver]")
{
  {
    std::vector<double> params { 52.0, 34.8, 15.2 };

    Subst::Map map;

    // Cannot add an offset between a parameter and itself
    REQUIRE(!map.add_relation(Subst::Relation { .first=0, .second=0, .offset=2.0 }));


    REQUIRE(map.add_relation(Subst::Relation { .first=0, .second=1, .offset=2.0 }));
    
    REQUIRE(map.add_relation(Subst::Relation { .first=2, .offset=-4.0, .is_assignement=true}));

    REQUIRE(map.blobs.size() == 2);

    map.solve(params);

    REQUIRE(sepsilon_equal(params[0], params[1] - 2.0, Prc::kSolver));
    REQUIRE(sepsilon_equal(params[2], -4.0, Prc::kSolver));
  }
}
