
#include <Core/Curve/ClosedShape/MinShapeBasis.hpp>
#include <Core/Curve/ClosedShape/PlanarMinCycleBasis.hpp>

#include <catch2/catch.hpp>

using namespace Core;
using namespace Core::Crv::_2d;
using namespace Core::Crv::_2d::Pmcb;


// TEST_CASE("pcmb", "[minshapebasis]")
// {
// 	Graph_t out_graph(2);
//   boost::add_edge(0, 1, out_graph);
//   boost::add_edge(1, 0, out_graph);

//   // Graph position will be set twice, not sure how to set them only once
//   out_graph[0] = glm::dvec2(0.5, 3.0);
//   out_graph[1] = glm::dvec2(6.0, 9.2);

//   auto pcmb = planarMincCycleBasis(out_graph);

//   REQUIRE(pcmb.first.size() == 1);

// }

TEST_CASE("Circle shape", "[minshapebasis]")
{
  Circle circ { .center={0.0, 0.0}, .radius=5.0 };
  Shape shape({to_nurbs(circ)});

  REQUIRE(shape.point_inside(glm::dvec2(0.0)));
  
}
TEST_CASE("Circle shape basis", "[minshapebasis]")
{
  {
    Circle circ { .center={0.0, 0.0}, .radius=5.0 };

    MinShapeBasis basis({circ});

    REQUIRE(basis.shapes.size() == 1);

    auto points = basis.shapes[0].rough_discretization(20);
    
    std::cout<<"xy = [\n";
    for(size_t i = 0; i < points.size(); ++i) {
      std::cout<<points[i].x<<", "<<points[i].y<<";";
    }
    std::cout<<"]\n";

    REQUIRE(basis.shapes[0].point_inside(glm::dvec2(0.0)));
  }
  {
    Circle circ { .center={0.0, 0.0}, .radius=5.0 };
    Line line({-5.0, 4.0}, {5.0, 4.0});

    MinShapeBasis basis({circ, line});

    REQUIRE(basis.shapes.size() == 2); 
  }
}