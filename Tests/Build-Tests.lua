
project "Test"
  kind "ConsoleApp"
  language "C++"
  cppdialect "C++20"
  targetdir "build/bin/%{cfg.buildcfg}"
  staticruntime "off"

  files { "Src/**.h", "Src/**.cpp", "Src/**.h", "Src/**.c" }

  includedirs
  {
    "Src/",

	  -- Include Core
    "../Core/Src",
    "../Vendor/include/",
    "gtkmm-4.0"--,
    -- "/usr/include/"
  }

  -- linkoptions '-v'
  buildoptions {"`pkg-config --cflags gtkmm-4.0`"}
  links { "Core", "gtkmm-4.0", "gtk-4", "glibmm-2.68", "gobject-2.0", "sigc-3.0", "giomm-2.68", "epoxy", "freetype", "yaml-cpp", "cairomm-1.16", "glib-2.0", "gio-2.0" }
  
  targetdir ("../build/bin/" .. OutputDir .. "/%{prj.name}")
  objdir ("../build/bin/Intermediates/" .. OutputDir .. "/%{prj.name}")

  filter "system:windows"
    systemversion "latest"
    defines { "WINDOWS" }

  filter "configurations:Debug"
    defines { "DEBUG" }
    runtime "Debug"
    symbols "On"

  filter "configurations:Release"
    defines { "RELEASE" }
    runtime "Release"
    optimize "On"
    symbols "On"

  filter "configurations:Dist"
    defines { "DIST" }
    runtime "Release"
    optimize "On"
    symbols "Off"