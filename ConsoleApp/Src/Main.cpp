
#include <yaml-cpp/yaml.h>

#include <glm/glm.hpp>

#include <Core/Workpiece/Handler.hpp>

#include <iostream>
#include <fstream>


int main()
{

  // YAML::Node node = YAML::LoadFile("testline.bpart");
  
  // std::cout<<"Node: "<<node<<"\n";

  // line ln = node["firstline"].as<line>();
  // ln.a.first += glm::vec3(0.4f, 0.2f, 0.2f);

  // // line ln{.a={glm::vec3(0.4f, 0.2f, 0.2f), "yo"}, .b={glm::vec3(0.8, -0.4f, 0.2), "popp"}};

  // YAML::Node outNode;
  // outNode["firstline"] = ln;
  // std::ofstream outfile("testline.bpart");
  // outfile<<outNode<<"\n";

  // YAML::Node node = YAML::Load("start: [1, 3, 0]");

  // YAML::Node line;
  // line["name"] = "linename";
  // line["p0"] = glm::vec3(0.5, 0.3, 0.0);
  // line["p1"] = glm::vec3(0.7, 0.8, 0.9);

  // YAML::Node nullline;

  // YAML::Node part;
  // part["kind"] = "part";
  // part["children"].push_back("sketch1");
  // part["children"].push_back("sketch2");
  // part["children"].push_back("sketchSkel");

  // YAML::Node sketch1;
  // sketch1["kind"] = "sketch";
  // sketch1["lines"].push_back(line);
  // sketch1["lines"].push_back(nullline);

  // YAML::Node sketch2;
  // sketch2["kind"] = "sketch";
  // sketch2["points"].push_back("point1");
  // sketch2["points"].push_back("point2");

  // YAML::Node sketchSkel;
  // sketchSkel["external"] = "skelPart.bcad/";


  // YAML::Node root;
  // root["workpieces"].push_back(part);
  // root["workpieces"].push_back(sketch1);
  // root["workpieces"].push_back(sketch2);
  // root["workpieces"].push_back(sketchSkel);

  Core::Wp::Handler handler;

  std::cout<<"boop\n";
}