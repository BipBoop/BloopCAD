
## Levenberg-Marquardt algorithm

* https://en.wikipedia.org/wiki/Levenberg%E2%80%93Marquardt_algorithm
* http://www.eng.auburn.edu/~wilambm/pap/2011/K10149_C012.pdf
* https://www.researchgate.net/profile/Luc_Knockaert/publication/224516793_Vector_Fitting_vs_Levenberg-Marquardt_Some_experiments/links/0912f5096cfd9c3a20000000/Vector-Fitting-vs-Levenberg-Marquardt-Some-experiments.pdf
* https://www.sciencedirect.com/science/article/pii/S0377042715002666
* https://www.sciencedirect.com/science/article/abs/pii/S0096300313002798
* https://www.sciencedirect.com/science/article/pii/S0377042704001256?via%3Dihub
* https://www.sciencedirect.com/science/article/pii/S0377042714005433
* https://www.youtube.com/watch?v=P-Qa0qR2rCU
* http://users.ics.forth.gr/~lourakis/levmar/levmar.pdf

* http://www2.imm.dtu.dk/pubdb/edoc/imm3215.pdf


## Graph decomposition (constraints solving)

* https://www.cs.purdue.edu/cgvlab/papers/cmh/decompJSC01a.pdf
* https://upcommons.upc.edu/bitstream/handle/2117/86309/R07-31.pdf;sequence=1
* https://arxiv.org/pdf/1405.6131.pdf
* https://en.wikipedia.org/wiki/Tarjan%27s_strongly_connected_components_algorithm
* https://www.cs.cmu.edu/~15451-f18/lectures/lec19-DFS-strong-components.pdf
* https://www.geeksforgeeks.org/maximum-bipartite-matching/?ref=lbp
* https://en.wikipedia.org/wiki/Hopcroft%E2%80%93Karp_algorithm

* https://hal.archives-ouvertes.fr/hal-00848677/PDF/1993_Reduction-of-Constraints-Systems_Compugraphics.pdf
* https://arxiv.org/pdf/2202.13795.pdf

## Graph theory (general)

* https://webdocs.cs.ualberta.ca/~stewart/c672/Definitions.pdf

## Bezier curve constraints

* https://www.cspaul.com/publications/Ahn.2014.JCAM.pdf

## 2d intersections

* https://www.geometrictools.com/Documentation/IntersectionLine2Circle2.pdf

## Find loops in set of 2d geometries

* http://web.ist.utl.pt/alfredo.ferreira/publications/12EPCG-PolygonDetection.pdf
* https://www.geometrictools.com/Documentation/MinimalCycleBasis.pdf
* https://link.springer.com/content/pdf/10.1007/s00453-007-9064-z.pdf

## Triangulation of surfaces

* https://people.eecs.berkeley.edu/~jrs/papers/meshbook/chapter2.pdf
* https://cs.uwaterloo.ca/~alubiw/CS763/CS763-Lecture12-final.pdf
* http://www.geom.uiuc.edu/~samuelp/del_project.html
* https://www.newcastle.edu.au/__data/assets/pdf_file/0019/22519/23_A-fast-algortithm-for-generating-constrained-Delaunay-triangulations.pdf