
# Constraint Solving

## Concepts

* Variable
	> A number that usualy has a geometric meaning (e.g. a radius)
	* It is moved around as a pointer (double*) because it ensures that if it is modified the geometric primitive underneath is updated and because pointers can be used as keys in sets/maps
* Constraint
	> An object that represents a relationship between one or (typicaly) more variables
	* The relationship can be linear or non linear
	* Typicaly, the constraint holds primitive geometries as references
* SubstRelation
	> An object that represents a simple, linear relationship
	* Const 	; A variable has a constant value (e.g fixed constraint such as base axis) 
	* Equality 	; Two variables have the same value (e.g. coincidence constraint between two points)
	* Offset 	; Two variables have the same value plus or minus a constant (e.g. vertical distance between two points)
* SubstBlob
	> An object that represents a cluster of variables interconnected with SubstRelation[s]
	* It's handle is a "root" number used to update it's variables
	* It outputs at most one handle to the non-linear optimizer (0 if a constant is in the SubstBlob, no more than 1 constant can be in the SubstBlob)
* ConstrCluster
	> An object that represents a "pipeline" of ConstrSubCluster[s] which need to be solved in order (the second depends on the result of the first, etc.)
* ConstrSubCluster
	> An object that represents a set of constraints and it's variable and that can solve itself
	* It contains an "optimization vector" which is handed to the non-linear optimizer and is used to update the variables
## Solve pipeline

*Not fully implmented yet*

### When the system is not initialized (e.g. a new constraint was added)

1. Make a vector of all SubstRelation[s] of the constraint set
2. Create a graph with those relationships and the variables they link
3. Find connected components of the graph
4. Create SubstBlob[s] from the connected component and make a map that maps each variable in a SubstBlob to it's SubstBlob's handle

=========================

5. Create a new graph with all relationships from the constraints and uses variables (from geometries or from SubstBlob[s]' handles) as vertices
6. Find connected components of the graph
7. Create ConstrCluster[s] with the connected components 
8. ConstrSubCluster[s] are generated and ordered with strongly connected components analysis which makes it possible to flag their constrainedness (wellconstrained, underconstrained, overconstrained)
9. Flag constraints so that the UI can reflect the constrainedness of the geometries

=========================

10. Sove the system

### Whan the system is initialized (e.g. a mouse drag moved a point)

1. Ask each SubstBlob[s] to chose a value for their exported value based on the greatest difference between the current "root" and the each of the "would be roots" (variables of the blob)

[For all the clusters (they are independant; could be multithreaded)]

2. Solve each ConstrSubCluster in order and return if one fails
	1. Check if there is a difference between the optimization vector and the variables
		* If not; skip solve
	2. Assign variables to optimization vector
	3. Make a copy of optimization vector
	4. Loop until solved/failure
		1. Non-linear optimizer step
		2. Assign optimizer vector' values to variables
	5. Output
		* If failure: use copy to return to previous state of variables
		* If success: yay
