
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "File_util.hpp"

#include <iostream>
#include <fstream>
#include <sstream>

namespace IO {

std::string readFromFile(std::string const& filePath)
{
	std::ifstream stream(filePath);
	if(!stream) {
		std::cerr<<"Could not open file \""<<filePath<<"\""<<std::endl;
		return "";
	}

	std::string str(std::istreambuf_iterator<char>(stream), {}); // Some stl wizardry

	stream.close();
	return str;
}

void writeToFile(std::string const& filePath, std::string const& fileContent)
{
	std::ofstream stream(filePath);
	if(!stream) {
		std::cerr<<"Could not open file \""<<filePath<<"\""<<std::endl;
		return;
	}

	stream<<fileContent;
	stream.close();
}

std::string getStdoutFromCommand(std::string cmd)
{
	std::string data;
	FILE * stream;
	const int max_buffer = 256;
	char buffer[max_buffer];
	cmd.append(" 2>&1");

	stream = popen(cmd.c_str(), "r");
	if (stream) {
	while (!feof(stream))
		if (fgets(buffer, max_buffer, stream) != NULL) data.append(buffer);
			pclose(stream);
	}
	return data;
}

} // !IO