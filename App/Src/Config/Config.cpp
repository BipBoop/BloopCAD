
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "Config.hpp"

#include <Core/Sketch/Kinds.hpp>
#include <Core/Part/Kinds.hpp>
#include <Core/Workpiece/Kinds.hpp>

namespace App {

double Config::get_hover_dist(Core::Id id) const
{
  switch(id.wpid.kind) {
  case Core::Wp::Kind::Sketch:
    return get_hover_dist_sk(id.entId);
  case Core::Wp::Kind::Part:
    return get_hover_dist_prt(id.entId);
  }
  return 0.0;
}

double Config::get_hover_dist_sk(Core::SibId id) const
{
  if(Core::Sk::is_point(id)) {
    return kSkConfig.pointhoverdist_px;
  } else if(Core::Sk::is_curve(id.kind)) {
    return kSkConfig.linehoverdist_px;
  }
  return 0.0;
}

double Config::get_hover_dist_prt(Core::SibId id) const
{
  switch(id.kind) {
  case Core::Prt::Kind::Origin_point:
    return kPrtConfig.pointhoverdist_px;
  }
  return 0.0;
}

} // !App