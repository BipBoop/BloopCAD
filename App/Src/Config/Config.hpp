
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef BLOOP_CONFIG_HPP_
#define BLOOP_CONFIG_HPP_

#include <Core/Id.hpp>

#include <glm/glm.hpp>

namespace App {

struct NavConfig {
  double kOrbit_radians_per_pixel = 0.002;
  double kZoom_scroll_mult = 0.005;
  double kZoom_drag_mult = 0.05;
};

struct SkConfig {
  glm::vec3 kUnderConstrained_color { 0.631, 0.075, 0.51 };
  glm::vec3 kWellConstrained_color { 0.365, 0.102, 0.58 };
  glm::vec4 kPatchColor { 0.0235, 0.529, 0.698, 0.4 };

  double pointsize_px { 5 };
  double linewidth_px { 5 };

  double pointhoverdist_px { 8 };
  double linehoverdist_px { 6 };
};
struct PrtConfig {
  double pointsize_px { 5 };
  double pointhoverdist_px { 15 };

  glm::vec4 kShell_color { 0.282, 0.847, 0.663, 1.0 };

  glm::vec4 kCurve_color { 1.0, 0.733, 0.333, 1.0 };
  glm::vec4 kPoint_color { 0.965, 0.322, 0.471, 1.0 };

  glm::vec4 kSurface_color0 { 1.0, 0.35, 0.9, 0.7 };
  glm::vec4 kSurface_color1 { 0.35, 1.0, 0.78, 0.7 };
};
struct AssConfig {

};

struct Config {
  glm::vec3 kHover_color { 1.00, 0.906, 0.549 }; //{ 1.0, 0.0, 0.0 };//{ 0.753, 0.361, 0.584 };
  glm::vec3 kSelection_color { 1.00, 0.816, 0.011 }; //{ 0.643, 0.22, 0.459 };

  glm::vec3 kIncSel_color { 0.345, 0.878, 0.745 }; // { 0.663, 0.557, 0.855 };
  glm::vec3 kExcSel_color { 0.91, 0.357, 0.773 }; //{ 0.525, 0.737, 0.831 };

  glm::vec3 kBackground_color { 0.1, 0.1, 0.1 };

  glm::ivec2 spriteAnnotDim { 20, 20 };

  unsigned int kWorkpieceTransition_time { 800 };

  SkConfig kSkConfig;
  PrtConfig kPrtConfig;
  AssConfig kAssConfig;
  NavConfig kNavConfig;


  unsigned int kUndoRedo_firstTimer { 600 }, kUndoRedo_timer { 400 };

  double get_hover_dist(Core::Id id) const;
  double get_hover_dist_sk(Core::SibId id) const;
  double get_hover_dist_prt(Core::SibId id) const;

};

} // !App

#endif