
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "FrameBuffer.hpp"

#include <Graphics_utils/GLCall.hpp>

FrameBuffer::FrameBuffer(unsigned int width, unsigned int height)
	: mTexture(width, height, GL_REPEAT, GL_REPEAT, GL_NEAREST, GL_NEAREST, 4) // No need to get fancy with interpolation, it will likely be the exact same size as the screen
{
	int initialFrameBuffer; // Record the context's frame buffer
	glGetIntegerv(GL_DRAW_FRAMEBUFFER_BINDING, &initialFrameBuffer);

	// Creation of frame buffer
	GLCall(glGenFramebuffers(1, &mRendererID));

	// Bind the frame buffer with mTexture
	bind();	
	GLCall(glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, mTexture.id(), 0));

	// Generate and setup render buffer (should the id be kept to dealocate it in the destructor??)
	unsigned int rb = 0;
	GLCall(glGenRenderbuffers(1, &rb));
	GLCall(glBindRenderbuffer(GL_RENDERBUFFER, rb));
	GLCall(glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, width, height));

	// Assign the render buffer to the frame buffer (and therefore the texture)
	GLCall(glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, rb));
	
	// Set the output of the fragment Shaders to our render buffer
	unsigned int draw_bufs[] = {GL_COLOR_ATTACHMENT0};
	GLCall(glDrawBuffers(1, draw_bufs));

	// Error check
	GLCall(unsigned int status = glCheckFramebufferStatus(GL_FRAMEBUFFER));
	if (status != GL_FRAMEBUFFER_COMPLETE) {
		std::cerr<<"Incomplete frame buffer: "<<status<<std::endl;
	}
	
	// Clean up
	GLCall(glBindFramebuffer(GL_FRAMEBUFFER, initialFrameBuffer));
}
FrameBuffer::~FrameBuffer()
{
	// The destructor of mTexture should clean everything up
}

void FrameBuffer::bind() const
{
#ifdef BLOOP_TEST_SELCTIONCOLORS_ON_MAINBUFFER
#warning "Selection colors will be drawn on main buffer" // Do nothing to keeep it on the main frame buffer (screen)
#else
	GLCall(glBindFramebuffer(GL_FRAMEBUFFER, mRendererID));
#endif
}
void FrameBuffer::unbind(unsigned int defaultID) const
{
	GLCall(glBindFramebuffer(GL_FRAMEBUFFER, defaultID)); // Bind to an id (defaults is 0)
}