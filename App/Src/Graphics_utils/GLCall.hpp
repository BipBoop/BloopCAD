
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef GLCALL_HPP_
#define GLCALL_HPP_

#include <epoxy/gl.h>

#include <iostream>
#include <string>

/*
	@function GLClearErrors makes sure openGL has no error accumulated, since it can only give the newest one
*/
void GLClearErrors();
/*
	@function GLLogCall logs a warning if an internal error is detected in openGL
*/
bool GLLogCall(std::string const& call, std::string const& file, std::string const& function, unsigned int line);

/*
	@macro GLCall calls an openGL function and logs a warning if a new error appears
*/
#define GLCall(x) \
	GLClearErrors(); \
	x; \
	GLLogCall(#x, __FILE__, __FUNCTION__, __LINE__)

#define GLCall_msg(x, msg) \
	GLClearErrors(); \
	x; \
	if(!GLLogCall(#x, __FILE__, __FUNCTION__, __LINE__)) \
		std::cout<<msg<<std::endl;

#endif