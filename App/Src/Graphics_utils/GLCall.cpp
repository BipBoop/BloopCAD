
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "GLCall.hpp"

#include <iostream>

// #define LOG_EVERY_GLCALL // Uncomment to have every openGL call logged, error or no error

void GLClearErrors()
{
  while(glGetError() != GL_NO_ERROR);
}

bool GLLogCall(std::string const& call, std::string const& file, std::string const& function, unsigned int line)
{
	if(GLenum error = glGetError()) {
		std::cerr<<"OpenGL error "<<file<<",  "<<function<<",  "<<line<<" while calling "<<call<<": "<<std::to_string(error)<<std::endl;
		return false;
	} else {
#ifdef LOG_EVERY_GLCALL
		std::cerr<<"Called "<<file<<",  "<<function<<",  "<<line<<",  "<<call<<std::endl;
#endif
	}
	return true;
}