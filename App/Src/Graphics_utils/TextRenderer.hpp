
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

// https://learnopengl.com/In-Practice/Text-Rendering

#ifndef TEXTRENDERER_HPP_
#define TEXTRENDERER_HPP_

#include "Texture.hpp"

#include <glm/glm.hpp>
#include <Eigen/Eigen>

#include <string>
#include <map>
#include <memory>

class TextRenderer {
public:
	struct GlyphParam {
		glm::ivec2 topleft, bottomright;
		glm::ivec2 offset;
		int advance;        // x advance when rendering
	};

private:
	using CharMat = Eigen::Matrix<unsigned char, -1, -1>;

	static const int n_glyphs = 255;
	std::string mFontName;
	std::map<char, GlyphParam> mCharMap;
	CharMat mAtlas;
	GlyphParam mGlyphsParams[n_glyphs];
	size_t mSize;
	int mMinY, mMaxY;

public:
	TextRenderer();

	void set_font(std::string const& name);

	std::shared_ptr<Texture> render_textLine(std::string const& text);

	static bool get_font_path(std::string const& name, std::string& path);
};

#endif