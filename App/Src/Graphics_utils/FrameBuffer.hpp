
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef FRAMEBUFFER_HPP_
#define FRAMEBUFFER_HPP_

#include "Texture.hpp"

/*
    @class FrameBuffer describes an abstraction of a GL_FRAMEBUFFER specifically to use it as a selection buffer

    @warning Make sure that all function are used within the same openGL context
*/
class FrameBuffer {
private:
    unsigned int mRendererID { 0 }; // The generated openGL id    
    Texture mTexture; // The texture it represents... maybe it should just derive from texture instead
    
public:
    /*
        @function FrameBuffer creates a FrameBuffer object

        @param width  The width in pixels of the FrameBuffer
        @param height The height in pixels of the FrameBuffer
    */
    FrameBuffer(unsigned int width, unsigned int height);
    /*
        @function ~FrameBuffer destroys the FrameBuffer and cleans up openGL handles
    */
    ~FrameBuffer();

    /*
        @function bind activates the FrameBuffer within openGL
    */
    void bind() const;
    /*
        @function unbind desactivate the FrameBuffer within openGL

        @param defaultID [defaults to 0] A GL_FRAMEBUFFER id to bind instead
    */
    void unbind(unsigned int defaultID = 0) const;

    /*
        @function id

        @return The openGL id of the FrameBuffer
    */
    unsigned int id() const { return mRendererID; }
};

#endif