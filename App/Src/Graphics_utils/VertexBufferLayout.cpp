
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "VertexBufferLayout.hpp"
#include "GLCall.hpp"

VertexBufferLayout::VertexBufferLayout():
	mStride(0)
{

}

unsigned int VertexBufferProprety::typeSize() const
{
	return typeSize(type);
}
unsigned int VertexBufferProprety::typeSize(unsigned int tp)
{
	switch(tp) {
		case GL_FLOAT:        	return sizeof(float);
		case GL_UNSIGNED_INT: 	return sizeof(unsigned int);
		case GL_INT: 			return sizeof(int);
		case GL_UNSIGNED_BYTE:	return sizeof(unsigned char);
		case GL_DOUBLE:			return sizeof(double);
	};

	std::cerr<<"Unknown type." + std::to_string(tp)<<std::endl;
	return 0;
}

void VertexBufferLayout::add_proprety(unsigned int count, Type type)
{
	bool asInteger = false;
	unsigned int glType;

	switch(type) {
	case Type::UChar: 	glType = GL_UNSIGNED_BYTE; break;
	case Type::Int:		glType = GL_INT; asInteger = true; break;
	case Type::UInt:	glType = GL_UNSIGNED_INT; asInteger = true; break;
	case Type::Float: 	glType = GL_FLOAT; break;
	case Type::Double: 	glType = GL_DOUBLE; break;
	default: return; // should not be reached
	};

	mPropreties.push_back( { count, glType, GL_FALSE, asInteger }); // Normalized is false, not an option
	mStride += VertexBufferProprety::typeSize(glType) * count; // The stride expands with each addition of proprety
}

VertexBufferLayout VertexBufferLayout::simple_3f_layout()
{
	VertexBufferLayout layout_;
	layout_.add_proprety(3, VertexBufferLayout::Type::Float);
	return layout_;
}