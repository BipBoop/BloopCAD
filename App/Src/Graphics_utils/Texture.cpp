
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "Texture.hpp"

#include <Graphics_utils/GLCall.hpp>

Texture::Texture()
	: mRendererID(0)
	, mWidth(0)
	, mHeight(0)
	, mN_channels(0)
{
	GLCall(glGenTextures(1, &mRendererID));
}
Texture::Texture(int width, int height, unsigned int channels,
			unsigned int wrap_s, unsigned int wrap_t, 
			unsigned int min_filter, unsigned int mag_filter, unsigned char* data)
	: mWidth(width)
	, mHeight(height)
	, mN_channels(channels)
{
	int internalFormat = GL_RGBA8;
	mFormat = GL_RGBA;
	mRendererID = make(width, height, channels, wrap_s, wrap_t, min_filter, mag_filter, data);
}
Texture::~Texture()
{
	GLCall(glDeleteTextures(1, &mRendererID));
}
std::shared_ptr<Texture> Texture::from_pixbuf(std::shared_ptr<Gdk::Pixbuf> pixbuf)
{
	int height = pixbuf->get_height();
	int width = pixbuf->get_width();
	int n_channels = pixbuf->get_n_channels();
	int rowstride = pixbuf->get_rowstride();
	unsigned char* first_pix = pixbuf->get_pixels();

	int bytes_per_pixel = (n_channels); // Only support 8 bits channels because.
	std::vector<unsigned char> data(height * width * bytes_per_pixel);

	// Copy the pixbuff into the simplest format
	// just each pixel one after the other, no spacers 1 byte per channel, each channel 
	// one after the other
	for(int y = 0; y < height; ++y) {
		for(int x = 0; x < width; ++x) {
			unsigned char* pixel = first_pix + y * rowstride + x * n_channels;
			for(int c = 0; c < n_channels; ++c) {
				data[y * (width*n_channels) + x * n_channels + c] = pixel[c];
			}
		}
	}
	
	return std::make_shared<Texture>
				(width, height, n_channels, GL_CLAMP_TO_BORDER, GL_CLAMP_TO_BORDER, 
				GL_LINEAR, GL_LINEAR, &data[0]);
}
unsigned int Texture::make(int width, int height, unsigned int channels,
		unsigned int wrap_s, unsigned int wrap_t, 
		unsigned int min_filter, unsigned int mag_filter, unsigned char* data)
{
	unsigned int out;
	// Create the texture
	GLCall(glGenTextures(1, &out));

	int internalFormat = GL_RGBA8;
	int format = GL_RGBA;

	switch(channels) {
	case 1: 
		internalFormat = format = GL_RED;
		break;
	case 2: 
		internalFormat = format = GL_RG;
		break;
	case 3:
		internalFormat = format = GL_RGB;
		break;
	case 4:
		internalFormat = GL_RGBA8;
		format = GL_RGBA;
		break;
	}

	// Parametrize the texture
	GLCall(glActiveTexture(GL_TEXTURE0)); // All Texture slots ids are contiguous 
	GLCall(glBindTexture(GL_TEXTURE_2D, out));

	GLCall(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrap_s));
	GLCall(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrap_t));
	GLCall(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, min_filter));
	GLCall(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, mag_filter));
	GLCall(glPixelStorei(GL_UNPACK_ALIGNMENT, 1));

	GLCall(glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, width, height, 0, format, GL_UNSIGNED_BYTE, data));
	return out;
}

void Texture::bind(unsigned int slot/* = 0*/) const
{	
	GLCall(glActiveTexture(GL_TEXTURE0+slot)); // All Texture slots ids are contiguous 
	GLCall(glBindTexture(GL_TEXTURE_2D, mRendererID));
}
void Texture::unbind() const
{
#ifndef RELEASE_MODE // legit in this context?
	GLCall(glBindTexture(GL_TEXTURE_2D, 0));
#endif
}

void Texture::set(int x, int y, int w, int h, unsigned int* data)
{
	GLCall(glTexSubImage2D(GL_TEXTURE_2D, 0, x, y, w, h, mFormat, GL_UNSIGNED_BYTE, data));
}
void Texture::set_safe(int x, int y, int w, int h, unsigned int* data)
{
	bind();

	set(x, y, w, h, data);

	unbind();
}
void Texture::set_safe(unsigned int* data)
{
	set_safe(0, 0, width(), height(), data);
}