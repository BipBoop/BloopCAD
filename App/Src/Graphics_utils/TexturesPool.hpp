
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef TEXTURESPOOL_HPP_
#define TEXTURESPOOL_HPP_

#include "Texture.hpp"

#include <string>
#include <memory>
#include <map>

class TexturesPool {
private:
	std::map<std::string, std::shared_ptr<Texture>> mTextures;
	
public:
	TexturesPool(TexturesPool const&) = delete; // As it is a singleton, the copy constructor is deleted

	static TexturesPool& get_instance();

	void add(std::string const& name, std::shared_ptr<Texture> texture);
	std::shared_ptr<Texture> get(std::string const& name);
private:
	TexturesPool();
	~TexturesPool();
};

#endif