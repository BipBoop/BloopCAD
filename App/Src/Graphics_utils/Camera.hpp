
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef CAMERA_HPP_
#define CAMERA_HPP_

#include "CameraSnap.hpp"

#include <Core/Maths/Ray_3d.hpp>
#include <Core/Maths/BoundingBox.hpp>

#include <glm/glm.hpp>
#include <glm/gtx/norm.hpp>
#include <glm/gtx/string_cast.hpp>

#include <iostream>
#include <memory>

struct CameraParams {
	enum class ProjectionType { Perspective, Orthographic };

	double FOV;
	ProjectionType projType;

	double nearPlane;
	double farPlane;

	CameraParams() = default;
	CameraParams(double FOV_, ProjectionType projType_ = ProjectionType::Perspective, double nearPlane_ = 0.001, double farPlane_ = 100.0)
		: FOV(FOV_)
		, projType(projType_)
		, nearPlane(nearPlane_)
		, farPlane(farPlane_)
	{

	}
};

struct Camera {
	CameraSnap state;
	CameraParams params;
	glm::dvec2 viewport;

	glm::dmat4 mvp;
	glm::dmat4 modTransf;

	glm::dmat4 vp_inv;

	Camera() = default;
	Camera(CameraSnap state_, CameraParams params_, glm::dvec2 viewport_, glm::dmat4 modelTransf = glm::dmat4(1));
	Camera(Camera const& cam, glm::dmat4 modelTransf);
	Camera(Camera const& cam, CameraSnap state_);

	Camera move_by(CameraSnap const& delta) const;
	Camera move_to(CameraSnap const& dest) const;
	Camera set_viewport(glm::dvec2 const& viewport_) const;
	Core::dBox viewport_box() const;

	glm::dmat4 view() const;
	glm::dmat4 projection() const;
	glm::dmat4 model() const;

	glm::dmat4 ortho_2d() const;

	double aspectRatio() const;
	glm::dvec3 front() const;

	glm::dvec2 world_to_screen(glm::dvec3 worldpos) const;
	glm::dvec2 world_to_screen_ndc(glm::dvec3 worldpos) const;

	Core::Ray cast_ray(glm::dvec2 screenPos) const;
	Core::Ray cast_ray_ndc(glm::dvec2 screenPos_ndc) const;
};

std::ostream& operator<<(std::ostream& os, Camera const& cam);

#endif
