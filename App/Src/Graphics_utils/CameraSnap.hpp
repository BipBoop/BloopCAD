
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef BLOOP_GRAPHICSUTILS_CAMERASNAP_HPP_
#define BLOOP_GRAPHICSUTILS_CAMERASNAP_HPP_

#include <glm/glm.hpp>

struct CameraSnap {
	glm::dvec3 pos { 1.0 };
	glm::dvec3 target { 1.0 };

	double zoom { 1.0 };

	glm::dvec3 up { 0.0 };
	glm::dvec3 right { 0.0 };

	CameraSnap() = default;
	CameraSnap(glm::dvec3 pos_, glm::dvec3 target_, glm::dvec3 right_, glm::dvec3 up_, double zoom_);

	static CameraSnap from_posTarget(glm::dvec3 pos_, glm::dvec3 target_, glm::dvec3 worldUp_, double zoom);
	static CameraSnap from_rightup(glm::dvec3 right_, glm::dvec3 up_, glm::dvec3 target_, double target_dist, double zoom_);
  static CameraSnap fromSpherical(glm::dvec3 spherical_pos_, glm::dvec3 target_, glm::dvec3 worldUp_, double zoom_);
	
	glm::dvec3 pos_spherical() const;
	glm::dvec3 front() const;
	bool operator==(CameraSnap const& other) const;
};

CameraSnap operator-(CameraSnap const& left, CameraSnap const& right);
CameraSnap operator+(CameraSnap const& left, CameraSnap const& right);
CameraSnap operator*(CameraSnap const& left, double right);
CameraSnap operator/(CameraSnap const& left, double right);


#endif