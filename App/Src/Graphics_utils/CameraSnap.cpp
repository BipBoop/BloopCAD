
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "CameraSnap.hpp"

CameraSnap::CameraSnap(glm::dvec3 pos_, glm::dvec3 target_, glm::dvec3 right_, glm::dvec3 up_, double zoom_)
  : pos(pos_)
  , target(target_)
  , up(up_)
  , right(right_)
  , zoom(zoom_)
{

}
CameraSnap CameraSnap::from_posTarget(glm::dvec3 pos_, glm::dvec3 target_, glm::dvec3 worldUp_, double zoom)
{
  auto front_ = glm::normalize(target_ - pos_);
  auto right_ = glm::normalize(glm::cross(front_, worldUp_));
  auto up_ = glm::normalize(glm::cross(right_, front_));
  return CameraSnap(pos_, target_, right_, up_, zoom);
}
CameraSnap CameraSnap::from_rightup(glm::dvec3 right_, glm::dvec3 up_, glm::dvec3 target_, double target_dist, double zoom_)
{
  up_ = glm::normalize(up_);
  right_ = glm::normalize(right_);
  auto front = glm::cross(up_, right_);
  
  return CameraSnap(target_-front*target_dist, target_, right_, up_, zoom_);
}
CameraSnap CameraSnap::fromSpherical(glm::dvec3 spherical_pos_, glm::dvec3 target_, glm::dvec3 worldUp_, double zoom_)
{
  glm::dvec3 cart_pos = glm::dvec3(	std::sin(spherical_pos_.z) * std::sin(spherical_pos_.y),
                    std::cos(spherical_pos_.z),
                    std::sin(spherical_pos_.z) * std::cos(spherical_pos_.y)) * spherical_pos_.r;
  
  return from_posTarget(cart_pos, target_, worldUp_, zoom_);
}

glm::dvec3 CameraSnap::pos_spherical() const
{
  glm::dvec3 rel_pos = pos - target;
  return glm::dvec3(	glm::length(rel_pos),
            std::atan2(rel_pos.x, rel_pos.z), 
            std::atan2(std::sqrt(rel_pos.z*rel_pos.z + rel_pos.x*rel_pos.x), rel_pos.y));
}

glm::dvec3 CameraSnap::front() const 
{
  return target - pos; 
}
bool CameraSnap::operator==(CameraSnap const& other) const
{
  return 	pos == other.pos &&
          target == other.target && 
          right == other.right && 
          up == other.up &&
          zoom == other.zoom;
}

CameraSnap operator-(CameraSnap const& lhs, CameraSnap const& rhs)
{
  return CameraSnap(lhs.pos-rhs.pos, lhs.target-rhs.target, lhs.right-rhs.right, lhs.up-rhs.up, lhs.zoom-rhs.zoom);
}
CameraSnap operator+(CameraSnap const& lhs, CameraSnap const& rhs)
{
  return CameraSnap(lhs.pos+rhs.pos, lhs.target+rhs.target, lhs.right+rhs.right, lhs.up+rhs.up, lhs.zoom+rhs.zoom);
}
CameraSnap operator*(CameraSnap const& lhs, double rhs)
{
  return CameraSnap(lhs.pos*rhs, lhs.target*rhs, lhs.right*rhs, lhs.up*rhs, lhs.zoom*rhs);
}
CameraSnap operator/(CameraSnap const& lhs, double rhs)
{
  return CameraSnap(lhs.pos/rhs, lhs.target/rhs, lhs.right/rhs, lhs.up/rhs, lhs.zoom/rhs);
}
