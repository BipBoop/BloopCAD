
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "Camera.hpp"

#include <Core/Maths/Utils.hpp>

#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/ext/quaternion_trigonometric.hpp>
#include <glm/gtx/vector_angle.hpp>

Camera::Camera(CameraSnap state_, CameraParams params_, glm::dvec2 viewport_, glm::dmat4 modelTransform_)
  : state(state_)
  , params(params_)
  , viewport(viewport_)
  , modTransf(modelTransform_)
{
  glm::dmat4 proj_ = projection();
  glm::dmat4 view_ = view();
  mvp = proj_ * view_ * modTransf;
  vp_inv = glm::inverse(proj_ * view_);
}
Camera::Camera(Camera const& cam, glm::dmat4 modelTransf)
    : Camera(cam.state, cam.params, cam.viewport, modelTransf)
{

}
Camera::Camera(Camera const& cam, CameraSnap state_)
    : Camera(state_, cam.params, cam.viewport, cam.modTransf)
{

}

Camera Camera::move_by(CameraSnap const& delta) const
{
  return Camera(*this, state + delta);
}
Camera Camera::move_to(CameraSnap const& dest) const
{
  return Camera(*this, dest);
}
Camera Camera::set_viewport(glm::dvec2 const& viewport_) const
{
  return Camera(state, params, viewport_, modTransf);
}

glm::dmat4 Camera::view() const
{
  return glm::lookAt(state.pos, state.target, state.up);
}
glm::dmat4 Camera::projection() const
{
  return glm::perspective(params.FOV, aspectRatio(), params.nearPlane, params.farPlane);
}
glm::dmat4 Camera::model() const
{
  return modTransf;
}

glm::dmat4 Camera::ortho_2d() const
{
  return glm::ortho(0.0, viewport.x, viewport.y, 0.0, 0.0, 1.0);
}
double Camera::aspectRatio() const
{
  return viewport.x / viewport.y;
}
glm::dvec3 Camera::front() const
{
  return state.target - state.pos;
}
glm::dvec2 Camera::world_to_screen(glm::dvec3 worldpos) const
{
  glm::dvec2 onscreen_ndc = world_to_screen_ndc(worldpos);

  glm::dvec2 onscreen(	Core::map(onscreen_ndc.x, -1.0, 1.0, 0.0, viewport.x),
                        Core::map(onscreen_ndc.y, -1.0, 1.0, 0.0, viewport.y));
  return onscreen;
}
glm::dvec2 Camera::world_to_screen_ndc(glm::dvec3 worldpos) const
{
    glm::dvec4 onscreen_ndc = mvp * glm::dvec4(worldpos, 1.0);
    return glm::dvec2(onscreen_ndc.x, onscreen_ndc.y) / onscreen_ndc.w;
}

Core::Ray Camera::cast_ray(glm::dvec2 screenPos) const
{
  return cast_ray_ndc(glm::dvec2(	Core::map(screenPos.x, 0.0, viewport.x, -1.0, 1.0), 
                                  Core::map(screenPos.y, viewport.y, 0.0, -1.0, 1.0)));
}
Core::Ray Camera::cast_ray_ndc(glm::dvec2 screenPos_ndc) const
{
  glm::dvec4 worldPos_far = vp_inv * glm::dvec4(screenPos_ndc, 1.0, 1.0);
  glm::dvec4 worldPos_near = vp_inv * glm::dvec4(screenPos_ndc, -1.0, 1.0);

  worldPos_far /= worldPos_far.w;
  worldPos_near /= worldPos_near.w;

  return Core::Ray{state.pos, glm::normalize(glm::dvec3(worldPos_far - worldPos_near)) };
}
Core::dBox Camera::viewport_box() const
{
  return Core::dBox(glm::dvec2(0.0, 0.0), glm::dvec2(viewport.x, viewport.y));
}

std::ostream& operator<<(std::ostream& os, Camera const& cam)
{
  os	<<"[pos    ]\t"<<glm::to_string(cam.state.pos)
      <<"\n[target ]\t"<<glm::to_string(cam.state.target)
      <<"\n[right ]\t"<<glm::to_string(cam.state.right)
      <<"\n[up ]\t"<<glm::to_string(cam.state.up)
      <<"\n[zoom   ]\t"<<cam.state.zoom;
  return os;
}
