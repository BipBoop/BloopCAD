
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef VERTEXBUFFERLAYOUT_HPP_
#define VERTEXBUFFERLAYOUT_HPP_

#include <vector>

/*
	@struct VertexBufferProprety describes a single proprety of a vertex (position, color, etc..)
*/
struct VertexBufferProprety {
	unsigned int count; // Number of element in the proprety
	unsigned int type; // Type of the elements (float, int, etc..)
	unsigned int normalized; // Whether ot not the proprety is normalized
	bool asInteger;
	/*
		@function typeSize

		@return : The size in bytes of the type of the proprety
	*/
	unsigned int typeSize() const;
	static unsigned int typeSize(unsigned int tp);
};

/*
	@class VertexBufferLayout describes the way propreties are laid out in a vertex buffer
*/
class VertexBufferLayout {
public:
	enum class Type { UChar, Int, UInt, Float, Double };

private:
	std::vector<VertexBufferProprety> mPropreties; // All the propreties of the layout
	unsigned int mStride; // The total size in bytes of a vertex with all the propreties

public:
	/*
		@function VertexBufferLayout creates an empty layout
	*/
	VertexBufferLayout();

	/*
		@function add_proprety adds a proprety to the layout

		@param count 	The number of element in the proprety
		@param type 	The type of the elements (int, unsigned int, unsigned char, float)
	*/
	void add_proprety(unsigned int count, Type type);

	/*
		@function propreties 

		@return	 All the propreties of the layout
	*/
	std::vector<VertexBufferProprety> const& propreties() const { return mPropreties; };
	/*
		@function stride 

		@return The total size of a single vertex with all the propreties
	*/
	unsigned int stride() const { return mStride; }

	static VertexBufferLayout simple_3f_layout();
};

#endif