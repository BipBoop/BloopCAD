
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "IndexBuffer.hpp"

#include "GLCall.hpp"

IndexBuffer::IndexBuffer(unsigned int const* data, unsigned int count_)
	: mCount(count_)
{
	// Generate the buffer
	GLCall(glGenBuffers(1, &mRendererID));

	// Setup the data, maybe the draw type will be an option in the future
	bind();
	GLCall(glBufferData(GL_ELEMENT_ARRAY_BUFFER, count_ * sizeof(unsigned int), data, GL_DYNAMIC_DRAW)); 
}

IndexBuffer::~IndexBuffer()
{
	GLCall(glDeleteBuffers(1, &mRendererID)); // Delete the buffer
}

void IndexBuffer::bind() const
{
	GLCall(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mRendererID));
}
void IndexBuffer::unbind() const
{
#ifndef RELEASE_MODE // In release there is no unecesary binds, but it's cleaner for debugging
	GLCall(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0));
#endif
}

void IndexBuffer::set(unsigned int const* data, unsigned int count_)
{
	mCount = count_;
	GLCall(glBufferData(GL_ELEMENT_ARRAY_BUFFER, count_ * sizeof(unsigned int), data, GL_DYNAMIC_DRAW)); 
}