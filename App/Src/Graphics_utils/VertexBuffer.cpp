
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "VertexBuffer.hpp"

#include "GLCall.hpp"

VertexBuffer::VertexBuffer(void const* data, unsigned int size_)
	: mSize(size_)
{
	// Create the buffer
	GLCall(glGenBuffers(1, &mRendererID));

	// Fill the buffer
	bind();
	GLCall(glBufferData(GL_ARRAY_BUFFER, size_, data, GL_STATIC_DRAW)); // The draw mode might become an option eventually
}
VertexBuffer::VertexBuffer(unsigned int size_)
	: mSize(size_)
{
	// Create the buffer
	GLCall(glGenBuffers(1, &mRendererID));
}

VertexBuffer::~VertexBuffer()
{
	// Destroy the buffer
	GLCall(glDeleteBuffers(1, &mRendererID));
}

void VertexBuffer::bind() const
{
	GLCall(glBindBuffer(GL_ARRAY_BUFFER, mRendererID));
}
void VertexBuffer::unbind() const
{
#ifndef RELEASE_MODE // No unecesary bind in release, but practical for debugging
	GLCall(glBindBuffer(GL_ARRAY_BUFFER, 0));
#endif
}

void VertexBuffer::set(void const* data, unsigned int size_)
{
	// if(size_ == mSize) {
		GLCall(glBufferData(GL_ARRAY_BUFFER, size_, data, GL_DYNAMIC_DRAW)); // The draw mode might become an option eventually
	// } else {
	// 	LOG_WARNING("Cannot dynamically reallocate buffers yet...");
	// }
}

