
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef VERTEXBUFFER_HPP_
#define VERTEXBUFFER_HPP_

#include <vector>

/*
	@class VertexBuffer describes an abstraction of openGL's vertex buffer
	@warning : Make sure that all functions are called whithin the same openGL context
*/
class VertexBuffer {
private:
	unsigned int mRendererID { 0 }; // The openGL handle to the buffer
	unsigned int mSize; // The size in bytes of the buffer
	
public:
	/*
		@function VertexBuffer creates a VertexBuffer object

		@param data : 	The data of the VertexBuffer, it can be of any types
		@param size_ : 	The size in bytes of the buffer
	*/
	VertexBuffer(void const* data, unsigned int size_);
	VertexBuffer(unsigned int size_);
	template<typename T>
	VertexBuffer(std::vector<T> data)
		: VertexBuffer(&data[0], data.size()*sizeof(T))
	{

	}
	/*
		@function ~VertexBuffer destroys a VertexBuffer object and cleanly handles the deallocation in openGL
	*/
	~VertexBuffer();

	/*
		@function bind activates the buffer in openGL so that it can be used
	*/
	void bind() const;
	/*
		@function unbind desactivates the buffer in openGL so that it can't be used anymore
	*/
	void unbind() const;

	/*
		@function size

		@return : The size in bytes of the buffer
	*/
	unsigned int size() const { return mSize; }
	void resize(unsigned int new_size);

	/*
		@function id

		@return : The openGL handle of the buffer
	*/
	unsigned int id() const { return mRendererID; }

	void set(void const* data, unsigned int size_);
};

#endif
