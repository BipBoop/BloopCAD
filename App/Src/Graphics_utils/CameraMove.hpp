
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef BLOOP_APP_CAMERAMOVE_HPP_
#define BLOOP_APP_CAMERAMOVE_HPP_

#include "Camera.hpp"

#include <chrono>
#include <iostream>

struct CameraMove {
  CameraSnap start;
  CameraSnap end;
  std::chrono::time_point<std::chrono::system_clock> startTime;
  unsigned int duration_ms;

  CameraMove(CameraSnap start_, CameraSnap end_, unsigned int duration_ms_)
    : start(start_)
    , end(end_)
    , duration_ms(duration_ms_)
    , startTime(std::chrono::system_clock::now())
  {

  }

  // Returns the current snapshot of the camera
  // with a bool of whether or not the
  // camera move is over
  std::pair<CameraSnap, bool> current()
  {
    unsigned int elaps_ = elapsed();
    if(elaps_ >= duration_ms) {
      return { end, true };
    }
    double t = static_cast<double>(elaps_) / static_cast<double>(duration_ms);
    return { start + ((end-start) * t), false };
  }

  bool over() const 
  {
    return elapsed() >= duration_ms; 
  }
  unsigned int elapsed() const 
  {
    return std::chrono::duration_cast<std::chrono::milliseconds>
            (std::chrono::system_clock::now() - startTime).count();
  }
};

#endif 