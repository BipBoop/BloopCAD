
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef TEXTURE_HPP_
#define TEXTURE_HPP_

#include <Graphics_utils/GLCall.hpp>

#include <gtkmm.h>
#include <memory>

/*
	@class texture describes an abstraction of an openGL texture
*/
class Texture {
protected:
	unsigned int mRendererID { 0 }; // The handle of the openGL texture
	int mWidth, mHeight, mN_channels; // Metrics 
	int mFormat;
	
public:
	/*
		@function texture creates an empty texture object
	*/
	Texture();
	/*
		@function texture creates a parametrized texture object

		@param width 		The width in pixels of the texture
		@param height 		The height in pixels of the texture
		@param wrap_s 		The wrap type on the s (x) axis of the texture
		@param wrap_t 		The wrap type on the t (y) axis of the texture
		@param min_filter 	The minifiying function of the texture
		@param mag_filter 	The magnifiying function of the texture
		@param channels 	The number of color channels of the texture
	*/
	Texture(int width, int height, unsigned int channels = 3,
			unsigned int wrap_s = GL_CLAMP_TO_BORDER, unsigned int wrap_t = GL_CLAMP_TO_BORDER, 
			unsigned int min_filter = GL_LINEAR, unsigned int mag_filter = GL_LINEAR, unsigned char* data = nullptr);

	static std::shared_ptr<Texture> from_pixbuf(std::shared_ptr<Gdk::Pixbuf> pixbuf);

	static unsigned int make(int width, int height, unsigned int channels = 3,
			unsigned int wrap_s = GL_CLAMP_TO_BORDER, unsigned int wrap_t = GL_CLAMP_TO_BORDER, 
			unsigned int min_filter = GL_LINEAR, unsigned int mag_filter = GL_LINEAR, unsigned char* data = nullptr);
	/*
		@function ~texture destroys a texture object and handle deallocation in openGL
	*/
	virtual ~Texture();

	/*
		@function bind activates the texture in openGL at a specific slot

		@param slot [defaults to 0] The texture slot in which the texture will be bound
	*/
	virtual void bind(unsigned int slot = 0) const;
	/*
		@function unbind desactivate the texture in opengl
	*/
	virtual void unbind() const;

	/*
		@function width

		@return The width of the texture in pixels
	*/
	unsigned int width() const { return mWidth; }
	/*
		@function height 

		@return The height of the texture in pixels
	*/
	unsigned int height() const { return mHeight; }

	unsigned int n_channels() const { return mN_channels; }

	void set(int x, int y, int w, int h, unsigned int* data);
	void set_safe(int x, int y, int w, int h, unsigned int* data);
	void set_safe(unsigned int* data);
	/*
		@function id

		@return The openGL handle of the texture
	*/
	unsigned int id() const { return mRendererID; }
};

#endif
