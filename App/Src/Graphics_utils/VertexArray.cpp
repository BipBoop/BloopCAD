
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "GLCall.hpp"
#include "VertexArray.hpp"

VertexArray::VertexArray()
{
	// Create the vertex array
	GLCall(glGenVertexArrays(1, &mRendererID));
}
VertexArray::~VertexArray()
{
	// Delete the vertex array
	GLCall(glDeleteVertexArrays(1, &mRendererID));
}

void VertexArray::add_buffer(VertexBuffer const& vb, VertexBufferLayout const& layout)
{
	vb.bind(); // Bind the vertex buffer to add it to the vertex array

	// Set the desired layout for the buffer within the vertex array
	std::vector<VertexBufferProprety> const& propreties = layout.propreties();
	unsigned int offset = 0;
	for(unsigned int i = 0; i < propreties.size(); ++i) {
		VertexBufferProprety const& proprety = propreties[i]; // Current proprety

		GLCall(glEnableVertexAttribArray(i)); // Enable said proprety
		if(proprety.asInteger) {
				GLCall(glVertexAttribIPointer(i, proprety.count, proprety.type, layout.stride(), (void*)(intptr_t)offset)); // Describe proprety
		} else {
			GLCall(glVertexAttribPointer(i, proprety.count, proprety.type, proprety.normalized, layout.stride(), (void*)(intptr_t)offset)); // Describe proprety
		}
		offset += proprety.count * proprety.typeSize(); // Increase our position in the vertex array's index
	}

	// Clean up
	vb.unbind();
}

void VertexArray::bind() const
{
	GLCall(glBindVertexArray(mRendererID));
}
void VertexArray::unbind() const
{
#ifndef RELEASE_MODE // No unecesary binding in release, but practical for debugging purposes
	GLCall(glBindVertexArray(0));
#endif
}
