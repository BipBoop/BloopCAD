
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef VERTEXARRAY_HPP_
#define VERTEXARRAY_HPP_

#include "VertexBuffer.hpp"
#include "VertexBufferLayout.hpp"

/*
	@class VertexArray describes an abstraction of openGL's vertex arrays
	@warning : Make sure that every function is called whithin the same openGL context
*/
class VertexArray {
private:
	unsigned int mRendererID { 0 }; // The openGL handle to the vertex array
	
public:
	/*
		@function VertexArray creates a VertexArray object
	*/
	VertexArray();
	/*
		@function ~VertexArray destroys a VertexArray object and cleanly handle the internal openGL stuff
	*/
	~VertexArray();

	/*
		@function add buffer adds a set of vertices to the vertex array

		@param vb : 	A VertexBuffer containing vertices data
		@param layout : A VertexBufferLayout describing the data of the VertexBuffer
	*/
	void add_buffer(VertexBuffer const& vb, VertexBufferLayout const& layout);

	/*
		@function bind activates the VertexArray in openGL so that it can be used
	*/
	void bind() const;
	/*
		@function unbind desactivates the VertexArray in openGL so that it can't be used
	*/
	void unbind() const;

	/*
		@function id

		@return : The openGL handle to the vertex array
	*/
	unsigned int id() const { return mRendererID; }
};

#endif
