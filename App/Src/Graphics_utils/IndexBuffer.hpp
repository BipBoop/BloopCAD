
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef INDEXBUFFER_HPP_
#define INDEXBUFFER_HPP_

/*
	@class IndexBuffer describes an abstraction of an openGL indices buffer
	@warning Make sure all the functions are called within the same openGL context
*/
class IndexBuffer {
private:
	unsigned int mRendererID { 0 }; // The internal openGL id
	unsigned int mCount; // The NUMBER of indices contained
	
public:
	/*
		@function IndexBuffer creates an IndexBuffer object

		@param data 	A pointer to the data assigned to the buffer, currently, only unsigned ints are supported
		@param count_ 	The NUMBER of data points
	*/
	IndexBuffer(unsigned int const* data, unsigned int count_);
	/*
		@function ~IndexBuffer destroys the IndexBuffer object and cleans it's openGL handle
	*/
	~IndexBuffer();

	/*
		@function bind activates the IndexBuffer inside openGL
	*/
	void bind() const;
	void unbind() const;
	/*
		@function unbind desactivates the IndexBuffer inside openGL
	*/

	/*
		@function count 

		@return The NUMBER of indices in the buffer
	*/
	unsigned int count() const { return mCount; };
	/*
		@function id

		@return The internal openGL handle to the buffer
	*/
	unsigned int id() const { return mRendererID; }

	void set(unsigned int const* data, unsigned int count_);
};

#endif
