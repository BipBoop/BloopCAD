
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "TextRenderer.hpp"

#include <IO/File_util.hpp>

#include <freetype2/ft2build.h>
#include FT_FREETYPE_H
#include <glm/gtx/string_cast.hpp>

#include <ctime>
#include <iostream>

TextRenderer::TextRenderer()
	: mFontName("")
{

}

void TextRenderer::set_font(std::string const& name)
{
	// Note: all the bit shift by 6 are to convert from 26.6 frac pixel to pixels
	// https://lists.nongnu.org/archive/html/freetype/2005-05/msg00046.html
	// https://gist.github.com/baines/b0f9e4be04ba4e6f56cab82eef5008ff


	mSize = 14; // TODO: Make this a configuration
	if(mFontName == name) {
		return;
	}

	std::string path;
	if(!get_font_path(name, path)) {
		std::cerr<<"Could not find font \""<<name<<"\""<<std::endl;
		return;
	}

	std::cout<<"Font path: "<<path<<"\n";

	mCharMap.clear();

	FT_Library ft;
	FT_Face face;

	if(FT_Init_FreeType(&ft)) {
		std::cerr<<"Could not init freetype"<<std::endl;
		return;
	}

	if(FT_New_Face(ft, path.c_str(), 0, &face)) {
		std::cerr<<"Could not load font \""<<path<<"\""<<std::endl;
		return;
	}

	mFontName = name;

	FT_Set_Pixel_Sizes(face, 0, mSize);  

	// Find dimensions of the texture (it is a square, thus the square root)
	int max_width = (1 + mSize) * std::ceil(std::sqrt(n_glyphs));
	int tex_width = 1;
	while(tex_width < max_width) { tex_width *= 2; } // Ensure that the texture dimension is a power of 2
	int tex_height = tex_width;

	mAtlas.resize(tex_width, tex_height);
	glm::ivec2 currPos(0, 0);
	
	mMinY = std::numeric_limits<int>::max(), mMaxY = std::numeric_limits<int>::min();

	for(int i = 0; i < n_glyphs; ++i) {
		FT_Load_Char(face, i, FT_LOAD_RENDER | FT_LOAD_FORCE_AUTOHINT | FT_LOAD_TARGET_LIGHT);
		FT_Bitmap* bmp = &face->glyph->bitmap;

		if(currPos.x + bmp->width >= tex_width){
			currPos.x = 0;
			currPos.y += (face->size->metrics.height / 64 + 1);
		}

		for(int row = 0; row < bmp->rows; ++row){
			for(int col = 0; col < bmp->width; ++col){
				mAtlas(currPos.x+col, currPos.y+row) = bmp->buffer[row * bmp->pitch + col];
			}
		}

		int y_min = face->glyph->bitmap_top - bmp->rows;
		int y_max = face->glyph->bitmap_top;
		if(y_min < mMinY)
			mMinY = y_min;
		if(y_max > mMaxY)
			mMaxY = y_max;

		mGlyphsParams[i].topleft = currPos;
		mGlyphsParams[i].bottomright = currPos + glm::ivec2(bmp->width, bmp->rows);	
		mGlyphsParams[i].offset = glm::ivec2(face->glyph->bitmap_left, face->glyph->bitmap_top);
		mGlyphsParams[i].advance = face->glyph->advance.x / 64;

		currPos.x += bmp->width + 1;
	}
	
	// mAtlas.conservativeResize(tex_width, currPos.y + 1);

	FT_Done_FreeType(ft);
}

std::shared_ptr<Texture> TextRenderer::render_textLine(std::string const& text)
{
	CharMat mTextureData = CharMat::Zero((mSize+1) * text.size(), std::abs(mMinY) + mMaxY);

	glm::ivec2 pen(0, mMaxY);

	for(char c : text) {
		GlyphParam gp = mGlyphsParams[c];

		for(int i = gp.topleft.y, t_i = 0; i < gp.bottomright.y; ++i, ++t_i) {
			for(int j = gp.topleft.x, t_j = 0; j < gp.bottomright.x; ++j, ++t_j) {
				int x_text = pen.x+gp.offset.x + t_j;
				int y_text = pen.y-gp.offset.y + t_i;

				int val_atlas = mAtlas(j, i);
				mTextureData(x_text, y_text) = val_atlas;
			}
		}
		pen.x += gp.advance;
	}
	mTextureData.conservativeResize(pen.x+1, std::abs(mMinY) + mMaxY);


	// Little loop to print the texture, might be useful for debugging
	// std::cout<<"Texture data size: "<<mTextureData.rows()<<",  "<<mTextureData.cols()<<"\n";

	// for(int i = 0; i < mTextureData.cols(); ++i) {
	// 	for(int j = 0; j < mTextureData.rows(); ++j) {
	// 		int val = mTextureData(j, i);

	// 		if(val < 10) {
	// 			std::cout<<"  "<<val;
	// 		} else if(val < 100) {
	// 			std::cout<<" "<<val;
	// 		} else if(val <= 255){
	// 			std::cout<<val;
	// 		} else {
	// 			std::cout<<"nul";
	// 		}
	// 		std::cout<<" ";
	// 	}
	// 	std::cout<<"\n";
	// }

	return std::make_shared<Texture>(mTextureData.rows(), mTextureData.cols(), 1, 
		GL_CLAMP_TO_BORDER, GL_CLAMP_TO_BORDER,
		GL_LINEAR, GL_LINEAR, mTextureData.data());
}

bool TextRenderer::get_font_path(std::string const& name, std::string& path)
{
	std::string paths = IO::getStdoutFromCommand("fc-list");
	
	size_t start_ind, end_ind;
	if(((end_ind = paths.find(": " + name + ":style=Regular", 0)) != std::string::npos) ||
		((end_ind = paths.find(": " + name, 0)) != std::string::npos)) {

		start_ind = paths.rfind('\n', end_ind) + 1;
		path = paths.substr(start_ind, end_ind-start_ind);
		return true;
	} else {
		return false;
	}
}