#ifndef BLOOP_APP_CONFRANGE_HPP_
#define BLOOP_APP_CONFRANGE_HPP_

#include <limits>

namespace App {
  
class RangeVal {
private:
	enum Ranges { NONE = 0, MIN = 1, MAX = 2 }; // Doesn't look like it but these are powers of two

	double mVal { 0.0 };
	double mMin { std::numeric_limits<double>::min() }, mMax { std::numeric_limits<double>::min() };
	int mRange { NONE };
  bool mClamped { false };

public:
	static RangeVal make_minMax(double val, double min, double max);
	static RangeVal make_min(double val, double min);
	static RangeVal make_max(double val, double max);
	static RangeVal make_unconstrained(double val);

  bool is_clamped() const { return mClamped; }
  
	bool set(double val);
	double val() { return mVal;	}

	double min() { return mMin; }
	void set_min(double min);
	bool has_min();

	double max() { return mMax; }
	void set_max(double max);
	bool has_max();

	operator double() { return mVal; }
	double operator =(double val) { set(val); return mVal; }

private:
	bool clamp();
};

} // !App

#endif