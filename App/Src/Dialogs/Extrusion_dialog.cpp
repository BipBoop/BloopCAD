
#include "Extrusion_dialog.hpp"
#include <iostream>

namespace App {

ExtrusionDialog::ExtrusionDialog()
{
  auto refBuilder = Gtk::Builder::create_from_resource("/design/prt_workspace.ui");

  auto body = refBuilder->get_widget<Gtk::Box>("prt_extrusion_dialog");
  // body->insert_child_at_start(*refBuilder->get_widget<Gtk::Box>("prt_extrusion_dialog"));

  // refBuilder->get_widget<Gtk::Button>("prt_extrusion_dialog_OK")
  //   ->signal_clicked().connect([this](){confirm=true;});
  // refBuilder->get_widget<Gtk::Button>("prt_extrusion_dialog_Cancel")
  //   ->signal_clicked().connect([this](){cancel=true;});


  ok_btn = refBuilder->get_widget<Gtk::Button>("prt_extrusion_ok_btn");
  ok_btn->signal_clicked().connect([this](){ confirm_ = true; });
  refBuilder->get_widget<Gtk::Button>("prt_extrusion_cancel_btn")->signal_clicked().connect([this]() { cancel_ = true; });
  refBuilder->get_widget<Gtk::Button>("prt_extrusion_flip_btn")->signal_clicked().connect([this]() { flipped_ = !flipped_; version_++; });

  input_ = Gtk::Builder::get_widget_derived<NumericInput>(refBuilder, "prt_extrusion_input");
  input_->set_onConfirm([this](){ ok_btn->grab_focus(); std::cout<<"try grab focus\n";});
  input_->set_valChangeCallback([this](double val){ std::cout<<"val="<<val<<"\n"; version_++; });

  detail_text_ = refBuilder->get_widget<Gtk::TextView>("prt_extrusion_detail_text");
  detail_text_->hide();
  auto loop_detail_btn = refBuilder->get_widget<Gtk::Button>("prt_extrusion_show_detail_btn");
  loop_detail_btn->signal_clicked().connect([this](){ if(detail_text_->get_visible()) detail_text_->hide(); else detail_text_->show(); });

  set_child(*body);
  set_title("Extrusion dialog");
}
void ExtrusionDialog::set_loopnames(std::string names)
{
  auto buffer = Gtk::TextBuffer::create();
  buffer->set_text(names);
  detail_text_->set_buffer(buffer);
}

double ExtrusionDialog::value()
{
  return input_->val();
}
std::string ExtrusionDialog::expression()
{
  return input_->str();
}

} // !App