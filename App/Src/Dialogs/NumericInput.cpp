
#include "NumericInput.hpp"

#include <Core/Stringparse/Exp/ExpBuilder.hpp>
#include <Core/Maths/Utils.hpp>

#include <iostream>

namespace App {

NumericInput::NumericInput()
	: mVal(RangeVal::make_unconstrained(0))
	, mIsNumber(true)
{
	init();
}
NumericInput::NumericInput(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refBuilder)
	: Gtk::Entry(cobject)
	, mVal(RangeVal::make_unconstrained(0))
	, mIsNumber(true)
{
	init();
}

void NumericInput::set_min(double min)
{
	mVal.set_min(min);
	update_progress();
}
void NumericInput::set_max(double max)
{
	mVal.set_max(max);
	update_progress();
}

void NumericInput::set_val(double v)
{
	mVal.set(v);
	mExpStr = std::to_string(mVal.val());
	set_text(mExpStr);
	mIsNumber = true;

	update_progress();
	callbacks();
}
void NumericInput::set_val(RangeVal v)
{
	mVal = v;
	mExpStr = std::to_string(val());
	set_text(mExpStr);
	mIsNumber = true;

	update_progress();
	callbacks();
}

void NumericInput::set_valChangeCallback(std::function<void (double)> func)
{
	mOnValChange = func;
}
void NumericInput::callbacks()
{
	if(mOnValChange) {
		(*mOnValChange)(mVal.val());
	}
}

void confirm_callback(GtkEntry* ent, gpointer* this_entry)
{
	reinterpret_cast<NumericInput*>(this_entry)->confirm_entry();
}
void NumericInput::init()
{
	update_textField();
	set_alignment(Gtk::Align::CENTER);
	// signal_insert_text().connect(sigc::ptr_fun(&on_my_insert));
	signal_changed().connect(sigc::mem_fun(*this, &NumericInput::change_text));

	auto focusCont = Gtk::EventControllerFocus::create();
	focusCont->signal_enter().connect(sigc::mem_fun(*this, &NumericInput::start_edit));
	focusCont->signal_leave().connect(sigc::mem_fun(*this, &NumericInput::end_edit));
	add_controller(focusCont);

	// signal_editing_done().connect(sigc::mem_fun(*this, &NumericInput::confirm_entry));
	g_signal_connect(
            gobj(), 
            "activate", 
            G_CALLBACK(confirm_callback), this);

	auto keyCont = Gtk::EventControllerKey::create();
	keyCont->signal_key_pressed().connect(sigc::mem_fun(*this, &NumericInput::catch_esc), false);
	add_controller(keyCont);

	set_can_focus();

	get_style_context()->add_class("automatic");
}
bool NumericInput::compute_val_or_prev()
{
	bool clamped = false;
	try {
		clamped = mVal.set(mExpBuilder.eval(mExpStr));
		mIsNumber = mExpBuilder.isNumber();
		set_tooltip_text("");
		get_style_context()->remove_class("invalid");
    return true;
	} catch(std::invalid_argument const& e) {
		clamped = mVal.set(mPrevVal);
		mIsNumber = false;
		set_tooltip_text(e.what());
		get_style_context()->add_class("invalid");
		std::cerr<<"Parsing error "<<e.what()<<std::endl;
    return false;
	}
}

void NumericInput::update_textField()
{
	if(!compute_val_or_prev() || mVal.is_clamped()) {
		set_text(std::to_string(mVal.val()) + " *");
	} else {
		set_text(std::to_string(mVal.val()));
	}

	callbacks();

	update_progress();
}
void NumericInput::update_progress()
{
	if(mVal.has_min() && mVal.has_max()) {
		set_progress_fraction(Core::map(mVal.val(), mVal.min(), mVal.max(), 0.0, 1.0));
	}
}

void NumericInput::change_text()
{
	std::cout<<"change\n";
	if(mIsEditing)
		mExpStr = get_text();
	
	if(compute_val_or_prev()) {
		callbacks();
	}
}

void NumericInput::confirm_entry()
{
	std::cout<<"Confirm!\n";
	auto parent = get_parent();
	if(parent) {
		parent->grab_focus();
	}
	// end_edit();

	mOnConfirm();
}

void NumericInput::start_edit()
{
	mIsEditing = true;
	mPrevVal = mVal.val();
	set_text(mExpStr);

	if(mSelectOnEdit)
		select_region(0, -1);
}
void NumericInput::end_edit()
{
	mIsEditing = false;

	update_textField();
}
bool NumericInput::catch_esc(guint keyval, guint keycode, Gdk::ModifierType state)
{
	std::cout<<"key time \n";
	if(keyval == GDK_KEY_Escape) {
		mOnCancel();
		return true;
	} 
	
	return false;
}

} // !App