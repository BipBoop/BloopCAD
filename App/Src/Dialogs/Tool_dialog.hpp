
#ifndef BLOOP_APP_TOOL_DIALOG_HPP_
#define BLOOP_APP_TOOL_DIALOG_HPP_

#include <gtkmm.h>

namespace App {

class ToolDialog : public Gtk::Dialog {
public:
  ToolDialog() {}

  // Returns false if the dialog can be closed
  virtual bool is_relevant() const = 0;
};

} // !App

#endif