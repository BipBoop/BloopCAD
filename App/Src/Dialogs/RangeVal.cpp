
#include "RangeVal.hpp"

namespace App {

RangeVal RangeVal::make_minMax(double val, double min, double max)
{
	RangeVal out;
	out.mVal = val;
	out.mMin = min;
	out.mMax = max;
	out.mRange = MIN | MAX;
	
	return out;
}
RangeVal RangeVal::make_min(double val, double min)
{
	RangeVal out;
	out.mVal = val;
	out.mMin = min;
	out.mMax = std::numeric_limits<double>::max();
	out.mRange = MIN;

	return out;
}
RangeVal RangeVal::make_max(double val, double max)
{
	RangeVal out;
	out.mVal = val;
	out.mMin = std::numeric_limits<double>::min();
	out.mMax = max;
	out.mRange = MAX;

	return out;
}
RangeVal RangeVal::make_unconstrained(double val)
{
	RangeVal out;
	out.mVal = val;
	out.mMin = std::numeric_limits<double>::min();
	out.mMax = std::numeric_limits<double>::max();
	out.mRange = NONE;

	return out;
}

bool RangeVal::set(double val)
{
	mVal = val;
	mClamped = clamp();
  return mClamped;
}

void RangeVal::set_min(double min)
{
	mMin = min;
	mRange |= MIN;
}
bool RangeVal::has_min()
{
	return mRange & MIN;
}

void RangeVal::set_max(double max)
{
	mMax = max;
	mRange |= MAX;
}
bool RangeVal::has_max()
{
	return mRange & MAX;
}

bool RangeVal::clamp()
{
	if(mVal > mMax) {
		mVal = mMax;
		return true;
	} else if(mVal < mMin) {
		mVal = mMin;
		return true;
	}
	return false;
}

} // !App