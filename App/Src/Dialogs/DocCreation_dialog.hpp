
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef BLOOP_APP_DOCCREATION_DIALOG_HPP_
#define BLOOP_APP_DOCCREATION_DIALOG_HPP_

#include <Translation.hpp>

#include <Core/Workpiece/Kinds.hpp>

#include <gtkmm.h>

#include <string>

struct DocCreationData {
	std::string name { "Unnamed" };
  Core::Wp::Kind::inner kind;
};

class DocCreationDialog : public Gtk::Dialog {
private:
	Gtk::Box mDocType_chooser { Gtk::Orientation::HORIZONTAL };
	Gtk::Label mDocType_label { i18n_("Is part")};
	Gtk::CheckButton mDocType_btn;

	Gtk::Box mName_chooser { Gtk::Orientation::HORIZONTAL };
	Gtk::Label mName_label { i18n_("Name")};
	Gtk::Entry mName_entry;
	
public:
	DocCreationDialog(std::string const& defaultName);
	
	/*
		@function data generates the DocCreationData from the state of
		the parameter widgets
	*/
	DocCreationData data() const;
};

#endif
