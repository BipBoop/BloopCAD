
#include "Dimension_dialog.hpp"
#include <iostream>

namespace App {

DimensionDialog::DimensionDialog(double init_val)
{
  auto refBuilder = Gtk::Builder::create_from_resource("/design/sk_workspace.ui");

  auto body = refBuilder->get_widget<Gtk::Box>("prt_extrusion_dialog");
  // body->insert_child_at_start(*refBuilder->get_widget<Gtk::Box>("prt_extrusion_dialog"));

  // refBuilder->get_widget<Gtk::Button>("prt_extrusion_dialog_OK")
  //   ->signal_clicked().connect([this](){confirm=true;});
  // refBuilder->get_widget<Gtk::Button>("prt_extrusion_dialog_Cancel")
  //   ->signal_clicked().connect([this](){cancel=true;});


  ok_btn = refBuilder->get_widget<Gtk::Button>("sk_dimension_ok");
  ok_btn->signal_clicked().connect([this](){ confirm_ = true; });

  input_ = Gtk::Builder::get_widget_derived<NumericInput>(refBuilder, "sk_dimension_input");
  input_->set_onConfirm([this](){ ok_btn->grab_focus(); std::cout<<"try grab focus\n";});
  input_->set_valChangeCallback([this](double val){ std::cout<<"val="<<val<<"\n"; version_++; });
  input_->set_val(init_val);

  set_child(*body);
  set_title("Extrusion dialog");
}

double DimensionDialog::value()
{
  return input_->val();
}
std::string DimensionDialog::expression()
{
  return input_->str();
}

} // !App