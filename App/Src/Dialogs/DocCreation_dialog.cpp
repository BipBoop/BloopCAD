
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "DocCreation_dialog.hpp"

DocCreationDialog::DocCreationDialog(std::string const& defaultName)
{
	set_title("Doc creation");

	mDocType_chooser.append(mDocType_label);
	mDocType_chooser.append(mDocType_btn);
	mDocType_btn.set_active(false);
	get_content_area()->append(mDocType_chooser);

	mName_chooser.append(mName_label);
	mName_chooser.append(mName_entry);
	mName_entry.set_text(defaultName);
	get_content_area()->append(mName_chooser);

	add_button(i18n_("_Ok"), Gtk::ResponseType::OK)->grab_focus();
	add_button(i18n_("_Cancel"), Gtk::ResponseType::CANCEL);	

	set_default_response(Gtk::ResponseType::OK);
}

DocCreationData DocCreationDialog::data() const
{
	return { mName_entry.get_text(), 
			mDocType_btn.get_active() ? Core::Wp::Kind::Part : Core::Wp::Kind::Sketch };
}
