
#ifndef BLOOP_APP_NUMERICINPUT_HPP_
#define BLOOP_APP_NUMERICINPUT_HPP_

#include <gtkmm.h>
#include <Core/Stringparse/Exp/ExpBuilder.hpp>
#include "RangeVal.hpp"

namespace App {

class NumericInput : public Gtk::Entry {
private:

	RangeVal mVal;
	double mPrevVal { 0.0 };

	bool mValidVal { true };

	bool mIsNumber { true }, mIsEditing { false };
	std::string mExpStr { "" };
	Core::ExpBuilder mExpBuilder;

	std::optional<std::function<void (double)>> mOnValChange { std::nullopt };

	std::function<void()> mOnConfirm { [](){} }, mOnCancel { [](){} };

	bool mSelectOnEdit { false };

	Gtk::Popover mErrormsg;

public:
	NumericInput();
	NumericInput(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refBuilder);

	virtual ~NumericInput() {}

	void set_min(double min);
	void set_max(double max);

	double val() { return mVal; }
	void set_val(double v);
	void set_val(RangeVal v);

	bool isNumber() { return mIsNumber; }

	bool valid() { return mValidVal; }

	void set_valChangeCallback(std::function<void (double)> func);
	void set_onConfirm(std::function<void()> func) { mOnConfirm = func; }
	void set_onCancel(std::function<void()> func) { mOnCancel = func; }

	void callbacks();

	std::string str() const { return mExpStr; }

	void set_selectOnEdit(bool select = true) { mSelectOnEdit = select; }

// private:
	void init();
	bool compute_val_or_prev();

	bool clamp_val();
	void update_textField();
	void update_progress();

	void change_text();
	void confirm_entry();
	void start_edit();
	void end_edit();
	bool catch_esc(guint keyval, guint keycode, Gdk::ModifierType state);
};

} // !App

#endif