
#ifndef BLOOP_APP_DIMENSION_DIALOG_HPP_
#define BLOOP_APP_DIMENSION_DIALOG_HPP_

#include "Tool_dialog.hpp"
#include "NumericInput.hpp"

namespace App {

class DimensionDialog : public ToolDialog {
public:
  bool confirm_ { false };
  size_t version_ { 0 };

private:
  Gtk::Button* ok_btn { nullptr };
  Gtk::TextView* detail_text_ { nullptr };
  NumericInput* input_;

public:
  DimensionDialog(double init_val = 0.0);

  bool is_relevant() const override { return !confirm_; }

  double value();
  std::string expression();
};

}  // !App

#endif