
#ifndef BLOOP_APP_EXTRUSION_DIALOG_HPP_
#define BLOOP_APP_EXTRUSION_DIALOG_HPP_

#include "Tool_dialog.hpp"
#include "NumericInput.hpp"

namespace App {

class ExtrusionDialog : public ToolDialog {
public:
  bool confirm_ { false };
  bool cancel_ { false };
  bool flipped_ { false };
  size_t version_ { 0 };

private:
  Gtk::Button* ok_btn { nullptr };
  Gtk::TextView* detail_text_ { nullptr };
  NumericInput* input_;

public:
  ExtrusionDialog();

  void set_loopnames(std::string names);

  bool is_relevant() const override { return !confirm_ && !cancel_; }

  double value();
  std::string expression();
};

}  // !App

#endif