
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "Window.hpp"

#include <Core/Utils/Paths.hpp>
#include <Core/Print/IDOstream.hpp>

#include <iostream>

namespace App {

BloopWindow::BloopWindow(std::string to_open)
	: mEditor(this)
{
  set_title("Bloop CAD");
  Gtk::IconTheme::get_for_display(get_display())->add_resource_path("/guidata/Icons/");

  set_default_size(-1, -1);

  set_child(mEditor);


	auto keyController = Gtk::EventControllerKey::create();
	keyController->signal_key_pressed().connect(sigc::mem_fun(*this, &BloopWindow::aggregate_keypress), true);
	keyController->signal_key_released().connect(sigc::mem_fun(*this, &BloopWindow::aggregate_keyrelease), true);
	add_controller(keyController);


	// Actions
	add_action("new", sigc::mem_fun(*this, &BloopWindow::start_create_document));
	add_action("open", sigc::mem_fun(*this, &BloopWindow::start_open_document));
	add_action("save", sigc::bind(sigc::mem_fun(*this, &BloopWindow::start_save_document), false));
	add_action("save_as", sigc::bind(sigc::mem_fun(*this, &BloopWindow::start_save_document), true));
	add_action_with_parameter("export_wp", Glib::Variant<Glib::ustring>::variant_type(), sigc::mem_fun(*this, &BloopWindow::start_export_wp));

	auto refBuilder = Gtk::Builder::create_from_resource("/design/prt_workspace.ui");


  // Dirty hack
  Glib::signal_timeout().connect([=, this](){
    this->maximize();
		if(!to_open.empty())
			open_document(to_open);
    return false;
  }, 100);

  size_t update_period_ms = 40;
  mEditor.set_update_period(update_period_ms);
  Glib::signal_timeout().connect(sigc::mem_fun(*this, &BloopWindow::update), update_period_ms);

	std::cout<<"create document\n";
	create_document_impl(DocCreationData{.name=mAutosaver.tmp_directory() + "/umamed_doc", .kind=Core::Wp::Kind::Part});

	mAutosaver.add_workpiece(mEditor.root());
}
bool BloopWindow::update()
{
	if(mInDialog) {
		mKeyState = KeyState{}; // Restore default because main windows may not have focus
	}

  mEditor.gen_currInputData(mKeyState, mCoreHandler);
  bool nav_active = mEditor.update_navigation();
  if(!nav_active) {
    std::optional<Core::UpdDesc> updDesc = mEditor.gen_update(mCoreHandler, mMeshes);
    if(updDesc || mEditor.must_rebuild_wp()) {
			if(mAutosaver.should_autosave(mEditor.root())) {
				mAutosaver.autosave(mEditor.root(), mCoreHandler);
			}
			Core::UpdReport modIds;

			if(mEditor.must_rebuild_wp()) {
				modIds = mCoreHandler.rebuild(mEditor.focused_workpiece());
				mEditor.rebuilt_wp();
			} else {
				modIds = mCoreHandler.update(*updDesc);
			}

			if(!modIds.empty()) {
				mMeshes.update(modIds, mCoreHandler);
				mEditor.notify_corediff(modIds, mCoreHandler);
			}
    }
   }
   if(mEditor.is_render_requested())
      mEditor.queue_render(mCoreHandler, mMeshes);
  // Keep updating pls
  return true;
}

void BloopWindow::instanciate_editor(Core::WorkpieceId workpieceId)
{
  mEditor.set_root(workpieceId, mCoreHandler);
  mMeshes.update(mCoreHandler.constituents_of(workpieceId), mCoreHandler);
  mEditor.request_render();
	mAutosaver.add_workpiece(mEditor.root());
}

void BloopWindow::start_create_document()
{
	if(mInDialog)
		return;

	auto dialog = new DocCreationDialog(i18n_("Unamed_") + std::to_string(mNum_createdDocs++));
	dialog->set_transient_for(*this);
	dialog->set_modal(true);
	dialog->signal_response().connect(sigc::bind(sigc::mem_fun(*this, &BloopWindow::create_document_dialog), dialog));
	
	dialog->show();
}   
void BloopWindow::create_document_dialog(int response_id, DocCreationDialog* dialog)
{
	//Handle the response:
	switch (response_id)
	{
	case Gtk::ResponseType::OK:
	{
		auto creationData = dialog->data();
		create_document_impl(creationData);
		break;
	}
	case Gtk::ResponseType::CANCEL:
	{
		std::cout<<"Cancel doc creation."<<std::endl;
		break;
	}
	}
	mInDialog = false;
	delete dialog;
}
void BloopWindow::create_document_impl(DocCreationData data)
{
	instanciate_editor(mCoreHandler.create_empty(Core::Wp::CreateEmpty{	
																										.kind=data.kind,
																										.name_suffix=data.name,
																										.external_load=true,
																										.is_file=true}));
}

void BloopWindow::start_save_document(bool force_choose_location)
{
	if(mInDialog)
		return;

	Core::WorkpieceId doc = mEditor.root();
	std::string doc_location = mCoreHandler.locaname(doc);
	std::string doc_name = Core::get_filename(doc_location);

	if(force_choose_location || doc_location == "" || doc_location.find(mAutosaver.tmp_directory()) == 0) {
		auto dialog = new Gtk::FileChooserDialog(i18n_("Save file"), Gtk::FileChooser::Action::SAVE);
		dialog->set_current_name(doc_name);
		dialog->set_transient_for(*this);
		dialog->set_modal(true);
		dialog->signal_response().connect(sigc::bind(
			sigc::mem_fun(*this, &BloopWindow::save_document_dialog), dialog));

		//Add response buttons to the dialog:
		dialog->add_button(i18n_("_Cancel"), Gtk::ResponseType::CANCEL);
		dialog->add_button(i18n_("_Save"), Gtk::ResponseType::OK);
		dialog->set_create_folders(true);

		dialog->show();
		mInDialog = true;
	} else {
		mCoreHandler.save(doc, true);
	}
}

void BloopWindow::save_document_dialog(int response_id, Gtk::FileChooserDialog* dialog)
{
  //Handle the response:
	switch (response_id)
	{
	case Gtk::ResponseType::OK:
	{
		auto file = dialog->get_file();
		Core::WorkpieceId doc = mEditor.root();

		mCoreHandler.set_namesuffix(doc, file->get_path());
		mCoreHandler.save(doc, true);
		break;
	}
	case Gtk::ResponseType::CANCEL:
	{
		std::cout << "Cancel clicked." << std::endl;
		break;
	}
	default:
	{
		std::cout << "Unexpected button clicked." << std::endl;
		break;
	}
	}
	mInDialog = false;
	delete dialog;
}

void BloopWindow::start_open_document()
{
	if(mInDialog)
		return;

	auto dialog = new Gtk::FileChooserDialog(i18n_("Open file"), Gtk::FileChooser::Action::OPEN);
	dialog->set_transient_for(*this);
	dialog->set_modal(true);
	dialog->signal_response().connect(sigc::bind(
		sigc::mem_fun(*this, &BloopWindow::open_document_dialog), dialog));

	//Add response buttons to the dialog:
	dialog->add_button(i18n_("_Cancel"), Gtk::ResponseType::CANCEL);
	dialog->add_button(i18n_("_Open"), Gtk::ResponseType::OK);
	dialog->set_create_folders(true);

	dialog->show();
	mInDialog = true;
}
void BloopWindow::open_document_dialog(int response_id, Gtk::FileChooserDialog* dialog)
{
	//Handle the response:
	switch (response_id)
	{
	case Gtk::ResponseType::OK:
	{
		//Notice that this is a std::string, not a Glib::ustring.
		auto filename = dialog->get_file()->get_path();
		std::cout << "File selected: " <<  filename << std::endl;

		open_document(filename);
	}
		break;
	case Gtk::ResponseType::CANCEL:
	{
		std::cout << "Cancel clicked." << std::endl;
		break;
	}
	default:
	{
		std::cout << "Unexpected button clicked." << std::endl;
		break;
	}
	}
	mInDialog = false;
	delete dialog;
}
bool BloopWindow::open_document(std::string filepath)
{
	std::cout<<"Try to load "<<filepath<<"\n";
	std::optional<Core::WorkpieceId> maybe_doc = mCoreHandler.find_or_load(filepath);
	if(!maybe_doc) {
		std::cerr<<"Could not load file T_T"<<std::endl;
		return false;
	} else {
		std::cout<<"Could load file\n";
		instanciate_editor(*maybe_doc);
		return true;
	}
}

void BloopWindow::start_export_wp(Glib::VariantBase const& param)
{
	if(mInDialog)
		return;

	Glib::Variant<Glib::ustring> s = Glib::VariantBase::cast_dynamic<Glib::Variant<Glib::ustring> >(param);
	std::string id_str = s.get();
	auto wpid = Core::WorkpieceId::from_string(id_str);

	if(!wpid.valid()) {
		wpid = mEditor.focused_workpiece();
		std::cout<<"Export current focus"<<std::endl;
	} else {
		std::cout<<"Export worpiece "<<wpid<<"\n";
	}

	std::string wp_locaname = mCoreHandler.locaname(wpid);
	auto [wp_folderpath, wp_name] = Core::get_folderpath_and_bloopath(wp_locaname);
	std::cout<<"Folderpath: "<<wp_folderpath<<"\n";
	
	// wp_folderpath = Core::absolute_path(app_location, wp_folderpath);

	auto dialog = new Gtk::FileChooserDialog(i18n_("Export workpiece"), Gtk::FileChooser::Action::SAVE);
	dialog->set_create_folders(true);
	dialog->set_current_folder(Gio::File::create_for_path(wp_folderpath));
	dialog->set_current_name(wp_name);
	dialog->set_transient_for(*this);
	dialog->set_modal(true);
	dialog->signal_response().connect(sigc::bind(
		sigc::mem_fun(*this, &BloopWindow::export_wp_dialog), dialog, wpid));

	//Add response buttons to the dialog:
	dialog->add_button(i18n_("_Cancel"), Gtk::ResponseType::CANCEL);
	dialog->add_button(i18n_("_Save"), Gtk::ResponseType::OK);
	dialog->set_create_folders(true);

	dialog->show();
	mInDialog = true;
}
void BloopWindow::export_wp_dialog(int response_id, Gtk::FileChooserDialog* dialog, Core::WorkpieceId wpid)
{
	//Handle the response:
	switch (response_id) {
	case Gtk::ResponseType::OK: {
		// Set export path of wp to avoid selecting file at each export command??
		auto file = dialog->get_file();
		std::cout<<"Export file "<<file<<"\n";
		mCoreHandler.export_wp(wpid, file->get_path());
		break;
	}
	case Gtk::ResponseType::CANCEL: 
		std::cout << "Cancel clicked." << std::endl;
		break;
	default:
		std::cout << "Unexpected button clicked." << std::endl;
		break;
	}
	mInDialog = false;
	delete dialog;
}

bool BloopWindow::aggregate_keypress(guint keyval, guint keycode, Gdk::ModifierType state)
{
	if(keyval == GDK_KEY_Control_L || keyval == GDK_KEY_Control_R) {
		mKeyState.ctrlMod = true;
	} else if(keyval == GDK_KEY_Shift_L || keyval == GDK_KEY_Shift_R) {
		mKeyState.shiftMod =	true;
	} else if(keyval == GDK_KEY_Alt_L || keyval == GDK_KEY_Alt_R) {
		mKeyState.altMod = true;
	} else if(keyval == GDK_KEY_Escape) {
		mKeyState.esc = true;
	} else if(keyval == GDK_KEY_KP_Enter) {
		mKeyState.enter = true;
	} else if(keyval == GDK_KEY_Delete) {
		mKeyState.del = true;
	} else {
		mKeyState.key = keyval;
	}
	return false;
}
void BloopWindow::aggregate_keyrelease(guint keyval, guint keycode, Gdk::ModifierType state)
{
	if(keyval == GDK_KEY_Control_L || keyval == GDK_KEY_Control_R) {
		mKeyState.ctrlMod = false;
	} else if(keyval == GDK_KEY_Shift_L || keyval == GDK_KEY_Shift_R) {
		mKeyState.shiftMod =	false;
	} else if(keyval == GDK_KEY_Alt_L || keyval == GDK_KEY_Alt_R) {
		mKeyState.altMod = false;
	} else if(keyval == GDK_KEY_Escape) {
		mKeyState.esc = false;
	} else if(keyval == GDK_KEY_KP_Enter) {
		mKeyState.enter = false;
	} else if(keyval == GDK_KEY_Delete) {
		mKeyState.del = false;
	} else {
		mKeyState.key = 0;
	}
}

} // !App