
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "Autosave.hpp"

#include <Core/Print/IDOstream.hpp>
#include <Core/Utils/Paths.hpp>

#include <filesystem>
#include <iostream>
#include <fstream>

namespace App {

AutosaveManager::AutosaveManager()
{
  mTmp_directory = std::filesystem::temp_directory_path();
  mAutosave_directory = mTmp_directory + "/bloopcadautosave";

  std::cout<<"tmpdir=\""<<mTmp_directory<<"\"\n";
  std::cout<<"autdir=\""<<mAutosave_directory<<"\"\n";

  if(std::filesystem::directory_entry(autosave_directory()).exists()) {
    std::cout<<"Did not recover gracefully from crash..\n";
  }
  // Start clean slate
  // Eventualy do something more elaborate to allow
  // the user to recover is's file
  std::filesystem::remove_all(autosave_directory());

  std::filesystem::create_directories(autosave_directory());
}
AutosaveManager::~AutosaveManager()
{
  std::cout<<"Autosavemanager out!\n";
  std::filesystem::remove_all(autosave_directory());
}
void AutosaveManager::add_workpiece(Core::WorkpieceId file_wp)
{
  if(wp_saveInfos.find(file_wp) != wp_saveInfos.end()) {
    std::cerr<<"Unexpected, file_wp "<<file_wp<<" already in AutosaveManager on add"<<std::endl;
    return;
  }
  wp_saveInfos[file_wp] = SaveInfo { .lastsave=std::chrono::steady_clock::now(), .backup_id=n_workpiecesTotal++ };
}
void AutosaveManager::remove_workpiece(Core::WorkpieceId file_wp)
{
  if(wp_saveInfos.find(file_wp) == wp_saveInfos.end()) {
    std::cerr<<"Unexpected, file_wp "<<file_wp<<" not in AutosaveManager on remove"<<std::endl;
    return;
  }
  std::filesystem::remove(filepath_for(file_wp));
  wp_saveInfos.erase(file_wp);
}

void AutosaveManager::autosave(Core::WorkpieceId file_wp, Core::Wp::Handler const& handler)
{
  if(wp_saveInfos.find(file_wp) == wp_saveInfos.end()) {
    std::cerr<<"Unexpected, file_wp "<<file_wp<<" not in AutosaveManager on autosave"<<std::endl;
    return;
  }
	std::string true_filename = Core::get_folderpath_and_bloopath(handler.locaname(file_wp)).first;
  std::cout<<"Autosave!\n";
  std::ofstream stream(filepath_for(file_wp));
  stream<<"# "<<true_filename<<"\n";
  handler.write_on_stream(file_wp, stream);
}

bool AutosaveManager::should_autosave(Core::WorkpieceId file_wp) const
{
  return true;
}
std::string AutosaveManager::filepath_for(Core::WorkpieceId file_wp)
{
  return autosave_directory() + "/backup_" + std::to_string(wp_saveInfos[file_wp].backup_id);
}

} // !App