
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef BLOOP_APP_WINDOW_AUTOSAVE_HPP_
#define BLOOP_APP_WINDOW_AUTOSAVE_HPP_

#include <Core/Id.hpp>
#include <Core/Workpiece/Handler.hpp>

#include <vector>
#include <string>
#include <chrono>

namespace App {

class AutosaveManager {
  struct SaveInfo {
    std::chrono::time_point<std::chrono::steady_clock> lastsave; // Lasttime this workpiece file was autosaved
    size_t backup_id; // Number appended to the name of the file
  };

  std::string mTmp_directory;
  std::string mAutosave_directory;

  // Stores the workpiece that we might manage
  std::map<Core::WorkpieceId, SaveInfo> wp_saveInfos;

  // Used to generate backup filenames
  // they all have the form /tmp/backupbloop/backup0
  size_t n_workpiecesTotal { 0 };

public:
  AutosaveManager();
  ~AutosaveManager();

  std::string tmp_directory() const { return mTmp_directory; }
  std::string autosave_directory() const { return mAutosave_directory; }

  void add_workpiece(Core::WorkpieceId file_wp);
  void remove_workpiece(Core::WorkpieceId file_wp);

  // Manager must decide if the workpiece
  // must be autosaved at this time
  bool should_autosave(Core::WorkpieceId file_wp) const;

  void autosave(Core::WorkpieceId file_wp, Core::Wp::Handler const& handler);
  std::string filepath_for(Core::WorkpieceId file_wp);
};

}

#endif