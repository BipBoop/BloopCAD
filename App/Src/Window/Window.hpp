
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef BLOOP_WINDOW_HPP_
#define BLOOP_WINDOW_HPP_

#include <Editor/Editor.hpp>
#include <Dialogs/DocCreation_dialog.hpp>

#include "MeshRepo.hpp"
#include "Autosave.hpp"

#include <gtkmm.h>

namespace App {

class BloopWindow : public Gtk::ApplicationWindow {
private:
  Editor mEditor;
  MeshRepo mMeshes;
  Core::Wp::Handler mCoreHandler;
  KeyState mKeyState;
  int mNum_createdDocs { 0 };
  bool mInDialog { false };
  AutosaveManager mAutosaver;

public:
  BloopWindow(std::string to_open = "");

  bool update();
  void instanciate_editor(Core::WorkpieceId workpieceId);

  void start_create_document();
  void create_document_dialog(int response_id, DocCreationDialog* dialog);
  void create_document_impl(DocCreationData data);
  void start_save_document(bool force_choose_location);
  void save_document_dialog(int response_id, Gtk::FileChooserDialog* dialog);
  
  void start_open_document();
  void open_document_dialog(int response_id, Gtk::FileChooserDialog* dialog);
  bool open_document(std::string filepath);

  void start_export_wp(Glib::VariantBase const& param);
  void export_wp_dialog(int response_id, Gtk::FileChooserDialog* dialog, Core::WorkpieceId wpid);

  bool aggregate_keypress(guint keyval, guint keycode, Gdk::ModifierType state);
  void aggregate_keyrelease(guint keyval, guint keycode, Gdk::ModifierType state);

  
};

} // !App

#endif