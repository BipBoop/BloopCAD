
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef BLOOP_MESHREPO_HPP_
#define BLOOP_MESHREPO_HPP_

#include <Core/Id.hpp>
#include <Core/Discrete/Mesh.hpp>
#include <Core/Workpiece/Handler.hpp>
#include <Config/Config.hpp>

#include <Graphics_utils/Camera.hpp>

#include <map>

namespace App {

struct MeshRepo {
  // Maps id to meshes
  // each id has at most 1 + n_handles meshes
  // for instance, a sketch line has 1 curve mesh and 2 point meshes
  // the first mesh of the vector is the entity's mesh, followed by the
  // handle' meshes
  std::map<Core::Id, std::vector<Core::Mesh>> id_to_meshes;

  void update(Core::UpdReport report, Core::Wp::Handler const& handler);
  void update(Core::IdVec ids, Core::Wp::Handler const& handler);
  std::vector<Core::Mesh> const& get(Core::Id id) const;
  Core::Mesh const& get_one(Core::Id id) const;

  // void add(Core::Id id, Core::Mesh mesh);
  // void remove(Core::Id id);
  // void remove(Core::WorkpieceId);
  // size_t find(Core::Id id);
  // size_t find_firstOf(Core::WorkpieceId wpId);

  std::optional<double> mesh_intersection(Camera cam, glm::dmat4 transform, Core::Id id, Config configs, glm::dvec2 point) const;
  std::optional<double> mesh_intersection_curve(Core::Mesh const& mesh, Camera cam, Core::Id id, double dist, glm::dvec2 point) const;
  std::optional<double> mesh_intersection_surface(Core::Mesh const& mesh, Camera cam, Core::Id id, double dist, glm::dvec2 point) const;

};

} // !App

#endif