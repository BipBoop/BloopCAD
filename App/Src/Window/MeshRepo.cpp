
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "MeshRepo.hpp"

#include <Core/Print/IDOstream.hpp>
#include <Core/Maths/Utils.hpp>
#include <Core/Precision.hpp>

namespace App {

void MeshRepo::update(Core::UpdReport report, Core::Wp::Handler const& handler)
{
  // Potential for parallelism?
  for(auto diff : report.wp_diffs) {
    for(auto id : diff.second.removed) {
      if(id.is_workpiece())
        continue;
      id_to_meshes.erase(id);
    }
    for(auto id : diff.second.added) {
      if(id.is_workpiece())
        continue;
      id_to_meshes[id] = handler.meshes(id);
    }
    for(auto id : diff.second.modified) {
      if(id.is_workpiece())
        continue;
      id_to_meshes[id] = handler.meshes(id);
    }

  }
}
void MeshRepo::update(Core::IdVec ids, Core::Wp::Handler const& handler)
{
  // Potential for parallelism?
  for(auto id : ids) {
    if(id.is_workpiece())
      continue;
    id_to_meshes[id] = handler.meshes(id);
  }
}
std::vector<Core::Mesh> const& MeshRepo::get(Core::Id id) const 
{
  try {
    return id_to_meshes.at(id);
  } catch(std::exception& err) {
    std::cerr<<"Error "<<__FUNCTION__<<",  "<<__FILE__<<" : "<<err.what()<<std::endl;
  }
  return id_to_meshes.at(id); // eh.. makes the warning silent
}
Core::Mesh const& MeshRepo::get_one(Core::Id id) const
{
  Core::Id id_no_handle = id;
  id_no_handle.entId.handle_num = -1;

  try {
    return id_to_meshes.at(id_no_handle)[id.entId.handle_num+1];
  } catch(std::exception& err) {
    std::cerr<<"Error "<<__FUNCTION__<<",  "<<__FILE__<<" : "<<err.what()<<std::endl;
  }
  return id_to_meshes.at(id_no_handle)[id.entId.handle_num+1];
}

std::optional<double> MeshRepo::mesh_intersection(Camera cam, glm::dmat4 transform, Core::Id id, Config configs, glm::dvec2 point) const
{
  double dist = configs.get_hover_dist(id);

  int handle_num = id.entId.handle_num;
  id.entId.handle_num = -1;
  
  if(id_to_meshes.find(id) == id_to_meshes.end()) {
    return false;
  }
  try {
    Core::Mesh const& mesh = id_to_meshes.at(id).at(handle_num+1);
    cam = Camera(cam, transform);

    switch(mesh.kind) {
    case Core::Mesh::Kind::Point:
      return glm::distance(glm::vec3(cam.state.pos), mesh.get_points()[0]);
    case Core::Mesh::Kind::Curve:
      return mesh_intersection_curve(mesh, cam, id, dist, point);
    case Core::Mesh::Kind::TrianglesNorm:
      return mesh_intersection_surface(mesh, cam, id, dist, point);
    default:
      break;
    }  
  } catch(std::exception& err) {
    std::cerr<<"Error "<<__FUNCTION__<<",  "<<__FILE__<<" : "<<err.what()<<std::endl;
    return std::nullopt;
  }

  return std::nullopt;
}
std::optional<double> MeshRepo::mesh_intersection_curve(Core::Mesh const& mesh, Camera cam, Core::Id id, double dist, glm::dvec2 point) const
{
  std::vector<glm::vec3> world_points = mesh.get_points();

  glm::dvec2 prev_point_screen = cam.world_to_screen(world_points[0]);
  for(size_t i = 1; i < world_points.size(); ++i) {
    glm::dvec2 new_point_screen = cam.world_to_screen(world_points[i]);
    double found_si = Core::Crv::_2d::Line(prev_point_screen, new_point_screen).dist(point);

    if(found_si <= dist) {
      return std::max(glm::distance(glm::vec3(cam.state.pos), world_points[i]), 
                      glm::distance(glm::vec3(cam.state.pos), world_points[i-1]));
    }
    prev_point_screen = new_point_screen;
  }
  return std::nullopt;
}
double area(glm::dvec2 pt1, glm::dvec2 pt2, glm::dvec2 pt3)
{
  return abs((pt1.x*(pt2.y-pt3.y) + pt2.x*(pt3.y-pt1.y)+ pt3.x*(pt1.y-pt2.y))/2.0);
}
std::optional<double> MeshRepo::mesh_intersection_surface(Core::Mesh const& mesh, Camera cam, Core::Id id, double dist, glm::dvec2 point) const 
{
  // https://www.geeksforgeeks.org/check-whether-a-given-point-lies-inside-a-triangle-or-not/

  std::vector<std::array<glm::vec3, 3>> triangles = mesh.get_triangles();

  for(size_t i = 0; i < triangles.size(); ++i) {
    glm::dvec2 pt1 = cam.world_to_screen(triangles[i][0]);
    glm::dvec2 pt2 = cam.world_to_screen(triangles[i][1]);
    glm::dvec2 pt3 = cam.world_to_screen(triangles[i][2]);

    double A  = area(pt1, pt2, pt3);
    double A1 = area(point, pt2, pt3);
    double A2 = area(pt1, point, pt3);
    double A3 = area(pt1, pt2, point);
    
    if(Core::sepsilon_equal(A, (A1+A2+A3), Core::Prc::kIntersection)) {
      return glm::distance(glm::vec3(cam.state.pos), (triangles[i][0] + triangles[i][1] + triangles[i][2]) / 3.0f);
    }
  }
  return std::nullopt;
}


} // !App