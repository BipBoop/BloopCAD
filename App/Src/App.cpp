
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "Window/Window.hpp"

#include <Core/Utils/Paths.hpp>

#include <iostream>
#include <bitset>
#include <set>
#include <filesystem>

class BloopApp : public Gtk::Application {
private:
	App::BloopWindow* wind { nullptr };
	std::vector<std::string> open_on_startup;
	std::shared_ptr<Gtk::Builder> refBuilder;

public:
	BloopApp()
		: Gtk::Application("org.bloop.cad", Gio::Application::Flags::HANDLES_OPEN)
	{
		Glib::set_application_name("BloopCAD ~ Attempt");
		
		add_main_option_entry_filename(sigc::mem_fun(*this, &BloopApp::manage_arg_filename), "open", 'o', "File to open on startup", "../file.bcad");
		// signal_window_added().connect(sigc::mem_fun(*this, &BloopApp::manage_window_added));
		signal_startup().connect(sigc::mem_fun(*this, &BloopApp::manage_startup));
	}
	~BloopApp()
	{
		delete wind;
	}
	void manage_startup()
	{
	  Gtk::Application::on_startup();

		refBuilder = Gtk::Builder::create_from_resource("/design/interface.ui");
		
		auto gmenu = refBuilder->get_object<Gio::Menu>("menu-bar-bloop");
		if (!gmenu) {
			std::cerr<<"GMenu not found"<<std::endl;
		}
		else {
			set_menubar(gmenu);
		}

		set_accel_for_action("win.new", "<Primary>n");
	  set_accel_for_action("win.open", "<Primary>o");
	  set_accel_for_action("win.save", "<Primary>s");
	  set_accel_for_action("win.save_as", "<Primary><Shift>s");
	  set_accel_for_action("win.export_wp('')", "<Primary>e");
	}
	void on_activate()
	{
		if(open_on_startup.empty()) {
			wind = new App::BloopWindow("");
		} else {
			wind = new App::BloopWindow(open_on_startup[0]);
		}

		add_window(*wind);

		wind->set_visible(true);
		wind->set_show_menubar();
	}
	// void manage_window_added(Gtk::Window* window)
	// {
	// 	std::cout<<"Window added!\n";
	// }
	bool manage_arg_filename(const Glib::ustring& option_name, const std::string& value, bool has_value)
	{
		if(has_value && option_name == "--open" || option_name == "-o") {
			open_on_startup.push_back(Core::absolute_path(std::filesystem::current_path(), value));
		}
		return true;
	}
};

int main(int argc, char* argv[])
{
	for(size_t i = 0; i < argc; ++i) {
		std::cout<<"arg i: \""<<argv[i]<<"\"\n";
	}
	BloopApp app;

	// App::BloopWindow window;
	// app.add_window(window)/;
	// app.run(argc, argv)
	return app.run(argc, argv);
  // return app.make_window_and_run<App::BloopWindow>(argc, argv);
}

// #include "exampleapplication.h"

// int main(int argc, char* argv[])
// {
// 	ExampleApplication app;

//   // Start the application, showing the initial window,
//   // and opening extra windows for any files that it is asked to open,
//   // for instance as a command-line parameter.
//   // run() will return when the last window has been closed by the user.
//   const int status = app.run(argc, argv);
//   return status;
// }
