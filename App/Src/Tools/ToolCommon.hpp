
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef BLOOP_TOOLOUT_HPP_
#define BLOOP_TOOLOUT_HPP_

#include <Dialogs/Tool_dialog.hpp>
#include <Editor/InputData.hpp>

#include <Core/Id.hpp>
#include <Core/Workpiece/Actions.hpp>
#include <Core/Workpiece/Handler.hpp>

#include <functional>
#include <optional>

namespace App {

class Tool {
public:
  struct Out {
    std::optional<Core::UpdDesc> upddesc { std::nullopt };
    Tool* newTool { nullptr }; // When a new tool is returned, the editor takes care to delete the current

    std::shared_ptr<ToolDialog> dialog { nullptr };

    bool clear_selection { false };
    bool deactivate_selarea { false };

    static Out make_simple(Core::WorkpieceId wpid_, Core::Wp::Action action_)
    {
      return Out { .upddesc=Core::UpdDesc{ .wpid=wpid_, .action=action_ }};
    }
    static Out make_branchAction(Core::WorkpieceId wpid_, Core::Wp::BranchAction action_)
    {
      return Out { .upddesc=Core::UpdDesc { .wpid=wpid_, .branchaction=action_ }};
    }
  };
  struct InitOut {
    std::shared_ptr<ToolDialog> dialog { nullptr };
  };

protected:
  Core::IdVec hold_ {};
  // Core::TimeCapsule start_capsule_; // Workpice with herstory pos, so that the tool can 
                                    // be canceled and return to initial state

public:
  Tool(/*Core::TimeCapsule start_capsule*/) {}
  virtual ~Tool() { }

  Out update( InputData const& input, Core::WorkpieceId cwp, Core::Wp::Handler const& handler, 
              Core::IdVec const& selection, Core::IdVec const& hover)
  {
    if(input.delta.key.esc == 1 && !override_esc_signal()) {
      // Merge branch if there is one so that 
      // we dont stay in a dangling branch for next tool
      clear();
      return Out{ 
          .upddesc=Core::UpdDesc { 
            .wpid=cwp, 
            .branchaction=Core::Wp::BranchAction::Remove }, 
          .newTool=make_default() };
    }
    return update_impl(input, cwp, handler, selection, hover);
  }

  Core::IdVec const& hold() const { return hold_; }
  Core::IdVec& hold() { return hold_; }

  // Init function of tool called
  // when the tool is set in the editor
  // it can return init data, which can be empty
  virtual InitOut init() { return InitOut{}; }

  virtual std::vector<size_t> filter_hover(Core::IdVec raw_hover) const 
  { 
    std::vector<size_t> out(raw_hover.size());
    for(size_t i = 0; i < out.size(); ++i) { out[i] = i; }
    return out;
  }
  
  // For tools that may have multiple states
  // e.g. if a line is doing a polyline, the tool
  // should still be line after one escape, but change
  // to default if no line is currently being drawn
  virtual bool override_esc_signal() const { return false; }

  // An opportunity for the tool to update itself
  // with the feedback from the core handler
  // e.g. the line tool takes the created line as a handle
  virtual void notify_coreupd(Core::WorkpieceId cwp, Core::UpdReport const& updrep) { }
  
  // Main update of the tool
  virtual Out update_impl(InputData const& input, Core::WorkpieceId cwp, Core::Wp::Handler const& handler, 
                          Core::IdVec const& selection, Core::IdVec const& hover) = 0;
  virtual Tool* make_default() const = 0;
  virtual std::string name() const = 0;
  virtual void clear() {}          
};

using StageToolFunc = std::function<void(Tool*)>;

} // !App

#endif