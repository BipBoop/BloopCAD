
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
#include "PrtToolbar.hpp"

#include "StartSketch_tool.hpp"
#include "Extrusion_tool.hpp"

#include <gtkmm/builder.h>

namespace App {

PrtToolbar::PrtToolbar(StageToolFunc stage_tool_func)
    : mStage_tool_func(stage_tool_func)
{
  auto refBuilder = Gtk::Builder::create_from_resource("/design/prt_workspace.ui");

  set_can_focus(false);
  
  refBuilder->get_widget<Gtk::Button>("prt_extrusion_btn")
      ->signal_clicked().connect([this](){ mStage_tool_func(new PrtTools::ExtrusionTool); });

  refBuilder->get_widget<Gtk::Button>("prt_startSketch_btn")
      ->signal_clicked().connect([this](){ mStage_tool_func(new PrtTools::StartSketchTool); });

  set_child(*refBuilder->get_widget<Gtk::Box>("prt_ws_toolbar"));
}

} // !App