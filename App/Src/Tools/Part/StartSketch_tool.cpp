
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "StartSketch_tool.hpp"

#include <Core/Print/IDOstream.hpp>

namespace App {
namespace PrtTools {

std::vector<size_t> StartSketchTool::filter_hover(Core::IdVec raw_hover) const
{
  std::vector<size_t> out;
  for(size_t i = 0; i < raw_hover.size(); ++i) {
    auto hover = raw_hover[i];
    if(hover.wpid.kind == Core::Wp::Kind::Part && hover.entId.kind == Core::Prt::Kind::Origin_plane) {
      out.push_back(i);
    }
  }

  return out;
}
Tool::Out StartSketchTool::update_impl( InputData const& input, Core::WorkpieceId cwp, Core::Wp::Handler const& handler, 
                                        Core::IdVec const& selection, Core::IdVec const& hover)
{
  if(selection.size() == 1) {
    std::cout<<"Create sketch on plane "<<selection[0]<<"\n";
    return Out {
      .upddesc=Core::UpdDesc{
        .wpid=cwp,
        .action=Core::Wp::CreateEmpty {
          .kind=Core::Wp::Kind::Sketch, 
          .name_suffix="boopsk",
          .attach=Core::Wp::Attach{ 
            .to=cwp, // The part
            .attachAction=Core::Prt::AttachSketch{.plane_id=selection[0].entId},
            .as_child=true
          },
        },
        .toolmsg="focus_next" // Tell the editor to focus the next new workpiece
      },
      .clear_selection=true
    };
  }
  return Out{};
}

} // !PrtTools
} // !App