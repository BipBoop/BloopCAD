
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef BLOOP_PRTDEFAULTTOOL_HPP_
#define BLOOP_PRTDEFAULTTOOL_HPP_

#include "../ToolCommon.hpp"

#include <Core/Workpiece/Handler.hpp>
#include <Editor/InputData.hpp>

namespace App {
namespace PrtTools {

class DefaultTool : public Tool {
public:
  DefaultTool() = default;

  std::vector<size_t> filter_hover(Core::IdVec raw_hover) const override;
  bool override_esc_signal() const override { return true; }
  Out update_impl(InputData const& input, Core::WorkpieceId cwp, Core::Wp::Handler const& handler, 
                  Core::IdVec const& selection, Core::IdVec const& hover) override;

  Tool* make_default() const override { return new DefaultTool(); }
  std::string name() const override { return "prt_default"; }
};

class PrtTool : public Tool {
public:
  Tool* make_default() const override { return new DefaultTool(); }
};

} // !PrtTools

} // !App


#endif

