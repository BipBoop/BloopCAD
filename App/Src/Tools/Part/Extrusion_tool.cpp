
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "Extrusion_tool.hpp"

#include <Dialogs/Extrusion_dialog.hpp>

#include <Core/Part/Actions.hpp>
#include <Core/Print/IDOstream.hpp>

namespace App {
namespace PrtTools {

ExtrusionTool::ExtrusionTool()
{
  dialog_ = std::make_shared<ExtrusionDialog>();
}

Tool::InitOut ExtrusionTool::init()
{
  return InitOut { .dialog=dialog_ };
}
std::vector<size_t> ExtrusionTool::filter_hover(Core::IdVec raw_hover) const
{
  std::vector<size_t> out;
  Core::WorkpieceId skid = sketch();

  for(size_t i = 0; i < raw_hover.size(); ++i) {
    auto hover = raw_hover[i];
    if(hover.wpid.kind == Core::Wp::Kind::Sketch && hover.entId.kind == Core::Sk::Kind::Loopdiloop) {
      if(!skid.valid() || skid == hover.wpid) {
        out.push_back(i);
        skid = hover.wpid;
      }
    }
  }
  return out;
}

Tool::Out ExtrusionTool::update_impl( InputData const& input, Core::WorkpieceId cwp, Core::Wp::Handler const& handler, 
                                      Core::IdVec const& selection, Core::IdVec const& hover)
{
  bool changed_loops = false;
  if(hold_ != selection) {
    changed_loops = true;
    hold_ = selection;
    if(hold_.empty()) {
      dialog_->set_loopnames("");
    } else {
      auto sk = handler.get_wp<Core::Sk::Sketch>(hold_[0].wpid);
      if(!sk) {
        std::cerr<<"Could not find sk.. "<<__FILE__<<",  "<<__LINE__<<std::endl;
      }
      dialog_->set_loopnames(loopsnames(*sk));
    }
  }

  if(dialog_->confirm_) {
    if(dialog_->version_ > last_dialog_version || changed_loops) {
      return Out { .upddesc=extrusion_desc(cwp), .newTool=make_default() };
    }
    return Out { .newTool=make_default() };
  } else if(dialog_->cancel_) {
    // return cancel();
  } else if(dialog_->version_ > last_dialog_version || changed_loops) {
    last_dialog_version = dialog_->version_;
    return Out { .upddesc=extrusion_desc(cwp) };
  }

  return Out{};
}
void ExtrusionTool::notify_coreupd(Core::WorkpieceId cwp, Core::UpdReport const& updrep)
{
  for(auto mod : updrep.wp_buildSequence_mod) {
    if(mod.first == cwp) {
      extrusion_coreIndex = mod.second;
    }
  }
}

std::optional<Core::UpdDesc> ExtrusionTool::extrusion_desc(Core::WorkpieceId cwp)
{
  if(hold_.empty()) {
    return std::nullopt;
  }
  std::vector<Core::Named<Core::SibId>> loopsvec(hold_.size());
  for(size_t i = 0; i < hold_.size(); ++i) {
    loopsvec[i] = Core::Named<Core::SibId>(hold_[i].entId);
  }
  Core::Prt::Extrude extrusion {
    .sk_id=sketch(),
    .loopdiloops=loopsvec,
    .amount=dialog_->value(),
    .flipped=dialog_->flipped_,
    .amount_expr=dialog_->expression()
  };

  if(extrusion_coreIndex == Core::invalid_index()) {
    return Core::UpdDesc {
      .wpid=cwp,
      .action=extrusion
    };
  } else {
    return Core::UpdDesc {
      .wpid=cwp,
      .action=Core::Prt::SetBuildAction {
        .action_index=extrusion_coreIndex,
        .new_buildAction=extrusion
      }
    };
  }
}
std::string ExtrusionTool::loopsnames(Core::Sk::Sketch const& sk)
{
  std::string out = "";
  for(size_t i = 0; i < hold_.size(); ++i) {
    out += sk.name_of(hold_[i].entId);
    if(i < hold_.size()-1) {
      out += "; ";
    }
  }
  return out;
}
Core::WorkpieceId ExtrusionTool::sketch() const
{
  if(hold_.empty()) {
    return Core::WorkpieceId{};
  }
  return hold_[0].wpid;
}

} // !PrtTools
} // !App