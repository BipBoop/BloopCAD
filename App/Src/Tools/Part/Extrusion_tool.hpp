
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef BLOOP_APP_PRTEXTRUSIONTOOL_HPP_
#define BLOOP_APP_PRTEXTRUSIONTOOL_HPP_

#include "Default_tool.hpp"
#include <Dialogs/Extrusion_dialog.hpp>

namespace App {
namespace PrtTools {
   
class ExtrusionTool : public PrtTool {
private:
  bool is_dragging { false };
  std::shared_ptr<ExtrusionDialog> dialog_ { nullptr };

  // Index of the extrusion in the part build tree
  // can be provided in constructor if we are modifying
  // an already existing extrusion, or be created in time,,
  size_t extrusion_coreIndex { Core::invalid_index() };
  size_t last_dialog_version = 0;

public:
  ExtrusionTool();
  ExtrusionTool(size_t extrusion);

  std::optional<Core::UpdDesc> extrusion_desc(Core::WorkpieceId cwp);

  InitOut init() override;
  std::vector<size_t> filter_hover(Core::IdVec raw_hover) const override;
  void notify_coreupd(Core::WorkpieceId cwp, Core::UpdReport const& updrep) override;
  Out update_impl(InputData const& input, Core::WorkpieceId cwp, Core::Wp::Handler const& handler, 
                  Core::IdVec const& selection, Core::IdVec const& hover) override;
  std::string name() const override { return "prt_extrusion"; }

  std::string loopsnames(Core::Sk::Sketch const& sk);
  Core::WorkpieceId sketch() const;
};

} // !PrtTools
} // !App


#endif