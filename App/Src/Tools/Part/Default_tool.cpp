
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "Default_tool.hpp"

namespace App {
namespace PrtTools {

std::vector<size_t> DefaultTool::filter_hover(Core::IdVec raw_hover) const
{
  std::vector<size_t> out;
  for(size_t i = 0; i < raw_hover.size(); ++i) {
    auto hover = raw_hover[i];
    if(hover.wpid.kind == Core::Wp::Kind::Part) {
      out.push_back(i);
    }
  }
  return out;
}
Tool::Out DefaultTool::update_impl( InputData const& input, Core::WorkpieceId cwp, Core::Wp::Handler const& handler, 
                                    Core::IdVec const& selection, Core::IdVec const& hover)
{
  return Out{};
}

} // !PrtTools
} // !App