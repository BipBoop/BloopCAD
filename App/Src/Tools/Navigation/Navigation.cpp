
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "Navigation.hpp"

#include <Core/Utils/Visitor.hpp>
#include <Core/Print/GlmOstream.hpp>

namespace App {
namespace Nav {

CameraSnap pan(glm::dvec3 worldAnchor, glm::dvec2 const& new_screenPos, Camera const& cam)
{
  // std::cout<<"Pan!\n";
	glm::dvec3 focus_new_app_pos = Core::intersection(Core::Srf::Plane{ worldAnchor, cam.front() }, 
														                        cam.cast_ray(new_screenPos));
	focus_new_app_pos = focus_new_app_pos;
	glm::dvec3 cam_move = worldAnchor - focus_new_app_pos;

  return CameraSnap::from_posTarget(cam.state.pos+cam_move, cam.state.target+cam_move, glm::dvec3(0.0, 1.0, 0.0), 1.0);
}

CameraSnap zoom(glm::dvec3 worldAnchor, glm::dvec2 const& screenAnchor, double amount, Camera const& cam)
{
	CameraSnap out = cam.state;
	out.pos += out.front() * amount;
	Camera new_cam(out, cam.params, cam.viewport, cam.modTransf);

	return pan(worldAnchor, screenAnchor, new_cam);
}

CameraSnap orbit(glm::dvec2 const& dragPos, glm::dvec2 const& newPos, CameraSnap startSnap, double radians_per_pixel)
{
  // std::cout<<"Orbit "<<dragPos<<",  "<<newPos<<"\n";
	glm::dvec2 mouse_move = newPos - dragPos;

	mouse_move *= radians_per_pixel; // Radians per pixel

	glm::dvec3 newPos_spherical = startSnap.pos_spherical();

	newPos_spherical.y -= mouse_move.x;
	newPos_spherical.z -= mouse_move.y;

	return CameraSnap::fromSpherical(newPos_spherical, startSnap.target, glm::dvec3(0.0, 1.0, 0.0), startSnap.zoom);
}

NavTool instant_invoke(NavTool navTool, InputData inData, Camera const& cam)
{
  if(inData.delta.mouseScroll != glm::dvec2(0.0)) {
    navTool.instant = ZoomData { .worldAnchor=inData.state.objPoint.worldPos, .screenAnchor=inData.state.mousePos };
  } else if(inData.state.middleMouse) {
    navTool.instant = OrbitData { .dragPos=inData.state.mousePos, .startSnap=cam.state };
  } else {
    navTool.instant = std::nullopt;
  }
  return navTool;
}

std::pair<std::optional<CameraSnap>, Data> update_tool(Data navData, InputData inData, Camera const& cam, NavConfig config)
{
  // std::cout<<"update nav as tool\n";
  // Update tools
  return std::visit(Core::Visitor{
    [&](PanData panData) -> std::pair<std::optional<CameraSnap>, Data> {
      
      if(inData.delta.mousePressed > 0) {
        panData = PanData { .worldAnchor=inData.state.objPoint.worldPos };
      }
      if(inData.state.mousePressed && inData.delta.mousePos != glm::dvec2(0.0)) {
        return { pan(panData.worldAnchor, inData.state.mousePos, cam), panData };
      }
      return { std::nullopt, panData };
    },
    [&](OrbitData orbitData) -> std::pair<std::optional<CameraSnap>, Data> {
      if(inData.delta.mousePressed > 0) {
        orbitData = OrbitData { .dragPos=inData.state.mousePos, .startSnap=cam.state };
      }
      if(inData.delta.mousePos != glm::dvec2(0.0) && inData.state.mousePressed > 0) {
        return { orbit(orbitData.dragPos, inData.state.mousePos, orbitData.startSnap, config.kOrbit_radians_per_pixel), orbitData };  
      }
      return { std::nullopt, orbitData };      
    },
    [&](ZoomData zoomData) -> std::pair<std::optional<CameraSnap>, Data> {
      if(inData.delta.mousePressed > 0) {
        zoomData = ZoomData { .worldAnchor=inData.state.objPoint.worldPos, .screenAnchor=inData.state.mousePos };
      }
      if(inData.state.mousePressed && inData.delta.mousePos.y != 0.0) {
        double amount = inData.delta.mousePos.y * config.kZoom_drag_mult;
        return { zoom(zoomData.worldAnchor, zoomData.screenAnchor, amount, cam), zoomData };
      }
      return { std::nullopt, zoomData };
    }
  }, navData);
}
std::optional<CameraSnap> update_instant(Data navData, InputData inData, Camera const& cam, NavConfig config)
{
  // std::cout<<"Update navigation as instant!\n";
  // Update tools
  return std::visit(Core::Visitor{
    [&](PanData panData) -> std::optional<CameraSnap> {
      if(inData.delta.mousePos == glm::dvec2(0.0))
        return std::nullopt;
      return pan(panData.worldAnchor, inData.state.mousePos, cam);
    },
    [&](OrbitData orbitData) -> std::optional<CameraSnap> {
      if(inData.delta.mousePos == glm::dvec2(0.0))
        return std::nullopt;
      return orbit(orbitData.dragPos, inData.state.mousePos, orbitData.startSnap, config.kOrbit_radians_per_pixel);
    },
    [&](ZoomData zoomData) -> std::optional<CameraSnap> {
      if(inData.delta.mouseScroll.y == 0.0)
        return std::nullopt;
        double amount = inData.delta.mouseScroll.y * config.kZoom_scroll_mult;
      return zoom(zoomData.worldAnchor, zoomData.screenAnchor, amount, cam);
    }
  }, navData);
}

std::pair<std::optional<CameraSnap>, NavTool> 
  update_navigation(NavTool tool, InputData inData, Camera const& cam, NavConfig config)
{
  tool = instant_invoke(tool, inData, cam);

  if(tool.instant) {
    return { update_instant(*tool.instant, inData, cam, config), tool };
  } else if(tool.tool) {
    std::optional<CameraSnap> maybe_newCamSnap;
    std::tie(maybe_newCamSnap, tool.tool) = update_tool(*tool.tool, inData, cam, config);
    return { maybe_newCamSnap, tool };
  } else {
    return { std::nullopt, NavTool{} };
  }
}

} // !Nav
} // !App