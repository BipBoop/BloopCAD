
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef BLOOP_NAVIGATION_HPP_
#define BLOOP_NAVIGATION_HPP_

#include <Graphics_utils/Camera.hpp>
#include <Editor/InputData.hpp>
#include <Config/Config.hpp>

#include <variant>
#include <optional>

namespace App {
namespace Nav {

struct PanData {
  glm::dvec3 worldAnchor;
};
struct OrbitData {
  glm::dvec2 dragPos;
  CameraSnap startSnap;
};
struct ZoomData {
  glm::dvec3 worldAnchor;
  glm::dvec2 screenAnchor;
};

using Data = std::variant<PanData, OrbitData, ZoomData>;

struct NavTool {
  std::optional<Data> instant { std::nullopt };
  std::optional<Data> tool { std::nullopt };

  bool has_any() const { return instant || tool; }
  bool has_tool_only() const { return !instant && tool; }

  // Check with has_any first, undefined if no data available
  Data data() const { return instant ? *instant : *tool; }
};

std::pair<std::optional<CameraSnap>, NavTool> 
    update_navigation(NavTool tool, InputData inData, Camera const& cam, NavConfig config);

} // !Nav
} // !App

#endif