
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef BLOOP_APP_SKLINELINEPERPTOOL_HPP_
#define BLOOP_APP_SKLINELINEPERPTOOL_HPP_

#include "Default_tool.hpp"

namespace App {
namespace SkTools {

class SymbolicConstraintTool : public SkTool {
private:
  std::vector<size_t> looking_for;

public:
  SymbolicConstraintTool() = default;

  InitOut init() override;
  std::vector<size_t> filter_hover(Core::IdVec raw_hover) const override;

  Tool::Out update_impl(InputData const& input, Core::WorkpieceId cwp, Core::Wp::Handler const& handler, 
                        Core::IdVec const& selection, Core::IdVec const& hover) override;

  virtual Core::Sk::Action make_constraint(Core::SibIdVec geoms) const = 0;

  void clear() override;

  // Returns what the tool is looking for given the held ids
  // e.g. if the coincidence tool is plain, it may look for
  // points, lines and circles but once a line is selected,
  // it may only want points to complete the constraint
  virtual std::vector<size_t> comp_looking_for(Core::SibIdVec current) const = 0; 
};

class LLPerpTool : public SymbolicConstraintTool {
public:
  LLPerpTool() = default;
  std::vector<size_t> comp_looking_for(Core::SibIdVec current) const override; 
  Core::Sk::Action make_constraint(Core::SibIdVec geoms) const override;

  std::string name() const override { return "sk_linelineperp"; }
};
class CoincidentTool : public SymbolicConstraintTool {
public:
  CoincidentTool() = default;
  std::vector<size_t> comp_looking_for(Core::SibIdVec current) const override; 
  Core::Sk::Action make_constraint(Core::SibIdVec geoms) const override;

  std::string name() const override { return "sk_coincident"; }

  static Core::Sk::Action constraint(Core::SibId first, Core::SibId second);
};
class HorizVertTool : public SymbolicConstraintTool {
private:
  Core::Sk::Kind::inner line_kind, point_kind;

public:
  HorizVertTool(Core::Sk::Kind::inner line_kind_, Core::Sk::Kind::inner point_kind_);
  std::vector<size_t> comp_looking_for(Core::SibIdVec current) const override; 
  Core::Sk::Action make_constraint(Core::SibIdVec geoms) const override;

  static Core::Sk::Action constraint(Core::SibId line, Core::Sk::Kind::inner true_kind);
  static Core::Sk::Action constraint(Core::SibId pt1, Core::SibId pt2, Core::Sk::Kind::inner true_kind);

};
class HorizontalTool : public HorizVertTool {
public:
  HorizontalTool() : HorizVertTool(Core::Sk::Kind::LineHorizontal, Core::Sk::Kind::PointPointHorizontal) {}

  std::string name() const override { return "sk_horizontal"; }
};
class VerticalTool : public HorizVertTool {
public:
  VerticalTool() : HorizVertTool(Core::Sk::Kind::LineVertical, Core::Sk::Kind::PointPointVertical) {}

  std::string name() const override { return "sk_vertical"; }
};

} // !SkTools

} // !App

#endif