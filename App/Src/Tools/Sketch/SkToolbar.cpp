
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "SkToolbar.hpp"

#include "Line_tool.hpp"
#include "Point_tool.hpp"
#include "Circle_tool.hpp"
#include "Rect_tool.hpp"
#include "SymbolicConstraint_tool.hpp"
#include "Dimensions_tool.hpp"

#include <gtkmm/builder.h>

namespace App {

SkToolbar::SkToolbar(StageToolFunc stage_tool_func)
  : mStage_tool_func(stage_tool_func)
{
  auto refBuilder = Gtk::Builder::create_from_resource("/design/sk_workspace.ui");

  set_can_focus(false);
  
  refBuilder->get_widget<Gtk::Button>("sk_line_tool_btn")
      ->signal_clicked().connect([this](){ mStage_tool_func(new SkTools::LineTool()); });
  refBuilder->get_widget<Gtk::Button>("sk_point_tool_btn")
      ->signal_clicked().connect([this](){ mStage_tool_func(new SkTools::PointTool()); });
  refBuilder->get_widget<Gtk::Button>("sk_circle_tool_btn")
  	->signal_clicked().connect([this](){ mStage_tool_func(new SkTools::CircleTool()); });
  refBuilder->get_widget<Gtk::Button>("sk_rect_tool_btn")
  	->signal_clicked().connect([this](){ mStage_tool_func(new SkTools::RectTool()); });

  refBuilder->get_widget<Gtk::Button>("sk_coinc_tool_btn")
  	->signal_clicked().connect([this](){ mStage_tool_func(new SkTools::CoincidentTool()); });

  refBuilder->get_widget<Gtk::Button>("sk_horiz_tool_btn")
  	->signal_clicked().connect([this](){ mStage_tool_func(new SkTools::HorizontalTool()); });
  refBuilder->get_widget<Gtk::Button>("sk_vert_tool_btn")
  	->signal_clicked().connect([this](){ mStage_tool_func(new SkTools::VerticalTool()); });

  refBuilder->get_widget<Gtk::Button>("sk_perp_tool_btn")
  	->signal_clicked().connect([this](){ mStage_tool_func(new SkTools::LLPerpTool()); });
  // refBuilder->get_widget<Gtk::Button>("sk_parall_tool_btn")
  // 	->signal_clicked().connect(sigc::bind(mStage_tool_func, parallelismTool{}));

  refBuilder->get_widget<Gtk::Button>("sk_dimension_tool_btn")
  	->signal_clicked().connect([this](){ mStage_tool_func(new SkTools::DimensionTool()); });


  set_child(*refBuilder->get_widget<Gtk::Box>("sk_ws_toolbar"));
}

} // !App