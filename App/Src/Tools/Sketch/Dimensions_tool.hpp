
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef BLOOP_APP_SKDIMENSIONSTOOL_HPP_
#define BLOOP_APP_SKDIMENSIONSTOOL_HPP_

#include "Default_tool.hpp"

#include <Dialogs/Dimension_dialog.hpp>

namespace App {
namespace SkTools {

class DimensionTool : public SkTool {
private:
  std::optional<size_t> mode;
  bool pt_pending;
  Core::Sk::Point pt1, pt2; // U

  Core::Id current_handle;
  // int lookingFor { static_cast<int>(Geometry::Kind::kPoint) | static_cast<int>(Geometry::Kind::kLine) };
  std::string expression;
  double numeric_val;
  std::shared_ptr<DimensionDialog> dialog { nullptr };

public:
  DimensionTool() = default;

  std::vector<size_t> filter_hover(Core::IdVec raw_hover) const override;
  bool override_esc_signal() const override { return false; }
  void notify_coreupd(Core::WorkpieceId cwp, Core::UpdReport const& updrep) override;
  Out update_impl(InputData const& input, Core::WorkpieceId cwp, Core::Wp::Handler const& handler, 
                  Core::IdVec const& selection, Core::IdVec const& hover) override;
  std::string name() const override { return "sk_dimension"; }

  void clear() override;

private:
  Out start_dialog(Core::WorkpieceId cwp);
  Out drag(InputData const& input, Core::WorkpieceId cwp);
  std::pair<size_t, double> default_ptpt_dist_mode(size_t currMode, double dist_h, double dist_v);
  std::pair<size_t, double> mode_selection_ptpt_dist(size_t currMode, glm::dvec2 mouse_plPos);
  Out mutate(Core::WorkpieceId cwp, Core::Wp::Handler const& handler);
  Out make_constr(Core::WorkpieceId cwp, std::optional<double> new_numeric_val = std::nullopt, 
                  std::optional<Core::Wp::BranchAction> branchaction = std::nullopt);
  std::pair<std::optional<size_t>, double> mode_for_hold1(Core::Wp::Handler const& handler);
  std::pair<std::optional<size_t>, double> mode_for_hold2(Core::Wp::Handler const& handler);
};

} // !SkTools

} // !App

#endif
