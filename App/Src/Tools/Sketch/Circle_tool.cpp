
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "Circle_tool.hpp"

#include <glm/gtx/string_cast.hpp>

#include <iostream>

namespace App {
namespace SkTools {

std::vector<size_t> CircleTool::filter_hover(Core::IdVec raw_hover) const
{
  if(raw_hover.empty()) {
    return {};
  }
  if(is_dragging && !current_handle && raw_hover[0].entId.handle_num != -1) {
    return { 0 };
  }
  return {};
}

void CircleTool::notify_coreupd(Core::WorkpieceId cwp, Core::UpdReport const& updrep)
{
  if(is_dragging && !current_handle) {
    for(auto wp_diff : updrep.wp_diffs) {
      if(wp_diff.first != cwp)
        continue;
      for(auto added : wp_diff.second.added) {
        if(added.entId.kind == Core::Sk::Kind::Circle) {
          current_handle = added.entId;
          return;
        }
      }
    }
  }
}

Tool::Out CircleTool::update_impl(InputData const& input, Core::WorkpieceId cwp, Core::Wp::Handler const& handler, 
                                Core::IdVec const& selection, Core::IdVec const& hover)
{
  // Reset the tool if esc is pressed while dragging the radius
  if(is_dragging && current_handle && input.delta.key.esc == 1) {
    clear(); // Clear the tool
    return Out {
      .upddesc=Core::UpdDesc {
        .wpid=cwp,
        .branchaction=Core::Wp::BranchAction::Remove
      }
    };
  }

  // Create a new branch and add a circle in said branch
  if(input.clicked() && !is_dragging) { // Clicked and is up
    is_dragging = true;
    current_handle = std::nullopt; // New handle will be parsed from Core update notification
    
    center = glm::dvec2(input.state.objPoint.wpPos);
    return Out {
      .upddesc=Core::UpdDesc {
        .wpid=cwp,
        .branchaction=Core::Wp::BranchAction::MergeCreate,
        .action=Core::Sk::AddCircle {
          .circle=Core::Sk::Circle{.center=center, .radius=0.0}
        }
      }
    };
  } else if(input.clicked() && is_dragging) { // End the current circle
    is_dragging = false;
    current_handle = std::nullopt;

    return Out {
      .upddesc=Core::UpdDesc {
        .wpid=cwp,
        .branchaction=Core::Wp::BranchAction::Merge
      }
    };
  }

  // Drag current handle on the plane
  if(is_dragging && current_handle && input.moved()) {
    double new_rad = glm::distance(center, glm::dvec2(input.state.objPoint.wpPos));
    return Out {
      .upddesc=Core::UpdDesc{
        .wpid=cwp,
        .action=Core::Sk::SetRadius {
          .which=*current_handle,
          .newRad=new_rad
        }
      },
      .deactivate_selarea=true
    };
  }

  return Out{};
}

void CircleTool::clear()
{
  current_handle = std::nullopt;
  is_dragging = false;
}

} // !SkTools
} // !App