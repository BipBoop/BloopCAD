
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "Line_tool.hpp"

#include <glm/gtx/string_cast.hpp>

#include <iostream>

namespace App {
namespace SkTools {

std::vector<size_t> LineTool::filter_hover(Core::IdVec raw_hover) const
{
  if(raw_hover.empty()) {
    return {};
  }
  if(is_dragging && current_handle) {
    for(size_t i = 0; i < raw_hover.size(); ++i) {
      auto hover = raw_hover[i];
      if(hover.wpid.kind == Core::Wp::Kind::Sketch && hover.entId != *current_handle && Core::Sk::is_point(hover)) {
        return { i };
      }
    }
  }
  return {};
}

void LineTool::notify_coreupd(Core::WorkpieceId cwp, Core::UpdReport const& updrep)
{
  if(is_dragging && !current_handle) {
    for(auto wp_diff : updrep.wp_diffs) {
      if(wp_diff.first != cwp)
        continue;
      for(auto added : wp_diff.second.added) {
        if(added.entId.kind == Core::Sk::Kind::Line) {
          Core::Id handle = added;
          handle.entId.handle_num = 1;
          current_handle = handle;

          handle.entId.handle_num = 0;
          stich_2 = handle;
          return;
        }
      }
    }
  }
}

Tool::Out LineTool::update_impl(InputData const& input, Core::WorkpieceId cwp, Core::Wp::Handler const& handler, 
                                Core::IdVec const& selection, Core::IdVec const& hover)
{
  // Add a coincidence constraint to a hovered 
  // point then reset the tool
  if(!selection.empty()) {
    stich_1 = current_handle;
    stich_2 = selection[0];
    clear_afterstich = true;
  }

  // Reset the tool if esc is pressed while dragging
  if(is_dragging && current_handle && input.delta.key.esc == 1) {
    clear(); // Clear the tool
    return Out {
      .upddesc=Core::UpdDesc {
        .wpid=cwp,
        .branchaction=Core::Wp::BranchAction::Remove
      }
    };
  }

  // Just added a newline in polyline
  // use this update to add the coincidence constraint
  if(stich_1 && stich_2) {
    auto first = *stich_1;
    auto second = *stich_2;
    bool merge = clear_afterstich;

    if(clear_afterstich) {
      clear(); 
    } else {
      stich_1 = std::nullopt;
      stich_2 = std::nullopt;
    }

    return Out {
      .upddesc=Core::UpdDesc {
        .wpid=cwp,
        .branchaction=merge ? Core::Wp::BranchAction::Merge : Core::Wp::BranchAction::None,
        .action=Core::Sk::AddSymbolicConstraint {
          .kind=Core::Sk::Kind::PointPointCoincident,
          .constr=Core::Sk::SymbolicConstraint {
            .first=first,
            .second=second
          }
        }
      },
      .clear_selection=true
    };
  }

  // Create a new branch and add a line in said branch
  if(input.clicked()) { // Clicked and is up
    is_dragging = true;
    stich_1 = current_handle; // Remember the handle to add the coincidence constraint
    current_handle = std::nullopt; // New handle will be parsed from Core update notification
    
    glm::dvec2 pos2d = glm::dvec2(input.state.objPoint.wpPos);
    return Out {
      .upddesc=Core::UpdDesc {
        .wpid=cwp,
        .branchaction=Core::Wp::BranchAction::MergeCreate,
        .action=Core::Sk::AddLine {
          .line=Core::Sk::Line(pos2d, pos2d)
        }
      }
    };
  }

  // Drag current handle on the plane
  if(is_dragging && current_handle && input.moved()) {
    return Out {
      .upddesc=Core::UpdDesc{
        .wpid=cwp,
        .action=Core::Sk::Move {
          .delta=glm::dvec2(input.delta.objPoint.wpPos),
          .to=glm::dvec2(input.state.objPoint.wpPos),
          .which={*current_handle},
        }
      },
      .deactivate_selarea=true
    };
  }

  return Out{};
}

void LineTool::clear()
{
  current_handle = std::nullopt;
  stich_1 = std::nullopt;
  stich_2 = std::nullopt;
  is_dragging = false;
  clear_afterstich = false;
}

} // !SkTools
} // !App