
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "Rect_tool.hpp"
#include "SymbolicConstraint_tool.hpp"

#include <glm/gtx/string_cast.hpp>

#include <iostream>

namespace App {
namespace SkTools {

std::vector<size_t> RectTool::filter_hover(Core::IdVec raw_hover) const
{
  // if(raw_hover.empty()) {
  //   return {};
  // }
  // if(raw_hover[0].entId.handle_num != -1) {
  //   return raw_hover;
  // }
  return {};
}

void RectTool::notify_coreupd(Core::WorkpieceId cwp, Core::UpdReport const& updrep)
{
  for(auto wp_diff : updrep.wp_diffs) {
    if(wp_diff.first != cwp)
      continue;
    for(auto added : wp_diff.second.added) {
      if(added.entId.kind == Core::Sk::Kind::Line) {
        lines.push_back(added);
        return;
      }
    }
  }
}

Tool::Out RectTool::update_impl(InputData const& input, Core::WorkpieceId cwp, Core::Wp::Handler const& handler, 
                                Core::IdVec const& selection, Core::IdVec const& hover)
{
  // Start building the rectangle!
  if(input.clicked() && lines.size() == 0) {
    glm::dvec2 pos2d = glm::dvec2(input.state.objPoint.wpPos);
    return Out {
      .upddesc=Core::UpdDesc {
        .wpid=cwp,
        .branchaction=Core::Wp::BranchAction::Create,
        .action=Core::Sk::AddLine {
          .line=Core::Sk::Line(pos2d, pos2d)
        }
      }
    };
  }
  // Add all lines (one update call each)
  if(!lines.empty() && lines.size() < 4) {
    glm::dvec2 pos2d = glm::dvec2(input.state.objPoint.wpPos);
    return Out::make_simple(cwp, Core::Sk::AddLine {
            .line=Core::Sk::Line(pos2d, pos2d)
          });
  }
  if(!lines.empty() && n_constraints < 8) {
    switch(n_constraints++) {
    case 0: return Out::make_simple(cwp, CoincidentTool::constraint(lines[0].make_handle(1), lines[1].make_handle(0)));
    case 1: return Out::make_simple(cwp, CoincidentTool::constraint(lines[1].make_handle(1), lines[2].make_handle(0)));
    case 2: return Out::make_simple(cwp, CoincidentTool::constraint(lines[2].make_handle(1), lines[3].make_handle(0)));
    case 3: return Out::make_simple(cwp, CoincidentTool::constraint(lines[3].make_handle(1), lines[0].make_handle(0)));
    case 4: return Out::make_simple(cwp, HorizVertTool::constraint(lines[0], Core::Sk::Kind::LineHorizontal));
    case 5: return Out::make_simple(cwp, HorizVertTool::constraint(lines[1], Core::Sk::Kind::LineVertical));
    case 6: return Out::make_simple(cwp, HorizVertTool::constraint(lines[2], Core::Sk::Kind::LineHorizontal));
    case 7: 
      is_dragging = true;
      return Out::make_simple(cwp, HorizVertTool::constraint(lines[3], Core::Sk::Kind::LineVertical));
    }
  }
  if(input.clicked() && is_dragging) {
    clear();
    return Out{ 
        .upddesc=Core::UpdDesc { 
          .wpid=cwp, 
          .branchaction=Core::Wp::BranchAction::Merge }};
  }
  // Drag current handle on the plane
  if(is_dragging && input.moved()) {
    return Out {
      .upddesc=Core::UpdDesc{
        .wpid=cwp,
        .action=Core::Sk::Move {
          .delta=glm::dvec2(input.delta.objPoint.wpPos),
          .to=glm::dvec2(input.state.objPoint.wpPos),
          .which={lines[3].make_handle(1)},
        }
      },
      .deactivate_selarea=true
    };
  }

  return Out{};
}

void RectTool::clear()
{
  is_dragging = false;
  lines.clear();
  n_constraints = 0;
}

} // !SkTools
} // !App