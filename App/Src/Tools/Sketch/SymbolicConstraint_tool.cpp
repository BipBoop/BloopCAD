
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "SymbolicConstraint_tool.hpp"

#include <iostream>

namespace App {
namespace SkTools {

Tool::InitOut SymbolicConstraintTool::init()
{
  looking_for = comp_looking_for({});
  return InitOut{};
}
std::vector<size_t> SymbolicConstraintTool::filter_hover(Core::IdVec raw_hover) const
{
  for(size_t i = 0; i < raw_hover.size(); ++i) {
    auto hover = raw_hover[i];
    if(hover.wpid.kind != Core::Wp::Kind::Sketch)
      continue;
    for(size_t i = 0; i < looking_for.size(); ++i) {
      if(looking_for[i] == Core::Sk::Kind::Point && hover.entId.handle_num != -1) {
        return { i };
      } 
      if(looking_for[i] == hover.entId.kind && hover.entId.handle_num == -1) {
        return { i };
      }
    }
  }
  return { };
}
Tool::Out SymbolicConstraintTool::update_impl(InputData const& input, Core::WorkpieceId cwp, Core::Wp::Handler const& handler, 
                                              Core::IdVec const& selection, Core::IdVec const& hover)
{
  if(input.clicked() && !selection.empty()) { // Clicked and is up
    auto geometries = Core::Sk::order_geometries(selection);
    auto geometries_stripped = Core::strip_workpiece(geometries);
    looking_for = comp_looking_for(geometries_stripped);

    if(looking_for.empty()) {
      clear();
      return Out {
          .upddesc=Core::UpdDesc{
            .wpid=cwp,
            .action=make_constraint(geometries_stripped)
          },
          .clear_selection=true
      };
    } else {
      hold_ = geometries;
    }
  }
  return Out {};
}
void SymbolicConstraintTool::clear()
{
  hold_.clear();
  looking_for = comp_looking_for(Core::strip_workpiece(hold_));
}

// Concrete tools
Core::Sk::Action LLPerpTool::make_constraint(Core::SibIdVec geoms) const
{
  return Core::Sk::AddSymbolicConstraint {
            .kind=Core::Sk::Kind::LineLinePerpendicular,
            .constr=Core::Sk::SymbolicConstraint {
              .first=geoms[0],
              .second=geoms[1]
              }
          };
}
std::vector<size_t> LLPerpTool::comp_looking_for(Core::SibIdVec current) const
{
  if(current.size() < 2) {
    return { Core::Sk::Kind::Line };
  }
  return {};
}

std::vector<size_t> CoincidentTool::comp_looking_for(Core::SibIdVec current) const
{
  if(current.size() < 2) {
    return { Core::Sk::Kind::Point };
  }
  return {};
}
Core::Sk::Action CoincidentTool::make_constraint(Core::SibIdVec geoms) const
{
  return constraint(geoms[0], geoms[1]);
}
Core::Sk::Action CoincidentTool::constraint(Core::SibId first, Core::SibId second)
{
  return Core::Sk::AddSymbolicConstraint {
            .kind=Core::Sk::Kind::PointPointCoincident,
            .constr=Core::Sk::SymbolicConstraint {
              .first=first,
              .second=second
              }
          };
}

HorizVertTool::HorizVertTool(Core::Sk::Kind::inner line_kind_, Core::Sk::Kind::inner point_kind_)
  : line_kind(line_kind_)
  , point_kind(point_kind_)
{

}
std::vector<size_t> HorizVertTool::comp_looking_for(Core::SibIdVec current) const
{
  if(current.empty()) {
    return { Core::Sk::Kind::Point, Core::Sk::Kind::Line };
  } else if((current.size() == 1 && current[0].kind == Core::Sk::Kind::Line && !Core::Sk::is_point(current[0])) ||
            (current.size() == 2)) {
    return {};
  } else {
    return { Core::Sk::Kind::Point };
  }
}
Core::Sk::Action HorizVertTool::make_constraint(Core::SibIdVec geoms) const
{
  if(geoms.size() == 1) {
    return constraint(geoms[0], line_kind);
  } else if(geoms.size() == 2) {
    return constraint(geoms[0], geoms[1], point_kind);
  }
  return Core::Sk::Action{};
}
Core::Sk::Action HorizVertTool::constraint(Core::SibId line, Core::Sk::Kind::inner true_kind)
{
  return Core::Sk::AddSymbolicConstraint {
            .kind=true_kind,
            .constr=Core::Sk::SymbolicConstraint {
              .first=line,
              }
          };
}
Core::Sk::Action HorizVertTool::constraint(Core::SibId pt1, Core::SibId pt2, Core::Sk::Kind::inner true_kind)
{
  return Core::Sk::AddSymbolicConstraint {
            .kind=true_kind,
            .constr=Core::Sk::SymbolicConstraint {
              .first=pt1,
              .second=pt2
            }
          };
}
} // !SkTools
} // !App