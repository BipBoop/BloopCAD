
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "Dimensions_tool.hpp"

#include <Core/Print/IDOstream.hpp>

namespace App {
namespace SkTools {

std::vector<size_t> DimensionTool::filter_hover(Core::IdVec raw_hover) const
{
  for(size_t i = 0; i < raw_hover.size(); ++i) {
    auto hover = raw_hover[i];
    if(hover.wpid.kind != Core::Wp::Kind::Sketch)
      continue;
    if(!mode && Core::Sk::is_geom(hover)) {
      return { i };
    }
  }
  return { };
}
void DimensionTool::notify_coreupd(Core::WorkpieceId cwp, Core::UpdReport const& updrep)
{
  if(!mode)
    return;

  for(auto wp_diff : updrep.wp_diffs) {
    if(wp_diff.first != cwp)
      continue;
    for(auto added : wp_diff.second.added) {
      // Add the yoink the constraint that was just added
      // by core
      if(added.entId.kind == *mode) {
        Core::Id handle = added;
        handle.entId.handle_num = 0;
        current_handle = handle;
        return;
      }
    }
  }
}
Tool::Out DimensionTool::update_impl( InputData const& input, Core::WorkpieceId cwp, Core::Wp::Handler const& handler, 
                                      Core::IdVec const& selection, Core::IdVec const& hover)
{
  if(mode && input.clicked() && hover.empty()) { // Clicked on void
    return start_dialog(cwp);
  } else if(input.clicked()) {
    hold_ = Core::Sk::order_geometries(selection);
    return mutate(cwp, handler);
  } else if(dialog && dialog->confirm_) {
    auto exp = dialog->expression();
    auto val = dialog->value();
    auto hand = current_handle;
    clear();
    return Out {
      .upddesc=Core::UpdDesc{
        .wpid=cwp,
        .branchaction=Core::Wp::BranchAction::Merge,
        .action=Core::Sk::SetConstraintValue {
          .which=hand,
          .expression=exp,
          .value=val
        },
      },
      .clear_selection=true
    };
  } else if(!dialog) {
    return drag(input, cwp);
  }
  return Out{};
}
Tool::Out DimensionTool::drag(InputData const& input, Core::WorkpieceId cwp)
{
  if(mode && input.moved()) {
    if(*mode == Core::Sk::Kind::PointPointHdist || *mode == Core::Sk::Kind::PointPointVdist) {
      auto choice = mode_selection_ptpt_dist(*mode, glm::dvec2(input.state.objPoint.wpPos));
      if(*mode != choice.first) {
        mode = choice.first;
        return make_constr(cwp, choice.second, Core::Wp::BranchAction::RemoveCreate);
      }
    }

    return Out {
      .upddesc=Core::UpdDesc{
        .wpid=cwp,
        .action=Core::Sk::Move {
          .delta=glm::dvec2(input.delta.objPoint.wpPos),
          .to=glm::dvec2(input.state.objPoint.wpPos),
          .which={current_handle},
        }
      },
      .deactivate_selarea=true
    };
  }
  return Out{};
}

std::pair<size_t, double> DimensionTool::default_ptpt_dist_mode(size_t currMode, double dist_h, double dist_v)
{
	if(currMode == Core::Sk::Kind::PointPointHdist) {
    return { Core::Sk::Kind::PointPointHdist, dist_h };
  } else if(currMode == Core::Sk::Kind::PointPointVdist) {
    return { Core::Sk::Kind::PointPointVdist, dist_v };
  }
  
  if(dist_h > dist_v) {
    return { Core::Sk::Kind::PointPointHdist, dist_h };
  } else {
    return { Core::Sk::Kind::PointPointVdist, dist_v };
  }
}
std::pair<size_t, double> DimensionTool::mode_selection_ptpt_dist(size_t currMode, glm::dvec2 mouse_plPos)
{
	// Extreme positions of both points
	double max_x = std::max(pt1.x, pt2.x);
	double max_y = std::max(pt1.y, pt2.y);
	double min_x = std::min(pt1.x, pt2.x);
	double min_y = std::min(pt1.y, pt2.y);

	// where the mouse is in relation to the points
	// x_mid => x in the middle (not actual middle but between extremes)
	bool x_mid = mouse_plPos.x >= min_x && mouse_plPos.x <= max_x;
	bool y_mid = mouse_plPos.y >= min_y && mouse_plPos.y <= max_y;

	// both distances between points to make decision
	// the largest distance will dictate the default
	// orientation of the distance
	double dist_h = max_x - min_x;
	double dist_v = max_y - min_y;

	if(x_mid && y_mid) { // Mouse is between points, default
		return default_ptpt_dist_mode(currMode, dist_h, dist_v);
	} else if(!x_mid && !y_mid) { // Mouse is outside bounds of points, default
		return default_ptpt_dist_mode(currMode, dist_h, dist_v);
	} else if(!x_mid) { // On the right or the left
		return { Core::Sk::Kind::PointPointVdist, dist_v };
	} else if(!y_mid) { // Above or below
		return { Core::Sk::Kind::PointPointHdist, dist_h };
	}

  return default_ptpt_dist_mode(currMode, dist_h, dist_v);
}

Tool::Out DimensionTool::mutate(Core::WorkpieceId cwp, Core::Wp::Handler const& handler)
{
  if(hold_.size() == 1) {
    std::tie(mode, numeric_val) = mode_for_hold1(handler);
  } else if(hold_.size() == 2) {
    std::tie(mode, numeric_val) = mode_for_hold2(handler);
  } else {
    mode = std::nullopt;
    std::cerr<<"Unexepcted hold_ of size "<<hold_.size()<<" in "<<__FUNCTION__<<std::endl;
  }
  if(!mode) {
    return Out::make_branchAction(cwp, Core::Wp::BranchAction::Remove);
  }

  // Mode is valid
  // Remove the previous branch and add a new one
  // with the constraint
  return make_constr(cwp, numeric_val, Core::Wp::BranchAction::Create);
}
Tool::Out DimensionTool::make_constr(Core::WorkpieceId cwp, std::optional<double> new_numeric_val, std::optional<Core::Wp::BranchAction> branchaction)
{
  if(new_numeric_val) {
    numeric_val = *new_numeric_val;
  }
  return Out {
    .upddesc=Core::UpdDesc{
      .wpid=cwp,
      .branchaction = branchaction ? *branchaction : Core::Wp::BranchAction::None,
      .action=Core::Sk::AddMeasureConstraint{
        .kind=static_cast<Core::Sk::Kind::inner>(*mode),
        .constr=Core::Sk::MeasureConstraint{
          .first=hold_[0].entId,
          .second=hold_.size() == 1 ? Core::SibId{} : hold_[1].entId,
          .value=numeric_val,
          .expression=std::to_string(numeric_val)
        }
      }
    }
  };
}
std::pair<std::optional<size_t>, double> DimensionTool::mode_for_hold1(Core::Wp::Handler const& handler)
{
  if(Core::Sk::is_point(hold_[0])) {
    pt_pending = true;
    return { std::nullopt, 0.0 };
  }
  switch(hold_[0].entId.kind) {
  case Core::Sk::Kind::Line: {
    auto line = handler.get<Core::Sk::Line>(hold_[0]);
    if(!line) {
      std::cerr<<"Error, could not find line in dimension tool :("<<__LINE__<<std::endl;
      return { std::nullopt, 0.0 };
    }
    return { Core::Sk::Kind::LineLength, line->length() };
  }
  case Core::Sk::Kind::Circle: {
    auto circle = handler.get<Core::Sk::Circle>(hold_[0]);
    if(!circle) {
      std::cerr<<"Error, could not find circle in dimension tool :("<<__LINE__<<std::endl;
      return { std::nullopt, 0.0 };
    }

    return { Core::Sk::Kind::CircleDiameter, circle->radius * 2 };
  }
  default: return { std::nullopt, 0.0 };
  }
}
std::pair<std::optional<size_t>, double> DimensionTool::mode_for_hold2(Core::Wp::Handler const& handler)
{
  using namespace Core::Sk;
  if(is_point(hold_[0]) && is_point(hold_[1])) {
    auto point1 = handler.get<Point>(hold_[0]);
    auto point2 = handler.get<Point>(hold_[1]);
    if(!point1) {
      std::cerr<<"Error could not find point1 in dimension tool :("<<__LINE__<<std::endl;
      return { std::nullopt, 0.0 };
    }
    if(!point2) {
      std::cerr<<"Error could not find point2 in dimension tool :("<<__LINE__<<std::endl;
      return { std::nullopt, 0.0 };
    }
    pt1 = *point1;
    pt2 = *point2;
    std::cout<<"Pointpointhdist!!!\n";
    return default_ptpt_dist_mode(Core::invalid_index(), std::abs(pt1.x-pt2.x), std::abs(pt1.y-pt2.y));
  }
  if(is_point(hold_[0]) && is_line(hold_[1])) {
    return { Kind::PointLineDist, 0.0 };
  }
  if(is_point(hold_[0]) && is_circle(hold_[1])) {
    auto point = handler.get<Point>(hold_[0]);
    auto circle = handler.get<Circle>(hold_[0]);

    if(!point) {
      std::cerr<<"Error, could not find point in dimension tool :("<<__LINE__<<std::endl;
      return { std::nullopt, 0.0 };
    }
    if(!circle) {
      std::cerr<<"Error, could not find circle in dimension tool :("<<__LINE__<<std::endl;
      return { std::nullopt, 0.0 };
    }

    if(hold_[0].is_handle_of(hold_[1])) {
      hold_[0] = hold_[1];
      hold_.resize(1);

      return { Kind::CircleRadius, circle->radius };
    }

    return { Kind::PointCircleDist, 0.0 };
  }

  // Todo poke the sketch to check if two lines
  // are parallel and add LineLineDist instead
  if(is_line(hold_[0]) && is_line(hold_[1])) {
    return { Kind::LineLineAngle, 0.0 };
  }

  return { std::nullopt, 0.0 };
}
Tool::Out DimensionTool::start_dialog(Core::WorkpieceId cwp)
{
  // return Out::make_branchAction(cwp, Core::Wp::BranchAction::Merge);
  dialog = std::make_shared<DimensionDialog>(numeric_val);
  return Out {
    .dialog=dialog
  };
}

void DimensionTool::clear()
{
  mode = std::nullopt;
  current_handle = Core::Id{};
  expression = "";
  dialog = nullptr;
  hold_.clear();
}

} // !SkTools
} // !App