
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "Default_tool.hpp"

#include <Core/Print/IDOstream.hpp>

#include <glm/glm.hpp>
#include <glm/geometric.hpp>

namespace App {
namespace SkTools {

std::vector<size_t> DefaultTool::filter_hover(Core::IdVec raw_hover) const
{
  std::vector<size_t> out;
  for(size_t i = 0; i < raw_hover.size(); ++i) {
    auto hover = raw_hover[i];
    if(hover.wpid.kind != Core::Wp::Kind::Sketch)
      continue;
    if(hover.entId.kind == Core::Sk::Kind::OriginLine || hover.entId.kind == Core::Sk::Kind::OriginPoint)
      continue;
    out.push_back(i);
  }
  return out;
}

Tool::Out DefaultTool::update_impl( InputData const& input, Core::WorkpieceId cwp, Core::Wp::Handler const& handler, 
                                    Core::IdVec const& selection, Core::IdVec const& hover)
{
  // Start a drag
  if(input.state.mousePressed && dragging == Not && hover.size() != 0) {
    if(std::find(selection.begin(), selection.end(), hover[0]) != selection.end()) { // Dragged entity is selected
      hold_ = selection;
    } else { // Dragging a hovered thingy
      hold_ = hover;
    }
    dragging = DragLevel::Start;
    // Create a branch so that all movements of a drag can 
    // be squished into a single action latter
    return Out{ .upddesc=Core::UpdDesc{ .wpid=cwp, .branchaction=Core::Wp::BranchAction::Create }};
  }
  
  if(dragging != DragLevel::Not && 
                      input.state.mousePressed && input.delta.mousePos != glm::dvec2(0.0)) {
    // Continue drag
    dragging = DragLevel::Yes;

    // Special case of dragging a circle by it's radius
    if(hold().size() == 1 && hold()[0].entId.kind == Core::Sk::Kind::Circle && hold()[0].entId.handle_num == -1) {
      auto circle = handler.get<Core::Sk::Circle>(hold()[0]);
      if(!circle) {
        std::cerr<<"This is terribly bad "<<__FUNCTION__<<",  "<<__FILE__<<",  "<<__LINE__<<std::endl;
        return Out{};
      }
      std::cout<<"drag radius of "<<hold()[0]<<"\n";
      return Out {
        .upddesc=Core::UpdDesc {
          .wpid=cwp,
          .action=Core::Sk::SetRadius {
            .which=hold()[0].entId,
            .newRad=glm::distance(glm::dvec2(input.state.objPoint.wpPos), circle->center)
          }
        },
        .deactivate_selarea=true
      };
    }

    return Out {
      .upddesc=Core::UpdDesc{
        .wpid=cwp,
        .action=Core::Sk::Move {
          .delta=glm::dvec2(input.delta.objPoint.wpPos),
          .to=glm::dvec2(input.state.objPoint.wpPos),
          .which=Core::strip_workpiece(hold()),
        }
      },
      .deactivate_selarea=true
    };
  } else if(dragging && !input.state.mousePressed) {
    // End drag, merge action
    clear();
    return Out{ .upddesc=Core::UpdDesc{ .wpid=cwp, .branchaction=Core::Wp::BranchAction::Merge }};
  }

  if(input.delta.key.del && !selection.empty()) {
    clear();
    return Out {
      .upddesc=Core::UpdDesc{
        .wpid=cwp,
        .action=Core::Sk::Remove {
          .which=Core::strip_workpiece(selection)
        }
      },
    };
  }

  return Out{};
}

void DefaultTool::clear()
{
  dragging = Not;
  hold_.clear();
}

} // !SkTools
} // !App