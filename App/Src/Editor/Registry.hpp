
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef BLOOP_REGISTRY_HPP_
#define BLOOP_REGISTRY_HPP_

#include "Selection.hpp"
#include <Graphics_utils/Camera.hpp>
#include <Core/Maths/BoundingBox.hpp>
#include <Core/Curve/Line.hpp>
#include <Core/Discrete/Mesh.hpp>
#include <Core/Id.hpp>

#include <Window/MeshRepo.hpp>
#include <Config/Config.hpp>

#include <map>
#include <set>
#include <optional>

namespace App {

struct AABBNode {
  Core::dBox box;
  std::optional<Core::Id> entity { std::nullopt };

  AABBNode() = default;
  AABBNode(Core::dBox box_, Core::Id entity_);
  AABBNode(Core::dBox box_);

  bool is_leaf() const;

  // Tree stuff
  size_t right_ind { Core::invalid_index() };
  size_t left_ind { Core::invalid_index() }; 
  size_t parent_ind { Core::invalid_index() };

  // Dealocated node linked list within the vector
  size_t next_del { Core::invalid_index() };
};
struct AABBTree {
  // https://skypjack.github.io/2019-05-06-ecs-baf-part-3/
  // https://www.azurefromthetrenches.com/introductory-guide-to-aabb-tree-collision-detection/

  // Root node is node 0
  std::vector<AABBNode> nodes;
  size_t first_dealocated;
  size_t n_dealocated;
  size_t root_node { Core::invalid_index() };

  size_t root() const { return root_node; }

  void add_entity(Core::dBox box, Core::Id id);
  void remove(Core::Id id);

  Core::IdVec touched(glm::dvec2 pos) const;
  Core::IdVec traversed(Core::Crv::_2d::Line line) const;
  Core::IdVec contained(Core::dBox box) const;
  Core::IdVec touched(Core::dBox box) const;
  Core::IdVec all() const;

  size_t create_node(AABBNode node);
  size_t find_bestSibling_heur(Core::dBox box);
  std::pair<AABBNode, size_t> get_node(size_t ind) { return { nodes[ind], ind }; };
  void fix_upward(size_t from);

  template<typename fun_t>
  Core::IdVec match(fun_t match_fun) const
  {
    if(nodes.empty())
      return {};

    Core::IdVec found_boxes;
    std::stack<size_t> to_explore;
    to_explore.push(root());

    while(!to_explore.empty()) {
      AABBNode curr_node = nodes[to_explore.top()];
      to_explore.pop();
      

      if(!match_fun(curr_node))
        continue;
    
      if(curr_node.is_leaf()) {
        found_boxes.push_back(*curr_node.entity);
      } else {
        to_explore.push(curr_node.left_ind);
        to_explore.push(curr_node.right_ind);
      }
    }
    return found_boxes;
  }
  template<class It_t>
  Core::dBox get_screenArea(It_t it, It_t end)
  {
    Core::dBox area;
    while(it != end) {
      Core::dBox entity_box;
      for(auto node : nodes) {
        if(node.is_leaf() && *node.entity == *it) {
          entity_box = node.box;
        }
      }

      area = area.combine(entity_box);
      it++;
    }
    return area;
  }
  void print() const;
};

class Registry {
public:
  AABBTree aabbTree;
  std::map<Core::WorkpieceId, glm::dmat4> workpiece_transform;
  std::map<Core::WorkpieceId, CameraSnap> workpiece_camSnap;
  Core::IdVec selection, hover;

  Registry() = default;
  Registry(Core::WorkpieceId workpieceId);

  void build_aabbTree(Core::IdVec const& ids, MeshRepo const& meshes, Config const& configs, Camera const& cam);
  void update_aabbTree(Core::IdVec const& modIds, MeshRepo const& meshes, Config const& configs, Camera const& cam);
  void fill_aabbTree(Core::IdVec const& modIds, MeshRepo const& meshes, Config const& configs, Camera const& cam);

  Core::dBox mesh_to_box(Core::Mesh const& mesh, Camera const& cam);

  Core::IdVec get_hoveredBoxes(SelectionArea selArea);
  Core::IdVec confirm_hover(Core::IdVec maybids, SelectionArea selArea);

  glm::dmat4 workpieceTransform(Core::WorkpieceId wpid) const;

  static bool is_origin(Core::Id id);
  static Core::IdVec order_hoverPriority(Core::IdVec src);
};

} // !App

#endif
