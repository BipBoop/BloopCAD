
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "Editor.hpp"

#include "ContextMenu.hpp"
#include "WpInvestigation.hpp"

#include <Tools/Part/Default_tool.hpp>
#include <Tools/Sketch/Default_tool.hpp>

#include <Core/Print/IDOstream.hpp>
#include <Core/Id.hpp>

#include <Graphics_utils/GLCall.hpp>

#include <vector>

// #define SHOW_BOUNDINGBOXES

namespace App {

Editor::Editor(Gtk::Window* parent)//(Core::WorkpieceId root_workpieceId)
  : mCamera(CameraSnap(), CameraParams(glm::radians(20.0)), glm::dvec2(1.0, 1.0))
  , mPrtToolbar([this](Tool* tool) { set_tool(tool); })
  , mSkToolbar([this](Tool* tool) { set_tool(tool); })
  , mParentWindow(parent)
{
  request_bbmapUpdate();
  set_orientation(Gtk::Orientation::VERTICAL);

  // mouseDelta of input data must be zeroed when consumed by update
  auto mouseMoveController = Gtk::EventControllerMotion::create();
  mouseMoveController->signal_motion().connect([this](double x, double y){
    mInputState.mousePos = glm::dvec2(x, y); 
  }, true);
  mouseMoveController->signal_enter().connect([this](double x, double y){
    mSelArea.activate();
  });
  mouseMoveController->signal_leave().connect([this](){
    mSelArea.deactivate();
  });
  mEffectsLayer.add_controller(mouseMoveController);

  auto mouseClickController = Gtk::GestureClick::create();
  mouseClickController->signal_pressed().connect([this](int n_press, double x, double y){
  mInputState.mousePos = glm::dvec2(x, y);
    mInputState.mousePressed += n_press;
  }, false);
  mouseClickController->signal_released().connect([this](int n_press, double x, double y) {
  mInputState.mousePos = glm::dvec2(x, y); 
  // mInputState.mousePressed -= n_press;
    mInputState.mousePressed = 0;
  }, false);
  mEffectsLayer.add_controller(mouseClickController);

  auto scrollController = Gtk::EventControllerScroll::create();
  scrollController->set_flags(Gtk::EventControllerScroll::Flags::VERTICAL | Gtk::EventControllerScroll::Flags::HORIZONTAL);
  scrollController->signal_scroll().connect([this](double x, double y) {
  mInputState.mouseScroll += glm::dvec2(x, -y);
  return false;
  }, true);
  add_controller(scrollController);

  // Connect gl area signals
  mViewport.signal_realize().connect(sigc::mem_fun(*this, &Editor::realize));
  // Important that the unrealize signal calls our handler to clean up
  // GL resources _before_ the default unrealize handler is called (the "false")
  mViewport.signal_unrealize().connect(sigc::mem_fun(*this, &Editor::unrealize), false);
  mViewport.signal_render().connect(sigc::mem_fun(*this, &Editor::render), false);
  mViewport.signal_resize().connect(sigc::mem_fun(*this, &Editor::resize), false);
  mViewport.set_auto_render(false);
  mViewport.set_required_version(3, 2);
  mViewport.set_can_target(true);

  mEffectsLayer.set_draw_func(sigc::mem_fun(*this, &Editor::draw_effects));

  mToolLabel.set_valign(Gtk::Align::END);
  mToolLabel.set_halign(Gtk::Align::START);
  mToolLabel.set_can_target(false);

  mViewportOverlay.set_vexpand(true);
  mViewportOverlay.set_hexpand(true);
  mViewportOverlay.add_overlay(mViewport);
  mViewportOverlay.add_overlay(mEffectsLayer);
  mViewportOverlay.add_overlay(mNavBar);
  mViewportOverlay.add_overlay(mToolLabel);
  mViewportFrame.set_hexpand(true);
  mViewportFrame.set_size_request(600);
  mViewportFrame.set_child(mViewportOverlay);
  setup_navbar();
  setup_toolbar();

  mPanned.set_shrink_start_child(true);
  mPanned.set_shrink_end_child(false);
  mPanned.set_start_child(mSidebarFrame);
  mPanned.set_end_child(mViewportFrame);

  append(mToolbar);
  append(mPanned);

  show();
  set_tool(new PrtTools::DefaultTool());
}
void Editor::set_update_period(size_t period_ms)
{
  mUndoerRedoer = UndoerRedoer(mConfigs.kUndoRedo_firstTimer, mConfigs.kUndoRedo_timer, period_ms);
}

void Editor::set_root(Core::WorkpieceId root, Core::Wp::Handler const& handler)
{
  mRegistry = Registry(root);
  mRoot = root;
  mWorkpiece = std::stack<Core::WorkpieceId>(); // Clear
  focus(root, true);
  notify_corediff(Core::UpdReport{.wp_diffs={{root, Core::Wp::Diff{.added=handler.constituents_of(root)}}}}, handler);
  request_bbmapUpdate();
  mMustRebuildSidebar = true;
}
void Editor::setup_toolbar()
{
  mToolbar.add(mPrtToolbar);
  mToolbar.add(mSkToolbar);
  mToolbar.set_visible_child(mPrtToolbar);
}
void Editor::setup_sidebar()
{
  std::cout<<"Setup sidebar\n";
  mColumnView = Gtk::ColumnView();
	mSidebar.set_child(mColumnView);
	mSidebar.set_policy(Gtk::PolicyType::AUTOMATIC, Gtk::PolicyType::AUTOMATIC);
  mSidebar.set_expand();

  auto root = create_model();

  mTreeListModel = Gtk::TreeListModel::create(root,
    sigc::mem_fun(*this, &Editor::create_model), false, true);
  mTreeSelection = Gtk::MultiSelection::create(mTreeListModel);
  mColumnView.set_model(mTreeSelection);
  mColumnView.add_css_class("data-table");

  mTreeSelection->signal_selection_changed().connect([this](guint position, guint n_items) {
    select_fromTree();
  });

  add_sidebar_columns();
  mSidebar.set_child(mColumnView);

  mSidebarFrame.set_hexpand(false);
	mSidebarFrame.set_child(mSidebar);
}
void Editor::setup_navbar(/*std::shared_ptr<Gtk::Builder> refBuilder*/)
{
  mNavBar.orbit_btn.signal_clicked().connect([this](){
    mNavTool.tool = Nav::OrbitData{};
  });
  mNavBar.zoom_btn.signal_clicked().connect([this](){
    mNavTool.tool = Nav::ZoomData{};
  });
  mNavBar.pan_btn.signal_clicked().connect([this](){
    mNavTool.tool = Nav::PanData{};
  });
  mNavBar.default_btn.signal_clicked().connect(sigc::mem_fun(*this, &Editor::set_defaultTool));
  mNavBar.exit_btn.signal_clicked().connect(sigc::bind(sigc::mem_fun(*this, &Editor::unfocus), false));
}

void Editor::set_defaultTool()
{
  if(mWorkpiece.top().kind == Core::Wp::Kind::Part) {
    set_tool(new PrtTools::DefaultTool);
  } else if(mWorkpiece.top().kind == Core::Wp::Kind::Sketch) {
    set_tool(new SkTools::DefaultTool);
  }
}
void Editor::set_tool(Tool* tool_)
{
  delete mTool;
  mTool = tool_;

  auto init = mTool->init();
  setup_tooldialog(init.dialog);

  mRegistry.selection = hover_filter_by_tool(mRegistry.selection);
  mToolLabel.set_label(mTool->name());
  request_render();
}
void Editor::setup_tooldialog(std::shared_ptr<ToolDialog> dialog)
{
  if(dialog) {
    dialog->set_transient_for(*mParentWindow);
    dialog->show();
  }
}
void Editor::gen_currInputData(KeyState keyState, Core::Wp::Handler const& handler)
{
  mInputState.key = keyState;
  // Todo transform ray
  mInputState.objPoint = point(mRegistry, mWorkpiece.top(), mCamera.cast_ray(mInputData.state.mousePos), handler);
  

  mInputData = InputData::make_compare(mPrevInputState, mInputState);
  mPrevInputState = mInputState;
}
bool Editor::update_navigation()
{
  if(mNavTool.has_tool_only() && mInputData.delta.key.esc == 1) {
    mNavTool.tool = std::nullopt;
  }
  // Already launched camera moves have priority
  if(mCamMove) {
    bool nullify = false;
    CameraSnap snap;

    if(mInputData.state.key.enter) { // Skip camera move
      snap = mCamMove->end;
      nullify = true;
    } else {
      std::tie(snap, nullify) = mCamMove->current();  
    }

    set_camera(Camera(mCamera, snap));
    if(nullify) {
      mCamMove = std::nullopt;
    }
    return true;
  }

  std::optional<CameraSnap> maybe_newCamSnap { std::nullopt };
  std::tie(maybe_newCamSnap, mNavTool) = Nav::update_navigation(mNavTool, mInputData, mCamera, mConfigs.kNavConfig);

  if(maybe_newCamSnap) {
    set_camera(Camera(mCamera, *maybe_newCamSnap)); // Also queues render
    return true;
  }
  return false;
}
std::optional<Core::UpdDesc> Editor::gen_update(Core::Wp::Handler const& handler, MeshRepo const& meshes)
{
  // Update sidebar ui
  if(mMustRebuildSidebar) {
    mSidebarData = BuildSequence(mRoot, handler);
    setup_sidebar();
    mMustRebuildSidebar = false;
  }
  
  // Manage undo/redo
  UndoerRedoer::Option undoredo = mUndoerRedoer.what_do(mInputData.state.key.key == GDK_KEY_z && mInputData.state.key.ctrlMod, 
                                                        mInputData.state.key.key == GDK_KEY_y && mInputData.state.key.ctrlMod);
  
  if(undoredo == UndoerRedoer::Option::Undo) {
    std::cout<<"Undo!\n";
    return Core::UpdDesc {.wpid=mWorkpiece.top(), .action=Core::Wp::Undo{} };
  } else if(undoredo == UndoerRedoer::Option::Redo) {
    std::cout<<"Redo!\n";
    return Core::UpdDesc {.wpid=mWorkpiece.top(), .action=Core::Wp::Redo{} };
  }

  if(mContextAction) {
    (*mContextAction)(); // Execute context action
    mContextAction = std::nullopt;
  }

#ifndef SHOW_BOUNDINGBOXES
  if(!mSelArea.settled) 
#endif
  {
    mEffectsLayer.queue_draw();
  }

  if(mNavTool.has_any()) { // No need to update anything else!
    return std::nullopt;
  }

  manage_hoverSelection(meshes);

  Tool::Out toolout = mTool->update(mInputData, mWorkpiece.top(), handler, mRegistry.selection, mRegistry.hover);
  mClearsel = toolout.clear_selection;
  if(toolout.newTool) {
    set_tool(toolout.newTool);
  }
  setup_tooldialog(toolout.dialog);
  
  mSelArea.set_active(!toolout.deactivate_selarea);

  // The update area for selection and hover only as the action is not done in here

  return toolout.upddesc;
}


void Editor::manage_hoverSelection(MeshRepo const& meshrepo)
{
  // Mouse is released: selection becomes hover and hover is cleared
  if(mInputData.clicked()) {
    select_hovered(mInputData.state.key.ctrlMod);
  }
  if(mInputData.state.key.esc || mClearsel) {
    unselect();
  }

  // Update sel area
  mSelArea.mutate(  mInputData.state.mousePressed, mInputData.state.key.shiftMod, 
                    mInputData.state.mousePos, mInputData.delta.mousePos, mCamera.viewport.y);
  
  if(mSelArea.active()) {
    auto id_dist = hover_filter_by_mesh(Registry::order_hoverPriority(mRegistry.get_hoveredBoxes(mSelArea)), meshrepo);

    Core::IdVec new_hover = Core::select_from_vec(id_dist.first, hover_filter_by_dist(id_dist.second, mTool->filter_hover(id_dist.first)));
    std::sort(new_hover.begin(), new_hover.end());
    if(new_hover != mRegistry.hover) {
      mRegistry.hover = std::move(new_hover);
      request_render();
    }
  }
}
void Editor::select_hovered(bool keepsel)
{
  Core::IdVec new_sel;
  if(keepsel) {
    new_sel = mRegistry.selection;
  }
  for(auto id : mRegistry.hover) {
    new_sel.push_back(id);
  }
  for(auto id : mTool->hold()) {
    new_sel.push_back(id);
  }

  std::sort(new_sel.begin(), new_sel.end());
  auto it = std::unique(new_sel.begin(), new_sel.end());
  new_sel.erase(it, new_sel.end());

  if(mRegistry.selection != new_sel) {
    mRegistry.selection = std::move(new_sel);
    request_render();
  }
  mRegistry.hover.clear();
}
void Editor::unselect()
{
  if(!mRegistry.selection.empty())
    request_render();
  mRegistry.selection.clear();
  mTreeSelection->unselect_all();
}

template<typename T>
void combine_unique(std::vector<T>& lhs, std::vector<T> rhs) 
{
  if(rhs.empty()) {
    return;
  }
  if(lhs.empty()) {
    lhs = std::move(rhs);
  }
  for(int i = 0; i < rhs.size(); ++i) {
    if(std::find(lhs.begin(), lhs.end(), rhs[i]) == lhs.end()) {
      lhs.push_back(rhs[i]);
    } 
  }
}
void Editor::select_fromTree()
{
  auto model = mTreeSelection->get_model();
  int n_items = model->get_n_items();

  Core::IdVec potential_selection;
  for(int i = 0; i < n_items; ++i) {
    if(!mTreeSelection->is_selected(i))
      continue;

    auto row = mTreeListModel->get_row(i);
    if(!row)
      continue;
    auto col = std::dynamic_pointer_cast<SidebarColumn>(row->get_item());
    if(!col)
      continue;
    size_t dataInd = col->data_ind;
    combine_unique(potential_selection, ids_fromTree(i));
  }
  mRegistry.selection = hover_filter_by_tool(potential_selection);
  request_render();
}
void Editor::hover_fromTree(size_t index)
{
  Core::IdVec potential_hovers = ids_fromTree(index);
  mRegistry.hover = hover_filter_by_tool(potential_hovers);
  request_render();
}
std::vector<Core::Id> Editor::ids_fromTree(size_t index)
{
  std::vector<Core::Id> out;
  std::stack<size_t> to_explore;
  to_explore.push(index);

  while(!to_explore.empty()) {
    BuildNode const& currNode = mSidebarData.nodes[to_explore.top()];
    to_explore.pop();
    for(auto sub_step : currNode.children_steps) {
      to_explore.push(sub_step);
    }
    auto maybe_workpiece = currNode.workpiece();
    if(maybe_workpiece) {
      out.push_back(Core::Id{.wpid=*maybe_workpiece});
      continue;
    }
    auto maybe_id = currNode.geometry();
    if(maybe_id) {
      out.push_back(*maybe_id);
    }
  }
  return out;
}
void Editor::unhover_all()
{
  mRegistry.hover.clear();
  request_render();
}

std::pair<Core::IdVec, std::vector<double>> Editor::hover_filter_by_mesh(Core::IdVec ids, MeshRepo const& repo) const
{
  if( mSelArea.kind == SelectionArea::Kind::BoxInc || 
      mSelArea.kind == SelectionArea::Kind::Line) {
      
      std::cerr<<"Cant filter by mesh boxinc or line selarea"<<std::endl;
    }
  if(mSelArea.kind == SelectionArea::Kind::None) {
    return {};
  }
  if(mSelArea.kind == SelectionArea::Kind::BoxEx) {
    return { ids, std::vector<double>(ids.size(), 0.0) };
  }

  // Kind == point
  std::pair<Core::IdVec, std::vector<double>> out;
  Core::WorkpieceId curr_wpid;
  glm::dmat4 transform;
  for(auto id : ids) {
    if(curr_wpid != id.wpid) {
      curr_wpid = id.wpid;
      transform = mRegistry.workpieceTransform(curr_wpid);
    }
    
    auto dist = repo.mesh_intersection(mCamera, transform, id, mConfigs, mSelArea.start);
    if(dist) {
      out.first.push_back(id);
      out.second.push_back(*dist);
    }
  }

  return out;
}
std::vector<size_t> Editor::hover_filter_by_dist(std::vector<double> const& src, std::vector<size_t> pre_selection) const
{
  if(src.empty() || pre_selection.empty()) {
    return {};
  }

  if(mSelArea.kind == SelectionArea::Kind::Point) {
    size_t min_ind = 0;
    double min_dist = std::numeric_limits<double>::max();
    for(auto index : pre_selection) {
      if(src[index] < min_dist) {
        min_ind = index;
        min_dist = src[index];
      }
    }
    return { min_ind };
  }

  // Only point selection has a single selection constraint
  std::vector<size_t> out(src.size());
  for(size_t i = 0; i < src.size(); ++i) {
    out[i] = i;
  }
  return out;
}
Core::IdVec Editor::hover_filter_by_tool(Core::IdVec src) const
{
  return Core::select_from_vec(src, mTool->filter_hover(src));
}

void Editor::notify_corediff(Core::UpdReport upd, Core::Wp::Handler const& handler)
{
  bool focus_next = upd.toolmsg == "focus_next"; // Focus on added workpiece
  bool find_focus = focus_next;
  std::optional<Core::WorkpieceId> to_focus = std::nullopt;

  for(auto mod : upd.wp_buildSequence_mod) {
    if(mod.first == mRoot) {
      mMustRebuildSidebar = true;
    }
  }

  for(auto diff : upd.wp_diffs) {
    if(!diff.second.empty()) {
      request_bbmapUpdate();
      request_render();
    }
    for(auto id : diff.second.added) {
      if(id.is_workpiece()) {
        auto transform = handler.transform_descendant_to_ancestor(id.wpid, mRoot);
        if(!transform) {
          std::cerr<<"No transformation to root from "<<id.wpid<<std::endl;
          continue;
        }
        std::cout<<"Transform descendant to ancestor "<<glm::to_string(*transform)<<"\n";
        mRegistry.workpiece_transform[id.wpid] = *transform;
      }
    }
    for(auto id : diff.second.removed) {
      auto found = std::find(mRegistry.selection.begin(), mRegistry.selection.end(), id);
      if(found != mRegistry.selection.end()) {
        mRegistry.selection.erase(found);
      }
      found = std::find(mRegistry.hover.begin(), mRegistry.hover.end(), id);
      if(found != mRegistry.hover.end()) {
        mRegistry.hover.erase(found);
      }
    }
    if(find_focus && handler.parent(diff.first) == mWorkpiece.top()) {
      if(to_focus) { // Already found something to fucus,, fishy
        std::cerr<<"Multiple focus targets results in no focus being generated"<<std::endl;
        to_focus = std::nullopt;
        find_focus = false;
      } else {
        to_focus = diff.first;
      }
    }
  }

  if(to_focus) {
    focus(*to_focus);
  }
  if(focus_next && !find_focus) {
    std::cerr<<"Could not focus on new workpiece, too many options"<<std::endl;
  } else if(focus_next && !to_focus) {
    std::cerr<<"Could not find new workpiece to focus"<<std::endl;
  }

  mTool->notify_coreupd(mWorkpiece.top(), upd);
}

CameraSnap default_cameraSnap(Core::WorkpieceId wpid, glm::dmat4 const& transform)
{
  switch(wpid.kind) {
  case Core::Wp::Kind::Sketch: {
    auto right = glm::dvec3(transform * glm::dvec4(1.0, 0.0, 0.0, 1.0));
    auto up = glm::dvec3(transform * glm::dvec4(0.0, 1.0, 0.0, 1.0));
    auto target = glm::dvec3(transform[3][0], transform[3][1], transform[3][2]);
    return CameraSnap::from_rightup(right, up, target, 8.0, 1.0);
  }
  case Core::Wp::Kind::Part: {
    std::cout<<"Default camera snap for part\n";
    auto target = glm::dvec3(transform[0][3], transform[1][3], transform[2][3]);
    auto pos = glm::dvec3(transform * glm::dvec4(8.0, 8.0, 8.0, 1.0));
    auto worldUp = glm::dvec3(0.0, 1.0, 0.0);
    return CameraSnap::from_posTarget(pos, target, worldUp, 1.0);
  }
  }
  return CameraSnap();
}
void Editor::focus(Core::WorkpieceId wpid, bool instant)
{
  if(!mWorkpiece.empty() && wpid == mWorkpiece.top())
    return;
  mWorkpiece.push(wpid);

  // This workpiece was not viewed in this session
  if(mRegistry.workpiece_camSnap.find(wpid) == mRegistry.workpiece_camSnap.end()) {
    mRegistry.workpiece_camSnap[wpid] = default_cameraSnap(wpid, mRegistry.workpiece_transform[wpid]);
  }

  focus_impl(wpid, instant);
}
void Editor::unfocus(bool instant)
{
  if(mWorkpiece.size() > 1) {
    mWorkpiece.pop();
    focus_impl(mWorkpiece.top(), instant);
  }
}
void Editor::focus_impl(Core::WorkpieceId wpid, bool instant)
{
  switch(wpid.kind) {
  case Core::Wp::Kind::Sketch:
    mToolbar.set_visible_child(mSkToolbar);
    break;
  case Core::Wp::Kind::Part:
    mToolbar.set_visible_child(mPrtToolbar);
    break;
  }
  unsigned int duration = instant ? 0 : mConfigs.kWorkpieceTransition_time;
  mCamMove = CameraMove(mCamera.state, mRegistry.workpiece_camSnap[wpid], duration);
  set_defaultTool();
  mMustRebuildWP = true;
}

void Editor::realize()
{
  std::cout<<"Realize\n";
  mViewport.make_current();
  try {
    mViewport.set_has_depth_buffer(true);
    // resize(mViewport.get_width(), mViewport.get_height());
    std::cout<<"Viewport dimensions "<<mViewport.get_width()<<",  "<<mViewport.get_height()<<"\n";

    mRenderer.init(mConfigs);

    GLCall(glEnable(GL_MULTISAMPLE));
    // GLCall(glEnable(GL_SCISSOR_TEST));
    GLCall(glEnable(GL_DEPTH_TEST));
    GLCall(glDepthFunc(GL_LESS));
    GLCall(glEnable(GL_BLEND));
    GLCall(glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA));

    // GLCall(glPolygonMode(GL_FRONT_AND_BACK, GL_LINE));
  }
  catch(const Gdk::GLError& gle) {
    std::cerr << "An error occured making the context current during realize:" << std::endl;
    std::cerr << gle.domain() << "-" << gle.code() << "-" << gle.what() << std::endl;
  }
}

void Editor::unrealize()
{
  mViewport.make_current();
  try {
    mViewport.throw_if_error();
  }
  catch(const Gdk::GLError& gle) {
    std::cerr << "An error occured making the context current during unrealize" << std::endl;
    std::cerr << gle.domain() << "-" << gle.code() << "-" << gle.what() << std::endl;
  }
}

bool Editor::render(Glib::RefPtr<Gdk::GLContext> const& context)
{
  if(!mRenderQueued) {
    return true;
  }
  // std::cout<<"Render!\n";

  try {
    mViewport.throw_if_error();

    GLCall(glViewport(0, 0, mViewport.get_width(), mViewport.get_height()));
    GLCall(glClearColor(mConfigs.kBackground_color.r, mConfigs.kBackground_color.g, mConfigs.kBackground_color.b, 1.0));
    GLCall(glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));

    mRenderer.render(mRenderCall, mRegistry);
    GLCall(glFlush());
    mRenderCall.clear();

    mRenderQueued = false;

    return true;
  }
  catch(const Gdk::GLError& gle) {
    std::cerr << "An error occurred in the render callback of the GLArea" << std::endl;
    std::cerr << gle.domain() << "-" << gle.code() << "-" << gle.what() << std::endl;
    return false;
  }
}

void Editor::queue_render(Core::Wp::Handler const& handler, MeshRepo const& meshRep)
{
    // quad tree house keeping
  if(mMustRebuildBBmap) {
    auto children_of = handler.constituents_of(mRoot);
    mRegistry.build_aabbTree(children_of, meshRep, mConfigs, mCamera);
    mMustRebuildBBmap = false;
    request_render();
    // mRegistry.aabbTree.print();
  }

  if(mRenderQueued)
    return;
  if(!mRenderRequest) {
    std::cerr<<"Invalid render area on queue :("<<std::endl;
    return;
  }

  Core::IdVec ids = mRegistry.aabbTree.all();
  
  for(auto id : ids) {
    if(!mCamMove && id.wpid != mWorkpiece.top() && Registry::is_origin(id))
      continue;
    
    mRenderCall.push_back({id, meshRep.get_one(id)});
  }

  if(mRenderCall.empty())
    return;

  mRenderQueued = true;
  mRenderRequest = false;
  mViewport.queue_render();
}
void Editor::request_bbmapUpdate()
{
  mMustRebuildBBmap = true;
}
void Editor::resize(int width, int height)
{
  std::cout<<"Resize with "<<width<<",  "<<height<<"\n";
  set_camera(Camera(mCamera.state, mCamera.params, glm::dvec2(width, height), glm::inverse(mRegistry.workpiece_transform[mWorkpiece.top()])));
}
void Editor::draw_effects(const Cairo::RefPtr<Cairo::Context>& cr, int width, int height)
{
  double viewheight = mCamera.viewport.y;
  if(mSelArea.kind == SelectionArea::Kind::BoxEx) {
    cr->set_source_rgba(mConfigs.kExcSel_color.r, mConfigs.kExcSel_color.g, mConfigs.kExcSel_color.b, 0.8);
    cr->rectangle(mSelArea.start.x, viewheight-mSelArea.start.y, mSelArea.end.x-mSelArea.start.x, mSelArea.start.y-mSelArea.end.y);
    // cr->rectangle(mSelArea.end.x, mSelArea.end.y, mSelArea.end.x-mSelArea.start.x, mSelArea.end.y-mSelArea.start.y);
    cr->fill();
  } else if(mSelArea.kind == SelectionArea::Kind::BoxInc) {
    cr->set_source_rgba(mConfigs.kIncSel_color.r, mConfigs.kIncSel_color.g, mConfigs.kIncSel_color.b, 0.8);
    cr->rectangle(mSelArea.start.x, viewheight-mSelArea.start.y, mSelArea.end.x-mSelArea.start.x, mSelArea.start.y-mSelArea.end.y);
    // cr->rectangle(mSelArea.start.x, mSelArea.start.y, mSelArea.end.x-mSelArea.start.x, mSelArea.end.y-mSelArea.start.y);
    cr->fill();
  } else if(mSelArea.kind == SelectionArea::Kind::Line) {
    cr->set_line_width(5);
    cr->set_source_rgba(mConfigs.kIncSel_color.r, mConfigs.kIncSel_color.g, mConfigs.kIncSel_color.b, 0.8);
    cr->move_to(mSelArea.start.x, mSelArea.start.y);
    cr->line_to(mSelArea.end.x, mSelArea.end.y);
    cr->stroke();
  }

#ifdef SHOW_BOUNDINGBOXES
  for(auto node : mRegistry.aabbTree.nodes) {
    if(!node.is_leaf() || node.entity->wpid != mWorkpiece.top())
      continue;
    double r = ((double) rand() / (RAND_MAX)) + 1;
    double g = ((double) rand() / (RAND_MAX)) + 1;
    double b = ((double) rand() / (RAND_MAX)) + 1;
    cr->set_source_rgba(r, g, b, 0.8);
    cr->rectangle(node.box.topLeft().x, mCamera.viewport.y-node.box.topLeft().y, 
                  node.box.bottomRight().x-node.box.topLeft().x, node.box.topLeft().y-node.box.bottomRight().y);
    cr->fill();
  }
#endif
}

void Editor::set_camera(Camera newCam)
{
  mCamera = Camera(newCam, mRegistry.workpiece_transform[mRoot]); // TODO Use current workpiece rather than root
  mRenderer.set_camera(mCamera);
  request_bbmapUpdate();
  request_render();
}

Glib::RefPtr<Gio::ListModel> Editor::create_model(const Glib::RefPtr<Glib::ObjectBase>& item)
{
  if(mSidebarData.nodes.empty())
    return {};
  auto col = std::dynamic_pointer_cast<SidebarColumn>(item);
  
  if(col && (mSidebarData.nodes.size() < col->data_ind || mSidebarData.nodes[col->data_ind].children_steps.empty()))
    return {};

  auto result = Gio::ListStore<SidebarColumn>::create();
  std::vector<size_t> const& dataIndices_children = col ? mSidebarData.nodes[col->data_ind].children_steps : mSidebarData.top_level();

  for(auto childInd : dataIndices_children) {
    result->append(SidebarColumn::create(childInd));
  }
  return result;
}
void Editor::add_sidebar_columns()
{
  auto factory = Gtk::SignalListItemFactory::create();
  factory->signal_setup().connect(
    sigc::mem_fun(*this, &Editor::on_setup_stepName)
  );
  factory->signal_bind().connect(
    sigc::mem_fun(*this, &Editor::on_bind_stepName)
  );
  auto column = Gtk::ColumnViewColumn::create("Docnaaame", factory);
  mColumnView.append_column(column);
}

void Editor::on_setup_stepName(Glib::RefPtr<Gtk::ListItem> const& list_item)
{
  auto expander = Gtk::make_managed<Gtk::TreeExpander>();
  auto label = Gtk::make_managed<Gtk::Label>();
  label->set_halign(Gtk::Align::START);
  expander->set_child(*label);
  list_item->set_child(*expander);
}
void Editor::on_bind_stepName(Glib::RefPtr<Gtk::ListItem> const& list_item)
{
  auto row = std::dynamic_pointer_cast<Gtk::TreeListRow>(list_item->get_item());
  if(!row)
    return;
  auto col = std::dynamic_pointer_cast<SidebarColumn>(row->get_item());
  if(!col)
    return;
  auto expander = dynamic_cast<Gtk::TreeExpander*>(list_item->get_child());
  if(!expander)
    return;
  expander->set_list_row(row);
  auto mouseMoveController = Gtk::EventControllerMotion::create();
  size_t col_ind = col->data_ind;
  BuildNode step = mSidebarData.nodes[col_ind];
  mouseMoveController->signal_enter().connect([=, this](double x, double y){
    hover_fromTree(col_ind);
  });
  mouseMoveController->signal_leave().connect([this](){
    unhover_all();
  });
  auto mouseClickController = Gtk::GestureClick::create();
  mouseClickController->signal_pressed().connect([=, this](int n_press, double x, double y){
    if(n_press == 2)
      mContextAction = ContextMenu::get_firstOption(step, *this);
  }, false);
  
  expander->add_controller(mouseMoveController);
  expander->add_controller(mouseClickController);

  auto label = dynamic_cast<Gtk::Label*>(expander->get_child());
  if(!label)
    return;
  label->set_text(mSidebarData.nodes[col->data_ind].step_name);
}

} // !App
