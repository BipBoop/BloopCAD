
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef BLOOP_RENDERER_HPP_
#define BLOOP_RENDERER_HPP_

#include "Registry.hpp"
#include <Config/Config.hpp>
#include <Graphics_utils/Shader.hpp>
#include <Graphics_utils/Texture.hpp>
#include <Graphics_utils/TextRenderer.hpp>

#include <Core/Discrete/Mesh.hpp>

#include <optional>
#include <vector>
#include <map>
#include <unordered_map>

namespace App {

using Core::Mesh;

// struct Material {
//   glm::vec4 color;

//   // std::strong_ordering operator<=>(Material const& other) const;
// };

enum class DisplayStyle { Standard, Hovered, Selected };
using Material = std::function<void(Shader&)>;

struct RenderSubBatch {
  std::vector<Mesh> pointMeshes; 
  std::vector<Mesh> curveMeshes;
  std::vector<Mesh> triangleNormMeshes;
  // std::vector<Mesh> spriteMeshes;
};

using RenderBatch = std::map<size_t, RenderSubBatch>;

struct CamStuff {
  glm::vec2 viewport;
  glm::mat4 mvp;
  glm::vec3 pos;
  glm::vec3 front;
  glm::mat4 model, view, projection;
};

using RenderCall = std::vector<std::pair<Core::Id, Core::Mesh>>;

struct RenderKey {
  std::string material;
  Core::WorkpieceId workpiece;
  Core::Mesh const& mesh;
};

struct Renderer {
  Shader curveShader;
  Shader triangleNormShader;
  Shader pointShader;
  Shader spriteShader;
  Shader annotLineShader;

  TextRenderer textRenderer;

  Shader* currentShader;

  std::optional<CamStuff> newCamStuff;

  std::map<Core::Id, Core::Mesh> meshes;
  std::unordered_map<std::string, std::shared_ptr<Texture>> textPool;
  std::unordered_map<std::string, std::shared_ptr<Texture>> imagesPool;
  
  // Materials ordered by kind (2d geometry, 3d surface, 3d solidsurface)
  // then tag (hovered, selected, underconstrained) with hashed strings
  std::unordered_map<std::string, Material> materials;

  Renderer();

  // init must be called in a valid gl context
  void init(Config const& config);

  void load_sprites(Config const& config);

  // render must be called in a valid gl context
  void render(RenderCall call, Registry const& reg);
  void render(std::vector<RenderKey> const& keys, Registry const& reg);

  void set_camera(Camera const& camera);

  void gen_materials(Config const& config);
  std::string get_material(Core::Id id, size_t tag);
 
  void enable_shader(Mesh::Kind meshKind, std::string material, glm::mat4 model);

private:
  void apply_newCamStuff();

  void render_mesh(Mesh const& mesh);
  void render_triangleNorm(Mesh const& mesh);
  void render_curve(Mesh const& mesh);
  void render_point(Mesh const& mesh);
  void render_annotLine(Mesh const& mesh);
  void render_pointTexture(Mesh const& mesh);
  void render_offsetPointTexture(Mesh const& mesh);

  // Transforms the sprite name provided by Core into the 
  // resource name to load the sprite
  std::string sprite_resourceName(std::string spritename);

};

} // !App

#endif