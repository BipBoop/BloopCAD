
## Selection

There are 4 types of selection area

* Point -> What is under the mouse
* Line -> What crosses a screen line
* Inclusive box -> What crosses a screen orthographic box
* Exclusive box -> What is fully contained by a screen orthographic box

The selection area is used to determine what to hover.

Point selection is the default
When the mouse is down and dragged accross and the tool returns noops, the selection is one of the 2 points modes

Shift down -> Line
Start.x < End.x -> Inclusive box
Else -> Exclusive box

In any of those 3 modes, moving the selection area accross only changes hover states until the mouse is released and commits the selection
In point mode, the selection is commited on mouse press

Ctrl makes the selection additive. In point mode, it means it can add/remove an entity, in the other modes, only add

If the user clicks on nothing, it will register as a an empty point selection then commit nothing which clears the selection


The selection area and hover are managed by the editor. 
The tools return a vector of enttities which should be hovered given its state and the selection area
