
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "InputData.hpp"

#include <Core/Print/GlmOstream.hpp>

#include <glm/gtx/string_cast.hpp>

namespace App {

InputData InputData::make_compare(InputState prevState, InputState currState)
{
  InputData out;
  out.state = currState;
  out.delta.mousePos    = currState.mousePos    - prevState.mousePos;
  out.delta.mouseScroll = currState.mouseScroll - prevState.mouseScroll;

  out.delta.mousePressed  = currState.mousePressed  - prevState.mousePressed;
  out.delta.middleMouse   = currState.middleMouse   - prevState.middleMouse;

  out.delta.objPoint.worldPos  = currState.objPoint.worldPos  - prevState.objPoint.worldPos ;
  out.delta.objPoint.wpPos     = currState.objPoint.wpPos     - prevState.objPoint.wpPos;

  out.delta.key.shiftMod  = currState.key.shiftMod  - prevState.key.shiftMod;
  out.delta.key.ctrlMod   = currState.key.ctrlMod   - prevState.key.ctrlMod;
  out.delta.key.altMod    = currState.key.altMod    - prevState.key.altMod;
  out.delta.key.enter     = currState.key.enter     - prevState.key.enter;
  out.delta.key.esc       = currState.key.esc       - prevState.key.esc;
  out.delta.key.del       = currState.key.del       - prevState.key.del;

  return out;
} 

// Returns if the delta shows a leftclick
bool InputData::clicked() const
{
  return delta.mousePressed == -1;
}
bool InputData::moved() const
{
  return delta.objPoint.wpPos != glm::dvec3(0.0);
}

} // !App

std::ostream& operator<<(std::ostream& os, App::InputState const& indata)
{
  os<<"Scroll    : "<<indata.mouseScroll<<"\n";
  os<<"MousePress: "<<indata.mousePressed<<"\n";
  os<<"MousePos  : "<<indata.mousePos<<"\n";
  os<<"ShiftMod  : "<<indata.key.shiftMod<<"\n";
  os<<"CtrlMod   : "<<indata.key.ctrlMod<<"\n";
  os<<"AltMod    : "<<indata.key.altMod<<"\n";
  os<<"Esc       : "<<indata.key.esc<<"\n";
  os<<"Key       : "<<indata.key.key<<"\n";
  os<<"ObjPos  : "<<glm::to_string(indata.objPoint.worldPos)<<" | "
                  <<glm::to_string(indata.objPoint.wpPos)<<"\n";
  
  return os;
}
std::ostream& operator<<(std::ostream& os, App::InputData const& indata)
{
  os<<"State:\n"<<indata.state;
  os<<"Delta:\n"<<indata.delta;
  
  return os;
}
