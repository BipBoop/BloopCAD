
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef BLOOP_APP_BUILDSEQUENCE_HPP_
#define BLOOP_APP_BUILDSEQUENCE_HPP_

#include <Core/Id.hpp>
#include <Core/Workpiece/Handler.hpp>

namespace App {

struct BuildNode {
  using Content_t = std::variant<Core::Id, Core::WorkpieceId, size_t>;
  // A buildnode can be associated with (in order) a geometry, a workpiece or a buildstep of the root workpiece
  Content_t content;
  std::string step_name;
  std::vector<size_t> children_steps; // Indices in the build sequence

  std::optional<Core::Id> geometry() const;
  std::optional<Core::WorkpieceId> workpiece() const;
  std::optional<size_t> buildstep() const;
};

struct BuildSequence {
  std::vector<BuildNode> nodes;
  std::vector<size_t> top_level_nodes;

  BuildSequence(BuildNode root_step);
  BuildSequence();
  BuildSequence(Core::WorkpieceId wpid, Core::Wp::Handler const& handler);

  std::vector<size_t> top_level() const { return top_level_nodes; }
  size_t root() const { return 0; };
  size_t add(BuildNode::Content_t content, std::string name_);
  size_t add(BuildNode::Content_t content, std::string name_, size_t parent_node);
  void add_relationship(size_t parent_node, size_t child_node);
  
  void build_from_part(Core::WorkpieceId wpid, Core::Wp::Handler const& handler);
  void build_from_sketch(Core::WorkpieceId wpid, Core::Wp::Handler const& handler);
};

} // !App

#endif