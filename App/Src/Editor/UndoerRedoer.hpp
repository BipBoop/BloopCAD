
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef BLOOP_APP_UNDOERREDOER_HPP_
#define BLOOP_APP_UNDOERREDOER_HPP_

#include <cstddef>

namespace App {

class UndoerRedoer {
public:
  enum class Option { Staystill, Undo, Redo };
private:
  enum class UndoRedoState { None, First, Ongoing };
  UndoRedoState mState { UndoRedoState::None };
  size_t mTimer { 0 };
  int mFirst_delay, mOngoing_delay;

public:
  UndoerRedoer() = default;
  UndoerRedoer(size_t first_timer_ms, size_t second_timer_ms, size_t iteration_time_ms);

  Option what_do(bool is_ctrlz_on_or_something, bool is_ctrly_on_or_something);
};

} // !App

#endif