
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "UndoerRedoer.hpp"

namespace App {

UndoerRedoer::UndoerRedoer(size_t first_timer_ms, size_t ongoing_timer_ms, size_t iteration_time_ms)
  : mFirst_delay(first_timer_ms / iteration_time_ms)
  , mOngoing_delay(ongoing_timer_ms / iteration_time_ms)
{

}

UndoerRedoer::Option UndoerRedoer::what_do(bool is_ctrlz_on_or_something, bool is_ctrly_on_or_something)
{
  if(!is_ctrlz_on_or_something && !is_ctrly_on_or_something) {
    mState = UndoRedoState::None;
  }
  if(mState != UndoRedoState::None) {
    mTimer++;
  }

  if(is_ctrlz_on_or_something) {
    switch(mState) {
    case UndoRedoState::None:
      mState = UndoRedoState::First;
      mTimer = 0;
      return Option::Undo;
    case UndoRedoState::First:
      if(mTimer < mFirst_delay)
        return Option::Staystill;
      mTimer = 0;
      mState = UndoRedoState::Ongoing;
      return Option::Undo;
    case UndoRedoState::Ongoing:  
      if(mTimer < mOngoing_delay)
        return Option::Staystill;
      mTimer = 0;
      return Option::Undo;
    }
  } else if(is_ctrly_on_or_something) {
    switch(mState) {
    case UndoRedoState::None:
      mState = UndoRedoState::First;
      mTimer = 0;
      return Option::Redo;
    case UndoRedoState::First:
      if(mTimer < mFirst_delay)
        return Option::Staystill;
      mTimer = 0;
      mState = UndoRedoState::Ongoing;
      return Option::Redo;
    case UndoRedoState::Ongoing:  
      if(mTimer < mOngoing_delay)
        return Option::Staystill;
      mTimer = 0;
      return Option::Redo;
    }
  }
  return Option::Staystill;
}

} // !App