
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef BLOOP_EDITOR_HPP_
#define BLOOP_EDITOR_HPP_

#include "Selection.hpp"
#include "Registry.hpp"
#include "InputData.hpp"
#include <Tools/Navigation/Navigation.hpp>
#include <Tools/Part/PrtToolbar.hpp>
#include <Tools/Sketch/SkToolbar.hpp>
#include <Config/Config.hpp>
#include "Renderer.hpp"
#include <Window/MeshRepo.hpp>
#include <Tools/ToolCommon.hpp>
#include "UndoerRedoer.hpp"
#include <Graphics_utils/CameraMove.hpp>
#include "BuildSequence.hpp"

#include <gtkmm.h>

namespace App {

struct NavBar : public Gtk::Box {
  Gtk::Button orbit_btn   { "orbit" };
  Gtk::Button pan_btn     { "pan" };
  Gtk::Button zoom_btn    { "zoom" };
  Gtk::Button default_btn { "default" };
  Gtk::Button exit_btn    { "exit" };

  NavBar()
  {
    set_orientation(Gtk::Orientation::VERTICAL);
    set_halign(Gtk::Align::END);
  	set_valign(Gtk::Align::CENTER);
    append(orbit_btn);
    append(pan_btn);
    append(zoom_btn);
    append(default_btn);
    append(exit_btn);
  }
};

class Editor : public Gtk::Box {
private:
  Registry mRegistry;
  SelectionArea mSelArea;
  bool mClearsel { false };

  Nav::NavTool mNavTool;
  Tool* mTool { nullptr };

  Camera mCamera;
  Core::AnyWorkpiece mDocument;
  Core::WorkpieceId mRoot;
  std::stack<Core::WorkpieceId> mWorkpiece;
  Renderer mRenderer;

  Config mConfigs;
  RenderCall mRenderCall;

  Gtk::Stack mToolbar;
  SkToolbar mSkToolbar;
  PrtToolbar mPrtToolbar;

  Gtk::Paned mPanned;

  // Viewport 
  NavBar mNavBar;
  Gtk::GLArea mViewport;
  Gtk::DrawingArea mEffectsLayer;
  Gtk::Overlay mViewportOverlay;
  Gtk::Frame mViewportFrame;

  // Rendering and mesh flags
  bool mMustRebuildBBmap { true }, mMustRebuildSidebar { true }, mMustRebuildWP { false };
  bool mRenderQueued { false }, mRenderRequest { false };
  
  // Sidebar stuff
  BuildSequence mSidebarData;
  Gtk::ColumnView mColumnView;

  Glib::RefPtr<Gtk::TreeListModel> mTreeListModel;
  Glib::RefPtr<Gtk::MultiSelection> mTreeSelection;

  Gtk::Frame mSidebarFrame;
  Gtk::ScrolledWindow mSidebar;
  Gtk::Label mToolLabel { "No tool..." };

  UndoerRedoer mUndoerRedoer;

  std::optional<std::function<void()>> mContextAction { std::nullopt };

  std::optional<CameraMove> mCamMove;

  InputState mInputState, mPrevInputState;
  InputData mInputData;

  Gtk::Window* mParentWindow;

public:
  Editor(Gtk::Window* parent);

  void set_update_period(size_t period_ms);
  void set_root(Core::WorkpieceId root, Core::Wp::Handler const& handler);
  Core::WorkpieceId root() const { return mRoot; }
  Core::WorkpieceId focused_workpiece() const { return mWorkpiece.top(); }

  void setup_navbar();
  void setup_sidebar();
  void setup_toolbar();

  // Generates the final input state for an iteration
  // which includes queries to the workpiece manager
  void gen_currInputData(KeyState keyState, Core::Wp::Handler const& manager);
  // Updates the camera with the navigation tools
  // It has the power to queue a render
  // Returns true if there was a navigation action so that
  // App can not ask for updateDesc
  bool update_navigation();
  // Generates an update description from input data and
  // the Core workpiece manager
  // It can change the editor's tool, hover and selection
  // It can also queue a render if the hover or selection changes
  std::optional<Core::UpdDesc> gen_update(Core::Wp::Handler const& manager, MeshRepo const& meshes);
  void manage_hoverSelection(MeshRepo const& meshrepo);
  void select_hovered(bool keepsel);
  void unselect();
  void select_fromTree();
  void hover_fromTree(size_t index);
  Core::IdVec ids_fromTree(size_t index);
  void unhover_all();

  // Filters hover/selection by mesh using the selarea
  // assumes the preliminary tests have been made
  std::pair<Core::IdVec, std::vector<double>> hover_filter_by_mesh(Core::IdVec ids, MeshRepo const& repo) const;
  std::vector<size_t> hover_filter_by_dist(std::vector<double> const& src, std::vector<size_t> pre_selection) const;
  Core::IdVec hover_filter_by_tool(Core::IdVec src) const;

  void notify_corediff(Core::UpdReport upd, Core::Wp::Handler const& handler);
  void focus(Core::WorkpieceId wpid, bool instant = false);
  void unfocus(bool instant = false); // Pull back focus on the stack
  void focus_impl(Core::WorkpieceId wpid, bool instant = false);

  bool must_rebuild_wp() const { return mMustRebuildWP; }
  void rebuilt_wp() { mMustRebuildWP = false; }

  void set_defaultTool();
  void set_tool(Tool* tool_);
  void setup_tooldialog(std::shared_ptr<ToolDialog> dialog);

  void realize();
	void unrealize();
	bool render(Glib::RefPtr<Gdk::GLContext> const& context);
	void resize(int width, int height);

  void draw_effects(const Cairo::RefPtr<Cairo::Context>& cr, int width, int height);

  void set_camera(Camera newCam);

  // Queue whole screen render
  void queue_render(Core::Wp::Handler const& handler, MeshRepo const& meshRep);
  bool is_render_requested() const { return mRenderRequest; };
  void request_render() { mRenderRequest = true; }
  void request_bbmapUpdate();

  Glib::RefPtr<Gio::ListModel> create_model(
    const Glib::RefPtr<Glib::ObjectBase>& item = {});
  void add_sidebar_columns();

  // Signal handlers:
  void on_setup_stepName(const Glib::RefPtr<Gtk::ListItem>& list_item);
  void on_bind_stepName(const Glib::RefPtr<Gtk::ListItem>& list_item);
};

class SidebarColumn : public Glib::Object
{
public:
  size_t data_ind;
  static Glib::RefPtr<SidebarColumn> create(size_t data_ind_)
  {
    return Glib::make_refptr_for_instance<SidebarColumn>(new SidebarColumn(data_ind_));
  }
  
protected:
  SidebarColumn(size_t data_ind_)
      : data_ind(data_ind_)
  {
        
  }
};


} // !App

#endif