
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "Selection.hpp"

#include <iostream>

namespace App {

void SelectionArea::mutate(bool activate_2point, bool enable_line, glm::dvec2 point, glm::dvec2 move_delta, double viewheight)
{
  point = glm::dvec2(point.x, viewheight-point.y);
  if(kind == SelectionArea::None) {
    settled = false;
    return;
  }

  auto oldkind = kind;
  if(activate_2point && move_delta != glm::dvec2(0.0)) {
    if(enable_line) {
      // kind = SelectionArea::Line;
    } else if(kind != SelectionArea::BoxEx) {
      // kind = SelectionArea::BoxInc;
    }
  } else  if(!activate_2point) {
    kind = SelectionArea::Point;
  }
  if(kind != oldkind) {
    settled = false;
    return; // Has changed!
  }

  // Update selection area
  switch(kind) {
  case SelectionArea::Point:
    settled = start == point;
    start = point;
    break;
  // case SelectionArea::BoxEx:
  // case SelectionArea::BoxInc:
  //   kind = start.x > end.x ? SelectionArea::BoxEx : SelectionArea::BoxInc;
  //   [[fallthrough]];
  // case SelectionArea::Line:
  //   settled = end == point;
  //   end = point;
  //   break;
  default:
    break;
  }
}
void SelectionArea::set_active(bool activ_)
{
  if(!activ_ && active()) {
    deactivate();
  } else if(activ_ && !active()) {
    activate();
  }
}

} // !App