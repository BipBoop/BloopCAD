
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef BLOOP_INPUTDATA_HPP_
#define BLOOP_INPUTDATA_HPP_

#include <glm/glm.hpp>
#include <ostream>

namespace App {

struct ObjPoint {
  glm::dvec3 worldPos;
  // glm::dvec3 worldNormal;
  glm::dvec3 wpPos;  
};


struct KeyState {
  int shiftMod { false };
  int ctrlMod { false };
  int altMod { false };
  int esc { false };
  int enter { false };
  int del { false };
  unsigned int key { 0 };
};

struct InputState {
  glm::dvec2 mousePos { 0.0 };
  glm::dvec2 mouseScroll { 0.0 };
  ObjPoint objPoint;

  int mousePressed { 0 };
  int middleMouse { false };

  KeyState key;
};

struct InputData {
  InputState state;
  InputState delta;
  
  InputData() = default;
  
  static InputData make_compare(InputState prevState, InputState currState);

  // Returns if the delta shows a leftclick
  bool clicked() const;
  bool moved() const;
};

} // !App
std::ostream& operator<<(std::ostream& os, App::InputData const& indata);
std::ostream& operator<<(std::ostream& os, App::InputState const& instate);

#endif