
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "BuildSequence.hpp"

#include <Core/Print/IDOstream.hpp>

namespace App {

std::optional<Core::Id> BuildNode::geometry() const
{
  if(std::holds_alternative<Core::Id>(content)) {
    return std::get<Core::Id>(content);
  }
  return std::nullopt;
}
std::optional<Core::WorkpieceId> BuildNode::workpiece() const
{
  if(std::holds_alternative<Core::WorkpieceId>(content)) {
    return std::get<Core::WorkpieceId>(content);
  }
  return std::nullopt;
}
std::optional<size_t> BuildNode::buildstep() const
{
  if(std::holds_alternative<size_t>(content)) {
    return std::get<size_t>(content);
  }
  return std::nullopt;
}

BuildSequence::BuildSequence(BuildNode root_step)
{
  nodes.push_back(root_step);
}
BuildSequence::BuildSequence()
{

}
BuildSequence::BuildSequence(Core::WorkpieceId wpid, Core::Wp::Handler const& handler)
{
  switch(wpid.kind) {
  case Core::Wp::Kind::Sketch:
    build_from_sketch(wpid, handler);
    break;
  case Core::Wp::Kind::Part:
    build_from_part(wpid, handler);
    break;
  }
}

size_t BuildSequence::add(BuildNode::Content_t content, std::string name_)
{
  return add(content, name_, Core::invalid_index());
}
size_t BuildSequence::add(BuildNode:: Content_t content, std::string name_, size_t parent_node)
{
  size_t step_index = nodes.size();
  nodes.push_back(BuildNode{.content=content, .step_name=name_, .children_steps={}});

  if(parent_node != Core::invalid_index()) {
    nodes[parent_node].children_steps.push_back(step_index);
  } else {
    top_level_nodes.push_back(step_index);
  }
  return step_index;
}
void BuildSequence::add_relationship(size_t parent_node, size_t child_node)
{
  nodes[parent_node].children_steps.push_back(child_node);
  // Make sure the child does not appear twice
  top_level_nodes.erase(std::remove(top_level_nodes.begin(), top_level_nodes.end(), child_node));
}

void BuildSequence::build_from_part(Core::WorkpieceId wpid, Core::Wp::Handler const& handler)
{
  std::optional<Core::Prt::Factory> factory = handler.get_wp<Core::Prt::Factory>(wpid);
  std::optional<Core::Prt::Part> part = handler.get_wp<Core::Prt::Part>(wpid);

  if(!factory) {
    std::cerr<<"Could not find part factory at id "<<wpid<<std::endl;
    return;
  }
  if(!part) {
    std::cerr<<"Could not find part part at id "<<wpid<<std::endl;
    return;
  }

  /* Create origin folder to select origin plane or origin point */
  size_t origin_folder = add(Core::Id{}, "Origin"); // Add a null node to contain origin stuff
  Core::SibId origin_id {.kind=Core::Prt::Kind::Origin_point, .index=0};
  add(Core::Id{ .wpid=wpid, .entId=origin_id}, part->name_of(origin_id), origin_folder);

  for(size_t i = 0; i < 3; ++i) {
    Core::SibId plane_id {.kind=Core::Prt::Kind::Origin_plane, .index=i};
    add(Core::Id{ .wpid=wpid, .entId=plane_id }, part->name_of(plane_id), origin_folder);
  }

  /* Create build steps */
  std::map<Core::WorkpieceId, size_t> skid_to_nodeindex;
  for(size_t i = 0; i < factory->buildActions.size(); ++i) {
    auto act = factory->buildActions[i];
    if(std::holds_alternative<Core::Prt::AttachSketch>(act)) {
      auto attach = std::get<Core::Prt::AttachSketch>(act);
      skid_to_nodeindex[attach.sk_id] = add(attach.sk_id, handler.name_of(attach.sk_id));
    } else if(std::holds_alternative<Core::Prt::Extrude>(act)) {
      auto extrude = std::get<Core::Prt::Extrude>(act);
      Core::WorkpieceId extrude_sk = extrude.sk_id;
      if(!extrude_sk.valid()) {
        std::cerr<<"Invalid sketch for extrude"<<std::endl;
        continue;
      }
      auto found_skid = skid_to_nodeindex.find(extrude_sk);
      if(found_skid == skid_to_nodeindex.end()) {
        std::cerr<<"Could not find extrusion's sketch"<<std::endl;
        continue;
      }
      skid_to_nodeindex.erase(found_skid);
      size_t extrusion_node_index = add(i, "Extrusionnn");
      add_relationship(extrusion_node_index, found_skid->second);
    }
  }
  std::vector<Core::WorkpieceId> deps = handler.dependencies_of(wpid);
}
void BuildSequence::build_from_sketch(Core::WorkpieceId wpid, Core::Wp::Handler const& handler)
{

}

} // !App
