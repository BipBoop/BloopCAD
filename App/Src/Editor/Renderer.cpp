
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "Renderer.hpp"

#include <Graphics_utils/GLCall.hpp>
#include <Graphics_utils/VertexArray.hpp>
#include <Graphics_utils/VertexBuffer.hpp>
#include <Graphics_utils/IndexBuffer.hpp>

#include <Core/Part/Part.hpp>
#include <Core/Print/IDOstream.hpp>

#include <giomm.h>

#define SELECTION_TAG 101
#define HOVER_TAG 102

namespace App {

// std::strong_ordering Material::operator<=>(Material const& other) const 
// {
//   if(color.x < other.color.x)
//     return std::strong_ordering::less;
//   if(color.y < other.color.y)
//     return std::strong_ordering::less;
//   if(color.z < other.color.z)
//     return std::strong_ordering::less;
//   if(color.w < other.color.w) 
//     return std::strong_ordering::less;

//   if(color == other.color)
//     return std::strong_ordering::equivalent;

//   return std::strong_ordering::greater;
// }


Renderer::Renderer()
  // : solid_surface_id({ {Core::Doc::Kind::Part, 0}, {Core::Prt::Kind::Surface, 0}})
{

}

std::string shaderContent_from_resource(std::string const& resource_name)
{
  auto res = Gio::Resource::lookup_data_global("/shaders/Shaders/" + resource_name);
  if(!res) {
    std::cerr<<"Could not load shader from resource \""<<resource_name<<"\""<<std::endl;
  }
  size_t size;
  const char* bytes = reinterpret_cast<const char*>(res->get_data(size));
  return std::string(bytes, size);
}
void Renderer::init(Config const& config)
{
  std::cout<<"Init renderer \n";
  pointShader = Shader::createShader({
                  {shaderContent_from_resource("Point.vert"), GL_VERTEX_SHADER},
                  {shaderContent_from_resource("SquareExpand.geom"), GL_GEOMETRY_SHADER}, 
                  {shaderContent_from_resource("Point.frag"), GL_FRAGMENT_SHADER}});
  
	curveShader = Shader::createShader({
					      	{shaderContent_from_resource("Point.vert"), GL_VERTEX_SHADER},
						      {shaderContent_from_resource("Line.geom"), GL_GEOMETRY_SHADER},
						      {shaderContent_from_resource("Line.frag"), GL_FRAGMENT_SHADER}});

  triangleNormShader = Shader::createShader({
                        {shaderContent_from_resource("Point_normal.vert"), GL_VERTEX_SHADER}, 
										    {shaderContent_from_resource("Cell.frag"), GL_FRAGMENT_SHADER}});

  spriteShader = Shader::createShader({
                  {shaderContent_from_resource("OffsetPoint.vert"), GL_VERTEX_SHADER}, 
                  {shaderContent_from_resource("QuadExpand.geom"), GL_GEOMETRY_SHADER},
                  {shaderContent_from_resource("TextureBacklit.frag"), GL_FRAGMENT_SHADER}});
  
  annotLineShader = Shader::createShader({
                      {shaderContent_from_resource("AnnotLine.vert"), GL_VERTEX_SHADER},
                      {shaderContent_from_resource("AnnotLine.geom"), GL_GEOMETRY_SHADER},
                      {shaderContent_from_resource("Line.frag"), GL_FRAGMENT_SHADER}});

  load_sprites(config);
  gen_materials(config);  
  textRenderer.set_font("OpenDyslexic");
}
void Renderer::load_sprites(Config const& config)
{
  imagesPool["perp"] = Texture::from_pixbuf(
    	                      Gdk::Pixbuf::create_from_resource("/guidata/Icons/scalable/actions/sk_perpendicularity_constr-symbolic.svg"));
  imagesPool["horiz"] = Texture::from_pixbuf(
    	                      Gdk::Pixbuf::create_from_resource("/guidata/Icons/scalable/actions/sk_horizontal_constr-symbolic.svg"));
  imagesPool["vert"] = Texture::from_pixbuf(
    	                      Gdk::Pixbuf::create_from_resource("/guidata/Icons/scalable/actions/sk_vertical_constr-symbolic.svg"));
  imagesPool["coinc"] = Texture::from_pixbuf(
    	                      Gdk::Pixbuf::create_from_resource("/guidata/Icons/scalable/actions/sk_coincidence_constr-symbolic.svg"));

}

void Renderer::render(RenderCall to_render, Registry const& reg)
{
  if(to_render.empty()) 
    return;
  std::vector<RenderKey> keys;
  keys.reserve(to_render.size());

  for(size_t i = 0; i < to_render.size(); ++i) {
    if(to_render[i].second.empty())
      continue;
    size_t tag = 0;
    if(std::find(reg.selection.begin(), reg.selection.end(), to_render[i].first) != reg.selection.end()) {
      tag = SELECTION_TAG;
      // std::cout<<"Selection tag "<<to_render[i].first<<"\n";
    } else if(std::find(reg.hover.begin(), reg.hover.end(), to_render[i].first) != reg.hover.end()) {
      tag = HOVER_TAG;
      // std::cout<<"Hover tag "<<to_render[i].first<<"\n";
    } else {
      tag = to_render[i].second.tag;
      // std::cout<<"Nothing tag "<<to_render[i].first<<"\n";
    }

    keys.push_back(RenderKey{ .material=get_material(to_render[i].first, tag), 
                              .workpiece=to_render[i].first.wpid, 
                              .mesh=to_render[i].second});
  }

  render(keys, reg);
}
void Renderer::render(std::vector<RenderKey> const& keys, Registry const& reg)
{
  apply_newCamStuff();

  // sort keys;
  Core::WorkpieceId currWorkpiece; // Invalid by default
  std::string currMaterial = "";
  Core::Mesh::Kind currMeshKind = Core::Mesh::Kind::None;

  for(auto key : keys) {
    if(key.mesh.kind != currMeshKind) {
      currWorkpiece = key.workpiece;
      currMaterial = key.material;
      enable_shader(key.mesh.kind, currMaterial, reg.workpieceTransform(currWorkpiece));
    } else {
      if(key.workpiece != currWorkpiece) {
        currWorkpiece = key.workpiece;
        currentShader->setUniformMat4f("u_Model", reg.workpieceTransform(currWorkpiece));
      }
      if(key.material != currMaterial) {
        currMaterial = key.material;
        materials[currMaterial](*currentShader);
      }
    }

    render_mesh(key.mesh);
  }
}
void Renderer::enable_shader(Mesh::Kind meshKind, std::string material, glm::mat4 model)
{
  switch(meshKind) {
  case Mesh::Kind::Point:
    currentShader = &pointShader;
    break;
  case Mesh::Kind::Curve:
    // std::cout<<"CURVESHADERRRRRRRRRRRRRRRRRRRR\n";
    currentShader = &curveShader;
    break;
  case Mesh::Kind::TrianglesNorm:
    currentShader = &triangleNormShader;
    break;
  case Mesh::Kind::OffsetPointTexture:
    currentShader = &spriteShader;
    break;
  case Mesh::Kind::AnnotLine:
    currentShader = &annotLineShader;
    break;
  default:
    std::cerr<<"Mesh type not supported yet "<<static_cast<int>(meshKind)<<std::endl;
    return;
  }
  currentShader->bind();
  currentShader->setUniformMat4f("u_Model", model);
  materials[material](*currentShader);
}


std::string get_skmaterial(Core::SibId id, size_t tag)
{
  if(tag == SELECTION_TAG) {
    return "sketch_selected";
  } else if(tag == HOVER_TAG) {
    return "sketch_hovered";
  }
  switch(id.kind) {
  case Core::Sk::Kind::Point:
  case Core::Sk::Kind::Line:
  case Core::Sk::Kind::Circle:
  case Core::Sk::Kind::OriginPoint:
  case Core::Sk::Kind::OriginLine:
    if(tag == 1) {
      return "sketch_wellconstrained";
    } else {
      return "sketch_underconstrained";
    }
  case Core::Sk::Kind::LineLinePerpendicular:
  case Core::Sk::Kind::LineHorizontal:
  case Core::Sk::Kind::LineVertical:
  case Core::Sk::Kind::PointPointHorizontal:
  case Core::Sk::Kind::PointPointVertical:
  case Core::Sk::Kind::PointPointCoincident:
    return "sketch_sprite";
  case Core::Sk::Kind::LineLength:
  case Core::Sk::Kind::PointPointHdist:
  case Core::Sk::Kind::PointPointVdist:
  case Core::Sk::Kind::CircleRadius:
  case Core::Sk::Kind::CircleDiameter:
    return "sketch_annot";
  case Core::Sk::Kind::Loopdiloop:
    return "sketch_loop";
  }
  std::cerr<<"Assigning material \"no_skmaterial\""<<std::endl;
  return "no_skmaterial";
}
std::string get_prtmaterial(Core::SibId id, size_t tag)
{
  switch(id.kind) {
  case Core::Prt::Kind::Origin_plane:
    if(tag == SELECTION_TAG) {
      return "surface_selected";
    } else if(tag == HOVER_TAG) {
      return "surface_hovered";
    } else {
      return "surface_standard";
    }
  case Core::Prt::Kind::Surface:
    if(tag == SELECTION_TAG) {
      return "solidSurface_selected";
    } else if(tag == HOVER_TAG) {
      return "solidSurface_hovered";
    } else {
      return "solidSurface_standard";
    }
  case Core::Prt::Kind::Origin_point:
    if(tag == SELECTION_TAG) {
      return "part_point_selected";
    } else if(tag == HOVER_TAG) {
      return "part_point_hovered";
    } else {
      return "part_point_standard";
    }
  }
  return "no_prtmaterial";
}

std::string Renderer::get_material(Core::Id id, size_t tag)
{
  switch(id.wpid.kind) {
  case Core::Wp::Kind::Sketch:
    return get_skmaterial(id.entId, tag);
  case Core::Wp::Kind::Part:
    return get_prtmaterial(id.entId, tag);
  default:
    return "no_material";
  }

  return "no_material";
}

void Renderer::set_camera(Camera const& camera)
{
  newCamStuff = CamStuff {  .viewport=camera.viewport, .mvp=camera.mvp, .pos=camera.state.pos, .front=camera.front(),
                            .model=camera.model(), .view=camera.view(), .projection=camera.projection() };
}

Material simple_material(glm::vec3 color)
{
  return [color](Shader& shader) {
    shader.setUniform3f("u_Color", color);
  };
}
Material simple_material(glm::vec4 color)
{
  return [color](Shader& shader) {
    shader.setUniform4f("u_Color", color);
  };
}
Material randomColor_material(double alpha)
{
  return [alpha](Shader& shader) {
    shader.setUniform4f("u_Color", glm::vec4( static_cast<double>(rand()) / (RAND_MAX),
                                              static_cast<double>(rand()) / (RAND_MAX),
                                              static_cast<double>(rand()) / (RAND_MAX),
                                              alpha));
  };
}
void Renderer::gen_materials(Config const& config)
{
  materials["sketch_underconstrained"]  = simple_material(glm::vec4(config.kSkConfig.kUnderConstrained_color, 1.0));
  materials["sketch_wellconstrained"]   = simple_material(glm::vec4(config.kSkConfig.kWellConstrained_color, 1.0));
  materials["sketch_hovered"]           = simple_material(glm::vec4(config.kHover_color, 1.0));
  materials["sketch_selected"]          = simple_material(glm::vec4(config.kSelection_color, 1.0));
  materials["sketch_sprite"]            = simple_material(glm::vec4(0.0, 0.4, 0.4, 0.0));
  materials["sketch_annot"]             = simple_material(glm::vec4(0.8, 0.8, 0.8, 0.8));
  materials["sketch_loop"]              = simple_material(config.kSkConfig.kPatchColor);

  // Eventually material which sets two face colors
  materials["part_point_standard"] = simple_material(glm::vec4(config.kSkConfig.kWellConstrained_color, 1.0));
  materials["part_point_hovered"] = simple_material(glm::vec4(config.kHover_color, 1.0));
  materials["part_point_selected"] = simple_material(glm::vec4(config.kSelection_color, 1.0));

  materials["surface_standard"] = simple_material(config.kPrtConfig.kSurface_color0);
  materials["surface_hovered"] = simple_material(glm::vec4(config.kHover_color, 0.8));
  materials["surface_selected"] = simple_material(glm::vec4(config.kSelection_color, 0.8));

  materials["solidSurface_standard"] = simple_material(config.kPrtConfig.kShell_color);
  materials["solidSurface_hovered"] = simple_material(config.kHover_color);
  materials["solidSurface_selected"] = simple_material(config.kSelection_color);

  materials["no_material"] = [](Shader& shader) { };
}
void Renderer::apply_newCamStuff()
{
  if(!newCamStuff)
    return;

  pointShader.bind();
  pointShader.setUniformMat4f("u_View", newCamStuff->view);
  pointShader.setUniformMat4f("u_Projection", newCamStuff->projection);
  pointShader.setUniform2f("u_Viewport", newCamStuff->viewport);
  pointShader.setUniform1f("u_Diameter", 10.0f);

  curveShader.bind();
  curveShader.setUniform2f("u_Viewport", newCamStuff->viewport);
  curveShader.setUniformMat4f("u_View", newCamStuff->view);
  curveShader.setUniformMat4f("u_Projection", newCamStuff->projection);
  curveShader.setUniform1f("u_LineWidth", 5.0f);
  curveShader.setUniform1f("u_Feather", 0.6f);


  triangleNormShader.bind();
  // triangleNormShader.setUniformMat4f("u_Model", newCamStuff->model);
  triangleNormShader.setUniformMat4f("u_View", newCamStuff->view);
  triangleNormShader.setUniformMat4f("u_Projection", newCamStuff->projection);
  // triangleNormShader.setUniform3f("u_LightColor", 1.0, 1.0, 1.0);
  triangleNormShader.setUniform3f("u_LightPos", newCamStuff->pos - newCamStuff->front*4.f);

  spriteShader.bind();
  spriteShader.setUniform2f("u_Viewport", newCamStuff->viewport);
  spriteShader.setUniformMat4f("u_View", newCamStuff->view);
  spriteShader.setUniformMat4f("u_Projection", newCamStuff->projection);

  annotLineShader.bind();
  annotLineShader.setUniform2f("u_Viewport", newCamStuff->viewport);
  annotLineShader.setUniformMat4f("u_View", newCamStuff->view);
  annotLineShader.setUniformMat4f("u_Projection", newCamStuff->projection);
  annotLineShader.setUniform1f("u_LineWidth", 1.0f);
  annotLineShader.setUniform1f("u_Feather", 0.6f);


  newCamStuff = std::nullopt;
}

void Renderer::render_mesh(Mesh const& mesh)
{
  switch(mesh.kind) {
  case Mesh::Kind::Point:
    render_point(mesh);
    break;
  case Mesh::Kind::PointTexture:
    render_pointTexture(mesh);
    break;
  case Mesh::Kind::Curve:
    render_curve(mesh);
    break;
  case Mesh::Kind::TrianglesNorm:
    render_triangleNorm(mesh);
    break;
  case Mesh::Kind::OffsetPointTexture:
    render_offsetPointTexture(mesh);
    break;
  case Mesh::Kind::AnnotLine:
    render_annotLine(mesh);
    break;
  default:
    std::cerr<<"Trying to render mesh with unsuported kind "<<static_cast<int>(mesh.kind)<<std::endl;
    break;
  }
}
void Renderer::render_triangleNorm(Mesh const& mesh)
{
  VertexBuffer vb(&mesh.data[0], mesh.data.size()*sizeof(float));
  VertexArray va;
  va.bind();
  VertexBufferLayout vb_layout;
  vb_layout.add_proprety(3, VertexBufferLayout::Type::Float); // position
  vb_layout.add_proprety(3, VertexBufferLayout::Type::Float); // normal
  va.add_buffer(vb, vb_layout);

  IndexBuffer ib(&mesh.indices[0], mesh.indices.size());
  ib.bind();

  size_t vb_offset = 0;

  triangleNormShader.bind();
  GLCall(glDrawElements(GL_TRIANGLES, ib.count(), GL_UNSIGNED_INT, (void*)(vb_offset*sizeof(unsigned int))));
}
void Renderer::render_curve(Mesh const& mesh)
{
  VertexBuffer vb(&mesh.data[0], mesh.data.size()*sizeof(float));
  VertexArray va;
  va.bind();
  VertexBufferLayout vb_layout;
  vb_layout.add_proprety(3, VertexBufferLayout::Type::Float);
  va.add_buffer(vb, vb_layout);

  IndexBuffer ib(&mesh.indices[0], mesh.indices.size());
  size_t vb_offset = 0;

  curveShader.bind();
  GLCall(glDrawArrays(GL_LINE_STRIP, vb_offset, mesh.indices.size()));
  // GLCall(glDrawElements(GL_LINES, ib.count(), GL_UNSIGNED_INT, (void*)(vb_offset*sizeof(unsigned int))));
}
void Renderer::render_point(Mesh const& mesh)
{
  VertexBuffer vb(&mesh.data[0], mesh.data.size()*sizeof(float));
  VertexArray va;
  va.bind();
  VertexBufferLayout vb_layout;
  vb_layout.add_proprety(3, VertexBufferLayout::Type::Float); // position
  va.add_buffer(vb, vb_layout);

  size_t vb_offset = 0;

  pointShader.bind();
  GLCall(glDrawArrays(GL_POINTS, 0, mesh.indices.size()));
}
void Renderer::render_annotLine(Mesh const& mesh)
{
  VertexBuffer vb(&mesh.data[0], mesh.data.size()*sizeof(float));
  VertexArray va;
  va.bind();
  VertexBufferLayout vb_layout;
  vb_layout.add_proprety(3, VertexBufferLayout::Type::Float);
  vb_layout.add_proprety(1, VertexBufferLayout::Type::Float);

  va.add_buffer(vb, vb_layout);

  IndexBuffer ib(&mesh.indices[0], mesh.indices.size());
  ib.bind();
  size_t vb_offset = 0;

  annotLineShader.bind();
  GLCall(glDrawElements(GL_LINES, mesh.indices.size(), GL_UNSIGNED_INT, 0));
}
void Renderer::render_pointTexture(Mesh const& mesh)
{

}
void Renderer::render_offsetPointTexture(Mesh const& mesh)
{
  if(mesh.textureKind == Core::Mesh::TextureKind::None)
    return;
  
  std::shared_ptr<Texture> texture;

  if(mesh.textureKind == Core::Mesh::TextureKind::Text) {
    if(textPool.find(mesh.texture) == textPool.end()) {
      textPool[mesh.texture] = textRenderer.render_textLine(mesh.texture);
    }
    texture = textPool[mesh.texture];
  } else {
    texture = imagesPool[mesh.texture];
  }
  
	spriteShader.setUniform1i("u_Channels", texture->n_channels());
	spriteShader.setUniform1f("u_Width", texture->width());
	spriteShader.setUniform1f("u_Height", texture->height());

	texture->bind();

	VertexBuffer vb(mesh.data);

	VertexArray va;
	va.bind();
	VertexBufferLayout layout;
	layout.add_proprety(3, VertexBufferLayout::Type::Float); // Position in 3d
	layout.add_proprety(2, VertexBufferLayout::Type::Float); // Pixel offset

	va.add_buffer(vb, layout);

	GLCall(glDrawArrays(GL_POINTS, 0, 1));

	va.unbind();
}

} // !App