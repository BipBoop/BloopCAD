
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "WpInvestigation.hpp"

namespace App {


ObjPoint sk_point(Registry const& reg, Core::WorkpieceId sk_id, Core::Ray ray, Core::Wp::Handler const& handler);
ObjPoint prt_point(Registry const& reg, Core::WorkpieceId prt_id, Core::Ray ray, Core::Wp::Handler const& handler);

ObjPoint point(Registry const& reg, Core::WorkpieceId wpid, Core::Ray ray, Core::Wp::Handler const& handler)
{
  switch(wpid.kind) {
  case Core::Wp::Kind::Sketch:
    return sk_point(reg, wpid, ray, handler);
  case Core::Wp::Kind::Part:
    return prt_point(reg, wpid, ray, handler);
  default:
    return ObjPoint{};
  }
}

ObjPoint sk_point(Registry const& reg, Core::WorkpieceId sk_id, Core::Ray ray, Core::Wp::Handler const& handler) 
{
  std::optional<Core::Sk::Sketch> sketch = handler.get_wp<Core::Sk::Sketch>(sk_id);

  if(!sketch) {
    std::cerr<<"Error getting sketch from handler in "<<__FUNCTION__<<std::endl;
    return ObjPoint{};
  }
  glm::dmat4 transform = reg.workpieceTransform(sk_id);
  ray = ray.transform(transform);

  glm::dvec3 found_point = Core::intersection(
                            Core::Sk::Sketch::plane(), ray);
  
  return ObjPoint{.worldPos=glm::dvec3(glm::dvec4(found_point, 1.0)*transform), .wpPos=found_point};
}
ObjPoint prt_point(Registry const& reg, Core::WorkpieceId prt_id, Core::Ray ray, Core::Wp::Handler const& handler)
{
  std::optional<Core::Prt::Part> part = handler.get_wp<Core::Prt::Part>(prt_id);

  if(!part) {
    std::cerr<<"Error getting part from handler in "<<__FUNCTION__<<std::endl;
    return ObjPoint{};
  }

  glm::dmat4 transform = reg.workpieceTransform(prt_id);
  ray = ray.transform(transform);

  double min_dist = std::numeric_limits<double>::max();
  glm::dvec3 found_point;
  for(int i = 0; i < 3; ++i) {
    glm::dvec3 inter = Core::intersection(part->origin.planes[i].support.tangeant(0.0, 0.0), ray);
    double dist_2 = glm::distance2(inter, ray.origin);

    if(dist_2 < min_dist) {
      min_dist = dist_2;
      found_point = inter;
    }
  }
  return ObjPoint{.worldPos=glm::dvec3(glm::dvec4(found_point, 1.0)*transform), .wpPos=found_point};
}

} // !App