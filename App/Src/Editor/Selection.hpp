
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef BLOOP_SELECTIONAREA_HPP_
#define BLOOP_SELECTIONAREA_HPP_

#include <Window/MeshRepo.hpp>

#include <glm/glm.hpp>

namespace App {

struct SelectionArea {
  enum Kind { None, Point, BoxInc, BoxEx, Line };
  Kind kind { None };
  bool settled { false };

  glm::dvec2 start;
  glm::dvec2 end;

  // Mutates the kind (and start, end parameters of the selection)
  // activate_2point: flag up means we have to use a box or a line, usualy mouse down
  // enable line: line if true, box if false usualy shift
  void mutate(bool activate_2point, bool enable_line, glm::dvec2 point, glm::dvec2 move_delta, double viewheight);

  bool active() const { return kind != None; }
  void deactivate() { kind = None; }
  void activate() { kind = Point; }
  void set_active(bool activ);
};

} // !App

#endif