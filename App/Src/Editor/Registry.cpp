
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "Registry.hpp"

#include <Core/Print/IDOstream.hpp>

namespace App {

AABBNode::AABBNode(Core::dBox box_, Core::Id entity_)
  : box(box_)
  , entity(entity_)
{

}
AABBNode::AABBNode(Core::dBox box_)
  : box(box_)
{

}

bool AABBNode::is_leaf() const
{
  return entity.has_value();
}

void AABBTree::add_entity(Core::dBox box, Core::Id id)
{
  // Create node
  auto new_node = create_node(AABBNode(box, id));

  if(root_node == Core::invalid_index()) {
    root_node = new_node;
    return;
  }

  // Best new sibling
  auto best_sibling = find_bestSibling_heur(box);

  // Create parent for both nodes
  auto new_branch = create_node(AABBNode(box.combine(nodes[best_sibling].box)));

  // Attach the new node to the new branch
  nodes[new_node].parent_ind = new_branch;
  nodes[new_branch].left_ind = new_node;

  // steal the sibling's parent
  nodes[new_branch].parent_ind = nodes[best_sibling].parent_ind;
  if(nodes[best_sibling].parent_ind == Core::invalid_index()) {
    root_node = new_branch;
  } else {
    if(nodes[nodes[best_sibling].parent_ind].left_ind == best_sibling) {
      nodes[nodes[best_sibling].parent_ind].left_ind = new_branch;
    } else if(nodes[nodes[best_sibling].parent_ind].right_ind == best_sibling) {
      nodes[nodes[best_sibling].parent_ind].right_ind = new_branch;
    }
  }

  // Attach sibling to new branch
  nodes[best_sibling].parent_ind = new_branch;
  nodes[new_branch].right_ind = best_sibling;

  // walk back adjusting AABBs
  fix_upward(nodes[new_node].parent_ind);
}

size_t AABBTree::create_node(AABBNode node)
{
  size_t node_ind;
  if(n_dealocated) {
    n_dealocated--;
    node_ind = first_dealocated;

    if(n_dealocated) {
      first_dealocated = nodes[first_dealocated].next_del;
    }
    nodes[node_ind] = node;
  } else {
    node_ind = nodes.size();
    nodes.push_back(node);
  }
  return node_ind;
}
size_t AABBTree::find_bestSibling_heur(Core::dBox box)
{
  // See https://github.com/JamesRandall/SimpleVoxelEngine/blob/master/voxelEngine/src/AABBTree.cpp
  // insert leaf

  std::pair<AABBNode, size_t> curr_node = get_node(root());

  while(!curr_node.first.is_leaf()) {

    Core::dBox combAABB = curr_node.first.box.combine(box);
    double add_cost = combAABB.area() * 2.0;
    double min_pushDown_cost = (combAABB.area() - curr_node.first.box.area()) * 2.0;

    auto left_node = get_node(curr_node.first.left_ind);
    auto right_node = get_node(curr_node.first.right_ind);


    double left_cost = box.combine(left_node.first.box).area() + min_pushDown_cost;
    double right_cost = box.combine(right_node.first.box).area() + min_pushDown_cost;

    if(!left_node.first.is_leaf()) {
      left_cost -= left_node.first.box.area();
    }
    if(right_node.first.is_leaf()) {
      right_cost -= right_node.first.box.area();
    }

    if(add_cost <= left_cost && add_cost <= right_cost) {
      break;
    }
    if(left_cost < right_cost) {
      curr_node = left_node;
    } else {
      curr_node = right_node;
    }
  }
  return curr_node.second;
}
void AABBTree::fix_upward(size_t from)
{
  size_t node_ind = from;

  while(node_ind != Core::invalid_index()) {
    auto& node = nodes[node_ind];

    if(node.left_ind == Core::invalid_index() && node.right_ind == Core::invalid_index()) {
      std::cerr<<"Node should be a parent aaaaaaa"<<std::endl;
      break;
    }
    node.box = nodes[node.left_ind].box.combine(nodes[node.right_ind].box);
    node_ind = node.parent_ind;
  }
}
void AABBTree::print() const
{
  std::cout<<"AABBTree root:"<<root_node<<", nodes: \n";
  for(int i = 0; i < nodes.size(); ++i) {
    auto node = nodes[i];
    std::cout<<i<<" ";

    if(node.next_del != Core::invalid_index()) {
      std::cout<<"deleted -> "<<node.next_del<<"\n";
    } else {
      std::cout<<"["<<glm::to_string(node.box.topLeft())<<" : "<<glm::to_string(node.box.bottomRight())<<"] {";

      if(node.entity) {
        std::cout<<*node.entity;
      } else {
        std::cout<<"*";
      }
      if(node.left_ind == Core::invalid_index() && node.right_ind == Core::invalid_index()) {
        std::cout<<"} "<<node.parent_ind<<"\n";
      } else {
        std::cout<<"} "<<node.left_ind<<",  "<<node.right_ind<<",  "<<node.parent_ind<<"\n";
      }
    }
  }
}

Core::IdVec AABBTree::touched(glm::dvec2 pos) const 
{ 
  // std::cout<<"Touched with "<<glm::to_string(pos)<<"\n";
  return match([&](AABBNode const& node) { return node.box.contains(pos); }); 
}
Core::IdVec AABBTree::traversed(Core::Crv::_2d::Line line) const 
{ 
  return match([&](AABBNode const& node) { return node.box.intersects(line); }); 
}
Core::IdVec AABBTree::contained(Core::dBox box) const 
{ 
  return match([&](AABBNode const& node) { return box.contains(node.box); }); 
}
Core::IdVec AABBTree::touched(Core::dBox box) const 
{
  return match([&](AABBNode const& node) { return box.overlaps(node.box); }); 
}
Core::IdVec AABBTree::all() const
{
  Core::IdVec out;
  for(int i = 0; i < nodes.size(); ++i) {
    if(nodes[i].entity) {
      out.push_back(*nodes[i].entity);
    } 
  }
  return out;
}


Registry::Registry(Core::WorkpieceId workpieceId)
{
  workpiece_transform[workpieceId] = glm::dmat4(1); 
}
void Registry::build_aabbTree(Core::IdVec const& ids, MeshRepo const& meshes, Config const& configs, Camera const& cam)
{
  aabbTree = AABBTree();
  fill_aabbTree(ids, meshes, configs, cam);
}
void Registry::update_aabbTree(Core::IdVec const& modIds, MeshRepo const& meshes, Config const& configs, Camera const& cam)
{
  for(auto id : modIds) {
    // aabbTree.remove(id);
  }
  fill_aabbTree(modIds, meshes, configs, cam);
}
void Registry::fill_aabbTree(Core::IdVec const& modIds, MeshRepo const& meshesRep, Config const& configs, Camera const& cam)
{
  Core::WorkpieceId currWorkpiece; // Invalid by default
  Camera modelCamera;
  for(auto id : modIds) {
    if(id.is_workpiece())
      continue;
    if(id.wpid != currWorkpiece) {
      modelCamera = Camera(cam, workpiece_transform[id.wpid]);
      currWorkpiece = id.wpid;
    }
    auto meshes = meshesRep.get(id);
    for(int i = 0; i < meshes.size(); ++i) {
      Core::Id subId = id; // Id of the current thingy, maybe a normal entity, maybe a handle
      subId.entId.handle_num = i - 1; // First mesh is not a handle, -1 means not a handle!
      auto box = mesh_to_box(meshes[i], modelCamera);      
      // aabbTree.add_entity(expand_box(box, subId, configs), subId);
      aabbTree.add_entity(box.expand(configs.get_hover_dist(subId)), subId);
    }
  }
}
Core::IdVec Registry::get_hoveredBoxes(SelectionArea selArea)
{
  switch(selArea.kind) {
  case SelectionArea::Kind::Point:
    return aabbTree.touched(selArea.start);
  case SelectionArea::Kind::BoxInc:
    return aabbTree.touched(Core::dBox(selArea.start, selArea.end));
  case SelectionArea::Kind::BoxEx:
    return aabbTree.contained(Core::dBox(selArea.start, selArea.end));
  case SelectionArea::Kind::Line:
    return aabbTree.traversed(Core::Crv::_2d::Line(selArea.start, selArea.end));
  default:
    return {};
  }
}

Core::dBox mesh_to_box_triangleNorm(Core::Mesh const& mesh, Camera const& cam)
{
  auto points = mesh.get_pointNorm();

  Core::dBox box;
  for(auto const& point : points) {
    auto screenPoint = cam.world_to_screen(point.point);
    box = box.extend(screenPoint);
  }
  return box;
}
Core::dBox mesh_to_box_points(Core::Mesh const& mesh, Camera const& cam)
{
  auto points = mesh.get_points();

  Core::dBox box;
  for(auto const& point : points) {
    box = box.extend(cam.world_to_screen(point));
  }
  return box;
}
Core::dBox Registry::mesh_to_box(Core::Mesh const& mesh, Camera const& cam)
{
  switch(mesh.kind) {
  case Core::Mesh::Kind::TrianglesNorm:
    return mesh_to_box_triangleNorm(mesh, cam);
  case Core::Mesh::Kind::Point:
  case Core::Mesh::Kind::Curve:
    return mesh_to_box_points(mesh, cam);
  default:
    break;
  }
  return Core::dBox();
}

Core::dBox point3f_to_box(std::vector<float> const& data, Camera const& cam)
{
  if(data.size() < 3) {
    std::cerr<<"Data too small for point3f_to_box"<<std::endl;
  }
  Core::dBox box(cam.world_to_screen(glm::dvec3(data[0], data[1], data[2])));
  for(int i = 3; i < data.size(); i += 3) {
    box.extend(cam.world_to_screen(glm::dvec3(data[i+0], data[i+1], data[i+2])));
  }
  return box;
}
glm::dmat4 Registry::workpieceTransform(Core::WorkpieceId wpid) const
{
  try {
    return workpiece_transform.at(wpid);
  } catch(std::exception& err) {
    std::cerr<<"Error "<<__FUNCTION__<<",  "<<__FILE__<<": "<<err.what()<<std::endl;
  }
  return glm::dmat4(0.0);
}

bool Registry::is_origin(Core::Id id)
{
  switch(id.wpid.kind) {
  case Core::Wp::Kind::Sketch:
    return  id.entId.kind == Core::Sk::Kind::OriginLine || 
            id.entId.kind == Core::Sk::Kind::OriginPoint;
  case Core::Wp::Kind::Part:
    return  id.entId.kind == Core::Prt::Kind::Origin_plane ||
            id.entId.kind == Core::Prt::Kind::Origin_point;
  }
  return false;
}
Core::IdVec Registry::order_hoverPriority(Core::IdVec src)
{
  std::sort(src.begin(), src.end(), [](Core::Id const& ida, Core::Id const& idb) { 
    if(ida.wpid.kind != idb.wpid.kind) {
      return ida.wpid.kind < idb.wpid.kind;
    }
    if(ida.wpid.kind == Core::Wp::Kind::Sketch) {
      // check for sibid
      size_t kind_a = ida.entId.handle_num == -1 ? ida.entId.kind : Core::Sk::Kind::Point;
      size_t kind_b = idb.entId.handle_num == -1 ? idb.entId.kind : Core::Sk::Kind::Point;
      return kind_a < kind_b;
    }

    // Sorting by index is not really usefull, but 
    // the sort function must output a valid result for
    // every case
    if(ida.wpid.index != idb.wpid.index) {
      return ida.wpid.index < idb.wpid.index;
    }
    return ida.entId.index < idb.entId.index;
  });

  return src;
}

} // !App
