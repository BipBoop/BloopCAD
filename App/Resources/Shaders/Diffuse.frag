
#version 330 core

out vec4 fragColor;

in vec3 fragPos;
in vec3 normal;

// uniform vec3 u_ColorDark;
uniform vec4 u_ObjColor;
uniform vec3 u_LightPos;

void main()
{
  // diffuse 
  vec3 norm = normalize(normal);
  vec3 lightDir = normalize(u_LightPos - fragPos);
  float intensity = max(dot(norm, lightDir), 0.2);

  fragColor = vec4(u_ObjColor.xyz*intensity, u_ObjColor.w);
}