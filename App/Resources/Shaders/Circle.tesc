
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// Shader adapted from https://web.engr.oregonstate.edu/~mjb/cs519/Handouts/tessellation.1pp.pdf

/*
    This shader determines the tesselation level of a circle
*/

#version 400

uniform float u_Scale;
layout(vertices = 1)  out;

in vs_out {
    float radius;
    float startAngle;
    float endAngle;
    vec4 v;
    vec4 w;
} tscIn[];

out tsc_out {
    float radius;
    float startAngle;
    float endAngle;
    vec4 v;
    vec4 w;
} tscOut[];


void main()
{
    gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;

    // Pass through infos from vertex shader, might be redundant..?
    tscOut[gl_InvocationID].radius      = tscIn[gl_InvocationID].radius;
    tscOut[gl_InvocationID].startAngle  = tscIn[gl_InvocationID].startAngle;
    tscOut[gl_InvocationID].endAngle    = tscIn[gl_InvocationID].endAngle;
    tscOut[gl_InvocationID].v           = tscIn[gl_InvocationID].v;
    tscOut[gl_InvocationID].w           = tscIn[gl_InvocationID].w;

    // Set tesselation level
    gl_TessLevelOuter[0] = 1.0; // Must be 1 because it renders a single curve
    gl_TessLevelOuter[1] = 10 + pow(10, 5 * u_Scale) + pow(10, 5 * tscIn[gl_InvocationID].radius); // TODO: calculate this instead of having it a fixed value
}