
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
#version 330

layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec3 arrowDirections;
layout(location = 2) in vec3 exclPos;
layout(location = 3) in ivec2 exclDims;

uniform mat4 u_MVP;
uniform vec2 u_Viewport;

out VS_OUT {
    vec4 dir;
    flat vec2 up_left;
    flat vec2 down_right;
} vs_out;

float map(float x, float in_min, float in_max, float out_min, float out_max)
{
	// https://www.arduino.cc/reference/en/language/functions/math/map/
	return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

void main()
{
    gl_Position = u_MVP * vec4(vertexPosition.xyz, 1.0);
    vs_out.dir  = normalize(u_MVP * vec4(arrowDirections.xyz, 0.0));

    vec4 exclPos_proj = u_MVP * vec4(exclPos.xyz, 1.0);
    vec2 exclPos_2d = exclPos_proj.xy / exclPos_proj.w;
    exclPos_2d.x = map(exclPos_2d.x, -1.0, 1.0, 0.0, u_Viewport.x);
    exclPos_2d.y = map(exclPos_2d.y, 1.0, -1.0, 0.0, u_Viewport.y);
    vs_out.up_left      = vec2(exclPos_2d.x - float(exclDims.x) / 2.0, exclPos_2d.y - float(exclDims.y) / 2.0);
    vs_out.down_right   = vec2(exclPos_2d.x + float(exclDims.x) / 2.0, exclPos_2d.y + float(exclDims.y) / 2.0);
}
