
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// Shader adapted from https://web.engr.oregonstate.edu/~mjb/cs519/Handouts/tessellation.1pp.pdf

/*
    This shader evaluates the tesselation of a circle
*/

#version 400

layout(isolines, equal_spacing, ccw)  in;

uniform vec2 u_Viewport;

in tsc_out {
    float radius;
    float startAngle;
    float endAngle;
    vec4 v;
    vec4 w;
    flat vec2 up_left;
    flat vec2 down_right;
} tseIn[];

out tese_out {
    flat vec2 up_left;
    flat vec2 down_right;
} teseOut;

float map(float x, float in_min, float in_max, float out_min, float out_max)
{
	// https://www.arduino.cc/reference/en/language/functions/math/map/
	return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

void main()
{
    vec4 p0 = gl_in[0].gl_Position;
    float t = map(gl_TessCoord.x, 0.0, 1.0, tseIn[0].startAngle, tseIn[0].endAngle);

    vec3 aspectVec = vec3(u_Viewport.x/u_Viewport.y, 1.0, 1.0);
    vec4 v_vec = tseIn[0].v;
    vec4 w_vec = tseIn[0].w;
    float radius = tseIn[0].radius;

    // The equation for parametric circle in 3d space taken from:
    // https://math.stackexchange.com/questions/73237/parametric-equation-of-a-circle-in-3d-space
    gl_Position = p0 + radius * (cos(t) * v_vec + sin(t) * w_vec);
    teseOut.up_left     = tseIn[0].up_left;
    teseOut.down_right  = tseIn[0].down_right;
}