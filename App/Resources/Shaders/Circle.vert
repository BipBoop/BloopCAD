
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
/*
    This shader is a normal point shader that also passes along useful circle data
*/

#version 330

layout(location = 0) in vec3 center;
layout(location = 1) in float radius;
layout(location = 2) in float startAngle;
layout(location = 3) in float endAngle;
layout(location = 4) in vec3 v;
layout(location = 5) in vec3 w;


uniform mat4 u_MVP;

out vs_out {
    float radius;   // Radius of the circle
    float startAngle;
    float endAngle;    
    vec4 v;         // One of the axis of the plane (normalized for accurate size representation)
    vec4 w;         // One of the axis of the plane (normalized for accurate size representation)
} vrtOut;


void main()
{
    gl_Position = u_MVP * vec4(center, 1.0);

    float start = startAngle;
    float end = endAngle;
    if(start > end)
        end += 2 * 3.1415;

    vrtOut.radius       = radius;
    vrtOut.startAngle   = start;
    vrtOut.endAngle     = end;
    vrtOut.v            = u_MVP * vec4(v, 0.0);
    vrtOut.w            = u_MVP * vec4(w, 0.0);
}
