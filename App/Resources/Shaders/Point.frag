
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
/*
	This shader draws a singular point of the specified radius on the geometry (usualy a quad)
*/

#version 330

out vec4 fragColor;

uniform vec4 u_Color;
uniform float u_Diameter;
uniform vec2 u_Viewport;

in fData
{
	float radius_square;
    vec2 center;
}frag;    

void main()
{	
	float d_x = (frag.center.x - gl_FragCoord.x);
	float d_y = (frag.center.y - gl_FragCoord.y);
	if((d_x*d_x + d_y*d_y) < frag.radius_square)
		fragColor = u_Color;
	else 
		fragColor = vec4(0.0, 1.0, 0.0, 0.0);//pow((1.0-len)/(feather), 2.5));
}
