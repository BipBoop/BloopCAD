
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
/*
    This shader draws a texture with a background color on the geometry and can be occluded by a quad
*/

#version 330

layout(origin_upper_left) in vec4 gl_FragCoord;
out vec4 fragColor;

uniform vec3 u_Color;
uniform int u_Channels;

in data
{
	vec2 UV_coord;
    flat vec2 up_left;
    flat vec2 down_right;
}gs_out;

uniform sampler2D u_Texture;

void main()
{
    if( gl_FragCoord.x > gs_out.up_left.x && gl_FragCoord.x < gs_out.down_right.x && 
        gl_FragCoord.y > gs_out.up_left.y && gl_FragCoord.y < gs_out.down_right.y) {
        fragColor = vec4(0.0, 0.0, 0.0, 0.0);
    } else {
        if(u_Channels == 1) {
            vec4 tmp = texture(u_Texture, gs_out.UV_coord);
            fragColor = vec4(0.0, 0.0, 0.0, tmp.r);
        } else {
            fragColor = texture(u_Texture, gs_out.UV_coord);
        }
        fragColor.xyz += fragColor.w * u_Color;
    }
}
