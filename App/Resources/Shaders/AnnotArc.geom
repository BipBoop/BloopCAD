
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
/*
	This shader transforms a two point line segement into a triangle strip to render it at a specified width
*/

#version 330 core
layout (lines) in;
layout (triangle_strip, max_vertices = 4) out;

uniform vec2 u_Viewport;
uniform float u_LineWidth;
uniform float u_Feather;

in tese_out {
    flat vec2 up_left;
    flat vec2 down_right;
} geomIn[];

out fData
{
    vec2 normal;
    flat vec2 up_left;
	flat vec2 down_right;
}frag;

void main() 
{
	vec2 aspectVec = vec2(u_Viewport.x/u_Viewport.y, 1.0);
	vec2 pos_screen0 = gl_in[0].gl_Position.xy / gl_in[0].gl_Position.w * aspectVec;
	vec2 pos_screen1 = gl_in[1].gl_Position.xy / gl_in[1].gl_Position.w * aspectVec;

	vec2 direction = normalize(pos_screen1 - pos_screen0);
	vec2 normal = (vec2(-direction.y, direction.x)) / u_Viewport * (u_LineWidth/2/(1-u_Feather)) * gl_in[0].gl_Position.w;

	vec2 norm_normal = normalize(normal);

	frag.normal     = norm_normal;
    frag.up_left    = geomIn[0].up_left;
    frag.down_right = geomIn[0].down_right;
	gl_Position = gl_in[0].gl_Position + vec4(normal, 0.0, 0.0);
	EmitVertex();

	frag.normal = -norm_normal;
    frag.up_left    = geomIn[0].up_left;
    frag.down_right = geomIn[0].down_right;
	gl_Position = gl_in[0].gl_Position - vec4(normal, 0.0, 0.0);
	EmitVertex();

	frag.normal = norm_normal;
    frag.up_left    = geomIn[0].up_left;
    frag.down_right = geomIn[0].down_right;
    gl_Position = gl_in[1].gl_Position + vec4(normal, 0.0, 0.0);
	EmitVertex();
	
    frag.normal = -norm_normal;
    frag.up_left    = geomIn[0].up_left;
    frag.down_right = geomIn[0].down_right;
	gl_Position = gl_in[1].gl_Position - vec4(normal, 0.0, 0.0);
	EmitVertex();

	EndPrimitive();
}  