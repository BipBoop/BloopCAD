
#version 330 core

out vec4 fragColor;

in vec3 fragPos;
in vec3 normal;

// uniform vec3 u_ColorDark;
uniform vec4 u_Color;
uniform vec3 u_LightPos;

void main()
{
  vec3 norm = normalize(normal);
  vec3 lightDir = normalize(u_LightPos - fragPos);
  float intensity = max(dot(norm, lightDir), 0.0);

	float color_mult = 0.0;
  if (intensity > 0.90)
		color_mult = 1.1;
  else if(intensity > 0.6)
    color_mult = 1.0;
	else if (intensity > 0.4)
		color_mult = 0.7;
	else if (intensity > 0.2)
		color_mult = 0.3;
	else
		color_mult = 0.1;

	fragColor = vec4(u_Color.xyz*color_mult, u_Color.w);
}