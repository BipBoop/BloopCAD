
// Copyright (c) 2024

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
/*
	This shader expands a point into a quad of specified width and height
*/

#version 330 core
layout (points) in;
layout (triangle_strip, max_vertices = 4) out;

uniform vec2 u_Viewport;
uniform float u_Width, u_Height;

in VS_OUT {
    vec4 dir;
	flat vec2 up_left;
    flat vec2 down_right;
} gs_in[];  

out data
{
	vec2 UV_coord;
	flat vec2 up_left;
    flat vec2 down_right;
}gs_out;

void main() 
{
    /*
            *
           ***
          *****
        
        The arrow sprite looks something like this, the tip is in the midle
        at the top, it is the position that is in the input
    */

	vec2 aspectVec = vec2(u_Viewport.x/u_Viewport.y, 1.0);
	vec2 dir = normalize(gs_in[0].dir.xy * gl_in[0].gl_Position.w * aspectVec);
	vec2 mv_w = vec2(dir.y, -dir.x) * u_Width / u_Viewport * gl_in[0].gl_Position.w;
	vec2 mv_h = dir * u_Height / u_Viewport * gl_in[0].gl_Position.w * 2;

	gs_out.UV_coord 	= vec2(1.0, 1.0);
	gs_out.up_left 		= gs_in[0].up_left;
	gs_out.down_right 	= gs_in[0].down_right;
	gl_Position = gl_in[0].gl_Position + vec4((mv_w), 0.0, 0.0);
	EmitVertex();

	gs_out.UV_coord 	= vec2(1.0, 0.0);
	gs_out.up_left 		= gs_in[0].up_left;
	gs_out.down_right 	= gs_in[0].down_right;
	gl_Position = gl_in[0].gl_Position + vec4((mv_w - mv_h), 0.0, 0.0);
	EmitVertex();

	gs_out.UV_coord 	= vec2(0.0, 1.0);
	gs_out.up_left 		= gs_in[0].up_left;
	gs_out.down_right 	= gs_in[0].down_right;
	gl_Position = gl_in[0].gl_Position + vec4((-mv_w), 0.0, 0.0);
	EmitVertex();

	gs_out.UV_coord 	= vec2(0.0, 0.0);
	gs_out.up_left 		= gs_in[0].up_left;
	gs_out.down_right 	= gs_in[0].down_right;
	gl_Position = gl_in[0].gl_Position + vec4((-mv_w - mv_h), 0.0, 0.0);
	EmitVertex();

	EndPrimitive();
}  